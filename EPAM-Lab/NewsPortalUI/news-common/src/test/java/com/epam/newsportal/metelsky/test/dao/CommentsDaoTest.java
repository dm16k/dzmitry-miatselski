package com.epam.newsportal.metelsky.test.dao;

import com.epam.newsportal.metelsky.dao.CommentsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Comment;
import com.epam.newsportal.metelsky.domain.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author dmitr_000
 * @version 1.00 8/17/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-test-config.xml"})
@DatabaseSetup("classpath:db-test-input.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class CommentsDaoTest {
    private final static Logger logger = Logger.getLogger(CommentsDaoTest.class);
    @Autowired
    private CommentsDao commentsDao;

    @Test
    public void createTest() {
        Comment comment = new Comment();
        Comment commentRes = null;
        try {
            comment.setNewsId(102L);
            comment.setText("textT");
            comment.setCreationDate(new Timestamp(new Date().getTime() + 60000L));
            Long resId = commentsDao.add(comment);
            comment.setId(resId);
            List<Comment> list = commentsDao.getNewsComments(102L);
            for (Comment u : list) {
                if (u.getId() == resId) {
                    commentRes = u;
                }
            }
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertEquals(comment, commentRes);
    }

    @Test
    public void readByNewsTest() {
        List<Comment> comments = null;
        try {
            comments = commentsDao.getNewsComments(102L);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertNotNull(comments);
        Assert.assertEquals(2, comments.size());
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:commentsTest/update_result.xml")
    public void updateTest() {
        try {
            Comment c = new Comment();
            c.setId(103L);
            c.setText("Yep!");
            c.setCreationDate(new Timestamp(1439806332000L));
            c.setNewsId(102L);
            commentsDao.update(c);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:commentsTest/delete_result.xml")
    public void deleteTest() {
        Comment c1 = new Comment();
        c1.setId(101L);
        Comment c2 = new Comment();
        c2.setId(103L);
        Comment c3 = new Comment();
        c3.setId(104L);
        List<Comment> l = new ArrayList<>();
        l.add(c1);
        l.add(c2);
        l.add(c3);
        try {
            commentsDao.delete(l);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:commentsTest/delete_by_news_result.xml")
    public void deleteByNewsTest() {
        News news = new News();
        news.setId(102L);
        List<News> list = new ArrayList<>();
        list.add(news);
        try {
            commentsDao.deleteByNews(list);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }
}
