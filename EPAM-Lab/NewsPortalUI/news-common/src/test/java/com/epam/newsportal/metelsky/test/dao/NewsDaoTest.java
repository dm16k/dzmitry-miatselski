package com.epam.newsportal.metelsky.test.dao;

import com.epam.newsportal.metelsky.dao.CommentsDao;
import com.epam.newsportal.metelsky.dao.NewsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.service.NewsManagementService;
import com.epam.newsportal.metelsky.util.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author dmitr_000
 * @version 1.00 8/17/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-test-config.xml"})
@DatabaseSetup("classpath:db-test-input.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class NewsDaoTest {
    private final static Logger logger = Logger.getLogger(NewsDaoTest.class);
    @Autowired
    private NewsDao newsDao;
    @Autowired
    private CommentsDao commentsDao;
    @Autowired
    private NewsManagementService service;

    @Test
    public void createTest() {
        News news = new News();
        news.setTitle("titleT");
        news.setShortText("shortT");
        news.setFullText("fullT");
        news.setCreationDate(new Timestamp(new Date().getTime() + 60000L));
        Long longTime = new Date().getTime() + 360000L;
        longTime -= longTime % 1000;
        news.setModificationDate(new Timestamp(longTime));
        News newsRes = null;
        try {
            Long resId = newsDao.add(news);
            news.setId(resId);
            List<News> list = newsDao.getAll();
            for (News u : list) {
                if (u.getId().compareTo(resId) == 0) {
                    newsRes = u;
                }
            }
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertEquals(news, newsRes);
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/set_author_result.xml")
    public void setAuthorTest() {
        try {
            News n = new News();
            n.setId(101L);
            Author a = new Author();
            a.setId(102L);
            newsDao.setAuthor(n, a);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/set_tag_result.xml")
    public void setTagsTest() {
        try {
            News n = new News();
            n.setId(101L);
            Tag t1 = new Tag();
            t1.setId(101L);
            Tag t2 = new Tag();
            t2.setId(102L);
            Tag t3 = new Tag();
            t3.setId(103L);
            List<Tag> tagList = new ArrayList<>();
            tagList.add(t1);
            tagList.add(t2);
            tagList.add(t3);
            newsDao.setTags(n, tagList);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void getAuthorForNewsTest() {
        Author a = new Author();
        Author actual = null;
        a.setId(101L);
        a.setName("Andy");
        a.setExpired(new Timestamp(1441868400000L));
        try {
            Long newsId = 101L;
            actual = newsDao.getAuthorForNews(newsId);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertNotNull(actual);
        Assert.assertEquals(a, actual);
    }

    @Test
    public void getTagsForNewsTest() {
        Tag t1 = new Tag();
        t1.setId(101L);
        t1.setName("economics");
        Tag t2 = new Tag();
        t2.setId(102L);
        t2.setName("tech");
        List<Tag> list = new ArrayList<>();
        list.add(t1);
        list.add(t2);
        List<Tag> actual = null;
        try {
            Long newsId = 102L;
            actual = newsDao.getTagsForNews(newsId);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertNotNull(actual);
        Assert.assertEquals(list, actual);
    }

    @Test
    public void readAllTest() {
        List<News> resList = null;
        try {
            resList = newsDao.getAll();
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertNotNull(resList);
        Assert.assertEquals(2, resList.size());
    }

    @Test
    public void getByIdTest() {
        News news = null;
        News resNews = null;
        try {
            List<News> list = newsDao.getAll();
            int size = list.size();
            if (size != 0) {
                int index = new Random().nextInt(size - 1);
                news = list.get(index);
                resNews = newsDao.getById(news.getId());
            }
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertEquals(news, resNews);
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/update_result.xml")
    public void updateTest() {
        News news = new News();
        news.setId(102L);
        news.setTitle("Cheer");
        news.setShortText("Hi");
        news.setFullText("Hello world");
        news.setCreationDate(new Timestamp(1439802732000L));
        news.setModificationDate(new Timestamp(1442437200000L));
        try {
            newsDao.update(news);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/delete_result.xml")
    public void deleteTest() {
        News news = new News();
        news.setId(101L);
        List<News> l = new ArrayList<>();
        l.add(news);
        try {
            commentsDao.deleteByNews(l);
            newsDao.unbindTags(l);
            newsDao.unbindAuthors(l);
            newsDao.delete(l);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/unbind_authors_result.xml")
    public void unbindAuthorsTest() {
        News news = new News();
        news.setId(101L);
        List<News> list = new ArrayList<>();
        list.add(news);
        try {
            newsDao.unbindAuthors(list);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/unbind_tags_result.xml")
    public void unbindTagsTest() {
        News news = new News();
        news.setId(101L);
        List<News> list = new ArrayList<>();
        list.add(news);
        try {
            newsDao.unbindTags(list);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void searchTest() {
        Long auth = 103L;
        Long t1 = 101L;
        Long t2 = 102L;
        int start = 0;
        int end = 1;
        List<Long> lt = new ArrayList<>();
        lt.add(t1);
        lt.add(t2);
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(auth);
        sc.setTagList(lt);
        List<News> newsList = null;
        try {
            newsList = newsDao.search(sc, start, end);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertNotNull(newsList);
        Assert.assertEquals(1, newsList.size());
    }

    @Test
    public void getNumberOfRecordsTest() {
        List<News> list = null;
        long quantity = -1;
        try {
            list = newsDao.search(new SearchCriteria(), Integer.MIN_VALUE, Integer.MAX_VALUE);
            quantity = newsDao.countNews(null);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertNotNull(list);
        Assert.assertEquals(quantity, list.size());
    }

    @Test
    public void transactionTest() {
        News news = new News();
        news.setTitle("titleT");
        news.setShortText("shortT");
        news.setFullText("fullT");
        news.setCreationDate(new Timestamp(new Date().getTime() + 60000L));
        news.setModificationDate(new Timestamp(new Date().getTime() + 360000L));
        Author author = new Author();
        author.setId(101L);
        Tag tag1 = new Tag();
        tag1.setId(102L);
        Tag tag2 = new Tag();
        tag2.setId(103L);
        List<Tag> tagList = new ArrayList<>();
        tagList.add(tag1);
        tagList.add(tag2);
        int before = -1, after = -2;
        try {
            before = newsDao.getAll().size();
            service.addNews(news, author, tagList);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        try {
            after = newsDao.getAll().size();
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertEquals(before + 1, after);
    }
}
