package com.epam.newsportal.metelsky.test.dao;

import com.epam.newsportal.metelsky.dao.TagsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dmitr_000
 * @version 1.00 8/17/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-test-config.xml"})
@DatabaseSetup("classpath:db-test-input.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class TagsDaoTest {
    private final static Logger logger = Logger.getLogger(TagsDaoTest.class);
    @Autowired
    private TagsDao tagsDao;

    @Test
    public void createTest() {
        Tag tag = new Tag();
        tag.setName("nameT");
        Tag resTag = null;
        try {
            Long resId = tagsDao.add(tag);
            tag.setId(resId);
            List<Tag> list = tagsDao.getAll();
            for (Tag u : list) {
                if (u.getId() == resId) {
                    resTag = u;
                }
            }
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertEquals(tag, resTag);
    }

    @Test
    public void readAllTest() {
        List<Tag> resList = null;
        try {
            resList = tagsDao.getAll();
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertNotNull(resList);
        Assert.assertEquals(3, resList.size());
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:tagsTest/update_result.xml")
    public void updateTest() {
        Tag tag = new Tag();
        tag.setId(102L);
        tag.setName("social");
        try {
            tagsDao.update(tag);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:tagsTest/delete_result.xml")
    public void deleteTest() {
        Tag tag1 = new Tag();
        tag1.setId(102L);
        Tag tag2 = new Tag();
        tag2.setId(103L);
        List<Tag> l = new ArrayList<>();
        l.add(tag1);
        l.add(tag2);
        try {
            tagsDao.unbindNews(l);
            tagsDao.delete(l);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:tagsTest/unbind_result.xml")
    public void unbindNewsTest() {
        Tag t = new Tag();
        t.setId(101L);
        List<Tag> list = new ArrayList<>();
        list.add(t);
        try {
            tagsDao.unbindNews(list);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }
}
