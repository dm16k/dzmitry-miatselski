package com.epam.newsportal.metelsky.dao.impl;

import com.epam.newsportal.metelsky.dao.AuthorsDao;
import com.epam.newsportal.metelsky.dao.DAO;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.dao.util.ConnectionManager;
import com.epam.newsportal.metelsky.domain.Author;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The AuthorsDaoJdbc class provides CRUD methods to work with 'AUTHORS' table of DB
 *
 * @author dmitr_000
 * @version 1.00 8/10/2015
 */
public class AuthorsDaoJdbc implements AuthorsDao {
    private ConnectionManager manager;
    private final static String ADD_AUTHOR = "BEGIN INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) " +
            "VALUES (AUTHORS_SEQ.nextval, ?) RETURNING AUTHOR_ID INTO ?; END;";
    private final static String EXPIRE_AUTHOR = "UPDATE AUTHORS SET EXPIRED = ? WHERE AUTHOR_ID = ?";
    private final static String GET_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHORS";
    private final static String UPDATE_AUTHOR = "UPDATE AUTHORS SET AUTHOR_NAME = ? WHERE AUTHOR_ID = ?";
    private final static String SORTING = " ORDER BY AUTHOR_ID";

    /**
     * Constructor
     *
     * @param manager - manager to work with DB
     */
    public AuthorsDaoJdbc(ConnectionManager manager) {
        this.manager = manager;
    }

    /**
     * @see DAO#add(Object) add
     */
    public Long add(Author entity) throws DaoException {
        Connection c = null;
        CallableStatement cs = null;
        Long id;
        try {
            c = manager.getConnection();
            cs = c.prepareCall(ADD_AUTHOR);
            cs.setString(1, entity.getName());
            cs.registerOutParameter(2, Types.VARCHAR);
            cs.execute();
            id = cs.getLong(2);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, cs, c);
        }
        return id;
    }

    /**
     * @throws DaoException, method unsupported for this implementation
     */
    public Author getById(Long id) throws DaoException {
        throw new DaoException("Unsupported method for Author's DAO");
    }

    /**
     * @see DAO#getAll() getAll
     */
    public List<Author> getAll() throws DaoException {
        Connection c = null;
        Statement st = null;
        List<Author> list = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(GET_ALL_AUTHORS + SORTING);
            list = new ArrayList<>();
            while (rs.next()) {
                Author author = new Author();
                author.setId(rs.getLong("AUTHOR_ID"));
                author.setName(rs.getString("AUTHOR_NAME"));
                author.setExpired(rs.getTimestamp("EXPIRED"));
                list.add(author);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    /**
     * @see DAO#update(Object) update
     */
    public void update(Author entity) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UPDATE_AUTHOR);
            ps.setString(1, entity.getName());
            ps.setLong(2, entity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see DAO#delete(List) delete
     */
    public void delete(List<Author> entities) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(EXPIRE_AUTHOR);
            for (Author a : entities) {
                ps.setTimestamp(1, a.getExpired());
                ps.setLong(2, a.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }
}
