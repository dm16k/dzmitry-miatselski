package com.epam.newsportal.metelsky.domain;

import java.io.Serializable;

/**
 * The Tag class represents entries of 'TAGS' table in DB.
 * It provides getters and setters for every field of each entry.
 *
 * @author dmitr_000
 * @version 1.00 8/10/2015
 */
public class Tag implements Serializable {
    private static final long serialVersionUID = 504106644941155084L;

    /**
     * Tag ID
     */
    private Long id;

    /**
     * Tag name
     */
    private String name;

    /**
     * Default constructor
     */
    public Tag() {
    }

    /**
     * Gets tag ID
     *
     * @return tag ID
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets tag ID
     *
     * @param id - specifies tag ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets tag name
     *
     * @return tag name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets tag name
     *
     * @param name - specifies tag name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns true if and only if the argument is not null and is an Tag object that represents the same values as this object
     *
     * @param o - the object to compare with
     * @return true if the Tag objects represent the same values; false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag = (Tag) o;

        if (id != null ? !id.equals(tag.id) : tag.id != null) return false;
        return !(name != null ? !name.equals(tag.name) : tag.name != null);

    }

    /**
     * Returns a hash code for object values
     *
     * @return hash code for this Tag object
     */
    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    /**
     * Returns a String object representing this Tag's value
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
