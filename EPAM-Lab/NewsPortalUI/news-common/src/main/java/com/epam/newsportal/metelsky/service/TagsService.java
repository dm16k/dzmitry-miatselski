package com.epam.newsportal.metelsky.service;

import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.service.exception.ServiceException;

import java.util.List;

/**
 * An interface for tags service classes. Provides service for CRUD methods of DAO layer.
 *
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
public interface TagsService {
    /**
     * Adds tag to DB
     *
     * @param tag - specifies tag to add
     * @return ID of added tag
     * @throws ServiceException when an error occurred in DAO layer
     */
    Long add(Tag tag) throws ServiceException;

    /**
     * Gets list of all tags
     *
     * @return tag list
     * @throws ServiceException when an error occurred in DAO layer
     */
    List<Tag> getAll() throws ServiceException;

    /**
     * Updates tag info
     *
     * @param tag - specifies tag to update
     * @throws ServiceException when an error occurred in DAO layer
     */
    void update(Tag tag) throws ServiceException;

    /**
     * Deletes tags
     *
     * @param tags - specifies tag to delete
     * @throws ServiceException when an error occurred in DAO layer
     */
    void delete(List<Tag> tags) throws ServiceException;

    /**
     * Unbinds news for list of tags
     *
     * @param tags - specifies list of tags
     * @throws ServiceException when an error occurred in DAO layer
     */
    void unbindNews(List<Tag> tags) throws ServiceException;
}
