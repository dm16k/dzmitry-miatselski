package com.epam.newsportal.metelsky.service.impl;

import com.epam.newsportal.metelsky.domain.*;
import com.epam.newsportal.metelsky.service.*;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.epam.newsportal.metelsky.util.SearchCriteria;

import java.util.ArrayList;
import java.util.List;

/**
 * The NewsManagementServiceImplementation class has methods to manage news, using different services
 *
 * @author dmitr_000
 * @version 1.00 8/18/2015
 */
public class NewsManagementServiceImplementation implements NewsManagementService {
    private AuthorsService authorsService;
    private CommentsService commentsService;
    private NewsService newsService;
    private TagsService tagsService;

    /**
     * Constructor
     *
     * @param authorsService  - specifies author service
     * @param commentsService - specifies comment service
     * @param newsService     - specifies news service
     * @param tagsService     - specifies tag service
     */
    public NewsManagementServiceImplementation(AuthorsService authorsService, CommentsService commentsService,
                                               NewsService newsService, TagsService tagsService) {
        this.authorsService = authorsService;
        this.commentsService = commentsService;
        this.newsService = newsService;
        this.tagsService = tagsService;
    }

    /**
     * @see NewsManagementService#getAllNews() getAllNews
     */
    public List<NewsMessage> getAllNews() throws ServiceException {
        List<News> newsList = newsService.getAll();
        List<NewsMessage> messageList = new ArrayList<>();
        for (News n : newsList) {
            NewsMessage message = new NewsMessage();
            message.setNews(n);
            message.setAuthor(newsService.getAuthor(n.getId()));
            message.setTagList(newsService.getTags(n.getId()));
            message.setCommentList(commentsService.getComments(n.getId()));
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * @see NewsManagementService#search(SearchCriteria, int, int) search
     */
    public List<NewsMessage> search(SearchCriteria criteria, int noOfPage, int itemsPerPage) throws ServiceException {
        List<News> newsList = newsService.search(criteria, noOfPage, itemsPerPage);
        List<NewsMessage> messageList = new ArrayList<>();
        for (News n : newsList) {
            NewsMessage message = new NewsMessage();
            message.setNews(n);
            message.setAuthor(newsService.getAuthor(n.getId()));
            message.setTagList(newsService.getTags(n.getId()));
            message.setCommentList(commentsService.getComments(n.getId()));
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * @see NewsManagementService#countNews(SearchCriteria) count
     */
    public Long countNews(SearchCriteria criteria) throws ServiceException {
        return newsService.countNews(criteria);
    }

    /**
     * @see NewsManagementService#getNewsById(Long) getNewsById
     */
    public NewsMessage getNewsById(Long newsId) throws ServiceException {
        NewsMessage message = new NewsMessage();
        News news = newsService.getById(newsId);
        message.setNews(news);
        message.setAuthor(newsService.getAuthor(news.getId()));
        message.setTagList(newsService.getTags(news.getId()));
        message.setCommentList(commentsService.getComments(news.getId()));
        return message;
    }

    /**
     * @see NewsManagementService#checkNext(Long, SearchCriteria) checkNext
     */
    public Boolean checkNext(Long newsId, SearchCriteria criteria) throws ServiceException {
        return newsService.checkNext(newsId, criteria);
    }

    /**
     * @see NewsManagementService#getNext(Long, SearchCriteria) getNext
     */
    public NewsMessage getNext(Long newsId, SearchCriteria criteria) throws ServiceException {
        NewsMessage message = new NewsMessage();
        News news = newsService.getNext(newsId, criteria);
        message.setNews(news);
        message.setAuthor(newsService.getAuthor(news.getId()));
        message.setTagList(newsService.getTags(news.getId()));
        message.setCommentList(commentsService.getComments(news.getId()));
        return message;
    }

    /**
     * @see NewsManagementService#checkPrevious(Long, SearchCriteria) checkPrevious
     */
    public Boolean checkPrevious(Long newsId, SearchCriteria criteria) throws ServiceException {
        return newsService.checkPrevious(newsId, criteria);
    }

    /**
     * @see NewsManagementService#getPrevious(Long, SearchCriteria) getPrevious
     */
    public NewsMessage getPrevious(Long newsId, SearchCriteria criteria) throws ServiceException {
        NewsMessage message = new NewsMessage();
        News news = newsService.getPrevious(newsId, criteria);
        message.setNews(news);
        message.setAuthor(newsService.getAuthor(news.getId()));
        message.setTagList(newsService.getTags(news.getId()));
        message.setCommentList(commentsService.getComments(news.getId()));
        return message;
    }

    /**
     * @see NewsManagementService#addNews(News, Author, List) addNews
     */
    public Long addNews(News news, Author author, List<Tag> tags) throws ServiceException {
        news.setId(newsService.add(news));
        newsService.setAuthor(news, author);
        newsService.setTags(news, tags);
        return news.getId();
    }

    /**
     * @see NewsManagementService#editNews(News, Author, List) editNews
     */
    public void editNews(News news, Author author, List<Tag> tags) throws ServiceException {
        newsService.update(news);
        newsService.setAuthor(news, author);
        newsService.setTags(news, tags);
    }

    /**
     * @see NewsManagementService#deleteNews(List) deleteNews
     */
    public void deleteNews(List<News> news) throws ServiceException {
        newsService.unbindAuthors(news);
        newsService.unbindTags(news);
        commentsService.deleteByNews(news);
        newsService.delete(news);
    }

    /**
     * @see NewsManagementService#addComment(Comment) addComment
     */
    public Long addComment(Comment comment) throws ServiceException {
        return commentsService.add(comment);
    }

    /**
     * @see NewsManagementService#editComment(Comment) editComment
     */
    public void editComment(Comment comment) throws ServiceException {
        commentsService.update(comment);
    }

    /**
     * @see NewsManagementService#deleteComments(List) deleteComments
     */
    public void deleteComments(List<Comment> comments) throws ServiceException {
        commentsService.delete(comments);
    }

    /**
     * @see NewsManagementService#getAllAuthors() getAllAuthors
     */
    public List<Author> getAllAuthors() throws ServiceException {
        return authorsService.getAll();
    }

    /**
     * @see NewsManagementService#addAuthor(Author) addAuthor
     */
    public Long addAuthor(Author author) throws ServiceException {
        return authorsService.add(author);
    }

    /**
     * @see NewsManagementService#updateAuthor(Author) updateAuthor
     */
    public void updateAuthor(Author author) throws ServiceException {
        authorsService.update(author);
    }

    /**
     * @see NewsManagementService#expireAuthor(Author) expireAuthor
     */
    public void expireAuthor(Author author) throws ServiceException {
        authorsService.expire(author);
    }

    /**
     * @see NewsManagementService#getAllTags() getAllTags
     */
    public List<Tag> getAllTags() throws ServiceException {
        return tagsService.getAll();
    }

    /**
     * @see NewsManagementService#addTag(Tag) addTag
     */
    public Long addTag(Tag tag) throws ServiceException {
        return tagsService.add(tag);
    }

    /**
     * @see NewsManagementService#changeTag(Tag) changeTag
     */
    public void changeTag(Tag tag) throws ServiceException {
        tagsService.update(tag);
    }

    /**
     * @see NewsManagementService#deleteTag(Tag) deleteTag
     */
    public void deleteTag(Tag tag) throws ServiceException {
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        tagsService.unbindNews(tags);
        tagsService.delete(tags);
    }
}
