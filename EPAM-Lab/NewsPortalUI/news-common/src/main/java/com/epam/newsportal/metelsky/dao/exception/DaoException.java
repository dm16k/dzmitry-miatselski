package com.epam.newsportal.metelsky.dao.exception;

/**
 * Thrown if some error happened while executing some operations with DB.
 *
 * @author dmitr_000
 * @version 1.00 8/10/2015
 */
public class DaoException extends Exception {
    public DaoException(Throwable cause) {
        super(cause);
    }

    public DaoException(String message) {
        super(message);
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
