package com.epam.newsportal.metelsky.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The Author class represents entries of 'AUTHORS' table in DB.
 * It provides getters and setters for every field of each entry.
 *
 * @author dmitr_000
 * @version 1.00 8/10/2015
 */
public class Author implements Serializable {
    private static final long serialVersionUID = 5187665473691231804L;
    /**
     * Author ID
     */
    private Long id;

    /**
     * Author name
     */
    private String name;

    /**
     * Specifies author expiration date
     */
    private Timestamp expired;

    /**
     * Default constructor
     */
    public Author() {
    }

    /**
     * Gets author ID
     *
     * @return author ID
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets author ID
     *
     * @param id - specifies author ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Sets author name
     *
     * @return author name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets author name
     *
     * @param name - specifies author name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets author expired date & time
     *
     * @return author expired date & time
     */
    public Timestamp getExpired() {
        return expired;
    }

    /**
     * Sets author expired date & time
     *
     * @param expired - specifies author expired date & time
     */
    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    /**
     * Returns true if and only if the argument is not null and is an Author object that represents the same values as this object
     *
     * @param o - the object to compare with
     * @return true if the Author objects represent the same values; false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (id != null ? !id.equals(author.id) : author.id != null) return false;
        if (name != null ? !name.equals(author.name) : author.name != null) return false;
        return !(expired != null ? !expired.equals(author.expired) : author.expired != null);

    }

    /**
     * Returns a hash code for object values
     *
     * @return hash code for this Author object
     */
    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }

    /**
     * Returns a String object representing this Author's value
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", expired=" + expired +
                '}';
    }
}
