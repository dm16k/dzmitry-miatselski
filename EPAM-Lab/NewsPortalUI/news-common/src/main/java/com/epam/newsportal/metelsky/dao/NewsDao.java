package com.epam.newsportal.metelsky.dao;

import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.util.SearchCriteria;

import java.util.List;

/**
 * An interface for news DAO classes. Provides CRUD methods.
 *
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
public interface NewsDao extends DAO<News> {
    /**
     * @see DAO#add(Object) add
     */
    Long add(News entity) throws DaoException;

    /**
     * Sets author to news
     *
     * @param author - specifies author of news
     * @param news   - specifies news
     * @throws DaoException when an error occurred
     */
    void setAuthor(News news, Author author) throws DaoException;

    /**
     * Sets tags to news
     *
     * @param tags - specifies tags of news
     * @param news - specifies news
     * @throws DaoException when an error occurred
     */
    void setTags(News news, List<Tag> tags) throws DaoException;

    /**
     * @see DAO#getById(Long) getById
     */
    News getById(Long id) throws DaoException;

    /**
     * Checks existence of next news considering search criteria
     *
     * @param newsId - specifies current news' ID
     * @return false if current news is the last one
     * @throws DaoException when an error occurred
     */
    Boolean checkNext(Long newsId, SearchCriteria criteria) throws DaoException;

    /**
     * Gets next news considering search criteria
     *
     * @param newsId - specifies current news' ID
     * @return next news or null if current news is the last one
     * @throws DaoException when an error occurred
     */
    News getNext(Long newsId, SearchCriteria criteria) throws DaoException;

    /**
     * Checks existence of previous news considering search criteria
     *
     * @param newsId - specifies current news' ID
     * @return false if current news is the 1st
     * @throws DaoException when an error occurred
     */
    Boolean checkPrevious(Long newsId, SearchCriteria criteria) throws DaoException;

    /**
     * Gets previous news considering search criteria
     *
     * @param newsId - specifies current news' ID
     * @return previous news or null if current news is the 1st
     * @throws DaoException when an error occurred
     */
    News getPrevious(Long newsId, SearchCriteria criteria) throws DaoException;

    /**
     * Gets author associated with news
     *
     * @param newsId - specifies news
     * @return author
     * @throws DaoException when an error occurred
     */
    Author getAuthorForNews(Long newsId) throws DaoException;

    /**
     * Gets list of tags associated with news
     *
     * @param newsId - specifies news
     * @return list of tags
     * @throws DaoException when an error occurred
     */
    List<Tag> getTagsForNews(Long newsId) throws DaoException;

    /**
     * @see DAO#getAll() getAll
     */
    List<News> getAll() throws DaoException;

    /**
     * @see DAO#update(Object) update
     */
    void update(News entity) throws DaoException;

    /**
     * @see DAO#delete(List) delete
     */
    void delete(List<News> entities) throws DaoException;

    /**
     * Unbinds authors from news list
     *
     * @param news - specifies news list
     * @throws DaoException when an error occurred
     */
    void unbindAuthors(List<News> news) throws DaoException;

    /**
     * Unbinds tags from news list
     *
     * @param news - specifies news list
     * @throws DaoException when an error occurred
     */
    void unbindTags(List<News> news) throws DaoException;

    /**
     * Gets list of news associated with {@code SearchCriteria} object.
     *
     * @param criteria - specifies search
     * @return list of news
     * @throws DaoException when an error occurred
     */
    List<News> search(SearchCriteria criteria, int startRow, int endRow) throws DaoException;

    /**
     * Gets number of selected records in last usage of {@link #getAll() getAll()} or {@link #search(SearchCriteria, int, int)}  search(..)} methods.
     *
     * @return number of selected records
     */
    Long countNews(SearchCriteria criteria) throws DaoException;
}
