package com.epam.newsportal.metelsky.dao.util;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * The ConnectionManager class has methods of taking connections and resources releasing.
 *
 * @author dmitr_000
 * @version 1.00 8/24/2015
 */
public class ConnectionManager {
    private final static Logger logger = Logger.getLogger(ConnectionManager.class);
    private DataSource dataSource;

    public ConnectionManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Connection getConnection() throws SQLException {
        return DataSourceUtils.getConnection(dataSource);
    }

    public void releaseResources(ResultSet rs, Statement st, Connection c) {
        try {
            if (rs != null && !rs.isClosed()) {
                rs.close();
            }
            if (st != null && !st.isClosed()) {
                st.close();
            }
            if (c != null && !c.isClosed()) {
                DataSourceUtils.releaseConnection(c, dataSource);
            }
        } catch (SQLException e) {
            logger.error(e, e.getCause());
        }
    }
}
