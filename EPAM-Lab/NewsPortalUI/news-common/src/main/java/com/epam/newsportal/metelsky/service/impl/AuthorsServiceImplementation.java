package com.epam.newsportal.metelsky.service.impl;

import com.epam.newsportal.metelsky.dao.AuthorsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.service.AuthorsService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * The AuthorsServiceImplementation class provides methods to work with author DAOs
 *
 * @author dmitr_000
 * @version 1.00 8/18/2015
 */
public class AuthorsServiceImplementation implements AuthorsService {
    private AuthorsDao authorsDao;

    public AuthorsServiceImplementation(AuthorsDao authorsDao) {
        this.authorsDao = authorsDao;
    }

    public Long add(Author author) throws ServiceException {
        try {
            if (author != null) {
                return authorsDao.add(author);
            } else {
                throw new ServiceException("Trying to work with null-referenced entity");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<Author> getAll() throws ServiceException {
        try {
            return authorsDao.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void update(Author author) throws ServiceException {
        try {
            if (author != null) {
                authorsDao.update(author);
            } else {
                throw new ServiceException("Trying to work with null-referenced entity");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void expire(Author author) throws ServiceException {
        try {
            if (author != null) {
                List<Author> authorList = new ArrayList<>();
                authorList.add(author);
                authorsDao.delete(authorList);
            } else {
                throw new ServiceException("Trying to work with null-referenced entity");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
