package com.epam.newsportal.metelsky.service.impl;

import com.epam.newsportal.metelsky.dao.NewsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.service.NewsService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.epam.newsportal.metelsky.util.SearchCriteria;

import java.util.List;

/**
 * The NewsServiceImplementation class provides methods to work with news DAOs
 *
 * @author dmitr_000
 * @version 1.00 8/18/2015
 */
public class NewsServiceImplementation implements NewsService {
    private NewsDao newsDao;

    /**
     * Constructor
     *
     * @param newsDao - specifies news DAO
     */
    public NewsServiceImplementation(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    /**
     * @see NewsService#add(News) add
     */
    public Long add(News news) throws ServiceException {
        try {
            if (news != null) {
                return newsDao.add(news);
            } else {
                throw new ServiceException("Trying to work with null-referenced entity");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#setAuthor(News, Author) setAuthor
     */
    public void setAuthor(News news, Author author) throws ServiceException {
        try {
            if (news != null && author != null) {
                newsDao.setAuthor(news, author);
            } else {
                throw new ServiceException("Trying to work with null-referenced entity");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#setTags(News, List) setTags
     */
    public void setTags(News news, List<Tag> tags) throws ServiceException {
        try {
            if (news != null) {
                if (tags != null) { newsDao.setTags(news, tags); }
            } else {
                throw new ServiceException("Trying to work with null-referenced entity");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#getById(Long) getById
     */
    public News getById(Long id) throws ServiceException {
        News news;
        try {
            news = newsDao.getById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return news;
    }

    /**
     * @see NewsService#checkNext(Long, SearchCriteria) checkNext
     */
    public Boolean checkNext(Long newsId, SearchCriteria criteria) throws ServiceException {
        try {
            return newsDao.checkNext(newsId, criteria);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#getNext(Long, SearchCriteria) getNext
     */
    public News getNext(Long newsId, SearchCriteria criteria) throws ServiceException {
        try {
            return newsDao.getNext(newsId, criteria);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#checkPrevious(Long, SearchCriteria) checkPrevious
     */
    public Boolean checkPrevious(Long newsId, SearchCriteria criteria) throws ServiceException {
        try {
            return newsDao.checkPrevious(newsId, criteria);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#getPrevious(Long, SearchCriteria) getPrevious
     */
    public News getPrevious(Long newsId, SearchCriteria criteria) throws ServiceException {
        try {
            return newsDao.getPrevious(newsId, criteria);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#getAuthor(Long) getAuthors
     */
    public Author getAuthor(Long newsId) throws ServiceException {
        try {
            return newsDao.getAuthorForNews(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#getTags(Long) getTags
     */
    public List<Tag> getTags(Long newsId) throws ServiceException {
        try {
            return newsDao.getTagsForNews(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#getAll() getAll
     */
    public List<News> getAll() throws ServiceException {
        try {
            return newsDao.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#search(SearchCriteria, int, int) search
     */
    public List<News> search(SearchCriteria searchCriteria, int noOfPage, int itemsPerPage) throws ServiceException {
        try {
            return newsDao.search(searchCriteria, noOfPage * itemsPerPage, noOfPage * itemsPerPage + itemsPerPage);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#countNews(SearchCriteria) countNews
     */
    public Long countNews(SearchCriteria criteria) throws ServiceException {
        try {
            return newsDao.countNews(criteria);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#update(News) update
     */
    public void update(News news) throws ServiceException {
        try {
            if (news != null) {
                newsDao.update(news);
            } else {
                throw new ServiceException("Trying to work with null-referenced entity");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#delete(List) delete
     */
    public void delete(List<News> news) throws ServiceException {
        try {
            if (news != null) {
                newsDao.delete(news);
            } else {
                throw new ServiceException("Trying to work with null-referenced list");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#unbindAuthors(List) unbindAuthors
     */
    public void unbindAuthors(List<News> news) throws ServiceException {
        try {
            if (news != null) {
                newsDao.unbindAuthors(news);
            } else {
                throw new ServiceException("Trying to work with null-referenced list");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#unbindTags(List) unbindTags
     */
    public void unbindTags(List<News> news) throws ServiceException {
        try {
            if (news != null) {
                newsDao.unbindTags(news);
            } else {
                throw new ServiceException("Trying to work with null-referenced list");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
