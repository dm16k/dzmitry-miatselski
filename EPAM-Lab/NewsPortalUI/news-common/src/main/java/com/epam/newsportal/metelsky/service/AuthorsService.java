package com.epam.newsportal.metelsky.service;

import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.service.exception.ServiceException;

import java.util.List;

/**
 * An interface for authors service classes. Provides service for CRUD methods of DAO layer.
 *
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
public interface AuthorsService {
    /**
     * Adds author to DB
     *
     * @param author - specifies author to add
     * @return ID of added user
     * @throws ServiceException when an error occurred in DAO layer
     */
    Long add(Author author) throws ServiceException;

    /**
     * Gets list of all authors
     *
     * @return author list
     * @throws ServiceException when an error occurred in DAO layer
     */
    List<Author> getAll() throws ServiceException;

    /**
     * Updates author info
     *
     * @param author - specifies author to update
     * @throws ServiceException when an error occurred in DAO layer
     */
    void update(Author author) throws ServiceException;

    /**
     * Expires author
     *
     * @param author - specifies author to expire
     * @throws ServiceException when an error occurred in DAO layer
     */
    void expire(Author author) throws ServiceException;
}
