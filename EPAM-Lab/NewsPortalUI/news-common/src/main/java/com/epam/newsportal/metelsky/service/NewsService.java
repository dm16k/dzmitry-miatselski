package com.epam.newsportal.metelsky.service;

import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.epam.newsportal.metelsky.util.SearchCriteria;

import java.util.List;

/**
 * An interface for news service classes. Provides service for CRUD methods of DAO layer.
 *
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
public interface NewsService {
    /**
     * Adds news to DB
     *
     * @param news - specifies news to add
     * @return ID of added news
     * @throws ServiceException when an error occurred in DAO layer
     */
    Long add(News news) throws ServiceException;

    /**
     * Sets author for news
     *
     * @param news   - specifies news for setting
     * @param author - specifies author
     * @throws ServiceException when an error occurred in DAO layer
     */
    void setAuthor(News news, Author author) throws ServiceException;

    /**
     * Sets tags for news
     *
     * @param news - specifies news for setting
     * @param tags - specifies tags
     * @throws ServiceException when an error occurred in DAO layer
     */
    void setTags(News news, List<Tag> tags) throws ServiceException;

    /**
     * Gets news identified by ID
     *
     * @param id - specifies news ID
     * @return news identified by ID
     * @throws ServiceException when an error occurred in DAO layer
     */
    News getById(Long id) throws ServiceException;

    /**
     * Checks existence of next news considering search criteria
     *
     * @param newsId - specifies current news' ID
     * @return false if current news is the last one
     * @throws ServiceException when an error occurred in DAO layer
     */
    Boolean checkNext(Long newsId, SearchCriteria criteria) throws ServiceException;

    /**
     * Gets next news considering search criteria
     *
     * @param newsId - specifies current news' ID
     * @return next news or null if current news is the last one
     * @throws ServiceException when an error occurred in DAO layer
     */
    News getNext(Long newsId, SearchCriteria criteria) throws ServiceException;

    /**
     * Checks existence of previous news considering search criteria
     *
     * @param newsId - specifies current news' ID
     * @return false if current news is the 1st
     * @throws ServiceException when an error occurred in DAO layer
     */
    Boolean checkPrevious(Long newsId, SearchCriteria criteria) throws ServiceException;

    /**
     * Gets previous news considering search criteria
     *
     * @param newsId - specifies current news' ID
     * @return previous news or null if current news is the 1st
     * @throws ServiceException when an error occurred in DAO layer
     */
    News getPrevious(Long newsId, SearchCriteria criteria) throws ServiceException;

    /**
     * Gets authors for news
     *
     * @param newsId - specifies news ID
     * @return author list
     * @throws ServiceException when an error occurred in DAO layer
     */
    Author getAuthor(Long newsId) throws ServiceException;

    /**
     * Gets tags for news
     *
     * @param newsId - specifies news ID
     * @return tag list
     * @throws ServiceException when an error occurred in DAO layer
     */
    List<Tag> getTags(Long newsId) throws ServiceException;

    /**
     * Gets list of all news
     *
     * @return news list
     * @throws ServiceException when an error occurred in DAO layer
     */
    List<News> getAll() throws ServiceException;

    /**
     * Updates news info
     *
     * @param news - specifies news to update
     * @throws ServiceException when an error occurred in DAO layer
     */
    void update(News news) throws ServiceException;

    /**
     * Deletes news
     *
     * @param news - specifies news list to delete
     * @throws ServiceException when an error occurred in DAO layer
     */
    void delete(List<News> news) throws ServiceException;

    /**
     * Unbinds authors for list of news
     *
     * @param news - specifies list of news
     * @throws ServiceException when an error occurred in DAO layer
     */
    void unbindAuthors(List<News> news) throws ServiceException;

    /**
     * Unbinds tags for list of news
     *
     * @param news - specifies list of news
     * @throws ServiceException when an error occurred in DAO layer
     */
    void unbindTags(List<News> news) throws ServiceException;

    /**
     * Gets list of news according to search criteria
     *
     * @param searchCriteria - specifies search criteria
     * @param noOfPage       - specifies page number
     * @param itemsPerPage   - specifies quantity of news displaying on one page
     * @return news list
     * @throws ServiceException when an error occurred in DAO layer
     */
    List<News> search(SearchCriteria searchCriteria, int noOfPage, int itemsPerPage) throws ServiceException;

    /**
     * Gets number of selected records according to criteria
     *
     * @param criteria - specifies search criteria
     * @return number records
     */
    Long countNews(SearchCriteria criteria) throws ServiceException;
}
