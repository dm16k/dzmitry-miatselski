package com.epam.newsportal.metelsky.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The News class represents entries of 'NEWS' table in DB.
 * It provides getters and setters for every field of each entry.
 *
 * @author dmitr_000
 * @version 1.00 8/10/2015
 */
public class News implements Serializable {
    private static final long serialVersionUID = 6888009424824292864L;

    /**
     * News ID
     */
    private Long id;

    /**
     * News title
     */
    private String title;

    /**
     * News short text
     */
    private String shortText;

    /**
     * News full text
     */
    private String fullText;

    /**
     * News creation date
     */
    private Timestamp creationDate;

    /**
     * News modification date
     */
    private Timestamp modificationDate;

    /**
     * Default constructor
     */
    public News() {
    }

    /**
     * Gets news ID
     *
     * @return news ID
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets news ID
     *
     * @param id - specifies news ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets news title
     *
     * @return news title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets news title
     *
     * @param title - specifies news title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets news short text
     *
     * @return news short text
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * Sets news short text
     *
     * @param shortText - specifies news short text
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * Gets news full text
     *
     * @return news full text
     */
    public String getFullText() {
        return fullText;
    }

    /**
     * Sets news full text
     *
     * @param fullText - specifies news full text
     */
    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    /**
     * Gets news creation date & time
     *
     * @return news creation date & time
     */
    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * Sets news creation date & time
     *
     * @param creationDate - specifies news creation date & time
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Gets news modification date & time
     *
     * @return news modification date & time
     */
    public Timestamp getModificationDate() {
        return modificationDate;
    }

    /**
     * Sets news modification date & time
     *
     * @param modificationDate - specifies news modification date & time
     */
    public void setModificationDate(Timestamp modificationDate) {
        this.modificationDate = modificationDate;
    }

    /**
     * Returns true if and only if the argument is not null and is an News object that represents the same values as this object
     *
     * @param o - the object to compare with
     * @return true if the News objects represent the same values; false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (id != null ? !id.equals(news.id) : news.id != null) return false;
        if (title != null ? !title.equals(news.title) : news.title != null) return false;
        if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null) return false;
        if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null) return false;
        if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) return false;
        return !(modificationDate != null ? !modificationDate.equals(news.modificationDate) : news.modificationDate != null);

    }

    /**
     * Returns a hash code for object values
     *
     * @return hash code for this News object
     */
    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }

    /**
     * Returns a String object representing this News's value
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                '}';
    }
}
