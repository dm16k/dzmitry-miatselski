package com.epam.newsportal.metelsky.domain;

import java.io.Serializable;
import java.util.List;

/**
 * The NewsMessage class represents news with author, tags and comments.
 * It provides getters and setters for every field.
 *
 * @author dmitr_000
 * @version 1.00 8/24/2015
 */
public class NewsMessage implements Serializable {
    private static final long serialVersionUID = 2727530205622436977L;

    /**
     * Specifies news
     */
    private News news;

    /**
     * Specifies author for current news
     */
    private Author author;

    /**
     * Specifies tag list for current news
     */
    private List<Tag> tagList;

    /**
     * Specifies comment list for current news
     */
    private List<Comment> commentList;

    /**
     * Default constructor
     */
    public NewsMessage() {
    }

    /**
     * Gets news
     *
     * @return news
     */
    public News getNews() {
        return news;
    }

    /**
     * Sets news
     *
     * @param news - specifies news
     */
    public void setNews(News news) {
        this.news = news;
    }

    /**
     * Gets news author
     *
     * @return news author
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Sets news author
     *
     * @param author - specifies news author
     */
    public void setAuthor(Author author) {
        this.author = author;
    }

    /**
     * Gets news tag list
     *
     * @return news tag list
     */
    public List<Tag> getTagList() {
        return tagList;
    }

    /**
     * Sets news tag list
     *
     * @param tagList - specifies news tag list
     */
    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    /**
     * Gets news comment list
     *
     * @return news comment list
     */
    public List<Comment> getCommentList() {
        return commentList;
    }

    /**
     * Sets news comment list
     *
     * @param commentList - specifies news comment list
     */
    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsMessage message = (NewsMessage) o;

        if (!news.equals(message.news)) return false;
        if (!author.equals(message.author)) return false;
        if (!tagList.equals(message.tagList)) return false;
        return !(commentList != null ? !commentList.equals(message.commentList) : message.commentList != null);

    }

    @Override
    public int hashCode() {
        int result = news.hashCode();
        result = 31 * result + author.hashCode();
        result = 31 * result + tagList.hashCode();
        result = 31 * result + (commentList != null ? commentList.hashCode() : 0);
        return result;
    }
}
