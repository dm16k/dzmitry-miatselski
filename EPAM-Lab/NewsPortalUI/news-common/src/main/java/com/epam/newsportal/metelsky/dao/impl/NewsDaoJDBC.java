package com.epam.newsportal.metelsky.dao.impl;

import com.epam.newsportal.metelsky.dao.DAO;
import com.epam.newsportal.metelsky.dao.NewsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.dao.util.ConnectionManager;
import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.util.SearchCriteria;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The NewsDaoJdbc class provides methods to work with 'NEWS', 'NEWS_AUTHOR' and 'NEWS_TAG' tables of DB
 *
 * @author dmitr_000
 * @version 1.00 8/12/2015
 */
public class NewsDaoJdbc implements NewsDao {
    private final static String ADD_NEWS = "BEGIN INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) " +
            "VALUES (NEWS_SEQ.nextval, ?, ?, ?, ?, ?) " +
            "RETURNING NEWS_ID INTO ?; END;";
    private final static String INSERT_INTO_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
    private final static String INSERT_INTO_NEWS_TAG = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?, ?)";
    private final static String DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";
    private final static String DELETE_FROM_NEWS_AUTHOR_BY_NEWS = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
    private final static String DELETE_FROM_NEWS_TAG_BY_NEWS = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
    private final static String READ_ALL_NEWS = "SELECT N.NEWS_ID, N.TITLE, N.SHORT_TEXT, " +
            "N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE, " +
            "(SELECT COUNT(*) FROM COMMENTS C WHERE C.NEWS_ID = N.NEWS_ID) AS COMMENTS " +
            "FROM NEWS N";
    private final static String UPDATE_NEWS = "UPDATE NEWS " +
            "SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? " +
            "WHERE NEWS_ID = ?";
    private final static String GET_BY_ID = READ_ALL_NEWS + " WHERE NEWS_ID = ?";
    private final static String GET_AUTHOR_FOR_NEWS = "SELECT A.AUTHOR_ID, A.AUTHOR_NAME, A.EXPIRED " +
            "FROM NEWS_AUTHOR NA JOIN AUTHORS A ON A.AUTHOR_ID = NA.AUTHOR_ID " +
            "WHERE NA.NEWS_ID = ? " +
            "ORDER BY A.AUTHOR_ID";
    private final static String GET_TAGS_FOR_NEWS = "SELECT T.TAG_ID, T.TAG_NAME " +
            "FROM NEWS_TAG NT JOIN TAGS T ON T.TAG_ID = NT.TAG_ID " +
            "WHERE NT.NEWS_ID = ? " +
            "ORDER BY T.TAG_ID";
    private final static String SORTING = " ORDER BY COMMENTS, TRUNC(MODIFICATION_DATE) DESC";
    private final static String COUNT_QUERY_START = "SELECT COUNT(DISTINCT N.NEWS_ID) AS QUANTITY " +
            "FROM NEWS N FULL JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID " +
            "JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID";
    private final static String PAGINATION_QUERY_START = "SELECT * " +
            "FROM (SELECT A.*, ROWNUM AS rnum " +
                        "FROM(SELECT DISTINCT N.NEWS_ID, N.TITLE, N.SHORT_TEXT, N.FULL_TEXT, N.MODIFICATION_DATE, N.CREATION_DATE, " +
                                "(SELECT COUNT(*) FROM COMMENTS C WHERE C.NEWS_ID = N.NEWS_ID) AS COMMENTS " +
                                "FROM NEWS N FULL JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID " +
                                "JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID";
    private final static String PAGINATION_QUERY_TAIL = " ORDER BY TRUNC(N.MODIFICATION_DATE) DESC, COMMENTS DESC, N.MODIFICATION_DATE DESC, N.NEWS_ID DESC) A " +
            "WHERE ROWNUM <= ?) WHERE rnum > ?";
    private final static String PREV_NEXT_START_SUB_QUERY_1 = "SELECT N2.NEWS_ID, N2.TITLE, N2.SHORT_TEXT, " +
            "N2.FULL_TEXT, N2.CREATION_DATE, N2.MODIFICATION_DATE " +
            "FROM (SELECT SUB.NEWS_ID,";
    private final static String PREV_NEXT_START_SUB_QUERY_2 = "(SUB.NEWS_ID) OVER (ORDER BY NULL) AS \"NEXT_ID\" " +
            "FROM (SELECT N.NEWS_ID " +
                        "FROM NEWS N LEFT OUTER JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID " +
                        "LEFT OUTER JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID";
    private final static String PREV_NEXT_QUERY_TALE = " GROUP BY N.NEWS_ID, N.MODIFICATION_DATE, N.NEWS_ID " +
            "ORDER BY TRUNC(N.MODIFICATION_DATE) DESC, (SELECT COUNT(*) FROM COMMENTS C WHERE C.NEWS_ID = N.NEWS_ID) DESC, " +
            "N.MODIFICATION_DATE DESC, N.NEWS_ID DESC) SUB) RES " +
            "JOIN NEWS N2 ON RES.NEXT_ID = N2.NEWS_ID " +
            "WHERE RES.NEWS_ID = ?";
    private final static String CHECK_START_SUB_QUERY_1 = "SELECT RES.NEXT_ID AS \"NEWS_ID\" FROM (SELECT SUB.NEWS_ID,";
    private final static String CHECK_START_SUB_QUERY_2 = "(SUB.NEWS_ID) OVER (ORDER BY NULL) AS \"NEXT_ID\" " +
            "FROM (SELECT N.NEWS_ID " +
                        "FROM NEWS N LEFT OUTER JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID " +
                        "LEFT OUTER JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID";
    private final static String CHECK_QUERY_TALE = " GROUP BY N.NEWS_ID, N.MODIFICATION_DATE, N.NEWS_ID " +
            "ORDER BY TRUNC(N.MODIFICATION_DATE) DESC, (SELECT COUNT(*) FROM COMMENTS C WHERE C.NEWS_ID = N.NEWS_ID) DESC, " +
            "N.MODIFICATION_DATE DESC, N.NEWS_ID DESC) SUB) RES " +
            "WHERE RES.NEWS_ID = ?";
    private ConnectionManager manager;
    private int startCounter = 1;

    /**
     * Constructor
     *
     * @param manager - data source to connect to DB
     */
    public NewsDaoJdbc(ConnectionManager manager) {
        this.manager = manager;
    }

    /**
     * @see DAO#add(Object) add
     */
    public Long add(News entity) throws DaoException {
        Connection c = null;
        CallableStatement cs = null;
        Long id;
        try {
            c = manager.getConnection();
            cs = c.prepareCall(ADD_NEWS);
            cs.setString(1, entity.getTitle());
            cs.setString(2, entity.getShortText());
            cs.setString(3, entity.getFullText());
            cs.setTimestamp(4, entity.getCreationDate());
            cs.setTimestamp(5, entity.getModificationDate());
            cs.registerOutParameter(6, Types.VARCHAR);
            cs.execute();
            id = cs.getLong(6);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, cs, c);
        }
        return id;
    }

    /**
     * @see NewsDao#setAuthor(News, Author) setAuthor
     */
    public void setAuthor(News news, Author author) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_FROM_NEWS_AUTHOR_BY_NEWS);
            ps.setLong(1, news.getId());
            ps.executeUpdate();
            ps = c.prepareStatement(INSERT_INTO_NEWS_AUTHOR);
            ps.setLong(1, news.getId());
            ps.setLong(2, author.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see NewsDao#setTags(News, List) setTags
     */
    public void setTags(News news, List<Tag> tags) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_FROM_NEWS_TAG_BY_NEWS);
            ps.setLong(1, news.getId());
            ps.executeUpdate();
            ps = c.prepareStatement(INSERT_INTO_NEWS_TAG);
            for (Tag t : tags) {
                ps.setLong(1, news.getId());
                ps.setLong(2, t.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see DAO#getById(Long) getById
     */
    public News getById(Long id) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        News n = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(GET_BY_ID);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                n = new News();
                n.setId(id);
                n.setTitle(rs.getString("TITLE"));
                n.setShortText(rs.getString("SHORT_TEXT"));
                n.setFullText(rs.getString("FULL_TEXT"));
                n.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                n.setModificationDate(rs.getTimestamp("MODIFICATION_DATE"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return n;
    }

    /**
     * @see NewsDao#checkNext(Long, SearchCriteria) checkNext
     */
    public Boolean checkNext(Long newsId, SearchCriteria criteria) throws DaoException {
        return checkSibling(newsId, criteria, "LEAD");
    }

    /**
     * @see NewsDao#getNext(Long, SearchCriteria) getNext
     */
    public News getNext(Long newsId, SearchCriteria criteria) throws DaoException {
        return getSibling(newsId, criteria, "LEAD");
    }

    /**
     * @see NewsDao#checkPrevious(Long, SearchCriteria) checkPrevious
     */
    public Boolean checkPrevious(Long newsId, SearchCriteria criteria) throws DaoException {
        return checkSibling(newsId, criteria, "LAG");
    }

    /**
     * @see NewsDao#getPrevious(Long, SearchCriteria) getPrevious
     */
    public News getPrevious(Long newsId, SearchCriteria criteria) throws DaoException {
        return getSibling(newsId, criteria, "LAG");
    }

    /**
     * Checks existence of next or previous news depending on type of function.
     * 'LEAD()' gets next news, 'LAG()' gets previous news.
     *
     * @param newsId - specifies current news' ID
     * @return next (previous) news or null if current news is the first (last) one
     * @throws DaoException when an error occurred
     */
    private Boolean checkSibling(Long newsId, SearchCriteria criteria, String funcName) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        Boolean b = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = prepareCheckStatement(c, criteria.getAuthorId(), criteria.getTagList(), newsId, funcName);
            rs = ps.executeQuery();
            while (rs.next()) {
                Object o = rs.getObject("NEWS_ID");
                b = o != null;
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return b;
    }

    /**
     * Gets next or previous news depending on type of function.
     * 'LEAD()' gets next news, 'LAG' gets previous news.
     *
     * @param newsId - specifies current news' ID
     * @return next (previous) news or null if current news is the first (last) one
     * @throws DaoException when an error occurred
     */
    private News getSibling(Long newsId, SearchCriteria criteria, String funcName) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        News n = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = preparePrevNextStatement(c, criteria.getAuthorId(), criteria.getTagList(), newsId, funcName);
            rs = ps.executeQuery();
            while (rs.next()) {
                n = new News();
                n.setId(rs.getLong("NEWS_ID"));
                n.setTitle(rs.getString("TITLE"));
                n.setShortText(rs.getString("SHORT_TEXT"));
                n.setFullText(rs.getString("FULL_TEXT"));
                n.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                n.setModificationDate(rs.getTimestamp("MODIFICATION_DATE"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return n;
    }

    /**
     * @see NewsDao#getAuthorForNews(Long) getAuthorForNews
     */
    public Author getAuthorForNews(Long newsId) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Author author = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(GET_AUTHOR_FOR_NEWS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            while (rs.next()) {
                author = new Author();
                author.setId(rs.getLong("AUTHOR_ID"));
                author.setName(rs.getString("AUTHOR_NAME"));
                author.setExpired(rs.getTimestamp("EXPIRED"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return author;
    }

    /**
     * @see NewsDao#getTagsForNews(Long) getTagsForNews
     */
    public List<Tag> getTagsForNews(Long newsId) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Tag> tagList = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(GET_TAGS_FOR_NEWS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            tagList = new ArrayList<>();
            while (rs.next()) {
                Tag tag = new Tag();
                tag.setId(rs.getLong("TAG_ID"));
                tag.setName(rs.getString("TAG_NAME"));
                tagList.add(tag);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return tagList;
    }

    /**
     * @see DAO#getAll() getAll
     */
    public List<News> getAll() throws DaoException {
        Connection c = null;
        Statement st = null;
        ResultSet rs = null;
        List<News> list = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(READ_ALL_NEWS + SORTING);
            list = new ArrayList<>();
            while (rs.next()) {
                News news = new News();
                news.setId(rs.getLong("NEWS_ID"));
                news.setTitle(rs.getString("TITLE"));
                news.setShortText(rs.getString("SHORT_TEXT"));
                news.setFullText(rs.getString("FULL_TEXT"));
                news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                news.setModificationDate(rs.getTimestamp("MODIFICATION_DATE"));
                list.add(news);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    /**
     * @see DAO#update(Object) update
     */
    public void update(News entity) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UPDATE_NEWS);
            ps.setString(1, entity.getTitle());
            ps.setString(2, entity.getShortText());
            ps.setString(3, entity.getFullText());
            ps.setTimestamp(4, entity.getCreationDate());
            ps.setTimestamp(5, entity.getModificationDate());
            ps.setLong(6, entity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see DAO#delete(List) delete
     */
    public void delete(List<News> entities) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_NEWS);
            for (News n : entities) {
                ps.setLong(1, n.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see NewsDao#unbindAuthors(List) unbindAuthors
     */
    public void unbindAuthors(List<News> news) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_FROM_NEWS_AUTHOR_BY_NEWS);
            for (News n : news) {
                ps.setLong(1, n.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see NewsDao#unbindTags(List) unbindTags
     */
    public void unbindTags(List<News> news) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_FROM_NEWS_TAG_BY_NEWS);
            for (News n : news) {
                ps.setLong(1, n.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see NewsDao#search(SearchCriteria, int, int) search
     */
    public List<News> search(SearchCriteria criteria, int startRow, int endRow) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<News> list = null;
        try {
            c = manager.getConnection();
            ps = preparePaginationStatement(c, criteria.getAuthorId(), criteria.getTagList(), startRow, endRow);
            rs = ps.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                News news = new News();
                news.setId(rs.getLong("NEWS_ID"));
                news.setTitle(rs.getString("TITLE"));
                news.setShortText(rs.getString("SHORT_TEXT"));
                news.setFullText(rs.getString("FULL_TEXT"));
                news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                news.setModificationDate(rs.getTimestamp("MODIFICATION_DATE"));
                list.add(news);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return list;
    }

    /**
     * @see NewsDao#countNews(SearchCriteria)  countNews
     */
    public Long countNews(SearchCriteria criteria) throws DaoException {
        Long author = null;
        List<Long> tagList = null;
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Long count = 0L;
        try {
            c = manager.getConnection();
            if (criteria != null) {
                author = criteria.getAuthorId();
                tagList = criteria.getTagList();
            }
            ps = prepareCountNewsStatement(c, author, tagList);
            rs = ps.executeQuery();
            rs.next();
            count = rs.getLong("QUANTITY");
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return count;
    }

    /**
     * This method constructs {@code PreparedStatement} object depending on authorId and tagList used to count news
     *
     * @param c        - specifies connection to DB
     * @param authorId - specifies authorId
     * @param tagList  - specifies list of tags
     * @return PreparedStatement object
     * @throws SQLException when exception occurred while preparing statement
     */
    private PreparedStatement prepareCountNewsStatement(Connection c, Long authorId, List<Long> tagList) throws SQLException {
        startCounter = 1;
        return makePreparedStatement(c, authorId, tagList, COUNT_QUERY_START, "");
    }

    /**
     * This method constructs {@code PreparedStatement} object depending on authorId, tagList, start and end indexes to get news list for one page
     *
     * @param c        - specifies connection to DB
     * @param authorId - specifies authorId
     * @param tagList  - specifies list of tags
     * @param start    - specifies first row index
     * @param end      - specifies last row index
     * @return PreparedStatement object
     * @throws SQLException when exception occurred while preparing statement
     */
    private PreparedStatement preparePaginationStatement(Connection c, Long authorId, List<Long> tagList, int start, int end) throws SQLException {
        startCounter = 1;
        PreparedStatement ps = makePreparedStatement(c, authorId, tagList, PAGINATION_QUERY_START, PAGINATION_QUERY_TAIL);
        ps.setInt(startCounter++, end);
        ps.setInt(startCounter, start);
        return ps;
    }

    /**
     * This method constructs {@code PreparedStatement} object depending on authorId, tagList and function name to get next or previous news
     *
     * @param c        - specifies connection to DB
     * @param authorId - specifies authorId
     * @param tagList  - specifies list of tags
     * @param newsId   - specifies current news' ID
     * @param funcName - specifies type of function
     * @return PreparedStatement object
     * @throws SQLException when exception occurred while preparing statement
     */
    private PreparedStatement preparePrevNextStatement(Connection c, Long authorId, List<Long> tagList, Long newsId, String funcName) throws SQLException {
        startCounter = 1;
        String queryStart = PREV_NEXT_START_SUB_QUERY_1 + funcName + PREV_NEXT_START_SUB_QUERY_2;
        PreparedStatement ps = makePreparedStatement(c, authorId, tagList, queryStart, PREV_NEXT_QUERY_TALE);
        ps.setLong(startCounter, newsId);
        return ps;
    }

    /**
     * This method constructs {@code PreparedStatement} object depending on authorId, tagList and query
     *
     * @param c        - specifies connection to DB
     * @param authorId - specifies authorId
     * @param tagList  - specifies list of tags
     * @param newsId   - specifies current news' ID
     * @param funcName - specifies type of function
     * @return PreparedStatement object
     * @throws SQLException when exception occurred while preparing statement
     */
    private PreparedStatement prepareCheckStatement(Connection c, Long authorId, List<Long> tagList, Long newsId, String funcName) throws SQLException {
        startCounter = 1;
        String queryStart = CHECK_START_SUB_QUERY_1 + funcName + CHECK_START_SUB_QUERY_2;
        PreparedStatement ps = makePreparedStatement(c, authorId, tagList, queryStart, CHECK_QUERY_TALE);
        ps.setLong(startCounter, newsId);
        return ps;
    }

    /**
     * Constructs a {@code PreparedStatement} object depending on starting, ending queries, author ID and list of tag IDs.
     * Also it counts how much parameters are used in constructed {@code PreparedStatement} object.
     *
     * @param c          - connection used to create a {@code PreparedStatement} object
     * @param authorId   - author ID
     * @param tagList    - list of tag IDs
     * @param queryStart - starting query string
     * @param queryEnd   - ending query string
     * @return PreparedStatement object
     * @throws SQLException when exception occurred while preparing statement
     */
    private PreparedStatement makePreparedStatement(Connection c, Long authorId, List<Long> tagList, String queryStart, String queryEnd) throws SQLException {
        PreparedStatement ps;
        StringBuilder builder = new StringBuilder(queryStart);
        if (authorId == null && (tagList == null || tagList.size() == 0)) {
            builder.append(queryEnd);
            ps = c.prepareStatement(builder.toString());
        } else {
            builder.append(" WHERE ");
            if (authorId != null) {
                builder.append("NA.AUTHOR_ID = ?");
                if (tagList != null && tagList.size() != 0) {
                    builder.append(" AND ");
                }
            }
            if (tagList != null && tagList.size() != 0) {
                builder.append("(");
                for (int i = 0; i < tagList.size(); i++) {
                    if (i == 0) {
                        builder.append("NT.TAG_ID = ?");
                    } else {
                        builder.append(" OR NT.TAG_ID = ?");
                    }
                }
                builder.append(")");
            }
            builder.append(queryEnd);
            ps = c.prepareStatement(builder.toString());
            if (authorId != null) {
                ps.setLong(startCounter++, authorId);
            }
            if (tagList != null) {
                for (Long t : tagList) {
                    ps.setLong(startCounter++, t);
                }
            }
        }
        return ps;
    }
}
