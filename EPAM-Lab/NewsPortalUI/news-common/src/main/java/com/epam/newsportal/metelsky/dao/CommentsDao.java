package com.epam.newsportal.metelsky.dao;

import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Comment;
import com.epam.newsportal.metelsky.domain.News;

import java.util.List;

/**
 * An interface for comments DAO classes. Provides CRUD methods.
 *
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
public interface CommentsDao extends DAO<Comment> {
    /**
     * @see DAO#add(Object) add
     */
    Long add(Comment entity) throws DaoException;

    /**
     * Gets list of comments associated with one news
     *
     * @param newsId - specifies news
     * @return list of comments
     * @throws DaoException when an error occurred
     */
    List<Comment> getNewsComments(Long newsId) throws DaoException;

    /**
     * @see DAO#update(Object) update
     */
    void update(Comment entity) throws DaoException;

    /**
     * @see DAO#delete(List) delete
     */
    void delete(List<Comment> entities) throws DaoException;

    /**
     * Deletes comments for news
     *
     * @param news - specifies news
     * @throws DaoException when an error occurred
     */
    void deleteByNews(List<News> news) throws DaoException;
}
