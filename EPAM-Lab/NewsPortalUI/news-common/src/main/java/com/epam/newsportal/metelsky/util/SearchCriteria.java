package com.epam.newsportal.metelsky.util;

import java.util.List;

/**
 * It provides getters and setters for {@code Author} and {@code List<Tag>} fields
 *
 * @author dmitr_000
 * @version 1.00 8/14/2015
 */
public class SearchCriteria {
    private Long authorId;
    private List<Long> tagList;

    /**
     * Default constructor
     */
    public SearchCriteria() {
    }

    public List<Long> getTagList() {
        return tagList;
    }

    /**
     * Sets tag list
     *
     * @param tagList - specifies tag list
     */
    public void setTagList(List<Long> tagList) {
        this.tagList = tagList;
    }

    public Long getAuthorId() {
        return authorId;
    }

    /**
     * Sets author
     *
     * @param authorId - specifies author
     */
    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    /**
     * Returns true if and only if the argument is not null and is an SearchCriteria object that represents the same values as this object
     *
     * @param o - the object to compare with
     * @return true if the SearchCriteria objects represent the same values; false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchCriteria criteria = (SearchCriteria) o;

        if (authorId != null ? !authorId.equals(criteria.authorId) : criteria.authorId != null) return false;
        return !(tagList != null ? !tagList.equals(criteria.tagList) : criteria.tagList != null);

    }

    /**
     * Returns a hash code for object values
     *
     * @return hash code for this SearchCriteria object
     */
    @Override
    public int hashCode() {
        int result = authorId != null ? authorId.hashCode() : 0;
        result = 31 * result + (tagList != null ? tagList.hashCode() : 0);
        return result;
    }
}
