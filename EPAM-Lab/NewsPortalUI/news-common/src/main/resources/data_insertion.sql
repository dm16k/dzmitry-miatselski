ALTER SESSION SET current_schema = "USERA";
BEGIN
  DELETE FROM NEWS_AUTHOR;
  DELETE FROM AUTHORS;
  DELETE FROM NEWS_TAG;
  DELETE FROM TAGS;
  DELETE FROM COMMENTS;
  DELETE FROM NEWS;
  DELETE FROM ROLES;
  DELETE FROM USERS;
  reset_seq('AUTHORS_SEQ');
  reset_seq('COMMENTS_SEQ');
  reset_seq('NEWS_SEQ');
  reset_seq('TAGS_SEQ');
  reset_seq('USERS_SEQ');
  -- AUTHORS
  INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHORS_SEQ.nextval, 'Sandra H. Woodard');
  INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHORS_SEQ.nextval, 'Charlie C. Turner');
  INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHORS_SEQ.nextval, 'Michael M. Luna');
  INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHORS_SEQ.nextval, 'Clay R. Hilton');
  INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHORS_SEQ.nextval, 'Heather R. Johnston');
  INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHORS_SEQ.nextval, 'Robert M. Garr');
  INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHORS_SEQ.nextval, 'Juana J. Johnson');
  INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHORS_SEQ.nextval, 'Rebbecca R. Birchfield');
  INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHORS_SEQ.nextval, 'Fred R. Parkman');
  INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHORS_SEQ.nextval, 'Erma D. Callahan');
  --   INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHORS_SEQ.nextval, 'Sean M. McCoy', '20-Nov-15');
  --   INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHORS_SEQ.nextval, 'Leah D. Chapman', '5-Sep-15');
  --   INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHORS_SEQ.nextval, 'Carole S. Whitman', '30-Oct-15');
  --   INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHORS_SEQ.nextval, 'Teresa K. Beverage', '25-Oct-15');
  --   INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHORS_SEQ.nextval, 'Thelma J. Adams', '20-Dec-15');
  --   INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHORS_SEQ.nextval, 'Amy C. Perez', '15-Aug-15');
  --   INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHORS_SEQ.nextval, 'Harry A. Rodriguez', '10-Jan-16');
  --   INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHORS_SEQ.nextval, 'Clarence A. Johnston', '30-Sep-15');
  --   INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHORS_SEQ.nextval, 'Maria J. Bishop', '30-Nov-15');
  --   INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHORS_SEQ.nextval, 'Mavis R. Smith', '5-Nov-15');
    -- TAGS
  INSERT INTO TAGS (TAG_ID, TAG_NAME) VALUES (TAGS_SEQ.nextval, 'Asia');
  INSERT INTO TAGS (TAG_ID, TAG_NAME) VALUES (TAGS_SEQ.nextval, 'Science & Environment');
  INSERT INTO TAGS (TAG_ID, TAG_NAME) VALUES (TAGS_SEQ.nextval, 'Europe');
  INSERT INTO TAGS (TAG_ID, TAG_NAME) VALUES (TAGS_SEQ.nextval, 'Magazine');
  INSERT INTO TAGS (TAG_ID, TAG_NAME) VALUES (TAGS_SEQ.nextval, 'Middle East');
  INSERT INTO TAGS (TAG_ID, TAG_NAME) VALUES (TAGS_SEQ.nextval, 'Auto');
  --NEWS
  INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (NEWS_SEQ.nextval, '''No foreign role'' in Bangkok bomb',
          'Monday''s bombing of the Erawan Shrine in central Bangkok was "unlikely" to have been work of an international terror group, the Thai government says.',
          '"Monday''s bombing in Thailand was ""unlikely"" to have been the work of an international terror group, the Thai government has said.
          Col Winthai Suvaree, a spokesman for the ruling military junta, said this was the preliminary conclusion reached by investigators.
          The attack on the Erawan Shrine in central Bangkok on Monday evening killed 20 people and injured scores.
          Police say that at least 10 people are suspected of involvement in the attack."',
          TO_TIMESTAMP('20-08-15 10:30', 'DD-MM-YY HH24:MI'), '20-Aug-15');
  INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (NEWS_SEQ.nextval, 'Coal error'' skewed China CO2 data',
          'Confusion over the types of coal being burned in China''s power stations means its carbon emissions have been overestimated, say researchers.',
          'Confusion over the types of coal being burned in Chinese power stations has caused a significant overestimation of the country''s carbon emissions.
          Researchers, published in the journal Nature, say existing CO2 calculations had used a globally averaged formula.
          But when scientists tested the types of coal actually being burned in China, they found they produced 40% less carbon than had been assumed.
          The study says the error amounted to 10% of global emissions in 2013.
          China''s drive for economic growth over the past 15 years has seen the rapid expansion of coal burning for the production of energy.',
          TO_TIMESTAMP('19-08-15 19:00', 'DD-MM-YY HH24:MI'), '19-Aug-15');
  INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (NEWS_SEQ.nextval, 'Carbon fibres made from air',
          'Chemists discover a way to take carbon dioxide from the air and make carbon nanofibres, a valuable manufacturing material.',
          'Scientists in the US have found a way to take carbon dioxide (CO2) from the air and make carbon nanofibres, a valuable manufacturing material.
          Their solar-powered system runs just a few volts of electricity through a vat full of a hot, molten salt; CO2 is absorbed and the nanofibres gradually assemble at one of the electrodes.
          It currently produces 10g in an hour.
          The team suggests it could be scaled up and make an impact on CO2 emissions, but other researchers are unsure.
          Nonetheless, it could offer a cheaper way of making carbon nanofibres than existing methods.',
          TO_TIMESTAMP('20-08-15 9:00', 'DD-MM-YY HH24:MI'), '20-Aug-2015');
  INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (NEWS_SEQ.nextval, 'Pyongyang rocks',
          'What was the setlist at North Korea''s first Western rock gig?',
          'On Wednesday, the Slovenian band Laibach say they became the first western rock group to play inside North Korea.
          Photographs seem to show that the audience at the Ponghwa Theatre was appreciative, if not completely sure of what to make of the concert.
          Laibach describe themselves as "a music and cross-media group" and are known for playing eclectic cover versions of famous songs.
          The songs were accompanied by images styled from North Korean propaganda posters projected on to a screen, with translations in Korean.',
          TO_TIMESTAMP('20-08-15 1:00', 'DD-MM-YY HH24:MI'), '20-Aug-15');
  INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (NEWS_SEQ.nextval, 'Money for nothing',
          'Finland considers basic income to replace benefits',
          'The Finnish government is considering a pilot project that would see the state pay people a basic income regardless of whether they work.
          The details of how much the basic income might be and who would be eligible for it are yet to be announced, but already there is widespread interest in how it might work.
          Prime Minister Juha Sipila has praised the idea. "For me, a basic income means simplifying the social security system," he said.
          The scheme is of particular interest to people without jobs. In Finland, they now number 280,000 - 10% of the workforce.
          With unemployment an increasing concern, four out of five Finns now are in favour of a basic income.',
          TO_TIMESTAMP('20-08-15 1:00', 'DD-MM-YY HH24:MI'), '20-Aug-15');
  INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (NEWS_SEQ.nextval, 'Trapped',
          'The young people risking death at sea to escape boredom',
          'Many people trying to start a new life in Europe want to escape war or extreme poverty, but large numbers of young Algerians are willing to risk death crossing the Mediterranean because they are bored and fed up with the lack of opportunities at home.
          I crawl under a wire and nearly trip on a sleeping dog. "This way, quick!" hisses Rachid. It is pitch black so I gingerly follow the greenish glow from his mobile phone up four flights of stairs in a half-built apartment block.
          We''re up here because Rachid prefers to speak away from the prying eyes of the police or the twitching ears of informers. The warm wind is sweeping across from the sea. We are overlooking Sidi Salem, a rundown suburb of the Algerian port of Annaba. To our left a former French colonial army barracks has been turned into a shanty town.
          "Even though I''m free, my life here is like a prison," Rachid says. "As soon as I get a call about a boat, I''ll be off again to try to reach Sardinia."
          Rachid is a harraga. The word means "path burner" in Arabic, a runaway who flees his or her country for a better life elsewhere. It also describes the practice of setting fire to identity papers to avoid being traced and repatriated.
          Over the past decade, Sidi Salem has become a famous launch pad for illegal migrants. The Italian island of Sardinia can be reached in just 12 hours with the right weather conditions.',
          TO_TIMESTAMP('20-08-15 1:00', 'DD-MM-YY HH24:MI'), '20-Aug-15');
  INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (NEWS_SEQ.nextval, 'Preparing for battle',
          'Images of PKK fighters at their military training camp in northern Iraq.',
          'For the three decades, the Kurdistan Workers'' Party (PKK) has been fighting the Turkish government.
          The PKK is considered a terrorist organisation by the Turkish authorities and several Western states, but it is now a key player in the battle against the jihadist group Islamic State (IS).
          BBC Persian''s Jiyar Gol was granted rare access to a PKK training camp in northern Iraq.',
          TO_TIMESTAMP('20-08-15 1:00', 'DD-MM-YY HH24:MI'), '20-Aug-15');
  INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (NEWS_SEQ.nextval, 'Mr Palmyra',
          'Killed by IS militants after caring for ancient ruins for 40 years.',
          'Khaled al-Asaad, the archaeologist who has reportedly been killed by Islamic State militants, had a lifelong connection to the town, having been born into a prominent family in the area in 1934.
          Although curating the ruins at the Unesco World Heritage site would become his life''s work and he had a degree in history from Damascus University, he had no formal training in archaeology - all his knowledge in this field was self-taught.
          Archaeologist and former Syrian antiquities official Amr al-Azm, who knew Mr Asaad, told the BBC that he was an "icon of Palmyrene archaeology".
          "If you needed to do anything in Palmyra with regards to the archaeology or the monuments, you had to go through Khaled al-Asaad. He was essentially ''Mr Palmyra''," Mr Azm said.',
          TO_TIMESTAMP('19-08-15 7:00', 'DD-MM-YY HH24:MI'), '19-Aug-15');
  INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (NEWS_SEQ.nextval, 'Burlesque in China',
          'A showgirl''s story of sequins and censorship in Shanghai.',
          'If the strangeness of opening a burlesque club in China had not occurred to Amelia Kallman and Norman Gosney as a Buddhist cleansing ceremony took place in their future venue, it certainly did when they found themselves submitting Frank Sinatra lyrics to be vetted by the local cultural department.
          The couple opened the first cabaret nightclub of its kind in Shanghai in 2010.
          And the story of their rise and fall is now a stage show at the Edinburgh Fringe Festival.
          "It''s a cautionary tale" says Kallman. "And a love story".',
          TO_TIMESTAMP('20-08-15 1:00', 'DD-MM-YY HH24:MI'), '20-Aug-15');
  INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
  VALUES (NEWS_SEQ.nextval, 'Cadillac ''approaches perfection''',
          'Can the 2016 CTS-V outgun rivals from Mercedes and BMW?',
          'A halo car exists, by definition, to cast a warm glow over the rest of an automaker�s range.
          Relative to its marque, it is a model that pushes the boundaries of performance, price and exclusivity.
          So it has been for the CTS-V since the debut of the first generation model in 2004.
          That car, a brutish, Corvette-engined noisemaker, while impressive in its own way, never really became a threat to its imagined rivals from BMW and Mercedes-Benz.
          The same cannot be said of the 2016 CTS-V.',
          TO_TIMESTAMP('19-08-15 19:00', 'DD-MM-YY HH24:MI'), '19-Aug-15');
  -- COMMENTS
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 1, 'That''s awfull!', TO_TIMESTAMP('20-08-15 16:01', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 1, 'Interesting!', TO_TIMESTAMP('20-08-15 18:06', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 2, 'Interesting point of view.', TO_TIMESTAMP('20-08-15 16:01', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 2, 'I think you''re wrong.', TO_TIMESTAMP('20-08-15 18:06', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 3, 'Well done!', TO_TIMESTAMP('20-08-15 16:01', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 3, 'Hope it wil work!', TO_TIMESTAMP('20-08-15 18:06', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES
    (COMMENTS_SEQ.nextval, 4, 'Propaganda. On concert. Facepalm.', TO_TIMESTAMP('20-08-15 16:01', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES
    (COMMENTS_SEQ.nextval, 4, 'Hope it''s not for the last time�', TO_TIMESTAMP('20-08-15 18:06', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 5, 'Bad news.', TO_TIMESTAMP('20-08-15 16:01', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 5, '+', TO_TIMESTAMP('20-08-15 18:06', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES
    (COMMENTS_SEQ.nextval, 6, 'If you wanna work you''ll find it!', TO_TIMESTAMP('20-08-15 16:01', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 7, 'Why we support them?!', TO_TIMESTAMP('20-08-15 16:01', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 8, 'Interesting!', TO_TIMESTAMP('20-08-15 16:01', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 8, 'RIP.', TO_TIMESTAMP('20-08-15 18:06', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 9, 'Interesting!', TO_TIMESTAMP('20-08-15 16:01', 'DD-MM-YY HH24:MI'));
  INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
  VALUES (COMMENTS_SEQ.nextval, 10, 'Nice car, I think.', TO_TIMESTAMP('20-08-15 16:01', 'DD-MM-YY HH24:MI'));
  -- NEWS_AUTHOR
  INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (1, 1);
  INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (2, 2);
  INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (3, 3);
  INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (4, 4);
  INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (5, 5);
  INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (6, 6);
  INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (7, 7);
  INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (8, 8);
  INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (9, 9);
  INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (10, 10);
  -- NEWS_TAG
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (1, 1);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (2, 1);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (2, 2);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (3, 2);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (4, 1);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (5, 3);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (6, 4);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (7, 1);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (7, 5);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (8, 1);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (8, 5);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (9, 1);
  INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (10, 6);
--   -- USERS
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Jacob K. Williams', 'Pecar1988', 'OhGho4hahu5t');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Lucille J. Tuck', 'Asts1932', 'Ooqu2wee3ei');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Charles M. Hardy', 'Ambegrout', 'Lie0eewae');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Raul M. Rankin', 'Cely1977', 'Goomeav1Ahch');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Tommy D. Payne', 'Stinced', 'enudi0toCh2');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Brian C. Reynolds', 'Fend1963', 'ulup0Uz4oV2');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Rosa D. Santos', 'Caushre', 'Jeiref2ush');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Allen S. Gray', 'Hinknow', 'ad2Vegah2');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Louise G. Greer', 'Fait1939', 'EN6wie6o');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Hazel R. Wise', 'Legre1930', 'Pheim7ka');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Filomena J. Jackson', 'Jonan1973', 'Koo8noo0ei');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Joseph C. Gaudreau', 'Trablinever', 'yae2yei0Oh');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Amy B. Everett', 'Derydeartact88', 'eich7UJi3');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Alex R. Watson', 'Hintrues', 'ohreM6eroo');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Dana W. Green', 'Hatieverse', 'xohzeiJ9');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Jeffery B. Morrow', 'Ortogs', 'pohy2Ea9K');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Shawn R. Bramlett', 'Makeetelich', 'aeg7Giiz8Oi');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Anne R. Hoppe', 'Premosoming', 'FahG7cohGh');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Patrick E. Thomas', 'Kiny1929', 'ooF2riechae');
--   INSERT INTO USERS (USER_ID, USER_NAME, LOGIN, PASSWORD)
--   VALUES (USERS_SEQ.nextval, 'Shaun C. Stanley', 'Pailly', 'ea0Ataes');
--   -- ROLES
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (1, 'ADMIN');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (2, 'ADMIN');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (3, 'ADMIN');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (4, 'EDITOR');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (5, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (6, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (7, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (8, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (9, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (10, 'EDITOR');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (11, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (12, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (13, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (14, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (15, 'EDITOR');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (16, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (17, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (18, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (19, 'USER');
--   INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (20, 'USER');
  COMMIT;
  EXCEPTION
  WHEN OTHERS THEN ROLLBACK;
  RAISE;
END;