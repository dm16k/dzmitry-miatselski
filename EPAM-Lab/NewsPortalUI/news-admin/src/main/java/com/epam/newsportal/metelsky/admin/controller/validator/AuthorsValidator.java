package com.epam.newsportal.metelsky.admin.controller.validator;

import com.epam.newsportal.metelsky.domain.Author;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The AuthorsValidator class validates {@code Author} objects
 *
 * @author dmitr_000
 * @version 1.00 9/21/2015
 */
@Component
public class AuthorsValidator implements Validator {
    private final static String NAME_PATTERN = "^([A-Z�-�][a-z�-�]+\\s[A-Z�-�][a-z�-�]+)$";
    private Pattern pattern;
    private Matcher matcher;

    /**
     * Checks support of object to validate
     *
     * @param aClass - object that specifies supported class
     * @return true if it's an Author class
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return Author.class.equals(aClass);
    }

    /**
     * Validates {@code Author} objects
     *
     * @param o - object to validate
     * @param errors - {@code Error} object storing validation errors
     */
    @Override
    public void validate(Object o, Errors errors) {
        Author author = (Author) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.author.name");
        if (author != null && author.getName() != null) {
            if (author.getName().length() > 30) {
                errors.rejectValue("name", "TooLong.author.name");
            } else if (author.getName().length() > 0) {
                pattern = Pattern.compile(NAME_PATTERN);
                matcher = pattern.matcher(author.getName());
                if (!matcher.matches()) {
                    errors.rejectValue("name", "WrongFormat.author.name");
                }
            }
        }
    }
}
