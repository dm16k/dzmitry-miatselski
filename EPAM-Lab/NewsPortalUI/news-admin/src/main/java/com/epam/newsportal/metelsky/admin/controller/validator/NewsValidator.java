package com.epam.newsportal.metelsky.admin.controller.validator;

import com.epam.newsportal.metelsky.domain.NewsMessage;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * The NewsValidator class validates {@code NewsMessage} objects
 *
 * @author dmitr_000
 * @version 1.00 9/21/2015
 */
@Component
public class NewsValidator implements Validator {
    /**
     * Checks support of object to validate
     *
     * @param aClass - object that specifies supported class
     * @return true if it's an NewsMessage class
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return NewsMessage.class.equals(aClass);
    }

    /**
     * Validates {@code NewsMessage} objects
     *
     * @param o - object to validate
     * @param errors - {@code Error} object storing validation errors
     */
    @Override
    public void validate(Object o, Errors errors) {
        NewsMessage message = (NewsMessage) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.title", "NotEmpty.news.title");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.shortText", "NotEmpty.news.shortText");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.fullText", "NotEmpty.news.fullText");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.creationDate", "WrongFormat.news.creationDate");
        if (message.getAuthor() == null || message.getAuthor().getId() == 0) {
            errors.rejectValue("author", "NotEmpty.author");
        }
        if (message.getNews().getTitle().length() > 40) {
            errors.rejectValue("news.title", "TooLong.news.title");
        }
        if (message.getNews().getShortText().length() > 200) {
            errors.rejectValue("news.shortText", "TooLong.news.shortText");
        }
        if (message.getNews().getFullText().length() > 2000) {
            errors.rejectValue("news.fullText", "TooLong.news.fullText");
        }
    }
}
