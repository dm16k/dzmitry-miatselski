package com.epam.newsportal.metelsky.admin.controller;

import com.epam.newsportal.metelsky.admin.controller.validator.NewsValidator;
import com.epam.newsportal.metelsky.domain.Comment;
import com.epam.newsportal.metelsky.domain.NewsMessage;
import com.epam.newsportal.metelsky.service.NewsManagementService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * @author dmitr_000
 * @version 1.00 9/17/2015
 */
@Controller
@RequestMapping({"/news"})
public class NewsController {
    @Autowired
    private NewsManagementService service;

    @Autowired
    private NewsValidator validator;

    @InitBinder(value = "newsMessage")
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(value = "/{newsId}", method = RequestMethod.GET)
    public String viewNews(Model model, @PathVariable Long newsId) throws ServiceException {
        if (!model.containsAttribute("newsMessage")) {
            model.addAttribute("newsMessage", service.getNewsById(newsId));
        }
        populateModel(model);
        return "news";
    }

    private void populateModel(Model model) throws ServiceException {
        model.addAttribute("authorList", service.getAllAuthors());
        model.addAttribute("tagList", service.getAllTags());
    }

    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String createNews(Model model) throws ServiceException {
        if (!model.containsAttribute("newsMessage")) {
            model.addAttribute("newsMessage", new NewsMessage());
        }
        populateModel(model);
        return "news";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String saveNews(@ModelAttribute("newsMessage") @Validated final NewsMessage newsMessage, final BindingResult result, RedirectAttributes attributes) throws ServiceException {
        if (result.hasErrors()) {
            Long id = newsMessage.getNews().getId();
            attributes.addFlashAttribute("org.springframework.validation.BindingResult.newsMessage", result);
            attributes.addFlashAttribute("newsMessage", newsMessage);
            if (id == 0) {
                return "redirect:/news/new";
            } else {
                return "redirect:/news/" + id;
            }
        }
        Long newsId = newsMessage.getNews().getId();
        if (newsId == null || newsId == 0) {
            newsId = service.addNews(newsMessage.getNews(), newsMessage.getAuthor(), newsMessage.getTagList());
        } else {
            service.editNews(newsMessage.getNews(), newsMessage.getAuthor(), newsMessage.getTagList());
        }
        return "redirect:/news/" + newsId;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteComment(@RequestBody final List<Comment> commentList) throws ServiceException {
        service.deleteComments(commentList);
    }
}
