package com.epam.newsportal.metelsky.admin.controller;

import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.service.NewsManagementService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.epam.newsportal.metelsky.util.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author dmitr_000
 * @version 1.00 9/14/2015
 */
@Controller
@RequestMapping({"/"})
public class ListController {

    private final int itemsPerPage;

    @Autowired
    private NewsManagementService service;

    @Autowired
    public ListController(MessageSource source) {
        itemsPerPage = Integer.parseInt(source.getMessage("list.items", null, new Locale("en")));
    }

    @RequestMapping(value = {"/", "/list", "/search"})
    public String setSearchCriteria(HttpServletRequest request) {
        HttpSession session = request.getSession();
        SearchCriteria criteria = (SearchCriteria) session.getAttribute("criteria");
        if (criteria == null) {
            criteria = new SearchCriteria();
        }
        String authorId = request.getParameter("authorId");
        Long id = null;
        if (authorId != null && !authorId.equalsIgnoreCase("any")) {
            id = Long.parseLong(authorId);
        }
        String[] tags = request.getParameterValues("tagId");
        List<Long> selectedTags = null;
        if (tags != null) {
            selectedTags = new ArrayList<>();
            for (String t : tags) {
                selectedTags.add(Long.parseLong(t));
            }
        }
        criteria.setAuthorId(id);
        criteria.setTagList(selectedTags);
        session.setAttribute("criteria", criteria);
        return "redirect:/list/1";
    }

    @RequestMapping(value = "/list/{page}", method = RequestMethod.GET)
    public String filterList(Model model, @PathVariable Integer page, HttpSession session) throws ServiceException {
        SearchCriteria criteria = (SearchCriteria) session.getAttribute("criteria");
        if (criteria == null) criteria = new SearchCriteria();
        Long quantity = service.countNews(criteria);
        model.addAttribute("newsList", service.search(criteria, page - 1, itemsPerPage));
        model.addAttribute("currentPage", page);
        model.addAttribute("pageQuantity", (int) Math.ceil(quantity * 1.0 / itemsPerPage));
        model.addAttribute("authorList", service.getAllAuthors());
        model.addAttribute("tagList", service.getAllTags());
        return "list";
    }

    @RequestMapping(value = "/list/delete", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteNews(@RequestBody final List<News> newsList) throws ServiceException {
        service.deleteNews(newsList);
    }
}
