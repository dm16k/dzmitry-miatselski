package com.epam.newsportal.metelsky.admin.controller.error;

import com.epam.newsportal.metelsky.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.Locale;

/**
 * The GlobalExceptionHandler class handles {@code ServiceException, RuntimeException, SQLException}
 * exceptions thrown by any layer of the application
 *
 * @author dmitr_000
 * @version 1.00 10/13/2015
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    @Autowired
    private MessageSource messageSource;

    /**
     * Constructs model with view containing error data, error code and error message
     *
     * @param e      - exception to handle
     * @param locale - current locale
     * @return model with view
     */
    @ExceptionHandler(value = {ServiceException.class, RuntimeException.class, SQLException.class})
    public ModelAndView defaultErrorHandler(Exception e, Locale locale) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("ex", e);
        mav.addObject("title", "500");
        mav.addObject("code", "500");
        mav.addObject("message", messageSource.getMessage("security.error", null, locale));
        mav.addObject("homepage", messageSource.getMessage("security.homepage", null, locale));
        mav.setViewName("system");
        return mav;
    }
}
