package com.epam.newsportal.metelsky.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Locale;

/**
 * @author dmitr_000
 * @version 1.00 10/9/2015
 */
@Controller
public class SystemController {
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(@RequestParam(required = false) String error, Model model, Locale locale) {
        if (error != null) {
            model.addAttribute("error", messageSource.getMessage("security.badCredentials", null, locale));
        }
        return "login";
    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String forbiddenPage(Model model, Locale locale) {
        model.addAttribute("message", messageSource.getMessage("security.forbidden", null, locale));
        return populateSystemErrorPage(model, locale, "403", "403");
    }

    @RequestMapping(value = {"/404","/400"}, method = RequestMethod.GET)
    public String pageNotFound(Model model, Locale locale) {
        model.addAttribute("message", messageSource.getMessage("security.notFound", null, locale));
        return populateSystemErrorPage(model, locale, "404", "404");
    }

    private String populateSystemErrorPage(Model model, Locale locale, String title, String code) {
        model.addAttribute("title", title);
        model.addAttribute("code", code);
        model.addAttribute("homepage", messageSource.getMessage("security.homepage", null, locale));
        return "system";
    }
}
