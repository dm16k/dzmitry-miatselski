package com.epam.newsportal.metelsky.admin.controller;

import com.epam.newsportal.metelsky.admin.controller.validator.AuthorsValidator;
import com.epam.newsportal.metelsky.admin.controller.validator.JsonResponse;
import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.service.NewsManagementService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The AuthorController class handles request for "/authors" mapping
 *
 * @author dmitr_000
 * @version 1.00 9/23/2015
 */
@Controller
@RequestMapping(value = "/authors")
public class AuthorController {
    @Autowired
    private NewsManagementService service;
    @Autowired
    private AuthorsValidator validator;
    @Autowired
    private MessageSource messageSource;

    @InitBinder(value = "author")
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String viewAuthors(Model model) throws ServiceException {
        if (!model.containsAttribute("author")) {
            model.addAttribute("author", new Author());
        }
        model.addAttribute("authorList", service.getAllAuthors());
        return "authors";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveAuthor(@ModelAttribute("author") @Validated final Author author, final BindingResult result, RedirectAttributes attributes) throws ServiceException {
        if (result.hasErrors()) {
            attributes.addFlashAttribute("org.springframework.validation.BindingResult.author", result);
            attributes.addFlashAttribute("author", author);
        } else {
            service.addAuthor(author);
        }
        return "redirect:/authors";
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public Author updateAuthor(@RequestBody @Validated final Author author) throws ServiceException {
        service.updateAuthor(author);
        return author;
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void expireAuthor(@RequestBody final Author author) throws ServiceException {
        service.expireAuthor(author);
    }

    @ExceptionHandler
    @ResponseBody
    public JsonResponse handleException(MethodArgumentNotValidException exception, Locale locale) {
        JsonResponse response = new JsonResponse();
        Map<String, String> errors = new HashMap<>();
        List<FieldError> validationErrors = exception.getBindingResult().getFieldErrors();
        for (FieldError error : validationErrors) {
            errors.put(error.getObjectName(), messageSource.getMessage(error.getCode(), null, locale));
        }
        response.setErrors(errors);
        response.setStatus(JsonResponse.ResponseStatus.ERROR);
        response.setError("VALIDATION_ERROR");
        return response;
    }
}