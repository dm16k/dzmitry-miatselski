package com.epam.newsportal.metelsky.admin.controller.validator;

import java.util.Map;

/**
 * The JsonResponse class represents response object for REST API
 * It could contain response data and errors used on client side
 *
 * @author dmitr_000
 * @version 1.00 10/5/2015
 */
public class JsonResponse {
    private ResponseStatus status = ResponseStatus.OK;
    private String error;
    private Map<String, String> errors;
    private Map<String, Object> data;

    /**
     * Gets response status
     *
     * @return response status
     */
    public ResponseStatus getStatus() {
        return status;
    }

    /**
     * Sets response status
     *
     * @param status - specifies status
     */
    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    /**
     * Gets response error message
     *
     * @return error message
     */
    public String getError() {
        return error;
    }

    /**
     * Sets response error message for response object
     *
     * @param error - specifies error message
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * Gets map of errors
     *
     * @return list of errors
     */
    public Map<String, String> getErrors() {
        return errors;
    }

    /**
     * Sets map of errors
     *
     * @param errors - list of errors
     */
    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    /**
     * Gets map of data
     *
     * @return list of data
     */
    public Map<String, Object> getData() {
        return data;
    }

    /**
     * Sets map of data
     *
     * @param data - list of data
     */
    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    /**
     * Specifies status of the response object
     */
    public enum ResponseStatus {
        OK, ERROR
    }
}