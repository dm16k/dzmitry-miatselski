package com.epam.newsportal.metelsky.admin.controller.validator;

import com.epam.newsportal.metelsky.domain.Tag;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The TagsValidator class validates {@code Tag} objects
 *
 * @author dmitr_000
 * @version 1.00 9/21/2015
 */
@Component
public class TagsValidator implements Validator {
    private final static String NAME_PATTERN = "^(\\d+\\s)*([A-Z�-�][a-z�-�-]+(\\s\\d+)*(\\s[A-Z�-�][a-z�-�-]+)*(\\s\\d+)*)$";
    private Pattern pattern;
    private Matcher matcher;

    /**
     * Checks support of object to validate
     *
     * @param aClass - object that specifies supported class
     * @return true if it's an Tag class
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return Tag.class.equals(aClass);
    }

    /**
     * Validates {@code Tag} objects
     *
     * @param o - object to validate
     * @param errors - {@code Error} object storing validation errors
     */
    @Override
    public void validate(Object o, Errors errors) {
        Tag tag = (Tag) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.tag.name");
        if (tag != null && tag.getName() != null) {
            if (tag.getName().length() > 30) {
                errors.rejectValue("name", "TooLong.tag.name");
            } else if (tag.getName().length() > 0) {
                pattern = Pattern.compile(NAME_PATTERN);
                matcher = pattern.matcher(tag.getName());
                if (!matcher.matches()) {
                    errors.rejectValue("name", "WrongFormat.tag.name");
                }
            }
        }
    }
}
