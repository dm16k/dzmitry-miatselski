package com.epam.newsportal.metelsky.admin.controller;

import com.epam.newsportal.metelsky.admin.controller.validator.JsonResponse;
import com.epam.newsportal.metelsky.admin.controller.validator.TagsValidator;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.service.NewsManagementService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author dmitr_000
 * @version 1.00 10/6/2015
 */
@Controller
@RequestMapping(value = "/tags")
public class TagsController {
    @Autowired
    NewsManagementService service;
    @Autowired
    TagsValidator validator;
    @Autowired
    private MessageSource messageSource;

    @InitBinder(value = "tag")
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String viewTags(Model model) throws ServiceException {
        if (!model.containsAttribute("tag")) {
            model.addAttribute("tag", new Tag());
        }
        model.addAttribute("tagList", service.getAllTags());
        return "tags";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveTag(@ModelAttribute("tag") @Validated final Tag tag, final BindingResult result, RedirectAttributes attributes) throws ServiceException {
        if (result.hasErrors()) {
            attributes.addFlashAttribute("org.springframework.validation.BindingResult.tag", result);
            attributes.addFlashAttribute("tag", tag);
        } else {
            service.addTag(tag);
        }
        return "redirect:/tags";
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public Tag updateTag(@RequestBody @Validated final Tag tag) throws ServiceException {
        service.changeTag(tag);
        return tag;
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTag(@RequestBody final Tag tag) throws ServiceException {
        service.deleteTag(tag);
    }

    @ExceptionHandler
    @ResponseBody
    public JsonResponse handleException(MethodArgumentNotValidException exception, Locale locale) {
        JsonResponse response = new JsonResponse();
        Map<String, String> errors = new HashMap<>();
        List<FieldError> validationErrors = exception.getBindingResult().getFieldErrors();
        for (FieldError error : validationErrors) {
            errors.put(error.getObjectName(), messageSource.getMessage(error.getCode(), null, locale));
        }
        response.setErrors(errors);
        response.setStatus(JsonResponse.ResponseStatus.ERROR);
        response.setError("VALIDATION_ERROR");
        return response;
    }
}
