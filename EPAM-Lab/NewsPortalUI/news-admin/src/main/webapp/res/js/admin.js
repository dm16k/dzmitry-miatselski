$(document).ready(function () {
    var header = $("meta[name='_csrf_header']").attr('content');
    var token = $("meta[name='_csrf']").attr("content");
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        }
    })
});

function placePopup(after, text) {
    var popup = $('<div class="popup-warning popup">' + text + '</div>').on('click', function () {
        popup.remove();
    });
    popup.insertAfter($(after));
}

var newsList = [];

function NewsObject(id, title) {
    this.id = id;
    this.title = title;
}

function checkNews(news, id) {
    var newsBlock = $(news);
    var defaultWhiteColor = '#fafafa';
    var defaultGreyColor = 'rgba(0,0,0,0.1)';
    var selectedButtonColor = '#ff413e';
    var selectedNewsColor = 'rgba(0,0,255,0.2)';
    var checkbox = $('#news-check' + id);
    var status = checkbox.prop('checked');
    var title = newsBlock.find('.news-title').eq(0).text().substring(0, 10);
    var nObj = new NewsObject(id, title);
    if (!status) {
        checkbox.prop('checked', true);
        newsList.push(nObj);
        newsBlock.css('background-color', selectedNewsColor);
    } else {
        checkbox.prop('checked', false);
        newsList.splice(newsList.indexOf(nObj), 1);
        newsBlock.css('background-color', defaultWhiteColor);
    }
    var selectedBtn = $('#selected-news-button');
    selectedBtn.text('Delete selected (' + newsList.length + ')');
    if (newsList.length > 0) {
        selectedBtn.css('cursor', 'pointer');
        selectedBtn.css('background-color', selectedButtonColor);
    } else {
        selectedBtn.css('cursor', 'default');
        selectedBtn.css('background-color', defaultGreyColor);
    }
    setDeleteList();
}

function setDeleteList() {
    var list = $('#delete-list');
    list.children().remove();
    $.each(newsList, function (k, v) {
        list.append('<li>' + v.title + '...</li>');
    })
}

function deleteNews() {
    if(newsList.length>0) {
        $.ajax({
            url: 'delete',
            type: 'DELETE',
            dataType: 'json',
            data: JSON.stringify(newsList),
            contentType: 'application/json',
            mimeType: 'application/json',
            success: setTimeout(function () {
                location.reload()
            }, 1.5),
            error: function (data, status, er) {
                alert("error: " + data + " status: " + status + " er: " + er);
            }
        })
    }
}

function viewDelList(status) {
    var list = $('#delete-list');
    var delBlock = list.parent();
    if (status && list.children().size() !== 0) {
        delBlock.css('display', 'block');
    } else {
        delBlock.css('display', 'none');
    }
}

function deleteComment(id, btn) {
    var comment = [{
        id: id
    }];
    $.ajax({
        url: 'delete',
        type: 'DELETE',
        dataType: 'json',
        data: JSON.stringify(comment),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function () {
            $(btn).parents('.comment').remove();
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function setCreationDate() {
    $('#news\\.creationDate').val($('#spring-input-date').val() + ' 00:00:00.0');
}

function setFormTags() {
    var counter = 0;
    var boxes = $('input[type=checkbox]');
    $.each(boxes, function (k, v) {
        if ($(v).prop('checked') === true) {
            $(v).attr('name', 'tagList[' + counter++ + '].id');
        } else {
            $(v).removeAttr('name');
        }
    })
}

//authors

function showManage(id, status) {
    var manage = $('#manage' + id);
    var display = manage.css('display');
    if (status) {
        display = 'inline-block';
    } else {
        display = 'none';
    }
    manage.css('display', display);
}

function closeEdit(id) {
    var editRow = $('#' + id);
    var parentChildren = editRow.parent().children();
    var row;
    for (var q = 0; q < parentChildren.length; q++) {
        row = parentChildren.eq(q);
        if (row.attr('id') === editRow.attr('id'))
            row.next().css('display', 'table-row');
    }
    editRow.remove();
    $('.optional').css('visibility', 'visible');
}

var expired = 'Expired';

function expireAuthor(id, btn) {
    var today = new Date();
    var author = {
        id: id,
        expired: today
    };
    $.ajax({
        type: 'DELETE',
        dataType: 'json',
        data: JSON.stringify(author),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function () {
            var cell = $(btn).parents('.cell');
            cell.children().remove();
            cell.text(expired + ' ' + today.yyyymmdd());
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

var updateTitle = 'update';
var cancelTitle = 'cancel';

function editAuthor(id, innerBtn) {
    var edit = $(innerBtn).parents('.row');
    var currInd = edit.find('.author-number').text().trim();
    var currName = edit.find('.author-text').text().trim();
    var funcUpd = 'updateAuthor(' + id + ')';
    var funcClose = 'closeEdit("edit-author")';
    var editRow = $('#add-author').clone();

    $(editRow).children().eq(0).text(currInd);
    var authorName = $(editRow).children().eq(1);
    var authorStatus = $(editRow).children().eq(2);
    authorName.text('');
    authorStatus.text('').css('text-align', 'center');

    $('<input type="text">').val(currName).attr('size', '30').css('width', '100%').css('font-family', 'inherit').css('font-size', 'inherit').css('margin-top', '2px').css('text-align', 'right').appendTo(authorName);
    $('<button class="man-btn" title="' + updateTitle + '">&#10003;</button>').attr('onclick', funcUpd).appendTo(authorStatus);
    $('<button class="man-btn" title="' + cancelTitle + '">&#10005;</button>').attr('onclick', funcClose).appendTo(authorStatus);

    $(editRow).insertBefore(edit).attr('id', 'edit-author');
    $(edit).css('display', 'none');
    $('.optional').css('visibility', 'hidden');
}

function updateAuthor(id) {
    var name = $('#edit-author').find('input[type="text"]').val();
    var author = {
        id: id,
        name: name
    };
    $.ajax({
        type: 'PUT',
        dataType: 'json',
        data: JSON.stringify(author),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data, xhr) {
            var editRow = $('#edit-author');
            if (data.status === 'ERROR') {
                var text = data.errors.author;
                placePopup(editRow, text);
            } else {
                var parentChildren = editRow.parent().children();
                var row;
                for (var q = 0; q < parentChildren.length; q++) {
                    row = parentChildren.eq(q);
                    if (row.find('.author-number').text() === editRow.find('.author-number').text()) {
                        row.next().find('.author-text').text(name);
                        break;
                    }
                }
                closeEdit('edit-author');
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

// tags.jsp

function deleteTag(id, btn) {
    var tag = {
        id: id
    };
    $.ajax({
        type: 'DELETE',
        dataType: 'json',
        data: JSON.stringify(tag),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function () {
            $(btn).parents('.row').remove();
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function editTag(id, innerBtn) {
    var edit = $(innerBtn).parents('.row');
    var currInd = edit.find('.author-number').text().trim();
    var currName = edit.find('.author-text').text().trim();
    var funcUpd = 'updateTag(' + id + ')';
    var funcClose = 'closeEdit("edit-tag")';
    var editRow = $('#add-tag').clone();

    $(editRow).children().eq(0).text(currInd);
    var tagName = $(editRow).children().eq(1);
    var tagStatus = $(editRow).children().eq(2);
    tagName.text('');
    tagStatus.text('').css('text-align', 'center');

    $('<input type="text">').val(currName).attr('size', '30').css('width', '100%').css('font-family', 'inherit').css('font-size', 'inherit').css('margin-top', '2px').css('text-align', 'right').appendTo(tagName);
    $('<button class="man-btn" title="' + updateTitle + '">&#10003;</button>').attr('onclick', funcUpd).appendTo(tagStatus);
    $('<button class="man-btn" title="' + cancelTitle + '">&#10005;</button>').attr('onclick', funcClose).appendTo(tagStatus);

    $(editRow).insertBefore(edit).attr('id', 'edit-tag');
    $(edit).css('display', 'none');
    $('.optional').css('visibility', 'hidden');
}

function updateTag(id) {
    var name = $('#edit-tag').find('input[type="text"]').val();
    var tag = {
        id: id,
        name: name
    };
    $.ajax({
        type: 'PUT',
        dataType: 'json',
        data: JSON.stringify(tag),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data, xhr) {
            var editRow = $('#edit-tag');
            if (data.status == 'ERROR') {
                var text = data.errors.tag;
                placePopup(editRow, text);
            } else {
                var parentChildren = editRow.parent().children();
                var row;
                for (var q = 0; q < parentChildren.length; q++) {
                    row = parentChildren.eq(q);
                    if (row.find('.author-number').text() === editRow.find('.author-number').text()) {
                        row.next().find('.author-text').text(name);
                        break;
                    }
                }
                closeEdit('edit-tag');
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}
