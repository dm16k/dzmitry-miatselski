var separator = '-';
Date.prototype.yyyymmdd = function () {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString();
    var dd = this.getDate().toString();
    return yyyy + separator + (mm[1] ? mm : "0" + mm[0]) + separator + (dd[1] ? dd : "0" + dd[0]);// padding
};

Date.prototype.yyyyMMdd_24hhmmss = function () {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString();
    var dd = this.getDate().toString();
    var hh = this.getHours().toString();
    var MM = this.getMinutes().toString();
    var ss = this.getSeconds().toString();
    return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]) + ' ' + hh + ':' + MM + ':' + ss + '.0';// padding
};

function setPopupActions() {
    $('.popup').on('click', function () {
        $(this).remove();
    });
}

var display = 'block';

function showTagList() {
    var tags = $('#checkboxes');
    var display = tags.css('display');
    if (display === 'none')
        display = 'block';
    else
        display = 'none';
    tags.css('display', display);
}

$(document).ready(function () {
    var tagBoxes = $('#checkboxes').find('label');
    $.each(tagBoxes, function (key, value) {
        $(this).attr('onclick', 'setText()');
    });
});

var defaultText = 'Select tags...';

function setText() {
    var tagList = [];
    var checkBoxes = $('#checkboxes');
    var tags = checkBoxes.find('label');
    $.each(tags, function (key, value) {
        var input = $(this).find('input[type=checkbox]');
        var el = $(this);
        if (input.prop('checked') === true) {
            tagList.push(el.text().trim());
        }
    });
    var tagsSelector = checkBoxes.parent().find('option');
    var defText = defaultText;
    if (tagList.length > 0) {
        if (tagList.length < 4) {
            defText = tagList.join();
        } else {
            var sel = ' selected';
            if (defText.charAt(0) == 'В')
                sel = ' выбрано';
            defText = tagList.length + sel;
        }
    }
    tagsSelector.text(defText);
}

function submitForm(formId) {
    $('#' + formId).submit();
}

function submitFormWithValue(id, value) {
    var form = $('#' + id);
    form.find('input').eq(1).val(value);
    form.submit();
}

var selectedTags = [];

function setTags() {
    var tagList = $('#checkboxes').find('input[type=checkbox]');
    $.each(tagList, function (k, v) {
        var el = $(this);
        $.each(selectedTags, function (key2, value2) {
            if (parseInt(el.val()) === value2) {
                el.prop('checked', true);
                //el.prop('name','tagList['+ key2 +'].id');
            }
        });
    })
}

function postComment(newsId) {
    var commentText = $('#comment-text').val().replace('<', '&lt').replace('>', '&gt');
    var creationDate = new Date();
    var request = {};
    var comment = {
        newsId: newsId,
        text: commentText,
        creationDate: creationDate
    };
    request.command = 'add_comment';
    request.data = JSON.stringify(comment);
    $.ajax({
        url: 'client',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            location.reload();
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function nextNews(form, newsId) {
    var news = {
        id: newsId
    };
    var request = {};
    request.command = 'check_next';
    request.data = JSON.stringify(news);
    $.ajax({
        url: 'client',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            if (data === true) {
                submitFormWithValue(form, newsId)
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function previousNews(form, newsId) {
    var news = {
        id: newsId
    };
    var request = {};
    request.command = 'check_previous';
    request.data = JSON.stringify(news);
    $.ajax({
        url: 'client',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            if (data === true) {
                submitFormWithValue(form, newsId)
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function changeLocale() {
    var locale;
    var loc = $('#locale');
    if (loc.attr('class') == 'localeDiv rus')
        locale = 'ru';
    else
        locale = 'en';
    location.replace('?lang=' + locale);
}

var currLoc;

function viewPrevLocale() {
    var loc = $('#locale');
    currLoc = loc.attr('class');
    var locClass = loc.attr('class');
    if (locClass === 'localeDiv rus')
        locClass = 'localeDiv usa';
    else
        locClass = 'localeDiv rus';
    loc.attr('class', locClass);
}

function setCurrLocale() {
    $('#locale').attr('class', currLoc);
}