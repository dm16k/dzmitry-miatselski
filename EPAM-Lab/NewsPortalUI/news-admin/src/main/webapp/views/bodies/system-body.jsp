<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="bg">
    <div class="error-code"><c:out value="${code}"/></div>
    <div class="message-wrapper">
        <div><c:out value="${message}"/></div>
        <div><a href="${pageContext.request.contextPath}"><c:out value="${homepage}"/></a></div>
    </div>
</div>
<!--
<c:if test="${!empty ex}"><c:out value="${ex.localizedMessage}"/>
    <c:out value="Cause:"/>
    <c:out value="${ex.cause.toString()}"/>
-->
<!--
    <c:out value="Stack Trace"/>
    <c:forEach var="element" items="${ex.stackTrace}">
        <c:out value="${element}"/></c:forEach>
</c:if>
-->
