<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<div class="footer">
  <div class="footer-text"><s:message code="footer.text"/></div>
</div>