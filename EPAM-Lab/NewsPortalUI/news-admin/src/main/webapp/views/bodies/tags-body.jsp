<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<div class="authors-block table">
    <div id="add-tag" class="row" style="min-height: 32px">
        <div class="cell author-number">+</div>
        <div class="cell author-text">
            <sf:form id="add-tag-form" method="post" modelAttribute="tag" action="tags/save">
                <sf:input form="add-tag-form" path="name" size="30" cssStyle="margin-top: 3px; width: 100%"/>
                <sf:errors path="name" element="div" cssClass="popup-warning popup"/>
            </sf:form>
        </div>
        <div class="cell author-status">
            <input form="add-tag-form" value="<s:message code="btn.add"/>" type="submit" style="width: 100%">
        </div>
    </div>
    <c:forEach var="tag" items="${tagList}" varStatus="loop">
        <c:set var="ind" value="${loop.index+1}" scope="page"/>
        <div class="row" onmouseover="showManage(${ind},true)" onmouseout="showManage(${ind},false)">
            <div class="cell author-number">${ind}</div>
            <div class="cell author-text">${tag.name}</div>
            <div class="cell author-status" style="text-align: center">
                <div id="manage${ind}" class="optional">
                    <button class="man-btn" title="<s:message code="btn.delete"/>" onclick="deleteTag(${tag.id}, this)">
                        -
                    </button>
                    <button class="man-btn" title="<s:message code="btn.edit"/>" onclick="editTag(${tag.id}, this)">
                        &#9998;</button>
                </div>
            </div>
        </div>
    </c:forEach>
</div>
