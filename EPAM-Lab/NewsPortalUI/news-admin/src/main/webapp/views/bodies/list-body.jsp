<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="right-fixed" onmouseout="viewDelList(false)">
    <div id="selected-news-button" class="delete-news-block" onclick="deleteNews()" onmouseover="viewDelList(true)">
        <s:message code="list.delete.text"/> (0)
    </div>
    <div class="delete-news-list">
        <ol id="delete-list" style="margin: 0"></ol>
    </div>
</div>
<div class="news-filters">
    <form id="filter-news-form" action="<c:url value="/search"/>" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/></form>
    </form>
    <div class="table">
        <div class="cell">
            <select name="authorId" form="filter-news-form" class="author-selector" style="top:-1px">
                <option value="any"><s:message code="list.filter.author"/></option>
                <c:forEach var="author" items="${authorList}">
                    <c:if test="${empty author.expired}">
                        <option value="${author.id}" ${author.id == criteria.authorId ? 'selected="selected"' : ''}><c:out value="${author.name}"/></option>
                    </c:if>
                </c:forEach>
            </select>
        </div>
        <div class="cell" style="width: 100px">
            <div class="multiselect" style="height: 24px">
                <div class="select-box" style="height: 24px" onclick="showTagList()">
                    <select>
                        <option><s:message code="list.filter.tags"/></option>
                    </select>

                    <div id="checkboxes" class="tag-list">
                        <c:forEach var="tag" items="${tagList}">
                            <label><input type="checkbox" form="filter-news-form" name="tagId"
                                          value="${tag.id}"><c:out value="${tag.name}"/></label>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
        <div class="cell" style="width: 120px">
            <div class="row">
                <input form="filter-news-form" type="submit" style="position: relative;top: -1px;z-index: 2;"
                       value="<s:message code="list.filter.search"/>">
                <input form="filter-news-form" type="reset" style="position: relative;top: -1px;z-index: 2;"
                       value="<s:message code="list.filter.reset"/>">
            </div>
        </div>
    </div>
</div>
<div class="pagination-block table">
    <div class="row">
        <c:set var="page" scope="session" value="${currentPage}"/>
        <c:if test="${currentPage gt 2}">
            <div class="cell">
                <a class="pagination-link" href="1">1</a>
            </div>
        </c:if>
        <c:if test="${currentPage gt 1}">
            <div class="cell">
                <a class="pagination-link" href="${currentPage - 1}">&lt</a>
            </div>
        </c:if>
        <a class="pagination-link">${currentPage}</a>
        <c:if test="${currentPage lt pageQuantity}">
            <div class="cell">
                <a class="pagination-link" href="${currentPage + 1}">&gt</a>
            </div>
        </c:if>
        <c:if test="${currentPage lt pageQuantity - 1}">
            <div class="cell">
                <a class="pagination-link" href="${pageQuantity}">${pageQuantity}</a>
            </div>
        </c:if>
    </div>
</div>
<div class="news-block">
    <c:forEach var="news" items="${newsList}" varStatus="loop">
        <div class="short-news" onclick="checkNews(this, ${news.news.id})">
            <input id="news-check${news.news.id}" type="checkbox" style="display: none;">

            <div class="news-header table">
                <div class="news-title cell" style="padding: 0 20px;text-align: left"><c:out value="${news.news.title}"/></div>
                <div class="news-author cell" style="padding: 0 20px;text-align: left"><s:message
                        code="list.news.author.prefix"/> <c:out value="${news.author.name}"/></div>
                <div class="news-date cell"><fmt:formatDate pattern="d.MM.yyyy" value="${news.news.modificationDate}"/></div>
            </div>
            <div class="news-message"><c:out value="${news.news.shortText}"/></div>
            <div class="news-footer-wrapper table">
                <div class="news-footer row">
                    <div class="news-tags cell"><c:forEach var="tag" items="${news.tagList}"><c:out value="${tag.name} "/></c:forEach></div>
                    <c:set var="comments" scope="page" value="${fn:length(news.commentList)}"/>
                    <div class="news-comments cell">${comments} <c:if test="${comments == 1}"><s:message code="list.news.comment.one"/></c:if><c:if test="${comments != 1}"><s:message code="list.news.comment.many"/></c:if></div>
                    <div class="cell" style="width: 96px;">
                        <a class="view-link" href="<c:url value='/news/${news.news.id}'/>"
                           onclick="event.stopPropagation();"><s:message code="list.news.link"/></a>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
</div>
