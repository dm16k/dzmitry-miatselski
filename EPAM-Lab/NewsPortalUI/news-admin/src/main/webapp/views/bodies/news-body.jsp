<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<div class="news-block">
    <div class="short-news">
        <sf:form id="update-news-form" method="post" modelAttribute="newsMessage" action="save">
            <sf:hidden path="news.id"/>
            <div class="news-header table">
                <a class="link-button back-link" title="<s:message code="news.backlink"/>" href="<c:url value='/list/${page}'/>">Back</a>
                <div class="news-title cell">
                    <sf:input class="title-edit" type="text" maxlength="40" value="${newsMessage.news.title}"
                              path="news.title"/>
                    <sf:errors path="news.title" element="div" cssClass="popup popup-warning"/>
                </div>
                <div class="news-author cell">
                    <select class="author-selector" name="author.id">
                        <c:forEach var="author" items="${authorList}">
                            <c:if test="${empty author.expired}">
                                <option value="${author.id}" ${author.id == newsMessage.author.id ? 'selected="selected"' : ''}><c:out value="${author.name}"/></option>
                            </c:if>
                        </c:forEach>
                    </select>
                </div>
                <div class="news-date cell">
                    <div class="multiselect">
                        <div class="select-box" onclick="showTagList()">
                            <select>
                                <option><s:message code="list.filter.tags"/></option>
                            </select>

                            <div id="checkboxes" class="tag-list">
                                <c:forEach var="tag" items="${tagList}" varStatus="loop">
                                    <label>
                                        <input type="checkbox" name="tagList[${loop.index}].id" value="${tag.id}">
                                            <c:out value="${tag.name}"/></label>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="news-message">
                <div class="news-message-wrapper table" style="height:500px">
                    <div class="row" style="height:100px">
                        <div class="news-info cell"><s:message code="news.shortText"/></div>
                        <div class="cell">
                            <sf:textarea class="short-news-textarea" maxlength="200" rows="4" path="news.shortText"/>
                            <sf:errors path="news.shortText" element="div" cssClass="popup popup-warning"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="news-info cell"><s:message code="news.fullText"/></div>
                        <div class="cell">
                            <sf:textarea class="full-news-textarea" maxlength="2000" rows="20" path="news.fullText"/>
                            <sf:errors path="news.fullText" element="div" cssClass="popup popup-warning"/>
                        </div>
                    </div>
                    <div class="row" style="height: 30px;">
                        <div class="news-info cell"><s:message code="news.creationDate"/></div>
                        <div class="cell">
                            <fmt:formatDate value="${newsMessage.news.creationDate}" pattern="yyyy-MM-dd"
                                            var="creationDate"/>
                            <input id="spring-input-date" type="date" value="${creationDate}" required>
                            <sf:errors path="news.creationDate" element="div" cssClass="popup popup-warning"/>
                            <sf:hidden path="news.creationDate"/>
                        </div>
                    </div>
                </div>
                <div style="box-shadow:none;text-align:center">
                    <sf:hidden path="news.modificationDate"/>
                    <input type="submit" style="width: 90px" value="<s:message code="news.form.save"/>">
                    <input type="reset" style="width: 90px" value="<s:message code="news.form.cancel"/>" onclick="location.reload()">
                </div>
            </div>
        </sf:form>
        <div class="comments-block">
            <c:forEach items="${newsMessage.commentList}" var="comment">
                <div class="comment">
                    <div class="table">
                        <div class="row">
                            <div class="cell">
                                <div class="comment-date"><fmt:formatDate pattern="d.MM.yyyy H.mm"
                                                                          value="${comment.creationDate}"/></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell">
                                <div class="comment-text"><c:out value="${comment.text}"/></div>
                            </div>
                            <div class="del-block cell">
                                <input class="man-btn" type="button" title="<s:message code="news.comment.delete"/>"
                                       value="-"
                                       onclick="deleteComment(${comment.id}, this)">
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
