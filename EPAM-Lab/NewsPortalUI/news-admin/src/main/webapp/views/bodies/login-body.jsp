<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<s:url value="/login" var="authUrl"/>
<div class="news-block">
    <form id="login-form" action="${authUrl}" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

        <div class="profile__form">
            <div class="profile__fields">
                <div class="field">
                    <input type="text" id="fieldUser" name="username" class="input" required pattern=^[\wА-яа-я]+$>
                    <label for="fieldUser" class="label"><s:message code="security.username"/></label>
                </div>
                <div class="field">
                    <input type="password" id="fieldPassword" name="password" class="input" required
                           pattern=^[\wА-яа-я]+$>
                    <label for="fieldPassword" class="label"><s:message code="security.password"/></label>
                    <c:if test="${!empty error}">
                        <div class="popup-warning popup" style="width: 338px; margin-top: 4px;">${error}</div>
                    </c:if>
                </div>
                <div class="profile__footer">
                    <input class="btn" type="submit" name="commit" style="height: 2.5rem; width: auto"
                           value="<s:message code="security.login"/>">
                </div>
            </div>
        </div>
    </form>
</div>
