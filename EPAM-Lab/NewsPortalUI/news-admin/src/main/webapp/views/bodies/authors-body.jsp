<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="authors-block table">
    <div id="add-author" class="row" style="height: 32px">
        <div class="cell author-number">+</div>
        <div class="cell author-text">
            <sf:form id="add-author-form" method="post" modelAttribute="author" action="authors/save">
                <sf:input form="add-author-form" path="name" size="30" cssStyle="margin-top: 3px; width: 100%"/>
                <sf:errors path="name" element="div" cssClass="popup-warning popup"/>
            </sf:form>
        </div>
        <div class="cell author-status">
            <input form="add-author-form" value="<s:message code="btn.add"/>" type="submit" style="width: 100%">
        </div>
    </div>
    <c:forEach var="author" items="${authorList}" varStatus="loop">
        <c:set var="ind" value="${loop.index+1}" scope="page"/>
        <div class="row" <c:if test="${empty author.expired}">onmouseover="showManage(${ind},true)"
             onmouseout="showManage(${ind},false)"</c:if>>
            <div class="cell author-number">${ind}</div>
            <div class="cell author-text"><c:out value="${author.name}"/></div>
            <div class="cell author-status">
                <c:if test="${empty author.expired}">
                    <s:message code="authors.active"/>
                    <div id="manage${ind}" class="optional">
                        <button class="man-btn" title="<s:message code="btn.expire"/>"
                                onclick="expireAuthor(${author.id}, this)">-
                        </button>
                        <button class="man-btn" title="<s:message code="btn.edit"/>"
                                onclick="editAuthor(${author.id}, this)">&#9998;</button>
                    </div>
                </c:if>
                <c:if test="${!empty author.expired}">
                    <s:message code="authors.expired"/> <fmt:formatDate value="${author.expired}" pattern="yyyy.MM.dd"/>
                </c:if>
            </div>
        </div>
    </c:forEach>
</div>
