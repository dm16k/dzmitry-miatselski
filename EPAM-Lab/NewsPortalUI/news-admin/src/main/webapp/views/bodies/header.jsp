<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="header table">
    <div class="row">
        <div class="header-cell cell">News Portal<a href="${pageContext.request.contextPath}"><span></span></a></div>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <div class="header-cell cell"><s:message code="header.link.addNews"/><a
                    href="${pageContext.request.contextPath}/news/new"><span></span></a></div>
            <div class="header-cell cell"><s:message code="header.link.authors"/><a
                    href="${pageContext.request.contextPath}/authors"><span></span></a></div>
            <div class="header-cell cell"><s:message code="header.link.tags"/><a
                    href="${pageContext.request.contextPath}/tags"><span></span></a></div>
        </sec:authorize>
        <div class="cell" style="vertical-align: middle">
            <c:set var="locale" value="${pageContext.response.locale}"/>
            <button id="locale" class="localeDiv ${fn:substring(locale,0,2) == 'ru'? 'rus' : 'usa'}"
                    title="<s:message code="header.locale.title"/>"
                    onclick="changeLocale()"
                    onmouseover="viewPrevLocale()"
                    onmouseout="setCurrLocale()"
                    >Change Language
            </button>
        </div>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <div class="cell">
                <c:url var="logoutUrl" value="/logout"/>
                <form id="logout" action="${logoutUrl}" method="post">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </form>
                <button onclick="submitForm('logout')" class="personal" title="<s:message code="security.logout"/>">Personal</button>
            </div>
        </sec:authorize>
    </div>
    <%--<button class="personal">Account</button>--%>
</div>
