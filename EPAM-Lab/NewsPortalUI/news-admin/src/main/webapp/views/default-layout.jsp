<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><tiles:importAttribute name="title"/><spring:message code="${title}"/></title>
    <tiles:insertAttribute name="links"/>
    <tiles:insertAttribute name="basic-scripts"/>
    <tiles:insertAttribute name="admin-scripts"/>
    <tiles:insertAttribute name="custom-scripts"/>
</head>
<body>
<tiles:insertAttribute name="header"/>
<tiles:insertAttribute name="body"/>
<tiles:insertAttribute name="footer"/>
</body>
</html>
