<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:csrfMetaTags/>
<script>
    $(document).ready(function setDateSeparator(s) {
        setPopupActions();
        updateTitle = '<s:message code="btn.update"/>';
        cancelTitle = '<s:message code="btn.cancel"/>';
    });
</script>
