<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<sec:csrfMetaTags/>
<script>
    $(document).ready(function setDateSeparator() {
        setPopupActions();
        <c:set var="locale" value="${fn:substring(pageContext.response.locale, 0, 2)}"/>
        var loc = '${locale}';
        if (loc === 'en') {
            separator = '/';
            expired = 'Expired';
        } else {
            separator = '.';
            expired = '\u0423\u0432\u043e\u043b\u0435\u043d';
        }
        updateTitle = '<s:message code="btn.update"/>';
        cancelTitle = '<s:message code="btn.cancel"/>';
    });
</script>
