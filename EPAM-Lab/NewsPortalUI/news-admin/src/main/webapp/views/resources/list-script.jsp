<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<sec:csrfMetaTags/>
<script>
  $(document).ready(function () {
    <c:if test="${! empty criteria}"><c:forEach var="tag" items="${criteria.tagList}">selectedTags.push(${tag});</c:forEach></c:if>
    <c:set var="locale" value="${fn:substring(pageContext.response.locale, 0, 2)}"/>
    var loc = '${locale}';
    if (loc === 'en') {
      separator = '/';
      expired = 'Expired';
      defaultText = 'Select tags...';
    } else {
      separator = '.';
      expired = '\u0423\u0432\u043e\u043b\u0435\u043d';
      defaultText = '\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u0442\u044d\u0433\u0438...';
    }
    setTags();
    setText();
  })
</script>
