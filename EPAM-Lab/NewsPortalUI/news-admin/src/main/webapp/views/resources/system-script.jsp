<script src="${pageContext.request.contextPath}/res/js/jquery.interactive_bg.js"></script>
<script>
    $(document).ready(function () {
        $(".bg").interactive_bg();
        $(window).resize(function () {
            $(".bg > .ibg-bg").css({
                width: $(window).outerWidth(),
                height: $(window).outerHeight()
            })
        })
    });
</script>
