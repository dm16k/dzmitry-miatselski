<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:csrfMetaTags/>
<script>
    $(document).ready(function () {
        <c:forEach var="tag" items="${newsMessage.tagList}">selectedTags.push(${tag.id});
        </c:forEach>
        setTags();
        setText();
        <c:set var="locale" value="${fn:substring(pageContext.response.locale, 0, 2)}"/>
        var loc = '${locale}';
        if (loc === 'en') {
            separator = '/';
            defaultText = 'Select tags...';
        } else {
            separator = '.';
            defaultText = '\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u0442\u044d\u0433\u0438...';
        }
        setPopupActions();
        $('#news\\.modificationDate').val(new Date().yyyyMMdd_24hhmmss());
        $('#spring-input-date').attr('onchange', 'setCreationDate()');
        $('input[type=checkbox]').attr('onchange', 'setFormTags()');
    })
</script>
