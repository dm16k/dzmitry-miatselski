package com.epam.newsportal.metelsky.client.controller.command;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The CommandHelper class has method that gets commands by their name
 *
 * @author dmitr_000
 * @version 1.00 9/8/2015
 */
public class CommandHelper {
    private Map<CommandName, Command> commands = new HashMap<>();

    /**
     * Default constructor.
     * Sets a {@code List<Command>} to store commands
     */
    public CommandHelper(List<Command> commandList) {
        for (Command c : commandList) {
            commands.put(c.getName(), c);
        }
    }

    /**
     * Gets command by its name
     *
     * @param commandName - a string name of command
     * @return command
     */
    public Command getCommand(String commandName) {
        CommandName name = CommandName.valueOf(commandName.toUpperCase());
        return commands.get(name);
    }
}
