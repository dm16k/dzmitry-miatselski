package com.epam.newsportal.metelsky.client.controller.command.impl;

import com.epam.newsportal.metelsky.client.controller.PageHelper;
import com.epam.newsportal.metelsky.client.controller.command.Command;
import com.epam.newsportal.metelsky.client.controller.command.CommandName;
import com.epam.newsportal.metelsky.client.controller.command.exception.CommandException;
import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.domain.NewsMessage;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.service.NewsManagementService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.epam.newsportal.metelsky.util.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;

/**
 * The FilterListCommand class loads news data for specified page
 *
 * @author dmitr_000
 * @version 1.00 9/7/2015
 */
public class FilterListCommand implements Command {
    private final static String TAG_LIST = "tagList";
    private final static String NEWS_LIST = "newsList";
    private final static String AUTHOR_LIST = "authorList";
    private final static String NO_OF_PAGE = "noOfPage";
    private final static String PAGE_QUANTITY = "pageQuantity";
    private final static String CURRENT_PAGE = "currentPage";
    private final int itemsPerPage;
    private CommandName name = CommandName.FILTER_LIST;
    @Autowired
    @Qualifier(value = "newsManagementService")
    private NewsManagementService newsManagementService;

    /**
     * Constructor used to set quantity of news per page
     *
     * @param source - message source that contains quantity of news per page
     */
    @Autowired
    public FilterListCommand(MessageSource source) {
        itemsPerPage = Integer.parseInt(source.getMessage("list.items", null, new Locale("en")));
    }

    /**
     * Loads news data for specified page
     * @see Command#execute(HttpServletRequest, HttpServletResponse) execute
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            HttpSession session = request.getSession();
            SearchCriteria criteria = (SearchCriteria) session.getAttribute("criteria");
            if (criteria == null) {
                criteria = new SearchCriteria();
                session.setAttribute("criteria", criteria);
            }
            int noOfPage = 1;
            if (request.getParameter(NO_OF_PAGE) != null)
                noOfPage = Integer.parseInt(request.getParameter(NO_OF_PAGE));
            request.setAttribute(CURRENT_PAGE, noOfPage);
            List<NewsMessage> newsList = newsManagementService.search(criteria, noOfPage - 1, itemsPerPage);
            request.setAttribute(NEWS_LIST, newsList);
            Long quantity = newsManagementService.countNews(criteria);
            request.setAttribute(PAGE_QUANTITY, (int) Math.ceil(quantity * 1.0 / itemsPerPage));
            List<Author> authorList = newsManagementService.getAllAuthors();
            request.setAttribute(AUTHOR_LIST, authorList);
            List<Tag> tagList = newsManagementService.getAllTags();
            request.setAttribute(TAG_LIST, tagList);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PageHelper.NEWS_LIST_PAGE;
    }

    /**
     * @see Command#getName() getName
     */
    public CommandName getName() {
        return name;
    }
}
