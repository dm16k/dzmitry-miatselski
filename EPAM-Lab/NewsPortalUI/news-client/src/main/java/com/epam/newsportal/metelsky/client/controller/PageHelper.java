package com.epam.newsportal.metelsky.client.controller;

/**
 * The PageHelper class has method that gets pages locations from resource file
 * The PageHelper class has fields with info about pages locations
 *
 * @author dmitr_000
 * @version 1.00 17-May-15
 */
public class PageHelper {
    public final static String NEWS_PAGE = "/jsp/news.jsp";
    public final static String NEWS_LIST_PAGE = "/jsp/list.jsp";
    public final static String ERROR_PAGE = "/jsp/system.jsp";
}
