package com.epam.newsportal.metelsky.client.controller.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * The SessionCreatingListener class implements methods of {@code HttpSessionListener} interface
 * When new {@code HttpSession} object created {@code sessionCreated} method invokes.
 * When {@code HttpSession} object destroyed {@code sessionDestroyed) method invokes.
 *
 * @author dmitr_000
 * @version 1.00 9/13/2015
 */
@WebListener
public class SessionCreationListener implements HttpSessionListener {
    private static final String LOCALE = "locale";

    /**
     * Sets new {@code User} object and 'en_US' locale to current session.
     *
     * @param httpSessionEvent - the HttpSessionEvent containing the session
     */
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        HttpSession session = httpSessionEvent.getSession();
        synchronized (this) {
            session.setAttribute(LOCALE, "en_US");
        }
    }

    /**
     * A stub for superclass method
     *
     * @param httpSessionEvent - the HttpSessionEvent containing the session
     */
    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
    }
}
