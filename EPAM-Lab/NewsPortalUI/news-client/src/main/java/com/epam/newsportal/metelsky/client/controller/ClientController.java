package com.epam.newsportal.metelsky.client.controller;

import com.epam.newsportal.metelsky.client.controller.command.Command;
import com.epam.newsportal.metelsky.client.controller.command.CommandHelper;
import com.epam.newsportal.metelsky.client.controller.command.exception.CommandException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The ClientController class is a main servlet class that handles all command requests
 *
 * @author dmitr_000
 * @version 1.00 9/7/2015
 */
@WebServlet(urlPatterns = "/client")
public class ClientController extends HttpServlet {
    private static final long serialVersionUID = 3135349760638210214L;
    private CommandHelper commandHelper = (CommandHelper) ApplicationContextProvider.getApplicationContext().getBean("helper");

    /**
     * Handles GET requests to this servlet
     *
     * @param req  - an {@link HttpServletRequest} object that contains the request the client has made of the servlet
     * @param resp - an {@link HttpServletResponse} object that contains the response the servlet sends to the client
     * @throws ServletException if the request for the GET could not be handled
     * @throws IOException      if an input or output error is detected when the servlet handles the GET request
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doAction(req, resp);
    }

    /**
     * Handles POST requests to this servlet
     *
     * @param req  - an {@link HttpServletRequest} object that contains the request the client has made of the servlet
     * @param resp - an {@link HttpServletResponse} object that contains the response the servlet sends to the client
     * @throws ServletException if the request for the GET could not be handled
     * @throws IOException      if an input or output error is detected when the servlet handles the GET request
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doAction(req, resp);
    }

    /**
     * Handles all requests passed to this controller
     * Executes commands from request, handles errors
     *
     * @param request  - an {@link HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@link HttpServletResponse} object that contains the response the servlet sends to the client
     * @throws ServletException if the request for the GET could not be handled
     * @throws IOException      if an input or output error is detected when the servlet handles the GET request
     */
    private void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isAjax = isAjax(request);
        String commandName;
        if (isAjax) {
            commandName = (String) request.getAttribute("command");
        } else {
            commandName = request.getParameter("command");
        }
        String page;
        try {
            Command command = commandHelper.getCommand(commandName);
            page = command.execute(request, response);
            if (isAjax) {
                return;
            }
        } catch (CommandException | RuntimeException e) {
            request.getSession().setAttribute("ex", e);
            response.sendRedirect("error");
            return;
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

    /**
     * Checks the type of request header
     *
     * @param request - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @return true if there is an AJAX request
     */
    private boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }
}
