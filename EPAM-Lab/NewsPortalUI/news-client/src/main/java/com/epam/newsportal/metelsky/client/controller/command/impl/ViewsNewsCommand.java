package com.epam.newsportal.metelsky.client.controller.command.impl;

import com.epam.newsportal.metelsky.client.controller.PageHelper;
import com.epam.newsportal.metelsky.client.controller.command.Command;
import com.epam.newsportal.metelsky.client.controller.command.CommandName;
import com.epam.newsportal.metelsky.domain.NewsMessage;
import com.epam.newsportal.metelsky.service.NewsManagementService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.epam.newsportal.metelsky.client.controller.command.exception.CommandException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The ViewsNewsCommand class loads single news depending on ID
 *
 * @author dmitr_000
 * @version 1.00 9/7/2015
 */
public class ViewsNewsCommand implements Command {
    private final static String NEWS_ID = "newsId";
    private final static String NEWS_MESSAGE = "newsMessage";
    private CommandName name = CommandName.VIEW_NEWS;
    @Autowired
    @Qualifier(value = "newsManagementService")
    private NewsManagementService newsManagementService;

    /**
     * Loads single news depending on ID
     * @see Command#execute(HttpServletRequest, HttpServletResponse) execute
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            Long id = Long.parseLong(request.getParameter(NEWS_ID));
            NewsMessage newsMessage = newsManagementService.getNewsById(id);
            request.setAttribute(NEWS_MESSAGE, newsMessage);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PageHelper.NEWS_PAGE;
    }

    /**
     * @see Command#getName() getName
     */
    public CommandName getName() {
        return name;
    }
}
