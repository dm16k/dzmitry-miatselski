package com.epam.newsportal.metelsky.client.controller.command;

import com.epam.newsportal.metelsky.client.controller.command.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The Command interface provides {@code execute()} method to execute command
 *
 * @author dmitr_000
 * @version 1.00 9/7/2015
 */
public interface Command {
    /**
     * Executes command
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return null or location of page to forward
     * @throws CommandException when an error occurred while executing some command
     */
    String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;

    /**
     * Gets the name of command
     *
     * @return enumeration constant representing command
     */
    CommandName getName();
}
