package com.epam.newsportal.metelsky.client.controller.command.exception;

/**
 * Thrown if some error happened while executing some command.
 *
 * @author dmitr_000
 * @version 1.00 9/7/2015
 */
public class CommandException extends Exception {
    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandException(Throwable cause) {
        super(cause);
    }
}
