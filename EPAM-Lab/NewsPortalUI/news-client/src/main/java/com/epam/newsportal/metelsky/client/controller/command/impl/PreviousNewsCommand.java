package com.epam.newsportal.metelsky.client.controller.command.impl;

import com.epam.newsportal.metelsky.client.controller.PageHelper;
import com.epam.newsportal.metelsky.client.controller.command.Command;
import com.epam.newsportal.metelsky.client.controller.command.CommandName;
import com.epam.newsportal.metelsky.client.controller.command.exception.CommandException;
import com.epam.newsportal.metelsky.domain.NewsMessage;
import com.epam.newsportal.metelsky.service.NewsManagementService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.epam.newsportal.metelsky.util.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The PreviousNewsCommand class gets previous news for current news depending on search criteria
 *
 * @author dmitr_000
 * @version 1.00 9/11/2015
 */
public class PreviousNewsCommand implements Command {
    private final static String NEWS_ID = "newsId";
    private final static String NEWS_MESSAGE = "newsMessage";
    private CommandName name = CommandName.PREVIOUS_NEWS;
    @Autowired
    private NewsManagementService service;

    /**
     * Gets previous news for current news
     * @see Command#execute(HttpServletRequest, HttpServletResponse) execute
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            Long newsId = Long.parseLong(request.getParameter(NEWS_ID));
            SearchCriteria criteria = (SearchCriteria) request.getSession().getAttribute("criteria");
            NewsMessage message = service.getPrevious(newsId, criteria);
            request.setAttribute(NEWS_MESSAGE, message);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PageHelper.NEWS_PAGE;
    }

    /**
     * @see Command#getName() getName
     */
    public CommandName getName() {
        return name;
    }
}
