package com.epam.newsportal.metelsky.client.controller.command;

/**
 * Enumerations of all command constants
 *
 * @author dmitr_000
 * @version 1.00 9/8/2015
 */
public enum CommandName {
    VIEW_NEWS,
    SET_SEARCH_CRITERIA,
    FILTER_LIST,
    ADD_COMMENT,
    NEXT_NEWS,
    PREVIOUS_NEWS,
    CHECK_NEXT,
    CHECK_PREVIOUS,
    CHANGE_LOCALE
}
