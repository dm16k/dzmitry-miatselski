package com.epam.newsportal.metelsky.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

/**
 * The ErrorController class is a servlet class that handles error requests
 *
 * @author dmitr_000
 * @version 1.00 10/19/2015
 */
@WebServlet(urlPatterns = "/error")
public class ErrorController extends HttpServlet {
    @Autowired
    MessageSource messageSource = (MessageSource) ApplicationContextProvider.getApplicationContext().getBean("messageSource");

    /**
     * Handles GET requests to this servlet, fills request by error data, error code and localized message
     *
     * @param req  - an {@link HttpServletRequest} object that contains the request the client has made of the servlet
     * @param resp - an {@link HttpServletResponse} object that contains the response the servlet sends to the client
     * @throws ServletException if the request for the GET could not be handled
     * @throws IOException      if an input or output error is detected when the servlet handles the GET request
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String loc = (String) session.getAttribute("locale");
        String lang = loc.substring(0, 2);
        String country = loc.substring(3, 5);
        Locale locale = new Locale(lang, country);
        String homepage = messageSource.getMessage("homepage", null, locale);
        String message;
        Throwable ex = (Throwable) session.getAttribute("ex");
        String title, code;
        if (ex == null || ex instanceof RuntimeException) {
            code = title = "404";
            message = messageSource.getMessage("notFound", null, locale);
        } else {
            code = title = "500";
            message = messageSource.getMessage("error", null, locale);
        }
        session.removeAttribute("ex");
        req.setAttribute("ex", ex);
        req.setAttribute("title", title);
        req.setAttribute("code", code);
        req.setAttribute("message", message);
        req.setAttribute("homepage", homepage);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(PageHelper.ERROR_PAGE);
        dispatcher.forward(req, resp);
    }
}
