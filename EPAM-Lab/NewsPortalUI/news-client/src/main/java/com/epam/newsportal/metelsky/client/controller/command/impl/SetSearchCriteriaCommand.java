package com.epam.newsportal.metelsky.client.controller.command.impl;

import com.epam.newsportal.metelsky.client.controller.command.Command;
import com.epam.newsportal.metelsky.client.controller.command.CommandName;
import com.epam.newsportal.metelsky.client.controller.command.exception.CommandException;
import com.epam.newsportal.metelsky.util.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * The SetSearchCriteriaCommand class sets search criteria for current session
 *
 * @author dmitr_000
 * @version 1.00 9/10/2015
 */
public class SetSearchCriteriaCommand implements Command {
    private final static String CRITERIA = "criteria";
    private final static String AUTHOR_ID = "authorId";
    private final static String TAG_LIST = "tagId";
    private final CommandName name = CommandName.SET_SEARCH_CRITERIA;
    @Autowired
    @Qualifier(value = "filterList")
    private Command filterListCommand;

    /**
     * Sets search criteria for current session
     * @see Command#execute(HttpServletRequest, HttpServletResponse) execute
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        SearchCriteria criteria = (SearchCriteria) session.getAttribute(CRITERIA);
        if (criteria == null) {
            criteria = new SearchCriteria();
        }
        String authorId = request.getParameter(AUTHOR_ID);
        Long id = null;
        if (authorId != null && !authorId.equalsIgnoreCase("any")) {
            id = Long.parseLong(authorId);
        }
        String[] tags = request.getParameterValues(TAG_LIST);
        List<Long> selectedTags = null;
        if (tags != null) {
            selectedTags = new ArrayList<>();
            for (String t : tags) {
                selectedTags.add(Long.parseLong(t));
            }
        }
        criteria.setAuthorId(id);
        criteria.setTagList(selectedTags);
        session.setAttribute(CRITERIA, criteria);
        return filterListCommand.execute(request, response);
    }

    /**
     * @see Command#getName() getName
     */
    public CommandName getName() {
        return name;
    }
}
