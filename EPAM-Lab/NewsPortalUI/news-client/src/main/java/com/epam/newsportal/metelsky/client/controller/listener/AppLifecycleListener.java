package com.epam.newsportal.metelsky.client.controller.listener;

import com.epam.newsportal.metelsky.client.controller.ApplicationContextProvider;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * The AppLifecycleListener class contains methods to start initializing or reset Spring application content
 *
 * @author dmitr_000
 * @version 1.00 9/9/2015
 */
@WebListener
public class AppLifecycleListener implements ServletContextListener {
    /**
     * This method invoked by application when servlet context was initialized
     *
     * @param servletContextEvent - the event class for notifications about changes to the servlet context of a web application
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ApplicationContextProvider.initAppContext();
    }

    /**
     * This method invoked by application when servlet context was destroyed
     *
     * @param servletContextEvent - the event class for notifications about changes to the servlet context of a web application
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ApplicationContextProvider.resetAppContext();
    }
}
