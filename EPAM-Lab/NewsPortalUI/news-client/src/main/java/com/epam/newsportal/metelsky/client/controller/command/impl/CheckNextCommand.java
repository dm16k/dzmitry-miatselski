package com.epam.newsportal.metelsky.client.controller.command.impl;

import com.epam.newsportal.metelsky.client.controller.command.Command;
import com.epam.newsportal.metelsky.client.controller.command.CommandName;
import com.epam.newsportal.metelsky.client.controller.command.exception.CommandException;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.service.NewsManagementService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.epam.newsportal.metelsky.util.SearchCriteria;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The CheckNextCommand class check the existence of next news for current news and search criteria
 *
 * @author dmitr_000
 * @version 1.00 9/10/2015
 */
public class CheckNextCommand implements Command {
    private CommandName name = CommandName.CHECK_NEXT;
    @Autowired
    private NewsManagementService service;

    /**
     * Check the existence of next news for current news
     * @see Command#execute(HttpServletRequest, HttpServletResponse) execute
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute("json");
        ObjectMapper mapper = new ObjectMapper();
        boolean result;
        try {
            SearchCriteria criteria = (SearchCriteria) request.getSession().getAttribute("criteria");
            News news = mapper.readValue(json, News.class);
            try {
                result = service.checkNext(news.getId(), criteria);
            } catch (ServiceException e) {
                result = false;
            }
            mapper.writeValue(response.getOutputStream(), result);
        } catch (IOException e) {
            throw new CommandException(e);
        }
        return null;
    }

    /**
     * @see Command#getName() getName
     */
    public CommandName getName() {
        return name;
    }
}
