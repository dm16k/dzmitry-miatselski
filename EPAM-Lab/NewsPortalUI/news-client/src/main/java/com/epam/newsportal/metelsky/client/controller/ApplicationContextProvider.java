package com.epam.newsportal.metelsky.client.controller;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 * The ApplicationContextProvider class has methods to create and get {@code ApplicationContext}
 *
 * @author dmitr_000
 * @version 1.00 9/8/2015
 */
@Component
public class ApplicationContextProvider {
    private final static String APP_CONTEXT = "classpath:/spring/client-app-config.xml";
    private static ApplicationContext applicationContext = null;

    /**
     * Initializes ApplicationContext
     *
     * @throws BeansException when an error occurred while creating application context
     */
    public static void initAppContext() throws BeansException {
        applicationContext = new ClassPathXmlApplicationContext(APP_CONTEXT);
    }

    /**
     * Resets application context
     */
    public static void resetAppContext() {
        applicationContext = null;
    }

    /**
     * Gets application context
     *
     * @return {@code ApplicationContext} object
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
