package com.epam.newsportal.metelsky.client.controller.command.impl;

import com.epam.newsportal.metelsky.client.controller.command.Command;
import com.epam.newsportal.metelsky.client.controller.command.CommandName;
import com.epam.newsportal.metelsky.client.controller.command.exception.CommandException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The ChangeLocaleCommand class changes the localization parameter stored in current session
 *
 * @author dmitr_000
 * @version 1.00 9/13/2015
 */
public class ChangeLocaleCommand implements Command {
    private CommandName name = CommandName.CHANGE_LOCALE;

    /**
     * Changes the localization parameter stored in current session
     * @see Command#execute(HttpServletRequest, HttpServletResponse) execute
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute("json");
        ObjectMapper mapper = new ObjectMapper();
        try {
            String locale = mapper.readValue(json, String.class);
            HttpSession session = request.getSession();
            session.setAttribute("locale", locale);
            mapper.writeValue(response.getOutputStream(), 1);
        } catch (IOException e) {
            throw new CommandException(e);
        }
        return null;
    }

    /**
     * @see Command#getName() getName
     */
    @Override
    public CommandName getName() {
        return name;
    }
}
