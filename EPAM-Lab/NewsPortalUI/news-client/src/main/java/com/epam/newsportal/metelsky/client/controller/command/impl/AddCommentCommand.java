package com.epam.newsportal.metelsky.client.controller.command.impl;

import com.epam.newsportal.metelsky.client.controller.command.Command;
import com.epam.newsportal.metelsky.client.controller.command.CommandName;
import com.epam.newsportal.metelsky.client.controller.command.exception.CommandException;
import com.epam.newsportal.metelsky.domain.Comment;
import com.epam.newsportal.metelsky.service.NewsManagementService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

/**
 * The AddCommentCommand adds comment for news
 *
 * @author dmitr_000
 * @version 1.00 9/10/2015
 */
public class AddCommentCommand implements Command {
    @Autowired
    MessageSource messageSource;
    private CommandName name = CommandName.ADD_COMMENT;
    @Autowired
    private NewsManagementService service;

    /**
     * Adds comment for news
     * @see Command#execute(HttpServletRequest, HttpServletResponse) execute
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute("json");
        ObjectMapper mapper = new ObjectMapper();
        String message = null;
        try {
            Comment comment = mapper.readValue(json, Comment.class);
            String text = comment.getText();
            String code = null;
            if (text == null || text.length() == 0) {
                code = "NotEmpty.comment.text";
            } else if (text.length() > 100) {
                code = "TooLong.comment.text";
            }
            if (code == null) {
                try {
                    service.addComment(comment);
                } catch (ServiceException e) {
                    code = "Error.comment.post";
                }
            }
            if (code != null) {
                HttpSession session = request.getSession();
                String loc = (String) session.getAttribute("locale");
                Locale locale = new Locale(loc.substring(0, 2), loc.substring(3, 5));
                message = messageSource.getMessage(code, null, locale);
            }
            mapper.writeValue(response.getOutputStream(), message);
        } catch (IOException e) {
            throw new CommandException(e);
        }
        return null;
    }

    /**
     * @see Command#getName() getName
     */
    public CommandName getName() {
        return name;
    }
}
