var display = 'block';

function showTagList() {
    var tags = $('#checkboxes');
    var display = tags.css('display');
    if (display === 'none')
        display = 'block';
    else
        display = 'none';
    tags.css('display', display);
}

$(document).ready(function () {
    var tagBoxes = $('#checkboxes').find('label');
    $.each(tagBoxes, function (key, value) {
        $(this).attr('onclick', 'setText()');
    });
});

var defaultText = 'Select tags...';
//todo i18n for '%d selected' and for 'Select tags...'
function setText() {
    var tagList = [];
    var checkBoxes = $('#checkboxes');
    var tags = checkBoxes.find('label');
    $.each(tags, function (key, value) {
        var input = $(this).find('input[type=checkbox]');
        var el = $(this);
        if (input.prop('checked') === true) {
            tagList.push(el.text().trim());
        }
    });
    var tagsSelector = checkBoxes.parent().find('option');
    if (tagList.length > 0) {
        if (tagList.length < 4) {
            defaultText = tagList.join();
        } else {
            defaultText = tagList.length + ' selected';
        }
    }
    tagsSelector.text(defaultText);
}

function submitForm(id) {
    $('#' + id).submit();
}

function submitFormWithValue(id, value) {
    var form = $('#' + id);
    form.find('input').eq(1).val(value);
    form.submit();
}

var selectedTags = [];
function setTags() {
    var tagList = $('#checkboxes').find('input[type=checkbox]');
    $.each(tagList, function (k, v) {
        var el = $(this);
        $.each(selectedTags, function (key2, value2) {
            if (parseInt(el.val()) === value2) {
                el.prop('checked', true);
            }
        });
    })
}

function postComment(newsId, btn) {
    var commentText = $('#comment-text').val();//.replace(/</g, '&lt;').replace(/>/g, '&gt;');
    var creationDate = new Date();
    var request = {};
    var comment = {
        newsId: newsId,
        text: commentText,
        creationDate: creationDate
    };
    request.command = 'add_comment';
    request.data = JSON.stringify(comment);
    $.ajax({
        url: 'client',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            if (!data) {location.reload();}
            else {
                var popup = $('<div class="popup-warning popup">' + data + '</div>')
                    .on('click', function () {popup.remove();}).css('margin-top', '24px');
                popup.insertAfter($(btn).parent());
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function nextNews(form, newsId) {
    var news = {
        id: newsId
    };
    var request = {};
    request.command = 'check_next';
    request.data = JSON.stringify(news);
    $.ajax({
        url: 'client',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            if (data === true) {
                submitFormWithValue(form, newsId)
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function previousNews(form, newsId) {
    var news = {
        id: newsId
    };
    var request = {};
    request.command = 'check_previous';
    request.data = JSON.stringify(news);
    $.ajax({
        url: 'client',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            if (data === true) {
                submitFormWithValue(form, newsId)
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function changeLocale() {
    var locale;
    var loc = $('#locale');
    if (loc.attr('class') == 'localeDiv rus')
        locale = 'ru_RU';
    else
        locale = 'en_US';
    var request = {};
    request.command = 'change_locale';
    request.data = JSON.stringify(locale);
    $.ajax({
        url: "client",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            window.location.reload();
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

var currLoc;

function viewPrevLocale() {
    var loc = $('#locale');
    currLoc = loc.attr('class');
    var locClass = loc.attr('class');
    if (locClass === 'localeDiv rus')
        locClass = 'localeDiv usa';
    else
        locClass = 'localeDiv rus';
    loc.attr('class', locClass);
}

function setCurrLocale() {
    $('#locale').attr('class', currLoc);
}