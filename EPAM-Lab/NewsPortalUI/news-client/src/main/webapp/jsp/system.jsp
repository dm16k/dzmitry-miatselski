<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><c:out value="${title}"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/res/css/style.css">
    <script src="${pageContext.request.contextPath}/res/js/jquery-min.js"></script>
    <script src="${pageContext.request.contextPath}/res/js/jquery.interactive_bg.js"></script>
    <script>
        $(document).ready(function () {
            $(".bg").interactive_bg();
            $(window).resize(function () {
                $(".bg > .ibg-bg").css({
                    width: $(window).outerWidth(),
                    height: $(window).outerHeight()
                })
            })
        });
    </script>
</head>
<body>
<div class="bg">
    <div class="error-code"><c:out value="${code}"/></div>
    <div class="message-wrapper">
        <div><c:out value="${message}"/></div>
        <div><a href="${pageContext.request.contextPath}"><c:out value="${homepage}"/></a></div>
    </div>
</div>
    <!--
        <c:if test="${!empty ex}"><c:out value="${ex.localizedMessage}"/>
        <c:out value="Cause:"/>
        <c:out value="${ex.cause.toString()}"/>
    -->
    <!--
        <c:out value="Stack Trace"/>
        <c:forEach var="element" items="${ex.stackTrace}">
        <c:out value="${element}"/></c:forEach>
    </c:if>
    -->
</body>
</html>
