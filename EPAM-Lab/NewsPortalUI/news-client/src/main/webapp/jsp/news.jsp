<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>News</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/res/images/googleg_lodp.ico"
          type="image/x-icon"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/res/css/style.css"/>
    <script src="${pageContext.request.contextPath}/res/js/jquery-min.js"></script>
    <script src="${pageContext.request.contextPath}/res/js/script.js"></script>
</head>
<body>
<%@include file="header.jsp" %>
<div class="news-block">
    <jsp:useBean id="newsMessage" class="com.epam.newsportal.metelsky.domain.NewsMessage" scope="request"/>
    <div class="news-header table">
        <form id="back-form" action="client" method="get">
            <input type="hidden" name="command" value="filter_list">
            <input type="hidden" name="noOfPage" value="${!empty page? page : 1}">
        </form>
        <button class="back-link" onclick="submitForm('back-form')">Back</button>
        <form id="view-list-form" action="client" method="get">
            <input type="hidden" name="command" value="filter_list">
        </form>
        <div class="news-title cell"><c:out value="${newsMessage.news.title}"/></div>
        <div class="news-authorId cell"><fmt:message key="news.author"/> <c:out value="${newsMessage.author.name}"/></div>
        <div class="news-date cell"><fmt:formatDate pattern="d.MM.yyyy" value="${newsMessage.news.creationDate}"/></div>
    </div>
    <div class="news-message"><c:out value="${newsMessage.news.fullText}"/></div>
    <form id="next-news-form" action="client" method="get">
        <input type="hidden" name="command" value="next_news">
        <input type="hidden" name="newsId">
    </form>
    <form id="previous-news-form" action="client" method="get">
        <input type="hidden" name="command" value="previous_news">
        <input type="hidden" name="newsId">
    </form>
    <div class="table">
        <div class="row">
            <div class="nav-block-left cell" onclick="previousNews('previous-news-form', ${newsMessage.news.id})">
                <button class="nav-left">Previous</button>
            </div>
            <div class="nav-block-right cell" onclick="nextNews('next-news-form', ${newsMessage.news.id})">
                <button class="nav-right">Next</button>
            </div>
        </div>
    </div>
    <div class="comments-block">
        <div class="create-comment-block">
            <textarea id="comment-text" class="comment-textarea" maxlength="100"></textarea>
            <input type="submit" value="<fmt:message key="news.comment.send"/>" style="width: 90px"
                   onclick="postComment(${newsMessage.news.id}, this)">
        </div>
        <c:forEach items="${newsMessage.commentList}" var="comment">
            <div class="comment">
                <div class="comment-date"><fmt:formatDate pattern="d.MM.yyyy H.mm"
                                                          value="${comment.creationDate}"/></div>
                <div class="comment-text"><c:out value="${comment.text}"/></div>
            </div>
        </c:forEach>
    </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>
