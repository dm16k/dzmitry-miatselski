<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>News List</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/res/images/googleg_lodp.ico"
          type="image/x-icon"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/res/css/style.css"/>
    <script src="${pageContext.request.contextPath}/res/js/jquery-min.js"></script>
    <script src="${pageContext.request.contextPath}/res/js/script.js"></script>
    <script>
        $(document).ready(function () {
            //TODO Why Russian locale is using always?
            defaultText = '<fmt:message key="list.filter.tags"/>';<c:if test="${! empty criteria}"><c:forEach var="tag" items="${criteria.tagList}">
            selectedTags.push(${tag});</c:forEach></c:if>
            setTags();
            setText();
        })
    </script>
</head>
<body>
<%@include file="header.jsp" %>
<div class="news-filters">
    <form id="filter-news-form" action="client" method="post">
        <input type="hidden" name="command" value="set_search_criteria">
    </form>
    <div class="table">
        <div class="cell">
            <jsp:useBean id="authorList" type="java.util.List<com.epam.newsportal.metelsky.domain.Author>"
                         scope="request"/>
            <select name="authorId" form="filter-news-form" class="author-selector" style="top:-1px">
                <option value="any"><fmt:message key="list.filter.author"/></option>
                <c:forEach var="author" items="${authorList}">
                    <c:if test="${empty author.expired}">
                        <option value="${author.id}" ${author.id == criteria.authorId ? 'selected="selected"' : ''}><c:out value="${author.name}"/></option>
                    </c:if>
                </c:forEach>
            </select>
        </div>
        <div class="cell" style="width: 100px;">
            <jsp:useBean id="tagList" type="java.util.List<com.epam.newsportal.metelsky.domain.Tag>" scope="request"/>
            <div class="multiselect" style="height: 24px">
                <div class="select-box" style="height: 24px" onclick="showTagList()">
                    <select>
                        <option><fmt:message key="list.filter.tags"/></option>
                    </select>
                    <div id="checkboxes" class="tag-list">
                        <c:forEach var="tag" items="${tagList}">
                            <label><input type="checkbox" form="filter-news-form" name="tagId" value="${tag.id}"><c:out value="${tag.name }"/></label>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
        <div class="cell" style="width: 120px">
            <div class="row">
                <input form="filter-news-form" type="submit" style="position: relative;top: -1px;z-index: 2;" value="<fmt:message key="list.filter.search"/>">
                <input form="filter-news-form" type="reset" style="position: relative;top: -1px;z-index: 2;" value="<fmt:message key="list.filter.reset"/>">
            </div>
        </div>
    </div>
</div>
<form id="pagination-form" action="client" method="get">
    <input type="hidden" name="command" value="filter_list">
    <input type="hidden" name="noOfPage">
</form>
<div class="pagination-block table">
    <div class="row">
        <c:set var="page" scope="session" value="${currentPage}"/>
        <c:if test="${currentPage gt 2}">
            <div class="cell">
                <button class="pagination-link" onclick="submitFormWithValue('pagination-form',1)">1</button>
            </div>
        </c:if>
        <c:if test="${currentPage gt 1}">
            <div class="cell">
                <button class="pagination-link" onclick="submitFormWithValue('pagination-form',${currentPage - 1})">
                    &lt;</button>
            </div>
        </c:if>
        <div class="cell">
            <button class="pagination-link">${currentPage}</button>
        </div>
        <c:if test="${currentPage lt pageQuantity}">
            <div class="cell">
                <button class="pagination-link" onclick="submitFormWithValue('pagination-form',${currentPage + 1})">
                    &gt;</button>
            </div>
        </c:if>
        <c:if test="${currentPage lt pageQuantity - 1}">
            <div class="cell">
                <button class="pagination-link" onclick="submitFormWithValue('pagination-form',${pageQuantity})">${pageQuantity}</button>
            </div>
        </c:if>
    </div>
</div>
<div class="news-block">
    <jsp:useBean id="newsList" type="java.util.List<com.epam.newsportal.metelsky.domain.NewsMessage>" scope="request"/>
    <c:forEach var="news" items="${newsList}" varStatus="loop">
        <div class="short-news">
            <div class="news-header table">
                <div class="news-title cell"><c:out value="${news.news.title}"/></div>
                <div class="news-authorId cell"><fmt:message key="news.author"/> <c:out value="${news.author.name}"/></div>
                <div class="news-date cell"><fmt:formatDate pattern="d.MM.yyyy" value="${news.news.modificationDate}"/></div>
            </div>
            <div class="news-message"><c:out value="${news.news.shortText}"/></div>
            <div class="news-footer-wrapper table">
                <div class="news-footer row">
                    <div class="news-tags cell"><c:forEach var="tag" items="${news.tagList}"><c:out value="${tag.name}"/></c:forEach></div>
                    <c:set var="comments" scope="page" value="${fn:length(news.commentList)}"/>
                    <div class="news-comments cell">${comments} <c:if test="${comments == 1}"><fmt:message key="list.news.comment.one"/></c:if><c:if test="${comments != 1}"><fmt:message key="list.news.comment.many"/></c:if>
                    </div>
                    <div class="news-link">
                        <button class="view-link" onclick="submitForm('viewNews${loop.index}')"><fmt:message key="list.news.link"/></button>
                    </div>
                    <form id="viewNews${loop.index}" action="client" method="get">
                        <input type="hidden" name="command" value="view_news">
                        <input type="hidden" name="newsId" value="${news.news.id}">
                    </form>
                </div>
            </div>
        </div>
    </c:forEach>
</div>
<%@include file="footer.jsp" %>
</body>
</html>