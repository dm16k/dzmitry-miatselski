<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="locale" value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<div class="header table">
    <div class="row">
        <div class="header-cell cell" style="width: 150px;">News Portal<a href="${pageContext.request.contextPath}"><span></span></a></div>
        <div class="cell">
            <button id="locale" class="localeDiv ${locale == 'ru_RU'? 'rus' : 'usa'}"
                    title="<fmt:message key="header.locale.title"/>"
                    onclick="changeLocale()"
                    onmouseover="viewPrevLocale()"
                    onmouseout="setCurrLocale()"
                    >Change Language
            </button>
        </div>
    </div>
</div>
