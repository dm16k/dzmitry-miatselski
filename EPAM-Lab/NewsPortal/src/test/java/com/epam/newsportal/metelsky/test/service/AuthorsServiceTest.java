package com.epam.newsportal.metelsky.test.service;

import com.epam.newsportal.metelsky.dao.AuthorsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.service.AuthorsService;
import com.epam.newsportal.metelsky.service.impl.AuthorsServiceImplementation;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorsServiceTest {
    private final static Logger logger = Logger.getLogger(AuthorsServiceTest.class);
    @Mock
    private Author author;
    @Mock
    private AuthorsDao authorsDao;
    @Mock
    private List<Author> authorList;
    private AuthorsService as;

    @Before
    public void setUp() {
        as = new AuthorsServiceImplementation(authorsDao);
    }

    @After
    public void tearDown() {
        as = null;
        author = null;
        authorsDao = null;
        authorList = null;
    }


    @Test
    public void addTest() {
        try {
            long expected = 1L;
            when(authorsDao.add(any(Author.class))).thenReturn(expected);
            long actual = as.add(author);
            Assert.assertEquals(expected, actual);
            verify(authorsDao).add(author);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void getAllTest() {
        try {
            when(authorsDao.getAll()).thenReturn(authorList);
            List<Author> actual = as.getAll();
            Assert.assertEquals(authorList, actual);
            verify(authorsDao).getAll();
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void updateTest() {
        try {
            doNothing().when(authorsDao).update(any(Author.class));
            as.update(author);
            verify(authorsDao).update(author);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTest() {
        try {
            doNothing().when(authorsDao).delete(anyList());
            as.delete(author);
            List<Author> list = new ArrayList<>();
            list.add(author);
            verify(authorsDao).delete(eq(list));
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }
}
