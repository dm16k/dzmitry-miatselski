package com.epam.newsportal.metelsky.test.service;

import com.epam.newsportal.metelsky.dao.TagsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.service.TagsService;
import com.epam.newsportal.metelsky.service.impl.TagsServiceImplementation;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.*;

/**
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
@RunWith(MockitoJUnitRunner.class)
public class TagsServiceTest {
    private final static Logger logger = Logger.getLogger(TagsServiceTest.class);
    @Mock
    private Tag tag;
    @Mock
    private TagsDao tagsDao;
    @Mock
    private List<Tag> tagList;
    private TagsService ts;

    @Before
    public void setUp() {
        ts = new TagsServiceImplementation(tagsDao);
    }

    @After
    public void tearDown() {
        ts = null;
        tag = null;
        tagsDao = null;
    }

    @Test
    public void addTest() {
        try {
            long expected = 1l;
            when(tagsDao.add(any(Tag.class))).thenReturn(expected);
            long actual = ts.add(tag);
            Assert.assertEquals(expected, actual);
            verify(tagsDao).add(tag);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void readAllTest() {
        try {
            when(tagsDao.getAll()).thenReturn(tagList);
            List<Tag> actual = ts.getAll();
            Assert.assertEquals(tagList, actual);
            verify(tagsDao).getAll();
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void updateTest() {
        try {
            doNothing().when(tagsDao).update(any(Tag.class));
            ts.update(tag);
            verify(tagsDao).update(tag);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTest() {
        try {
            doNothing().when(tagsDao).delete(anyList());
            ts.delete(tagList);
            verify(tagsDao).delete(tagList);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void unbindTest() {
        try {
            doNothing().when(tagsDao).delete(anyList());
            ts.unbindNews(tagList);
            verify(tagsDao).unbindNews(tagList);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }
}
