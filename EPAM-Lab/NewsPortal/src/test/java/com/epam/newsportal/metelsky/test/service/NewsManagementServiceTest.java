package com.epam.newsportal.metelsky.test.service;

import com.epam.newsportal.metelsky.domain.*;
import com.epam.newsportal.metelsky.service.*;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.epam.newsportal.metelsky.service.impl.NewsManagementServiceImplementation;
import com.epam.newsportal.metelsky.util.SearchCriteria;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */

@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceTest {
    private final static Logger logger = Logger.getLogger(NewsManagementServiceTest.class);
    @Mock
    private Author author;
    @Mock
    private Comment comment;
    @Mock
    private News news;
    @Mock
    private Tag tag;
    @Mock
    private AuthorsService as;
    @Mock
    private CommentsService cs;
    @Mock
    private NewsService ns;
    @Mock
    private TagsService ts;
    @Mock
    private List<Author> authorList;
    @Mock
    private List<Comment> commentList;
    @Mock
    private List<News> newsList;
    @Mock
    private List<Tag> tagList;
    @Mock
    private Iterator<News> iterator;
    private NewsManagementService newsManagementService;

    @Before
    public void setUp() {
        newsManagementService = new NewsManagementServiceImplementation(as, cs, ns, ts);
    }

    @After
    public void tearDown() {
        newsManagementService = null;
        author = null;
        comment = null;
        news = null;
        tag = null;
        as = null;
        cs = null;
        ns = null;
        ts = null;
        authorList = null;
        commentList = null;
        newsList = null;
        tagList = null;
        iterator = null;
    }

    @Test
    public void getAllNewsTest() {
        try {
            when(ns.getAll()).thenReturn(newsList);
            when(newsList.iterator()).thenReturn(iterator);
            when(iterator.hasNext()).thenReturn(true).thenReturn(false);
            when(iterator.next()).thenReturn(news);
            when(ns.getAuthor(anyLong())).thenReturn(author);
            when(ns.getTags(anyLong())).thenReturn(tagList);
            when(cs.getComments(anyLong())).thenReturn(commentList);
            List<NewsMessage> list = newsManagementService.getAllNews();
            List<NewsMessage> expected = new ArrayList<>();
            NewsMessage message = new NewsMessage();
            message.setNews(news);
            message.setAuthor(author);
            message.setTagList(tagList);
            message.setCommentList(commentList);
            expected.add(message);
            verify(ns).getAll();
            verify(ns).getAuthor(anyLong());
            verify(ns).getTags(anyLong());
            verify(cs).getComments(anyLong());
            Assert.assertEquals(expected, list);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void searchTest() {
        try {
            when(ns.search(any(SearchCriteria.class))).thenReturn(newsList);
            when(newsList.iterator()).thenReturn(iterator);
            when(iterator.hasNext()).thenReturn(true).thenReturn(false);
            when(iterator.next()).thenReturn(news);
            when(ns.getAuthor(anyLong())).thenReturn(author);
            when(ns.getTags(anyLong())).thenReturn(tagList);
            when(cs.getComments(anyLong())).thenReturn(commentList);
            List<NewsMessage> list = newsManagementService.search(author, tagList);
            SearchCriteria sc = new SearchCriteria();
            sc.setAuthor(author);
            sc.setTagList(tagList);
            List<NewsMessage> expected = new ArrayList<>();
            NewsMessage message = new NewsMessage();
            message.setNews(news);
            message.setAuthor(author);
            message.setTagList(tagList);
            message.setCommentList(commentList);
            expected.add(message);
            verify(ns).search(sc);
            verify(ns).getAuthor(anyLong());
            verify(ns).getTags(anyLong());
            verify(cs).getComments(anyLong());
            Assert.assertEquals(expected, list);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void getNewsByIdTest() {
        try {
            long id = 1L;
            when(ns.getById(anyLong())).thenReturn(news);
            when(ns.getAuthor(anyLong())).thenReturn(author);
            when(ns.getTags(anyLong())).thenReturn(tagList);
            when(cs.getComments(anyLong())).thenReturn(commentList);
            NewsMessage actual = newsManagementService.getNewsById(id);
            NewsMessage expected = new NewsMessage();
            expected.setNews(news);
            expected.setAuthor(author);
            expected.setTagList(tagList);
            expected.setCommentList(commentList);
            verify(ns).getById(id);
            verify(ns).getAuthor(anyLong());
            verify(ns).getTags(anyLong());
            verify(cs).getComments(anyLong());
            Assert.assertEquals(expected, actual);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void createNewsTest() {
        try {
            long expected = 0L;
            when(ns.add(any(News.class))).thenReturn(expected);
            doNothing().when(ns).setAuthor(any(News.class), any(Author.class));
            doNothing().when(ns).setTags(any(News.class), anyList());
            newsManagementService.addNews(news, author, tagList);
            verify(ns).add(news);
            verify(ns).setAuthor(news, author);
            verify(ns).setTags(news, tagList);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void editNewsTest() {
        try {
            doNothing().when(ns).update(any(News.class));
            doNothing().when(ns).setAuthor(any(News.class), any(Author.class));
            doNothing().when(ns).setTags(any(News.class), anyList());
            newsManagementService.editNews(news, author, tagList);
            verify(ns).update(news);
            verify(ns).setAuthor(news, author);
            verify(ns).setTags(news, tagList);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void deleteNewsTest() {
        try {
            doNothing().when(ns).delete(anyList());
            doNothing().when(ns).unbindAuthors(anyList());
            doNothing().when(ns).unbindTags(anyList());
            doNothing().when(cs).deleteByNews(anyList());
            newsManagementService.deleteNews(newsList);
            verify(ns).unbindAuthors(newsList);
            verify(ns).unbindTags(newsList);
            verify(cs).deleteByNews(newsList);
            verify(ns).delete(newsList);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void addCommentTest() {
        try {
            long expected = 1L;
            when(cs.add(any(Comment.class))).thenReturn(expected);
            long actual = newsManagementService.addComment(comment);
            verify(cs).add(comment);
            Assert.assertEquals(expected, actual);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void editCommentTest() {
        try {
            doNothing().when(cs).update(any(Comment.class));
            newsManagementService.editComment(comment);
            verify(cs).update(comment);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void deleteCommentsTest() {
        try {
            doNothing().when(cs).delete(anyList());
            newsManagementService.deleteComments(commentList);
            verify(cs).delete(commentList);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void deleteCommentsByNewsTest() {
        try {
            doNothing().when(cs).deleteByNews(anyList());
            newsManagementService.deleteNews(newsList);
            verify(cs).deleteByNews(newsList);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void getAllAuthorsTest() {
        try {
            when(as.getAll()).thenReturn(authorList);
            List<Author> actual = newsManagementService.getAllAuthors();
            verify(as).getAll();
            Assert.assertEquals(authorList, actual);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void addAuthorTest() {
        try {
            long expected = 1l;
            when(as.add(any(Author.class))).thenReturn(expected);
            newsManagementService.addAuthor(author);
            verify(as).add(author);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void updateAuthorTest() {
        try {
            doNothing().when(as).update(any(Author.class));
            newsManagementService.updateAuthor(author);
            verify(as).update(author);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void deleteAuthorTest() {
        try {
            doNothing().when(as).delete(any(Author.class));
            newsManagementService.deleteAuthor(author);
            verify(as).delete(author);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void getAllTagsTest() {
        try {
            when(ts.getAll()).thenReturn(tagList);
            List<Tag> actual = newsManagementService.getAllTags();
            verify(ts).getAll();
            Assert.assertEquals(tagList, actual);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void addTagTest() {
        try {
            long expected = 1L;
            when(ts.add(any(Tag.class))).thenReturn(expected);
            long actual = newsManagementService.addTag(tag);
            verify(ts).add(tag);
            Assert.assertEquals(expected, actual);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void changeTagTest() {
        try {
            doNothing().when(ts).update(any(Tag.class));
            newsManagementService.changeTag(tag);
            verify(ts).update(tag);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTagTest() {
        try {
            doNothing().when(ts).delete(anyList());
            newsManagementService.deleteTag(tag);
            List<Tag> list = new ArrayList<>();
            list.add(tag);
            verify(ts).unbindNews(list);
            verify(ts).delete(list);
        } catch (ServiceException e) {
            logger.error(e, e.getCause());
        }
    }
}
