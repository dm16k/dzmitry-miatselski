package com.epam.newsportal.metelsky.test.dao;

import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.dao.AuthorsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author dmitr_000
 * @version 1.00 8/17/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-test-config.xml"})
@DatabaseSetup("classpath:db-test-input.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class AuthorDAOTest {
    private final static Logger logger = Logger.getLogger(AuthorDAOTest.class);
    @Autowired
    private AuthorsDao authorsDao;

    @Test
    public void createTest() {
        Author author = new Author();
        author.setName("authorT");
        Author resAuthor = null;
        try {
            long resId = authorsDao.add(author);
            author.setId(resId);
            List<Author> list = authorsDao.getAll();
            for (Author u : list) {
                if (u.getId() == resId) {
                    resAuthor = u;
                }
            }
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertEquals(author, resAuthor);
    }

    @Test
    public void readAllTest() {
        List<Author> resList = null;
        try {
            resList = authorsDao.getAll();
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
        Assert.assertNotNull(resList);
        Assert.assertEquals(3, resList.size());
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:authorTest/update_result.xml")
    public void updateTest() {
        Author author = new Author();
        author.setId(102L);
        author.setName("Bobby");
        author.setExpired(new Timestamp(1441868400000L));
        try {
            authorsDao.update(author);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:authorTest/delete_result.xml")
    public void deleteTest() {
        Author author = new Author();
        author.setId(103L);
        List<Author> l = new ArrayList<>();
        l.add(author);
        try {
            authorsDao.delete(l);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }
}
