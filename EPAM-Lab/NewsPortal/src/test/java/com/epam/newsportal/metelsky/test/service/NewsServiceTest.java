package com.epam.newsportal.metelsky.test.service;

import com.epam.newsportal.metelsky.dao.NewsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.service.NewsService;
import com.epam.newsportal.metelsky.service.impl.NewsServiceImplementation;
import com.epam.newsportal.metelsky.util.SearchCriteria;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.*;

/**
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
    private final static Logger logger = Logger.getLogger(NewsServiceTest.class);
    @Mock
    private News news;
    @Mock
    private Author author;
    @Mock
    private SearchCriteria sc;
    @Mock
    private NewsDao newsDao;
    @Mock
    private List<News> newsList;
    @Mock
    private List<Tag> tagList;
    private NewsService ns;

    @Before
    public void setUp() {
        ns = new NewsServiceImplementation(newsDao);
    }

    @After
    public void tearDown() {
        ns = null;
        news = null;
        author = null;
        sc = null;
        newsDao = null;
        newsList = null;
        tagList = null;
    }

    @Test
    public void addTest() {
        try {
            long expected = 1L;
            when(newsDao.add(any(News.class))).thenReturn(expected);
            long actual = ns.add(news);
            Assert.assertEquals(expected, actual);
            verify(newsDao).add(news);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void setAuthorTest() {
        try {
            doNothing().when(newsDao).setAuthor(any(News.class), any(Author.class));
            ns.setAuthor(news, author);
            verify(newsDao).setAuthor(news, author);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void setTagsTest() {
        try {
            doNothing().when(newsDao).setTags(any(News.class), anyList());
            ns.setTags(news, tagList);
            verify(newsDao).setTags(news, tagList);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void getById() {
        try {
            long id = 1L;
            when(newsDao.getById(anyLong())).thenReturn(news);
            News n = ns.getById(id);
            verify(newsDao).getById(id);
            Assert.assertEquals(news, n);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void getAuthorTest() {
        try {
            long id = 1L;
            when(newsDao.getAuthorForNews(anyLong())).thenReturn(author);
            Author actual = ns.getAuthor(id);
            verify(newsDao).getAuthorForNews(id);
            Assert.assertEquals(author, actual);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void getTagsTest() {
        try {
            long id = 1L;
            when(newsDao.getTagsForNews(anyLong())).thenReturn(tagList);
            List<Tag> list = ns.getTags(id);
            verify(newsDao).getTagsForNews(id);
            Assert.assertEquals(list, tagList);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void getAllTest() {
        try {
            when(newsDao.getAll()).thenReturn(newsList);
            List<News> actual = ns.getAll();
            Assert.assertEquals(newsList, actual);
            verify(newsDao).getAll();
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void searchTest() {
        try {
            when(newsDao.search(any(SearchCriteria.class))).thenReturn(newsList);
            List<News> list = ns.search(sc);
            verify(newsDao).search(sc);
            Assert.assertEquals(newsList, list);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void countTest() {
        try {
            long expected = 1L;
            when(newsDao.countNews(any(SearchCriteria.class))).thenReturn(expected);
            long actual = newsDao.countNews(sc);
            verify(newsDao).countNews(sc);
            Assert.assertEquals(expected, actual);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void updateTest() {
        try {
            doNothing().when(newsDao).update(any(News.class));
            ns.update(news);
            verify(newsDao).update(news);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTest() {
        try {
            doNothing().when(newsDao).delete(anyList());
            ns.delete(newsList);
            verify(newsDao).delete(newsList);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void unbindAuthorsTest() {
        try {
            doNothing().when(newsDao).unbindAuthors(anyList());
            ns.unbindAuthors(newsList);
            verify(newsDao).unbindAuthors(newsList);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }


    @Test
    public void unbindTagsTest() {
        try {
            doNothing().when(newsDao).unbindTags(anyList());
            ns.unbindTags(newsList);
            verify(newsDao).unbindTags(newsList);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }
}
