package com.epam.newsportal.metelsky.test.service;

import com.epam.newsportal.metelsky.dao.CommentsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Comment;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.service.CommentsService;
import com.epam.newsportal.metelsky.service.impl.CommentsServiceImplementation;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentsServiceTest {
    private final static Logger logger = Logger.getLogger(CommentsServiceTest.class);
    @Mock
    private Comment comment;
    @Mock
    private CommentsDao commentsDao;
    @Mock
    private List<Comment> commentList;
    @Mock
    private List<News> newsList;
    private CommentsService cs;

    @Before
    public void setUp() {
        cs = new CommentsServiceImplementation(commentsDao);
    }

    @After
    public void tearDown() {
        cs = null;
        comment = null;
        commentsDao = null;
        commentList = null;
        newsList = null;
    }

    @Test
    public void addTest() {
        try {
            long id = 1L;
            when(commentsDao.add(any(Comment.class))).thenReturn(id);
            long actual = cs.add(comment);
            Assert.assertEquals(id, actual);
            verify(commentsDao).add(comment);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void getCommentsTest() {
        try {
            when(commentsDao.getNewsComments(anyLong())).thenReturn(commentList);
            long id = 1L;
            List<Comment> actual = cs.getComments(id);
            Assert.assertEquals(commentList, actual);
            verify(commentsDao).getNewsComments(id);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void updateTest() {
        try {
            doNothing().when(commentsDao).update(any(Comment.class));
            cs.update(comment);
            verify(commentsDao).update(comment);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTest() {
        try {
            doNothing().when(commentsDao).delete(anyList());
            cs.delete(commentList);
            verify(commentsDao).delete(commentList);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }

    @Test
    public void deleteByNewsTest() {
        try {
            doNothing().when(commentsDao).deleteByNews(newsList);
            cs.deleteByNews(newsList);
            verify(commentsDao).deleteByNews(newsList);
        } catch (DaoException e) {
            logger.error(e, e.getCause());
        }
    }
}
