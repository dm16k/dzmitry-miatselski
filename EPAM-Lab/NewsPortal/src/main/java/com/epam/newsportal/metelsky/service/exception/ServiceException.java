package com.epam.newsportal.metelsky.service.exception;

import com.epam.newsportal.metelsky.dao.exception.DaoException;

/**
 * Thrown if some error happened while using some services.
 *
 * @author dmitr_000
 * @version 1.00 8/11/2015
 */
public class ServiceException extends DaoException {
    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
