package com.epam.newsportal.metelsky.dao;

import com.epam.newsportal.metelsky.dao.exception.DaoException;

import java.util.List;

/**
 * An interface for DAO classes. Provides CRUD methods.
 *
 * @author dmitr_000
 * @version 1.00 8/10/2015
 */
public interface DAO<T> {
    /**
     * Creates new entry in DB
     *
     * @param entity - an object to add entry
     * @return ID of created entity
     * @throws DaoException when an error occurred
     */
    long add(T entity) throws DaoException;

    /**
     * Gets entity specified by ID
     *
     * @param id - specifies entity ID
     * @return entity specified by ID
     * @throws DaoException when an error occurred
     */
    T getById(Long id) throws DaoException;

    /**
     * Gets all entities
     *
     * @return list of entities
     * @throws DaoException when an error occurred
     */
    List<T> getAll() throws DaoException;

    /**
     * Updates entry in DB
     *
     * @param entity - an object to update entry
     * @throws DaoException when an error occurred
     */
    void update(T entity) throws DaoException;

    /**
     * Deletes entries from DB
     *
     * @param entities - list of entities to delete from DB
     * @throws DaoException when an error occurred
     */
    void delete(List<T> entities) throws DaoException;
}
