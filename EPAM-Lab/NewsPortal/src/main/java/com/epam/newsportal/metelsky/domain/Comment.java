package com.epam.newsportal.metelsky.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The Comment class represents entries of 'COMMENTS' table in DB.
 * It provides getters and setters for every field of each entry.
 *
 * @author dmitr_000
 * @version 1.00 8/10/2015
 */
public class Comment implements Serializable {
    private static final long serialVersionUID = 4217224694684414261L;
    private long id;
    private long newsId;
    private String text;
    private Timestamp creationDate;

    /**
     * Default constructor
     */
    public Comment() {
    }

    /**
     * Gets comment ID
     *
     * @return comment ID
     */
    public long getId() {
        return id;
    }

    /**
     * Sets comment ID
     *
     * @param id - specifies comment ID
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets newsId associated with this comment
     *
     * @return newsId
     */
    public long getNewsId() {
        return newsId;
    }

    /**
     * Sets newsId associated with this comment
     *
     * @param newsId - newsId object
     */
    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }

    /**
     * Gets comment text
     *
     * @return comment text
     */
    public String getText() {
        return text;
    }

    /**
     * Sets comment text
     *
     * @param text - specifies comment text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Gets comment creation date & time
     *
     * @return comment creation date & time
     */
    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * Sets comment creation date & time
     *
     * @param creationDate - specifies comment creation date & time
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Returns true if and only if the argument is not null and is an Comment object that represents the same values as this object
     *
     * @param o - the object to compare with
     * @return true if the Comment objects represent the same values; false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (id != comment.id) return false;
        if (newsId != comment.newsId) return false;
        if (!text.equals(comment.text)) return false;
        return creationDate.equals(comment.creationDate);

    }

    /**
     * Returns a hash code for object values
     *
     * @return hash code for this Comment object
     */
    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + text.hashCode();
        result = 31 * result + creationDate.hashCode();
        return result;
    }

    /**
     * Returns a String object representing this Comment's value
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", newsId=" + newsId +
                ", text='" + text + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
