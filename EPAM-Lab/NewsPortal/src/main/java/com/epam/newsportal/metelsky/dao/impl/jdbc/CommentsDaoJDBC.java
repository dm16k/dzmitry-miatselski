package com.epam.newsportal.metelsky.dao.impl.jdbc;

import com.epam.newsportal.metelsky.dao.CommentsDao;
import com.epam.newsportal.metelsky.dao.DAO;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.dao.util.ConnectionManager;
import com.epam.newsportal.metelsky.domain.Comment;
import com.epam.newsportal.metelsky.domain.News;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The CommentsDaoJDBC class provides CRUD methods to work with 'COMMENTS' table of DB
 *
 * @author dmitr_000
 * @version 1.00 8/10/2015
 */
public class CommentsDaoJDBC implements CommentsDao {
    private ConnectionManager manager;
    private String ADD_COMMENT = "BEGIN INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) " +
            "VALUES (COMMENTS_SEQ.nextval, ?, ?, ?) RETURNING COMMENT_ID INTO ?; END;";
    private String DELETE_COMMENT = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
    private String DELETE_COMMENT_BY_NEWS = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
    private String READ_NEWS_COMMENTS = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE " +
            "FROM COMMENTS WHERE NEWS_ID = ?";
    private String UPDATE_COMMENT = "UPDATE COMMENTS SET COMMENT_TEXT = ? WHERE COMMENT_ID = ?";
    private String SORTING = " ORDER BY CREATION_DATE";

    /**
     * Constructor
     *
     * @param manager - manager to work with DB
     */
    public CommentsDaoJDBC(ConnectionManager manager) {
        this.manager = manager;
    }

    /**
     * @see DAO#add(Object) add
     */
    public long add(Comment entity) throws DaoException {
        Connection c = null;
        CallableStatement cs = null;
        long id;
        try {
            c = manager.getConnection();
            cs = c.prepareCall(ADD_COMMENT);
            cs.setLong(1, entity.getNewsId());
            cs.setString(2, entity.getText());
            cs.setTimestamp(3, entity.getCreationDate());
            cs.registerOutParameter(4, Types.VARCHAR);
            cs.execute();
            id = cs.getLong(4);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, cs, c);
        }
        return id;
    }

    /**
     * @throws DaoException, method unsupported for this implementation
     */
    public Comment getById(Long id) throws DaoException {
        throw new DaoException("Unsupported method for Comment's DAO");
    }

    /**
     * @throws DaoException, method unsupported for this implementation
     */
    public List<Comment> getAll() throws DaoException {
        throw new DaoException("Unsupported method for Comment's DAO");
    }

    /**
     * @see CommentsDao#getNewsComments(Long) getNewsComments
     */
    public List<Comment> getNewsComments(Long newsId) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Comment> list = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(READ_NEWS_COMMENTS + SORTING);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Comment comment = new Comment();
                comment.setId(rs.getLong("COMMENT_ID"));
                comment.setNewsId(rs.getLong("NEWS_ID"));
                comment.setText(rs.getString("COMMENT_TEXT"));
                comment.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                list.add(comment);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return list;
    }

    /**
     * @see DAO#update(Object) update
     */
    public void update(Comment entity) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UPDATE_COMMENT);
            ps.setString(1, entity.getText());
            ps.setLong(2, entity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see DAO#delete(List) delete
     */
    public void delete(List<Comment> entities) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_COMMENT);
            for (Comment com : entities) {
                ps.setLong(1, com.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see CommentsDao#deleteByNews(List) deleteByNews
     */
    public void deleteByNews(List<News> news) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_COMMENT_BY_NEWS);
            for (News n : news) {
                ps.setLong(1, n.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }
}
