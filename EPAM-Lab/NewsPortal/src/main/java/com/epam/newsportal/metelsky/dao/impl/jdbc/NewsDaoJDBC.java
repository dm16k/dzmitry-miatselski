package com.epam.newsportal.metelsky.dao.impl.jdbc;

import com.epam.newsportal.metelsky.dao.DAO;
import com.epam.newsportal.metelsky.dao.NewsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.dao.util.ConnectionManager;
import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.util.SearchCriteria;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The NewsDaoJDBC class provides methods to work with 'NEWS', 'NEWS_AUTHOR' and 'NEWS_TAG' tables of DB
 *
 * @author dmitr_000
 * @version 1.00 8/12/2015
 */
public class NewsDaoJDBC implements NewsDao {
    private ConnectionManager manager;
    private String ADD_NEWS = "BEGIN INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) " +
            "VALUES (NEWS_SEQ.nextval, ?, ?, ?, ?, ?) RETURNING NEWS_ID INTO ?; END;";
    private String INSERT_INTO_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
    private String INSERT_INTO_NEWS_TAG = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?, ?)";
    private String DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";
    private String DELETE_FROM_NEWS_AUTHOR_BY_NEWS = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
    private String DELETE_FROM_NEWS_TAG_BY_NEWS = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
    private String READ_ALL_NEWS = "SELECT " +
            "N.NEWS_ID, N.TITLE, N.SHORT_TEXT, " +
            "N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE, " +
            "(SELECT COUNT(*) FROM COMMENTS C " +
            "WHERE C.NEWS_ID = N.NEWS_ID) AS COMMENTS " +
            "FROM NEWS N";
    private String SEARCH_NEWS = "SELECT DISTINCT N.NEWS_ID, N.TITLE, N.SHORT_TEXT, " +
            "N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE " +
            "FROM NEWS N " +
            "JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID " +
            "JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID";
    private String UPDATE_NEWS = "UPDATE NEWS " +
            "SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, " +
            "CREATION_DATE = ?, MODIFICATION_DATE = ? " +
            "WHERE NEWS_ID = ?";
    private String GET_BY_ID = READ_ALL_NEWS + " WHERE NEWS_ID = ?";
    private String GET_AUTHOR_FOR_NEWS = "SELECT A.AUTHOR_ID, A.AUTHOR_NAME, A.EXPIRED FROM NEWS_AUTHOR NA " +
            "JOIN AUTHORS A ON A.AUTHOR_ID = NA.AUTHOR_ID " +
            "WHERE NA.NEWS_ID = ? ORDER BY A.AUTHOR_ID";
    private String GET_TAGS_FOR_NEWS = "SELECT T.TAG_ID, T.TAG_NAME FROM NEWS_TAG NT " +
            "JOIN TAGS T ON T.TAG_ID = NT.TAG_ID " +
            "WHERE NT.NEWS_ID = ? ORDER BY T.TAG_ID";
    private String COUNT_NEWS = "SELECT COUNT(DISTINCT N.NEWS_ID) AS QUANTITY FROM NEWS N " +
            "JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID " +
            "JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID";
    private String SORTING = " ORDER BY COMMENTS, MODIFICATION_DATE DESC";

    /**
     * Constructor
     *
     * @param manager - data source to connect to DB
     */
    public NewsDaoJDBC(ConnectionManager manager) {
        this.manager = manager;
    }

    /**
     * @see DAO#add(Object) add
     */
    public long add(News entity) throws DaoException {
        Connection c = null;
        CallableStatement cs = null;
        long id;
        try {
            c = manager.getConnection();
            cs = c.prepareCall(ADD_NEWS);
            cs.setString(1, entity.getTitle());
            cs.setString(2, entity.getShortText());
            cs.setString(3, entity.getFullText());
            cs.setTimestamp(4, entity.getCreationDate());
            cs.setDate(5, entity.getModificationDate());
            cs.registerOutParameter(6, Types.VARCHAR);
            cs.execute();
            id = cs.getLong(6);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, cs, c);
        }
        return id;
    }

    /**
     * @see NewsDao#setAuthor(News, Author) setAuthor
     */
    public void setAuthor(News news, Author author) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_FROM_NEWS_AUTHOR_BY_NEWS);
            ps.setLong(1, news.getId());
            ps.executeUpdate();
            ps = c.prepareStatement(INSERT_INTO_NEWS_AUTHOR);
            ps.setLong(1, news.getId());
            ps.setLong(2, author.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see NewsDao#setTags(News, List) setTags
     */
    public void setTags(News news, List<Tag> tags) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_FROM_NEWS_TAG_BY_NEWS);
            ps.setLong(1, news.getId());
            ps.executeUpdate();
            ps = c.prepareStatement(INSERT_INTO_NEWS_TAG);
            for (Tag t : tags) {
                ps.setLong(1, news.getId());
                ps.setLong(2, t.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see DAO#getById(Long) getById
     */
    public News getById(Long id) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        News n = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(GET_BY_ID);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            rs.next();
            n = new News();
            n.setId(id);
            n.setTitle(rs.getString("TITLE"));
            n.setShortText(rs.getString("SHORT_TEXT"));
            n.setFullText(rs.getString("FULL_TEXT"));
            n.setCreationDate(rs.getTimestamp("CREATION_DATE"));
            n.setModificationDate(rs.getDate("MODIFICATION_DATE"));
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return n;
    }

    /**
     * @see NewsDao#getAuthorForNews(Long) getAuthorForNews
     */
    public Author getAuthorForNews(Long newsId) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Author author = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(GET_AUTHOR_FOR_NEWS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            while (rs.next()) {
                author = new Author();
                author.setId(rs.getLong("AUTHOR_ID"));
                author.setName(rs.getString("AUTHOR_NAME"));
                author.setExpired(rs.getTimestamp("EXPIRED"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return author;
    }

    /**
     * @see NewsDao#getTagsForNews(Long) getTagsForNews
     */
    public List<Tag> getTagsForNews(Long newsId) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Tag> tagList = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(GET_TAGS_FOR_NEWS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            tagList = new ArrayList<>();
            while (rs.next()) {
                Tag tag = new Tag();
                tag.setId(rs.getLong("TAG_ID"));
                tag.setName(rs.getString("TAG_NAME"));
                tagList.add(tag);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return tagList;
    }

    /**
     * @see DAO#getAll() getAll
     */
    public List<News> getAll() throws DaoException {
        Connection c = null;
        Statement st = null;
        ResultSet rs = null;
        List<News> list = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(READ_ALL_NEWS + SORTING);
            list = new ArrayList<>();
            while (rs.next()) {
                News news = new News();
                news.setId(rs.getLong("NEWS_ID"));
                news.setTitle(rs.getString("TITLE"));
                news.setShortText(rs.getString("SHORT_TEXT"));
                news.setFullText(rs.getString("FULL_TEXT"));
                news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                news.setModificationDate(rs.getDate("MODIFICATION_DATE"));
                list.add(news);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    /**
     * @see DAO#update(Object) update
     */
    public void update(News entity) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UPDATE_NEWS);
            ps.setString(1, entity.getTitle());
            ps.setString(2, entity.getShortText());
            ps.setString(3, entity.getFullText());
            ps.setTimestamp(4, entity.getCreationDate());
            ps.setDate(5, entity.getModificationDate());
            ps.setLong(6, entity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see DAO#delete(List) delete
     */
    public void delete(List<News> entities) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_NEWS);
            for (News n : entities) {
                ps.setLong(1, n.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see NewsDao#unbindAuthors(List) unbindAuthors
     */
    public void unbindAuthors(List<News> news) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_FROM_NEWS_AUTHOR_BY_NEWS);
            for (News n : news) {
                ps.setLong(1, n.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see NewsDao#unbindTags(List) unbindTags
     */
    public void unbindTags(List<News> news) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_FROM_NEWS_TAG_BY_NEWS);
            for (News n : news) {
                ps.setLong(1, n.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see NewsDao#search(SearchCriteria) search
     */
    public List<News> search(SearchCriteria criteria) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<News> list = null;
        try {
            c = manager.getConnection();
            ps = prepareStatement(c, criteria.getAuthor(), criteria.getTagList(), SEARCH_NEWS);
            rs = ps.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                News news = new News();
                news.setId(rs.getLong("NEWS_ID"));
                news.setTitle(rs.getString("TITLE"));
                news.setShortText(rs.getString("SHORT_TEXT"));
                news.setFullText(rs.getString("FULL_TEXT"));
                news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                news.setModificationDate(rs.getDate("MODIFICATION_DATE"));
                list.add(news);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return list;
    }

    /**
     * @see NewsDao#countNews(SearchCriteria)  countNews
     */
    public long countNews(SearchCriteria criteria) throws DaoException {
        Author author = null;
        List<Tag> tagList = null;
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        long count = 0;
        try {
            c = manager.getConnection();
            //TODO check
            if (criteria != null) {
                author = criteria.getAuthor();
                tagList = criteria.getTagList();
            }
            ps = prepareStatement(c, author, tagList, COUNT_NEWS);
            rs = ps.executeQuery();
            rs.next();
            count = rs.getLong("QUANTITY");
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return count;
    }

    /**
     * This method constructs {@code PreparedStatement} object depending on author, tagList and query
     *
     * @param c          - specifies connection to DB
     * @param author     - specifies author
     * @param tagList    - specifies list of tags
     * @param basicQuery - specifies string query
     * @return PreparedStatement object
     * @throws SQLException when exception occurred while preparing statement
     */
    private PreparedStatement prepareStatement(Connection c, Author author, List<Tag> tagList, String basicQuery) throws SQLException {
        StringBuilder builder = new StringBuilder(basicQuery);
        PreparedStatement ps;
        if (author == null && (tagList == null || tagList.size() == 0)) {
            ps = c.prepareStatement(builder.toString());
        } else {
            builder.append(" WHERE ");
            if (author != null) {
                builder.append("NA.AUTHOR_ID = ?");
                if (tagList != null && tagList.size() != 0) {
                    builder.append(" AND ");
                }
            }
            if (tagList != null && tagList.size() != 0) {
                for (int i = 0; i < tagList.size(); i++) {
                    if (i == 0) {
                        builder.append("NT.TAG_ID = ?");
                    } else {
                        builder.append(" OR NT.TAG_ID = ?");
                    }
                }
            }
            ps = c.prepareStatement(builder.toString());
            int startCounter = 1;
            if (author != null) {
                ps.setLong(startCounter++, author.getId());
            }
            if (tagList != null) {
                for (Tag t : tagList) {
                    ps.setLong(startCounter++, t.getId());
                }
            }
        }
        return ps;
    }
}
