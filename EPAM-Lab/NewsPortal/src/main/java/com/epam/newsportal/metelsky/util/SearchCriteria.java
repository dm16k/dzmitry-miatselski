package com.epam.newsportal.metelsky.util;

import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.domain.Tag;

import java.util.List;

/**
 * It provides getters and setters for {@code Author} and {@code List<Tag>} fields
 *
 * @author dmitr_000
 * @version 1.00 8/14/2015
 */
public class SearchCriteria {
    private Author author;
    private List<Tag> tagList;

    /**
     * Default constructor
     */
    public SearchCriteria() {
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    /**
     * Sets tag list
     *
     * @param tagList - specifies tag list
     */
    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public Author getAuthor() {
        return author;
    }

    /**
     * Sets author
     *
     * @param author - specifies author
     */
    public void setAuthor(Author author) {
        this.author = author;
    }

    /**
     * Returns true if and only if the argument is not null and is an SearchCriteria object that represents the same values as this object
     *
     * @param o - the object to compare with
     * @return true if the SearchCriteria objects represent the same values; false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchCriteria that = (SearchCriteria) o;

        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (tagList != null ? !tagList.equals(that.tagList) : that.tagList != null) return false;

        return true;
    }

    /**
     * Returns a hash code for object values
     *
     * @return hash code for this SearchCriteria object
     */
    @Override
    public int hashCode() {
        int result = author != null ? author.hashCode() : 0;
        result = 31 * result + (tagList != null ? tagList.hashCode() : 0);
        return result;
    }
}
