package com.epam.newsportal.metelsky.service.impl;

import com.epam.newsportal.metelsky.domain.*;
import com.epam.newsportal.metelsky.service.*;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.epam.newsportal.metelsky.util.SearchCriteria;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The NewsManagementServiceImplementation class has methods to manage news, using different services
 *
 * @author dmitr_000
 * @version 1.00 8/18/2015
 */
public class NewsManagementServiceImplementation implements NewsManagementService {
    AuthorsService authorsService;
    CommentsService commentsService;
    NewsService newsService;
    TagsService tagsService;

    /**
     * Constructor
     *
     * @param authorsService  - specifies author service
     * @param commentsService - specifies comment service
     * @param newsService     - specifies news service
     * @param tagsService     - specifies tag service
     */
    public NewsManagementServiceImplementation(AuthorsService authorsService, CommentsService commentsService,
                                               NewsService newsService, TagsService tagsService) {
        this.authorsService = authorsService;
        this.commentsService = commentsService;
        this.newsService = newsService;
        this.tagsService = tagsService;
    }

    /**
     * @see NewsManagementService#getAllNews() getAllNews
     */
    public List<NewsMessage> getAllNews() throws ServiceException {
        List<News> newsList = newsService.getAll();
        List<NewsMessage> messageList = new ArrayList<>();
        for (News n : newsList) {
            NewsMessage message = new NewsMessage();
            message.setNews(n);
            message.setAuthor(newsService.getAuthor(n.getId()));
            message.setTagList(newsService.getTags(n.getId()));
            message.setCommentList(commentsService.getComments(n.getId()));
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * @see NewsManagementService#search(Author, List) search
     */
    public List<NewsMessage> search(Author author, List<Tag> tags) throws ServiceException {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthor(author);
        sc.setTagList(tags);
        List<News> newsList = newsService.search(sc);
        List<NewsMessage> messageList = new ArrayList<>();
        for (News n : newsList) {
            NewsMessage message = new NewsMessage();
            message.setNews(n);
            message.setAuthor(newsService.getAuthor(n.getId()));
            message.setTagList(newsService.getTags(n.getId()));
            message.setCommentList(commentsService.getComments(n.getId()));
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * @see NewsManagementService#getNewsById(long) getNewsById
     */
    public NewsMessage getNewsById(long id) throws ServiceException {
        NewsMessage message = new NewsMessage();
        News news = newsService.getById(id);
        message.setNews(news);
        message.setAuthor(newsService.getAuthor(news.getId()));
        message.setTagList(newsService.getTags(news.getId()));
        message.setCommentList(commentsService.getComments(news.getId()));
        return message;
    }

    /**
     * @see NewsManagementService#addNews(News, Author, List) addNews
     */
    public void addNews(News news, Author author, List<Tag> tags) throws ServiceException {
        news.setId(newsService.add(news));
        newsService.setAuthor(news, author);
        newsService.setTags(news, tags);
    }

    /**
     * @see NewsManagementService#editNews(News, Author, List) editNews
     */
    public void editNews(News news, Author author, List<Tag> tags) throws ServiceException {
        newsService.update(news);
        newsService.setAuthor(news, author);
        newsService.setTags(news, tags);
    }

    /**
     * @see NewsManagementService#deleteNews(List) deleteNews
     */
    public void deleteNews(List<News> news) throws ServiceException {
        newsService.unbindAuthors(news);
        newsService.unbindTags(news);
        commentsService.deleteByNews(news);
        newsService.delete(news);
        //TODO test
    }

    //TODO delete
//    /**
//     * @see NewsManagementService#getComments(long) getComments
//     */
//    public List<Comment> getComments(long newsId) throws ServiceException {
//        return commentsService.getComments(newsId);
//    }

    /**
     * @see NewsManagementService#addComment(Comment) addComment
     */
    public long addComment(Comment comment) throws ServiceException {
        return commentsService.add(comment);
    }

    /**
     * @see NewsManagementService#editComment(Comment) editComment
     */
    public void editComment(Comment comment) throws ServiceException {
        commentsService.update(comment);
    }

    /**
     * @see NewsManagementService#deleteComments(List) deleteComments
     */
    public void deleteComments(List<Comment> comments) throws ServiceException {
        commentsService.delete(comments);
    }

    /**
     * @see NewsManagementService#getAllAuthors() getAllAuthors
     */
    public List<Author> getAllAuthors() throws ServiceException {
        return authorsService.getAll();
    }

    /**
     * @see NewsManagementService#addAuthor(Author) addAuthor
     */
    public void addAuthor(Author author) throws ServiceException {
        authorsService.add(author);
    }

    /**
     * @see NewsManagementService#updateAuthor(Author) updateAuthor
     */
    public void updateAuthor(Author author) throws ServiceException {
        authorsService.update(author);
    }

    /**
     * @see NewsManagementService#deleteAuthor(Author) deleteAuthor
     */
    public void deleteAuthor(Author author) throws ServiceException {
        authorsService.delete(author);
    }

    /**
     * @see NewsManagementService#getAllTags() getAllTags
     */
    public List<Tag> getAllTags() throws ServiceException {
        return tagsService.getAll();
    }

    /**
     * @see NewsManagementService#addTag(Tag) addTag
     */
    public long addTag(Tag tag) throws ServiceException {
        return tagsService.add(tag);
    }

    /**
     * @see NewsManagementService#changeTag(Tag) changeTag
     */
    public void changeTag(Tag tag) throws ServiceException {
        tagsService.update(tag);
    }

    /**
     * @see NewsManagementService#deleteTag(Tag) deleteTag
     */
    public void deleteTag(Tag tag) throws ServiceException {
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        tagsService.unbindNews(tags);
        tagsService.delete(tags);
    }
}
