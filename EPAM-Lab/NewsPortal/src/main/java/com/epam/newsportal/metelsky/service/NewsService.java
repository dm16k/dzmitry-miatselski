package com.epam.newsportal.metelsky.service;

import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import com.epam.newsportal.metelsky.util.SearchCriteria;

import java.util.List;

/**
 * An interface for news service classes. Provides service for CRUD methods of DAO layer.
 *
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
public interface NewsService {
    /**
     * Adds news to DB
     *
     * @param news - specifies news to add
     * @return ID of added news
     * @throws ServiceException when an error occurred in DAO layer
     */
    long add(News news) throws ServiceException;

    /**
     * Sets author for news
     *
     * @param news   - specifies news for setting
     * @param author - specifies author
     * @throws ServiceException when an error occurred in DAO layer
     */
    void setAuthor(News news, Author author) throws ServiceException;

    /**
     * Sets tags for news
     *
     * @param news - specifies news for setting
     * @param tags - specifies tags
     * @throws ServiceException when an error occurred in DAO layer
     */
    void setTags(News news, List<Tag> tags) throws ServiceException;

    /**
     * Gets news identified by ID
     *
     * @param id - specifies news ID
     * @return news identified by ID
     * @throws ServiceException when an error occurred in DAO layer
     */
    News getById(long id) throws ServiceException;

    /**
     * Gets authors for news
     *
     * @param newsId - specifies news ID
     * @return author list
     * @throws ServiceException when an error occurred in DAO layer
     */
    Author getAuthor(long newsId) throws ServiceException;

    /**
     * Gets tags for news
     *
     * @param newsId - specifies news ID
     * @return tag list
     * @throws ServiceException when an error occurred in DAO layer
     */
    List<Tag> getTags(long newsId) throws ServiceException;

    /**
     * Gets list of all news
     *
     * @return news list
     * @throws ServiceException when an error occurred in DAO layer
     */
    List<News> getAll() throws ServiceException;

    /**
     * Updates news info
     *
     * @param news - specifies news to update
     * @throws ServiceException when an error occurred in DAO layer
     */
    void update(News news) throws ServiceException;

    /**
     * Deletes news
     *
     * @param news - specifies news list to delete
     * @throws ServiceException when an error occurred in DAO layer
     */
    void delete(List<News> news) throws ServiceException;

    /**
     * Unbinds authors for list of news
     *
     * @param news - specifies list of news
     * @throws ServiceException when an error occurred in DAO layer
     */
    void unbindAuthors(List<News> news) throws ServiceException;

    /**
     * Unbinds tags for list of news
     *
     * @param news - specifies list of news
     * @throws ServiceException when an error occurred in DAO layer
     */
    void unbindTags(List<News> news) throws ServiceException;

    /**
     * Gets list of news according to search criteria
     *
     * @param searchCriteria - specifies search criteria
     * @return news list
     * @throws ServiceException when an error occurred in DAO layer
     */
    List<News> search(SearchCriteria searchCriteria) throws ServiceException;

    /**
     * Gets number of selected records in last usage {@link #search(SearchCriteria) search()} or {@link #getAll() getAll()} methods
     *
     * @return number records
     */
    long countNews() throws ServiceException;
}
