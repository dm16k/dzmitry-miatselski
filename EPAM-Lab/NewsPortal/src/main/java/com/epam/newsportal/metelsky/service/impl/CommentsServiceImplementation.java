package com.epam.newsportal.metelsky.service.impl;

import com.epam.newsportal.metelsky.dao.CommentsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Comment;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.service.CommentsService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;

import java.util.List;

/**
 * The CommentsServiceImplementation class provides methods to work with comments DAOs
 *
 * @author dmitr_000
 * @version 1.00 8/18/2015
 */
public class CommentsServiceImplementation implements CommentsService {
    private CommentsDao commentsDao;

    /**
     * Constructor
     *
     * @param commentsDao - specifies comments DAO
     */
    public CommentsServiceImplementation(CommentsDao commentsDao) {
        this.commentsDao = commentsDao;
    }

    /**
     * @see CommentsService#add(Comment) add
     */
    public long add(Comment comment) throws ServiceException {
        try {
            if (comment != null) {
                return commentsDao.add(comment);
            } else {
                throw new ServiceException("Trying to add null-referenced entity");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see CommentsService#getComments(long) getComments
     */
    public List<Comment> getComments(long newsId) throws ServiceException {
        try {
            return commentsDao.getNewsComments(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see CommentsService#update(Comment) update
     */
    public void update(Comment comment) throws ServiceException {
        try {
            if (comment != null) {
                commentsDao.update(comment);
            } else {
                throw new ServiceException("Trying to update null-referenced entity");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see CommentsService#delete(List) delete
     */
    public void delete(List<Comment> comments) throws ServiceException {
        try {
            if (comments != null) {
                commentsDao.delete(comments);
            } else {
                throw new ServiceException("Trying to delete null-referenced list");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see CommentsService#deleteByNews(List) deleteByNews
     */
    public void deleteByNews(List<News> news) throws ServiceException {
        try {
            if (news != null) {
                commentsDao.deleteByNews(news);
            } else {
                throw new ServiceException("Trying to delete null-referenced list");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
