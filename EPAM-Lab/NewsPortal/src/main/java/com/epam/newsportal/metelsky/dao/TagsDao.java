package com.epam.newsportal.metelsky.dao;

import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Tag;

import java.util.List;

/**
 * An interface for tags DAO classes. Provides CRUD methods.
 *
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
public interface TagsDao extends DAO<Tag> {
    /**
     * @see DAO#add(Object) add
     */
    long add(Tag entity) throws DaoException;

    /**
     * @see DAO#getAll() getAll
     */
    List<Tag> getAll() throws DaoException;

    /**
     * @see DAO#update(Object) update
     */
    void update(Tag entity) throws DaoException;

    /**
     * @see DAO#delete(List) delete
     */
    void delete(List<Tag> entities) throws DaoException;

    /**
     * Unbinds news from tag list
     *
     * @param tags - specifies tag list
     * @throws DaoException when an error occurred
     */
    void unbindNews(List<Tag> tags) throws DaoException;
}
