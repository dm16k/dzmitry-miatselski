package com.epam.newsportal.metelsky.service;

import com.epam.newsportal.metelsky.domain.Comment;
import com.epam.newsportal.metelsky.domain.News;
import com.epam.newsportal.metelsky.service.exception.ServiceException;

import java.util.List;

/**
 * An interface for comments service classes. Provides service for CRUD methods of DAO layer.
 *
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
public interface CommentsService {
    /**
     * Adds comment to DB
     *
     * @param comment - specifies comment to add
     * @return ID of added comment
     * @throws ServiceException when an error occurred in DAO layer
     */
    long add(Comment comment) throws ServiceException;

    /**
     * Gets list of comments associated with news
     *
     * @param newsId - specifies news
     * @return comment list
     * @throws ServiceException when an error occurred in DAO layer
     */
    List<Comment> getComments(long newsId) throws ServiceException;

    /**
     * Updates comment info
     *
     * @param comment - specifies comment to update
     * @throws ServiceException when an error occurred in DAO layer
     */
    void update(Comment comment) throws ServiceException;

    /**
     * Deletes comments
     *
     * @param comments - specifies comment list to delete
     * @throws ServiceException when an error occurred in DAO layer
     */
    void delete(List<Comment> comments) throws ServiceException;

    /**
     * Deletes comments for news list
     *
     * @param news - specifies news list for deleting comments
     * @throws ServiceException when an error occurred in DAO layer
     */
    void deleteByNews(List<News> news) throws ServiceException;
}
