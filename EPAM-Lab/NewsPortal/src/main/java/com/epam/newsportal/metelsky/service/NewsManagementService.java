package com.epam.newsportal.metelsky.service;

import com.epam.newsportal.metelsky.domain.*;
import com.epam.newsportal.metelsky.service.exception.ServiceException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * An interface for application service classes. Provides methods for all application services.
 *
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
@Transactional(readOnly = false, rollbackFor = {RuntimeException.class, Exception.class})
public interface NewsManagementService {
    /**
     * Gets list of all news
     *
     * @return news list
     * @throws ServiceException when an error occurred in DAO layer
     */
    @Transactional(readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<NewsMessage> getAllNews() throws ServiceException;

    /**
     * Gets list of news according to author and tags
     *
     * @param author - specifies author
     * @param tags   - specifies tags
     * @return news list
     * @throws ServiceException when an error occurred in DAO layer
     */
    @Transactional(readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<NewsMessage> search(Author author, List<Tag> tags) throws ServiceException;

    /**
     * Gets news identified by ID
     *
     * @param id - specifies news ID
     * @return news identified by ID
     * @throws ServiceException when an error occurred in DAO layer
     */
    @Transactional(readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    NewsMessage getNewsById(long id) throws ServiceException;

    /**
     * Adds news to DB and sets tags and author for it
     *
     * @param news   - specifies news to add
     * @param author - specifies author
     * @param tags   - specifies tags
     * @throws ServiceException when an error occurred in DAO layer
     */
    void addNews(News news, Author author, List<Tag> tags) throws ServiceException;

    /**
     * Updates news message info
     *
     * @param news   - specifies news to update
     * @param author - specifies author
     * @param tags   - specifies tags
     * @throws ServiceException when an error occurred in DAO layer
     */
    void editNews(News news, Author author, List<Tag> tags) throws ServiceException;

    /**
     * Deletes news and unbinds associated tags and author. Also deletes comments.
     *
     * @param news - specifies news list to delete
     * @throws ServiceException when an error occurred in DAO layer
     */
    void deleteNews(List<News> news) throws ServiceException;

    /**
     * Adds comment to DB
     *
     * @param comment - specifies comment to add
     * @return ID of added comment
     * @throws ServiceException when an error occurred in DAO layer
     */
    long addComment(Comment comment) throws ServiceException;

    /**
     * Updates comment info
     *
     * @param comment - specifies comment to update
     * @throws ServiceException when an error occurred in DAO layer
     */
    void editComment(Comment comment) throws ServiceException;

    /**
     * Deletes comments
     *
     * @param comments - specifies comment list to delete
     * @throws ServiceException when an error occurred in DAO layer
     */
    void deleteComments(List<Comment> comments) throws ServiceException;

    /**
     * Gets list of all authors
     *
     * @return author list
     * @throws ServiceException when an error occurred in DAO layer
     */
    @Transactional(readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<Author> getAllAuthors() throws ServiceException;

    /**
     * Adds author to DB
     *
     * @param author - specifies author to add
     * @throws ServiceException when an error occurred in DAO layer
     */
    void addAuthor(Author author) throws ServiceException;

    /**
     * Updates author info
     *
     * @param author - specifies author to update
     * @throws ServiceException when an error occurred in DAO layer
     */
    void updateAuthor(Author author) throws ServiceException;

    /**
     * Expires author
     *
     * @param author - specifies author to delete
     * @throws ServiceException when an error occurred in DAO layer
     */
    void deleteAuthor(Author author) throws ServiceException;

    /**
     * Gets list of all tags
     *
     * @return tag list
     * @throws ServiceException when an error occurred in DAO layer
     */
    @Transactional(readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<Tag> getAllTags() throws ServiceException;

    /**
     * Adds tag to DB
     *
     * @param tag - specifies tag to add
     * @return ID of added tag
     * @throws ServiceException when an error occurred in DAO layer
     */
    long addTag(Tag tag) throws ServiceException;

    /**
     * Changes tag info
     *
     * @param tag - specifies tag to update
     * @throws ServiceException when an error occurred in DAO layer
     */
    void changeTag(Tag tag) throws ServiceException;

    /**
     * Deletes tags and unbinds them from news
     *
     * @param tag - specifies tag to delete
     * @throws ServiceException when an error occurred in DAO layer
     */
    void deleteTag(Tag tag) throws ServiceException;
}
