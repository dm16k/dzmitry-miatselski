package com.epam.newsportal.metelsky.service.impl;

import com.epam.newsportal.metelsky.dao.TagsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.domain.Tag;
import com.epam.newsportal.metelsky.service.TagsService;
import com.epam.newsportal.metelsky.service.exception.ServiceException;

import java.util.List;

/**
 * The TagsServiceImplementation class provides methods to work with tags DAOs
 *
 * @author dmitr_000
 * @version 1.00 8/18/2015
 */
public class TagsServiceImplementation implements TagsService {
    private TagsDao tagsDao;

    /**
     * Constructor
     *
     * @param tagsDao - specifies tags DAO
     */
    public TagsServiceImplementation(TagsDao tagsDao) {
        this.tagsDao = tagsDao;
    }

    /**
     * @see TagsService#add(Tag) add
     */
    public long add(Tag tag) throws ServiceException {
        try {
            if (tag != null) {
                return tagsDao.add(tag);
            } else {
                throw new ServiceException("Trying to add null-referenced entity");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see TagsService#getAll() getAll
     */
    public List<Tag> getAll() throws ServiceException {
        try {
            return tagsDao.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see TagsService#update(Tag) update
     */
    public void update(Tag tag) throws ServiceException {
        try {
            if (tag != null) {
                tagsDao.update(tag);
            } else {
                throw new ServiceException("Trying to update null-referenced entity");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see TagsService#delete(List) delete
     */
    public void delete(List<Tag> tags) throws ServiceException {
        try {
            if (tags != null) {
                tagsDao.delete(tags);
            } else {
                throw new ServiceException("Trying to delete null-referenced list");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see TagsService#unbindNews(List) unbindNews
     */
    public void unbindNews(List<Tag> tags) throws ServiceException {
        try {
            if (tags != null) {
                tagsDao.unbindNews(tags);
            } else {
                throw new ServiceException("Trying to unbind null-referenced list");
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}