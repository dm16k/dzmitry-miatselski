package com.epam.newsportal.metelsky.dao;

import com.epam.newsportal.metelsky.domain.Author;
import com.epam.newsportal.metelsky.dao.exception.DaoException;

import java.util.List;

/**
 * An interface for authors DAO classes. Provides CRUD methods.
 *
 * @author dmitr_000
 * @version 1.00 8/19/2015
 */
public interface AuthorsDao extends DAO<Author> {
    /**
     * @see DAO#add(Object) add
     */
    long add(Author entity) throws DaoException;

    /**
     * @see DAO#getAll() getAll
     */
    List<Author> getAll() throws DaoException;

    /**
     * @see DAO#update(Object) update
     */
    void update(Author entity) throws DaoException;

    /**
     * @see DAO#delete(List) delete
     */
    void delete(List<Author> entities) throws DaoException;
}
