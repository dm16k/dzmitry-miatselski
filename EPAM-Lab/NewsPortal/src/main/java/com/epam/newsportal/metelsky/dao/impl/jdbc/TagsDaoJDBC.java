package com.epam.newsportal.metelsky.dao.impl.jdbc;

import com.epam.newsportal.metelsky.dao.DAO;
import com.epam.newsportal.metelsky.dao.TagsDao;
import com.epam.newsportal.metelsky.dao.exception.DaoException;
import com.epam.newsportal.metelsky.dao.util.ConnectionManager;
import com.epam.newsportal.metelsky.domain.Tag;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The TagsDaoJDBC class provides CRUD methods to work with 'TAGS' and 'NEWS_TAG' tables of DB
 *
 * @author dmitr_000
 * @version 1.00 8/10/2015
 */
public class TagsDaoJDBC implements TagsDao {
    private ConnectionManager manager;
    private String ADD_TAG = "BEGIN INSERT INTO TAGS (TAG_ID, TAG_NAME) " +
            "VALUES (TAGS_SEQ.nextval, ?) RETURNING TAG_ID INTO ?; END;";
    private String DELETE_TAG = "DELETE FROM TAGS WHERE TAG_ID = ?";
    private String DELETE_FROM_NEWS_TAG_BY_TAG = "DELETE FROM NEWS_TAG WHERE TAG_ID = ?";
    private String READ_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM TAGS";
    private String UPDATE_TAG = "UPDATE TAGS SET TAG_NAME = ? WHERE TAG_ID = ?";
    private String SORTING = " ORDER BY TAG_ID";

    /**
     * Constructor
     *
     * @param manager - manager to work with DB
     */
    public TagsDaoJDBC(ConnectionManager manager) {
        this.manager = manager;
    }

    /**
     * @see DAO#add(Object) add
     */
    public long add(Tag entity) throws DaoException {
        Connection c = null;
        CallableStatement cs = null;
        long id;
        try {
            c = manager.getConnection();
            cs = c.prepareCall(ADD_TAG);
            cs.setString(1, entity.getName());
            cs.registerOutParameter(2, Types.VARCHAR);
            cs.executeUpdate();
            id = cs.getLong(2);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, cs, c);
        }
        return id;
    }

    /**
     * @throws DaoException, method unsupported for this implementation
     */
    public Tag getById(Long id) throws DaoException {
        throw new DaoException("Unsupported method for Tag's DAO");
    }

    /**
     * @see DAO#getAll() getAll
     */
    public List<Tag> getAll() throws DaoException {
        Connection c = null;
        Statement st = null;
        ResultSet rs = null;
        List<Tag> list = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(READ_ALL_TAGS + SORTING);
            list = new ArrayList<>();
            while (rs.next()) {
                Tag tag = new Tag();
                tag.setId(rs.getLong("TAG_ID"));
                tag.setName(rs.getString("TAG_NAME"));
                list.add(tag);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    /**
     * @see DAO#update(Object) update
     */
    public void update(Tag entity) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UPDATE_TAG);
            ps.setString(1, entity.getName());
            ps.setLong(2, entity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see DAO#delete(List) delete
     */
    public void delete(List<Tag> entities) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_TAG);
            for (Tag t : entities) {
                ps.setLong(1, t.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    /**
     * @see TagsDao#unbindNews(List) unbindNews
     */
    public void unbindNews(List<Tag> tags) throws DaoException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_FROM_NEWS_TAG_BY_TAG);
            for (Tag t : tags) {
                ps.setLong(1, t.getId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }
}
