package by.epam.task3.entity.library;

import by.epam.task3.entity.bookcollection.Book;
import by.epam.task3.entity.bookcollection.BookCollection;
import by.epam.task3.entity.person.Person;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Exchanger;


public class Library {
    public static final Logger logger = Logger.getRootLogger();

    private BookCollection libraryBookCollection;
    private BlockingQueue<Librarian> librarians;
    private Map<Person, Librarian> busyLibrarians;
    private Exchanger<Book> bookExchanger;

    public Library(int bookCollectionSize, int numberOfLibrarians) {
        this(new BookCollection(bookCollectionSize), numberOfLibrarians);
    }

    public Library(BookCollection bookCollection, int numberOfLibrarians) {
        libraryBookCollection = bookCollection;
        librarians = new ArrayBlockingQueue<Librarian>(numberOfLibrarians);
        for (int i = 0; i < numberOfLibrarians; i++) {
            librarians.add(new Librarian(i, libraryBookCollection));
        }
        busyLibrarians = new HashMap<Person, Librarian>();
        bookExchanger = new Exchanger<Book>();
    }

    public boolean lockLibrarian(Person person) {
        try {
            Librarian freeLibrarian = librarians.take();
            busyLibrarians.put(person, freeLibrarian);
            return true;
        } catch (InterruptedException ex) {
            logger.debug("person can't work with librarian");
            return false;
        }
    }

    public boolean unlockLibrarian(Person person) {
        Librarian librarian = busyLibrarians.get(person);
        try {
            librarians.put(librarian);
            busyLibrarians.remove(person);
            return true;
        } catch (InterruptedException ex) {
            logger.debug("Person " + person.getName() + "can't finish with librarian");
            return false;
        }

    }

    public void addBooksToBookCollection(List<Book> books) {
        libraryBookCollection.addBook(books);
    }

    public Librarian getPersonsLibrarian(Person person) throws LibraryException {
        Librarian result = busyLibrarians.get(person);
        if (result == null) {
            throw new LibraryException("try to use librarian without blocking");
        }
        return result;
    }

    public Exchanger<Book> getBookExchanger() {
        return bookExchanger;
    }
}
