package by.epam.task3.entity.library;


public class LibraryException extends Exception {
    public LibraryException(String message) {
        super(message);
    }
}
