package by.epam.task3.entity.library;

import by.epam.task3.entity.bookcollection.Book;
import by.epam.task3.entity.bookcollection.BookCollection;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;


public class Librarian {
    private int id;

    private BookCollection libraryBookCollection;

    public Librarian(int id, BookCollection libraryBookCollection) {
        this.id = id;
        this.libraryBookCollection = libraryBookCollection;
    }

    public boolean passBook(BookCollection personBookCollection) throws InterruptedException {
        boolean result = false;
        Lock libraryBookStoreLock = libraryBookCollection.getLock();
        boolean libraryLock = false;

        try {
            libraryLock = libraryBookStoreLock.tryLock(30, TimeUnit.SECONDS);
            if (libraryLock) {
                int newBookCount = libraryBookCollection.getRealSize() + 1;
                if (newBookCount <= libraryBookCollection.getFreeSize()) {
                    result = doMoveFromPerson(personBookCollection);
                }
            }
        } finally {
            if (libraryLock) {
                libraryBookStoreLock.unlock();
            }
        }

        return result;
    }

    private boolean doMoveFromPerson(BookCollection personBookCollection) throws InterruptedException {
        Lock personBookCollectionLock = personBookCollection.getLock();
        boolean personLock = false;

        try {
            personLock = personBookCollectionLock.tryLock(30, TimeUnit.SECONDS);
            if (personLock)
                if (personBookCollection.getRealSize() >= 1) {
                    Book book = personBookCollection.takeBook();
                    libraryBookCollection.addBook(book);
                    return true;
                }
        } finally {
            if (personLock) {
                personBookCollectionLock.unlock();
            }
        }

        return false;
    }

    public boolean takeBook(BookCollection personBookCollection) throws InterruptedException {
        boolean result = false;
        Lock libraryBookStoreLock = libraryBookCollection.getLock();
        boolean libraryLock = false;

        try {
            libraryLock = libraryBookStoreLock.tryLock(30, TimeUnit.SECONDS);
            if (libraryLock) {
                if (1 <= libraryBookCollection.getRealSize()) {
                    result = doMoveFromLibrary(personBookCollection);
                }
            }
        } finally {
            if (libraryLock) {
                libraryBookStoreLock.unlock();
            }
        }

        return result;
    }

    private boolean doMoveFromLibrary(BookCollection personBookCollection) throws InterruptedException {
        Lock personBookStoreLock = personBookCollection.getLock();
        boolean personLock = false;

        try {
            personLock = personBookStoreLock.tryLock(30, TimeUnit.SECONDS);
            if (personLock) {
                if (personBookCollection.getFreeSize() >= 1) {
                    Book book = libraryBookCollection.takeBook();
                    personBookCollection.addBook(book);
                    return true;
                }
            }
        } finally {
            if (personLock) {
                personBookStoreLock.unlock();
            }
        }

        return false;
    }

}
