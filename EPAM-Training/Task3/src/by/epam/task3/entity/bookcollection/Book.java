package by.epam.task3.entity.bookcollection;


public class Book {
    private String name;
    private String author;
    private int id;

    public Book(int id) {
        this.id = id;
    }

    public Book(String name, String author, int id) {
        this.name = name;
        this.author = author;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o.getClass() != Book.class)
            return false;
        Book book = (Book) o;
        if (id != book.id)
            return false;
        if (author != null ? !author.equals(book.author) : book.author != null)
            return false;
        if (name != null ? !name.equals(book.name) : book.name != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + id;
        return result;
    }
}
