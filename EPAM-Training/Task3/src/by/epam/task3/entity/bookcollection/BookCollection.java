package by.epam.task3.entity.bookcollection;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class BookCollection {
    private List<Book> bookList;
    private int size;
    private Lock lock;

    public BookCollection(int size) {
        this.bookList = new ArrayList<Book>(size);
        this.size = size;
        lock = new ReentrantLock();
    }

    public boolean addBook(Book book) {
        return bookList.add(book);
    }

    public boolean addBook(List<Book> books) {
        return bookList.addAll(books);
    }

    public Book takeBook(int id) {
        int index = -1;
        for (Book book : bookList) {
            if (Integer.compare(book.getId(), id) == 0) {
                index = bookList.indexOf(book);
            }
        }
        if (index != -1) {
            return bookList.remove(index);
        }
        return null;
    }

    public Book takeBook() {
        if (bookList.size() > 0)
            return bookList.remove(0);
        return null;
    }

    public Lock getLock() {
        return lock;
    }

    public int getRealSize() {
        return bookList.size();
    }

    public int getFreeSize() {
        return size - bookList.size();
    }
}
