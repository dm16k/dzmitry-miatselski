package by.epam.task3.entity.person;


import by.epam.task3.entity.bookcollection.Book;
import by.epam.task3.entity.bookcollection.BookCollection;
import by.epam.task3.entity.library.Librarian;
import by.epam.task3.entity.library.Library;
import by.epam.task3.entity.library.LibraryException;
import org.apache.log4j.Logger;

import java.util.Random;
import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Person implements Runnable {

    public static final Logger logger = Logger.getRootLogger();

    private BookCollection personBookCollection;
    private String name;
    private Library library;
    private PersonAction lastAction;
    private volatile boolean stopThread = false;

    public Person(String name, Library library, int personBookStoreSize) {
        this.name = name;
        this.library = library;
        personBookCollection = new BookCollection(personBookStoreSize);
        lastAction = PersonAction.GO_HOME;
    }

    public String getName() {
        return name;
    }

    public void stopTread() {
        stopThread = true;
    }

    @Override
    public void run() {
        try {
            while (!stopThread) {
                performAction(getNextAction());
            }
        } catch (InterruptedException | LibraryException ex) {
            logger.error("With person " + this.getName() + " happened something wrong and he died", ex);
        }
        logger.debug("Person " + this.getName() + " stopped");
    }

    private void readBook() throws InterruptedException {
        Thread.sleep(1000);
    }

    private void goHome() throws InterruptedException {
        Thread.sleep(2000);
    }

    private void borrowBook(Librarian librarian) throws InterruptedException {
        boolean result = librarian.takeBook(personBookCollection);
        if (result) {
            logger.debug("person " + getName() + " borrowed the book");
        } else {
            logger.debug("person " + getName() + " reached limit of books or books ended");
        }
    }

    private void passBook(Librarian librarian) throws InterruptedException {
        boolean result = librarian.passBook(personBookCollection);
        if (result) {
            logger.debug("person " + getName() + " passed the book");
        } else {
            logger.debug("person " + getName() + " don't have books for passing");
        }
    }

    private void exchangeBook() throws InterruptedException {
        Exchanger<Book> exchanger = library.getBookExchanger();
        Book book = personBookCollection.takeBook();
        logger.debug("person " + this.getName() + " ready for exchanging");
        try {
            exchanger.exchange(book, 1, TimeUnit.SECONDS);
            logger.debug("person " + this.getName() + " exchanged the book");
        } catch (TimeoutException ex) {
            logger.debug("person " + this.getName() + " didn't wait for exchanging");
        } finally {
            personBookCollection.addBook(book);
        }
    }

    private void workWithLibrarian(PersonAction action) throws InterruptedException, LibraryException {
        boolean isLockedLibrarian = false;
        Librarian librarian;
        try {
            isLockedLibrarian = library.lockLibrarian(this);

            if (isLockedLibrarian) {
                librarian = library.getPersonsLibrarian(this);
                switch (action) {
                    case BORROW_BOOK:
                        borrowBook(librarian);
                        break;
                    case PASS_BOOK:
                        passBook(librarian);
                }
            }
        } finally {
            if (isLockedLibrarian) {
                library.unlockLibrarian(this);
            }
        }
    }

    private PersonAction getNextAction() {
        Random random = new Random();
        int value = random.nextInt(6000);
        switch (lastAction) {
            case GO_HOME: {
                return PersonAction.GO_TO_LIBRARY;
            }
            case GO_TO_LIBRARY: {
                if (personBookCollection.getRealSize() > 0 && value > 3000) {
                    return PersonAction.PASS_BOOK;
                } else {
                    return PersonAction.BORROW_BOOK;
                }
            }
            case BORROW_BOOK: {
                if (value > 300) {
                    return PersonAction.GO_TO_READING_ROOM;
                } else {
                    return PersonAction.GO_HOME;
                }
            }
            case PASS_BOOK: {
                if (personBookCollection.getRealSize() > 0 && value > 3000) {
                    return PersonAction.GO_TO_READING_ROOM;
                } else {
                    return PersonAction.GO_HOME;
                }
            }
            case GO_TO_READING_ROOM: {
                if (value > 3000) {
                    return PersonAction.EXCHANGE_BOOK;
                } else {
                    return PersonAction.GO_HOME;
                }
            }

        }
        return PersonAction.GO_HOME;
    }

    private void performAction(PersonAction action) throws InterruptedException, LibraryException {
        lastAction = action;
        switch (action) {
            case GO_HOME:
                goHome();
                break;
            case GO_TO_LIBRARY:
                performAction(getNextAction());
                break;
            case BORROW_BOOK:
            case PASS_BOOK:
                workWithLibrarian(action);
                break;
            case GO_TO_READING_ROOM:
                performAction(getNextAction());
                break;
            case EXCHANGE_BOOK:
                exchangeBook();
                break;
            case READ_BOOK:
                readBook();
                break;
        }
    }

    private enum PersonAction {
        GO_TO_LIBRARY, READ_BOOK, GO_TO_READING_ROOM, EXCHANGE_BOOK, GO_HOME, BORROW_BOOK, PASS_BOOK
    }
}
