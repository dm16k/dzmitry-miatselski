package by.epam.task3.runner;

import by.epam.task3.entity.bookcollection.Book;
import by.epam.task3.entity.bookcollection.BookCollection;
import by.epam.task3.entity.library.Library;
import by.epam.task3.entity.person.Person;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


public class Runner {

    public static final Logger logger = Logger.getRootLogger();
    private static final int NUMBER_OF_BOOKS = 15;
    private static final int NUMBER_OF_VISITORS = 15;
    private static final int NUMBER_OF_LIBRARIANS = 6;
    private static final int SIZE_OF_PERSONS_BOOK_COLLECTION = 5;
    private static final int WORKING_TIME_MILLISECONDS = 5000;

    private Runner() {
    }

    public static void main(String[] args) {
        BookCollection libraryBookCollection = new BookCollection(NUMBER_OF_BOOKS);
        for (int i = 0; i < NUMBER_OF_BOOKS; i++) {
            libraryBookCollection.addBook(new Book(i));
        }
        Library library = new Library(libraryBookCollection, NUMBER_OF_LIBRARIANS);
        List<Person> personList = new ArrayList<Person>();

        for (int i = 0; i < NUMBER_OF_VISITORS; i++) {
            Person person = new Person(Integer.toString(i), library, SIZE_OF_PERSONS_BOOK_COLLECTION);
            personList.add(person);
            Thread thread = new Thread(person);
            thread.start();
        }
        try {
            Thread.sleep(WORKING_TIME_MILLISECONDS);
        } catch (InterruptedException ex) {
            logger.error("application error", ex);
        }

        for (Person person : personList) {
            person.stopTread();
        }
    }
}
