<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head lang="ru-RU">
    <meta charset="UTF-8">
    <title>Welcome to MySQLAirlines</title>
    <link rel="stylesheet" href="jsp/css/style.css"/>
    <script src="jsp/js/script.js"></script>
</head>

<body>
<%@include file="navbar.jsp"%>
<div class="selectors">
    <div class="selector">
        <div class="selWrapper">
            <div class="contSel">
                <input class="nameFilter" type="text" placeholder="Model">
            </div>
        </div>
    </div>
    <div class="selector">
        <div class="selWrapper">
            <div class="selText">
                Capacity (from)
            </div>
            <div class="contSel">
                <input class="capPicker" type="number" placeholder="">
            </div>
        </div>
    </div>
    <!--
            <div class="selector">
                <div class="selWrapper">
                    <input type="submit" value="filter">
                </div>
            </div>
    -->
</div>
<div class="mainBlock">
    <div class="table">
        <div class="header">
            <div class="cell" id="num">#</div>
            <div class="cell">Model</div>
            <div class="cell">Capacity</div>
        </div>
        <!--
            <c:forEach var="data" items="${dataList}">
                <div class="row">
                    <c:forEach var="cell" items="${data}">
                        <div class="cell">${data}</div>
                    </c:forEach>
                </div>
            </c:forEach>
            -->
    </div>
    <div class="noData">
        Nothing to show
    </div>
</div>
<script>
    $('#header').text('<fmt:message key="nav.planes"/>')
    $('#logo').siblings().eq(2).attr('id','selected');
</script>
</body>

</html>