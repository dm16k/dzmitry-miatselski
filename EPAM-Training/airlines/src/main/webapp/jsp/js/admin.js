function showManage(rowId) {
    document.getElementById(rowId).style.display = 'inline-block';
}

function hideManage(rowId) {
    document.getElementById(rowId).style.display = 'none';
}

//flights.jsp

function showAddFlightRow(show) {
    $('#confirmAdd').css('background', '#e0e0e0').css('cursor', 'default').attr('disabled', 'disabled');
    var row = document.getElementById('add-flight');
    var manages = $('.optional');
    if (!show) {
        row.style.display = 'none';
        manages.css('visibility', 'visible');
    } else {
        row.style.display = 'table-row';
        manages.css('visibility', 'hidden');
    }
}

function addFlight() {
    var airport = $("#airport-select").val();
    var dateFrom = $("#dateFrom").val();
    var dateTo = $("#dateTo").val();
    var flight = {
        code: $("#add-flight-code").val(),
        departureTime: ($("#add-flight-departure").val() /* + ':00'*/ ).replace('T', ' '),
        arrivalTime: ($("#add-flight-arrival").val() /* + ':00'*/ ).replace('T', ' '),
        plane: $("#add-flight-plane").val(),
        crew: $("#add-flight-crew").val(),
        from: $("#add-flight-airport-from").val(),
        to: $("#add-flight-airport-to").val()
    };
    var request = {};
    request.command = 'add_flight';
    request.data = JSON.stringify(flight);
    $.ajax({
        url: "controller",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            window.location.replace('controller?command=filter_flights' +
                '&airport=' + airport +
                '&dateFrom=' + dateFrom +
                '&dateTo=' + dateTo
            );
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function validateAddFlight() {
    var confirm = $('#confirmAdd');
    confirm.attr('disabled', 'disabled');
    confirm.css('cursor', 'default');
    confirm.css('background', '#e0e0e0');
    var codePatt = '^[A-Z]{2}[0-9]{2}$';
    var regCode = new RegExp(codePatt);
    var code = $('#add-flight-code').val();
    var from = $('#add-flight-airport-from').val();
    var to = $('#add-flight-airport-to').val();
    var dep = $('#add-flight-departure').val();
    var arr = $('#add-flight-arrival').val();
    if (regCode.test(code) &&
        from !== to &&
        dep !== '' && arr !== '' &&
        dep < arr) {
        confirm.removeAttr('disabled');
        confirm.css('cursor', 'pointer');
        confirm.css('background', '#0f0');
    }
}

function deleteFlight(ID, innerBtn) {
    var airport = $("#airport-select").val();
    var dateFrom = $("#dateFrom").val();
    var dateTo = $("#dateTo").val();
    var request = {};
    var flight = {
        id: ID
    };
    request.command = 'delete_flight';
    request.data = JSON.stringify(flight);
    $.ajax({
        url: "controller",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            innerBtn.parentElement.parentElement.parentElement.remove();
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function closeEdit() {
    var editRow = $('#edit-flight');
    var parentChildren = editRow.parent().children();
    var row;
    for (var q = 0; q < parentChildren.length; q++) {
        row = parentChildren.eq(q);
        if (row.attr('id') === editRow.attr('id'))
            row.next().css('display', 'table-row');
    }
    editRow.remove();
    $('.optional').css('visibility', 'visible');
}

function editFlight(innerBtn) {
    var edit = $(innerBtn).parents('.row');
    var curr = [];
    var children = $(edit).children();
    curr[0] = children.first().val();
    for (var q = 1; q < children.length; q++) {
        curr[q] = children.eq(q).text().trim();
    }
    var funcUpd = 'updateFlight(' + curr[0] + ')';
    var funcClose = 'closeEdit()';
    /*+ curr[0] +*/
    //')';
    curr[4] = curr[4].replace(' ', 'T');
    curr[5] = curr[5].replace(' ', 'T');

    var editRow = $('#add-flight').clone();
    //input
    $(editRow).children().eq(0).children().eq(0).val(curr[1]).attr('id', 'edit-flight-code').attr('onkeyup', 'validateEditFlight()');
    //select from
    var selects = $(editRow).children().eq(1).children().eq(0).children();
    for (i = 0; i < selects.length; i++) {
        if (selects.eq(i).text() === curr[2])
            selects.eq(i).attr('selected', 'selected');
    }
    $(editRow).children().eq(1).children().eq(0).attr('id', 'edit-flight-airport-from').attr('onchange', 'validateEditFlight()');
    //select to
    selects = $(editRow).children().eq(2).children().eq(0).children();
    for (i = 0; i < selects.length; i++) {
        if (selects.eq(i).text() === curr[3])
            selects.eq(i).attr('selected', 'selected');
    }
    $(editRow).children().eq(2).children().eq(0).attr('id', 'edit-flight-airport-to').attr('onchange', 'validateEditFlight()');
    //input dates
    $(editRow).children().eq(3).children().eq(0).val(curr[4]).attr('id', 'edit-flight-departure').attr('onkeyup', 'validateEditFlight()');
    $(editRow).children().eq(4).children().eq(0).val(curr[5]).attr('id', 'edit-flight-arrival').attr('onkeyup', 'validateEditFlight()');
    //plane
    selects = $(editRow).children().eq(5).children().eq(0).children();
    for (i = 0; i < selects.length; i++) {
        if (selects.eq(i).text() === curr[6])
            selects.eq(i).attr('selected', 'selected');
    }
    $(editRow).children().eq(5).children().eq(0).attr('id', 'edit-flight-plane');
    //crew
    selects = $(editRow).children().eq(6).children().eq(0).children();
    for (var i = 0; i < selects.length; i++) {
        if (selects.eq(i).text() === curr[7])
            selects.eq(i).attr('selected', 'selected');
    }
    $(editRow).children().eq(6).children().eq(0).attr('id', 'edit-flight-crew');

    $(editRow).find('.manBtn').first().attr('onclick', funcUpd).attr('id', 'confirmEdit').removeAttr('disabled').css('cursor', 'pointer').css('background', '#0f0');

    $(editRow).find('.manBtn').first().next().attr('onclick', funcClose);
    $(editRow).insertBefore(edit).css('display', 'table-row').attr('id', 'edit-flight');
    $(edit).css('display', 'none');
    $('.optional').css('visibility', 'hidden');
}

function validateEditFlight() {
    var confirm = $('#confirmEdit');
    confirm.attr('disabled', 'disabled');
    confirm.css('cursor', 'default');
    confirm.css('background', '#e0e0e0');
    var codePatt = '^[A-Z]{2}[0-9]{2}$';
    var regCode = new RegExp(codePatt);
    var code = $('#edit-flight-code').val();
    var from = $('#edit-flight-airport-from').val();
    var to = $('#edit-flight-airport-to').val();
    var dep = $('#edit-flight-departure').val();
    var arr = $('#edit-flight-arrival').val();
    if (regCode.test(code) &&
        from !== to &&
        dep !== '' && arr !== '' &&
        dep < arr) {
        confirm.removeAttr('disabled');
        confirm.css('cursor', 'pointer');
        confirm.css('background', '#0f0');
    }
}

function updateFlight(ID) {
    var airport = $("#airport-select").val();
    var dateFrom = $("#dateFrom").val();
    var dateTo = $("#dateTo").val();
    var flight = {
        id: ID,
        code: $("#edit-flight-code").val(),
        departureTime: ($("#edit-flight-departure").val() /* + ':00'*/ ).replace('T', ' '),
        arrivalTime: ($("#edit-flight-arrival").val() /* + *':00'*/ ).replace('T', ' '),
        plane: $("#edit-flight-plane").val(),
        crew: $("#edit-flight-crew").val(),
        from: $("#edit-flight-airport-from").val(),
        to: $("#edit-flight-airport-to").val()
    };
    var request = {};
    request.command = 'update_flight';
    request.data = JSON.stringify(flight);
    $.ajax({
        url: "controller",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            window.location.replace('controller?command=filter_flights' +
                '&airport=' + airport +
                '&dateFrom=' + dateFrom +
                '&dateTo=' + dateTo
            );
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

//crews.jsp
var lastClickedGear;

function manageCrew(gearID) {
    var gear = $('#' + gearID);
    lastClickedGear = gear;
    gear.css('display', 'none');
    gear.siblings().css('display', 'inline-block');
}

$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        showGear()
    }
});

function showGear() {
//    var gear = lastClickedGear;
    var gear = $('.crewMan');
    gear.css('display', 'inline-block');
    gear.siblings().css('display', 'none');
}

function showAddCrewBlock() {
    $('#add-crew').css('display', 'block');
    $('.optional').css('visibility', 'hidden');
    showGear();
}

function hideAddCrewBlock() {
    $('#add-crew').css('display', 'none');
    $('.optional').css('visibility', 'visible');
}

function addEmpl() {
    var row = $('#add-crew').find('.table').children().eq(0);
    row.children().eq(1).children().eq(1)
        .removeAttr('disabled')
        .removeAttr('style');
    var copyRow = row.clone();
    copyRow.appendTo(row.parent());
    validateAddCrew();
}

function deleteEmpl(but) {
    var table = $(but).parents('.table');
    $(but).parents('.row').remove();
    var rows = table.children();
    //    but.parentElement.parentElement.remove();
    if (rows.length == 1) {
        rows.eq(0)
            .children().eq(1)
            .children().eq(1)
            .attr('disabled', 'disabled').css('background', '#e0e0e0')
            .css('cursor', 'default');
    }
    validateAddCrew();
}

function validateAddCrew() {
    var crewBlock = $('#add-crew');
    var pattern = '[1-9][0-9]{2}';
    var crewReg = new RegExp(pattern);
    var crewNum = crewBlock
        .find('.crew-numb').eq(0).val();
    var empls = crewBlock.find('.add-sel');
    var ids = [];
    for (var i = 0; i < empls.length; i++)
        ids[i] = empls.eq(i).val();
    if (crewReg.test(crewNum) && validateArray(ids)) {
        crewBlock.find('#confirmAdd')
            .removeAttr('style')
            .removeAttr('disabled')
            .css('background', '#0f0');
    } else {
        crewBlock.find('#confirmAdd')
            .css('cursor', 'default')
            .attr('disabled', 'disabled')
            .css('background', '#e0e0e0');
    }
}

function validateArray(arr) {
    for (var k = 0; k < arr.length; k++)
        for (var q = k + 1; q < arr.length; q++)
            if (arr[k] === arr[q])
                return false;
    return true;
}

function addCrew() {
    var crewBlock = $('#add-crew');
    var crewNum = crewBlock
        .find('.crew-numb').eq(0).val();
    var empls = crewBlock.find('.add-sel');
    //    var ids = [];
    var crew = {
        crewId: crewNum,
        employees: []
    };
    for (var i = 0; i < empls.length; i++)
        crew.employees.push({
            id: empls.eq(i).val()
        })

    var request = {};
    request.command = 'add_crew';
    request.data = JSON.stringify(crew);
    $.ajax({
        url: "controller",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            if (data === 1) {
                window.location.reload();
            } else {
                alert('Crew with ID ' +
                    crewNum +
                    ' already exist!')
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function deleteCrew(but) {
    var crewID = $(but).parents('.crewInfo')
        .children().eq(0).val();
    var crew = {
        id: crewID
    };
    var request = {};
    request.command = 'delete_crew';
    request.data = JSON.stringify(crew);
    $.ajax({
        url: "controller",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            if (data === 1) {
                window.location.reload();
            } else {
                alert('Can\'t delete that crew!')
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function addEmplToEdit() {
    var row = $('#edit-crew').find('.table').children().eq(0);
    row.children().eq(1).children().eq(1)
        .removeAttr('disabled')
        .removeAttr('style');
    var copyRow = row.clone();
    copyRow.appendTo(row.parent());
    validateEditCrew();
}

function deleteEmplFromEdit(but) {
    var table = $(but).parents('.table');
    $(but).parents('.row').remove();
    var rows = table.children();
    if (rows.length == 1) {
        rows.eq(0)
            .children().eq(1)
            .children().eq(1)
            .attr('disabled', 'disabled').css('background', '#e0e0e0')
            .css('cursor', 'default');
    }
    validateEditCrew();
}

function validateEditCrew() {
    var crewBlock = $('#edit-crew');
    var pattern = '[1-9][0-9]{2}';
    var crewReg = new RegExp(pattern);
    var crewNum = crewBlock
        .find('.crew-numb').eq(0).val();
    var empls = crewBlock.find('.add-sel');
    var ids = [];
    for (var i = 0; i < empls.length; i++)
        ids[i] = empls.eq(i).val();
    if (crewReg.test(crewNum) && validateArray(ids)) {
        crewBlock.find('#confirmEdit')
            .removeAttr('style')
            .removeAttr('disabled')
            .css('background', '#0f0');
    } else {
        crewBlock.find('#confirmEdit')
            .css('cursor', 'default')
            .attr('disabled', 'disabled')
            .css('background', '#e0e0e0');
    }
}

function closeCrewEdit() {
    var editRow = $('#edit-crew');
    var parentChildren = editRow.parent().children();
    var row;
    for (var q = 0; q < parentChildren.length; q++) {
        row = parentChildren.eq(q);
        if (row.attr('id') === editRow.attr('id'))
            row.next().css('display', 'block');
    }
    editRow.remove();
    $('.optional').css('visibility', 'visible');
}

function editCrew(btn) {
    var crewBlock = $(btn).parents('.crewWrap');
    var current = [];
    var crewID = crewBlock.find('.crew-id').eq(0).val();
    var updFunc = 'updateCrew(' + crewID + ')';
    current[0] = crewBlock.children().eq(0).text().trim();
    var empls = crewBlock.find('.row');
    for (var i = 0; i < empls.length; i++) {
        current[i + 1] = empls.eq(i).children().eq(0).val();
    }
    crewBlock.css('display', 'none');
    var editBlock = $('#add-crew').clone();
//last  ???
    editBlock.attr('id', 'edit-crew');
//last  ???
    editBlock.find('.crew-numb')
        .val(current[0])
        .attr('onkeyup', 'validateEditCrew()');
    editBlock.find('#confirmAdd')
        .attr('id', 'confirmEdit')
        .attr('onclick', updFunc)
        .removeAttr('disabled')
        .removeAttr('style')
        .css('background', '#0f0');
    editBlock.find('.crewAddBtn').eq(1)
        .attr('onclick', 'closeCrewEdit()');
//etalon select empls row
    var selRow = editBlock.find('.row').eq(0).clone();
    selRow.find('.add-sel').eq(0)
        .attr('onchange','validateEditCrew()');
    //try .val(cur[i]) //
    selRow.find('.manBtn').eq(0)
        .attr('onclick','addEmplToEdit()');
    selRow.find('.manBtn').eq(1)
        .removeAttr('disabled')
        .removeAttr('style')
        .attr('onclick','deleteEmplFromEdit(this)');
//etalon select empls row ready
    editBlock.find('.row').remove();
    for(i = 1; i < current.length; i++) {
        var selCopy = selRow.clone();
        //try
        selCopy.find('.add-sel').val(current[i]);
        //try
        selCopy.appendTo(editBlock.find('.table'));
    }
    editBlock.insertBefore(crewBlock);
    editBlock.css('display', 'block');
    crewBlock.css('display','none');
    $('.optional').css('visibility', 'hidden');
    validateEditCrew();
}

function updateCrew(ID) {
    var crewBlock = $('#edit-crew');
    var crewNum = crewBlock
        .find('.crew-numb').eq(0).val();
    var crew = {
        id: ID,
        crewId: crewNum,
        employees: []
    };
    var empls = crewBlock.find('.add-sel');
    for (var i = 0; i < empls.length; i++)
        crew.employees.push({
            id: empls.eq(i).val()
        })
    var request = {};
    request.command = 'update_crew';
    request.data = JSON.stringify(crew);
    $.ajax({
        url: "controller",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            if (data === 1) {
                window.location.reload();
            } else {
                alert('Can\'t change that crew!')
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}
//accounts.jsp
function deleteAcc(but) {
    var accRow = $(but).parents('.row');
    var ID = accRow.children().eq(0).val();
    var account = {
        id: ID
    };
    var request = {};
    request.command = 'delete_account';
    request.data = JSON.stringify(account);
        $.ajax({
        url: "controller",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            if (data === 1) {
                accRow.remove();
            } else {
                alert('Can\'t delete that account!');
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
            window.location.reload();
        }
    })
}

function makeAdmin(btn) {
    var accRow = $(btn).parents('.row');
    var ID = accRow.children().eq(0).val();
    var Login = accRow.children().eq(1).text();
    var Pass = accRow.children().eq(2).text();
    var Name = accRow.children().eq(3).text();
    var account = {
        id: ID,
        login: Login,
        password: Pass,
        name: Name,
        admin: true
    };
    var request = {};
    request.command = 'update_account';
    request.data = JSON.stringify(account);
        $.ajax({
        url: "controller",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            if (data === 1) {
                window.location.reload();
            } else {
                alert('Can\'t set admin rights for this account!');
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}