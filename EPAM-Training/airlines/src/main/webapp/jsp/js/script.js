function setDate() {
    var currentDate = new Date();
    document.getElementById('dateFrom').valueAsDate = currentDate;
    document.getElementById('dateTo').valueAsDate =
        currentDate.setDate(currentDate.getDate() + 30);
}

function setDateTo() {
    var fromDate = document.getElementById('dateFrom').valueAsDate;
    document.getElementById('dateTo').valueAsDate = fromDate.setDate(fromDate.getDate() + 30);
}

var vis = 'visible';
var hid = 'hidden';
var v = vis;

function showMask(show) {
    if (show === true) {
        v = vis;
    } else {
        v = hid;
        showLeftBar(show);
        showSignIn(show);
        document.getElementById('navBar').style.boxShadow = '0 3px 5px rgba(0, 0, 0, 0.26)';
    }
    document.getElementById('mask').style.visibility = v;
    document.getElementById('mask').style.zIndex = 4;
}

function showLeftBar(show) {
    var leftBar = document.getElementById('leftBar');
    if (show === true) {
        v = vis;
        leftBar.className += ' active-leftBar';
        showMask(show);
    } else {
        v = hid;
        leftBar.className = 'leftBar';
    }
    document.getElementById('leftBar').style.visibility = v;
}

var originalClass = 'signIn';

function showSignIn(show) {
    var signIn = document.getElementById('signIn');
    if (show === true) {
        originalClass = signIn.className;
        v = vis;
        signIn.className += ' active-signIn';
        showMask(show);
    } else {
        v = hid;
        signIn.className = originalClass;
    }
    document.getElementById('navBar').style.boxShadow = '0 1px 2px rgba(0, 0, 0, 0.26)';
    document.getElementById('mask').style.zIndex = 2;
    document.getElementById('signIn').style.visibility = v;
}

function submitForm(formId) {
    document.getElementById(formId).submit();
}

var validInp = '1px solid rgb(0, 255, 0)';
var invalidInp = '1px solid #e73d3d';
var cl;

function checkLogin() {
    var pattern = '[A-Za-z][0-9A-Za-z_\-]*$';
    var reg = new RegExp(pattern);
    var logInp = $('#reg-log');
    var logVal = logInp.val();
    if (reg.test(logVal)) {
        cl = validInp;
    } else {
        cl = invalidInp;
    }
    logInp.css('border', cl);
    activateRegister();
}

function checkName() {
    var pattern = '(([A-ZА-ЯЁ][a-zа-яё]*)(\s[A-ZА-ЯЁ][a-zа-яё]*)*[a-zа-яё]$)';
    var reg = new RegExp(pattern);
    var nameInp = $('#reg-name');
    var nameVal = nameInp.val();
    if (reg.test(nameVal)) {
        cl = validInp;
    } else {
        cl = invalidInp;
    }
    nameInp.css('border', cl);
    activateRegister();
}

function checkPassword() {
    var pattern = '^[A-Za-z0-9_\-]+$';
    var reg = new RegExp(pattern);
    var passInp = $('#reg-pass');
    var passVal = passInp.val();
    if (reg.test(passVal)) {
        cl = validInp;
    } else {
        cl = invalidInp;
    }
    passInp.css('border', cl);
    activateRegister();
}

function activateRegister() {
    var form = $('#register-form');
    var logInp = $('#reg-log');
    var passInp = $('#reg-pass');
    var nameInp = $('#reg-name');
    if (logInp.css('border') === validInp &&
        passInp.css('border') === validInp &&
        nameInp.css('border') === validInp) {
        $('#register').removeAttr('style').removeAttr('disabled');
    } else {
        $('#register')
            .css('background', '#e0e0e0')
            .css('cursor', 'default')
            .attr('disabled', 'disabled');
    }
}

function validateForm() {
    var form = $('#register-form');
    var logInp = $('#reg-log');
    var passInp = $('#reg-pass');
    var nameInp = $('#reg-name');
    //    if (logInp.css('border') === validInp &&
    //        passInp.css('border') === validInp &&
    //        nameInp.css('border') === validInp) {
    registerUser(logInp.val(), passInp.val(), nameInp.val());
    //form.submit();
    //    }
}

function registerUser(log, pass, nam) {
    var user = {
        login: log,
        password: pass,
        name: nam
    };
    var request = {};
    request.command = 'register';
    request.data = JSON.stringify(user);
    $.ajax({
        url: "controller",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            if (data === 1) {
                alert('Registration completed!\nYou can sign in using entered credentials!');
                setTimeout(function () {
                    submitForm('forward');
                }, 1000);
            } else {
                alert('User with login ' + log + ' already exists!')
            }
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

function changeLocale() {
    var locale;
    var loc = $('#locale');
    if (loc.attr('class') == 'locale rus')
        locale = 'ru_RU';
    else
        locale = 'en_US';
    var request = {};
    request.command = 'change_locale';
    request.data = JSON.stringify(locale);
    $.ajax({
        url: "controller",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            window.location.reload();
            //TODO grant access for SIGN_IN for authorized users and SIGN_OUT for guests || exclude command-free URLs
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er: " + er);
        }
    })
}

var currLoc;

function viewPrevLocale() {
    var loc = $('#locale');
    currLoc = loc.attr('class');
    var locClass = loc.attr('class');
    if (locClass === 'locale rus')
        locClass = 'locale usa';
    else
        locClass = 'locale rus';
    loc.attr('class', locClass);
}

function setCurrLocale() {
    $('#locale').attr('class', currLoc);
}