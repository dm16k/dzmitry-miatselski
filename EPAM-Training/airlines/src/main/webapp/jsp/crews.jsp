<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Crews</title>
    <link rel="stylesheet" href="jsp/css/style.css"/>
</head>
<body>
<%@include file="navbar.jsp" %>
<div class="crewsBlock">
    <c:if test="${user.status == 'dispatcher'}">
        <div id="add-crew" class="crewWrap" <c:if test="${!empty crews}">style="display: none"</c:if><c:if test="${empty crews}">
                    style="display: block;"</c:if>>
            <div class="crewInfo">
                <input class="crew-numb"
                       onkeyup="validateAddCrew()"
                       type="text"
                       placeholder="<fmt:message key="crews.placeh.crewno"/>"
                       maxlength="3">
                <button id="confirmAdd"
                        class="crewAddBtn"
                        onclick="addCrew()"
                        style="background: #e0e0e0;
                               cursor: default"
                        disabled>✓
                </button>
                <button class="crewAddBtn"
                        onclick="hideAddCrewBlock()"
                        >✕
                </button>
            </div>
            <div class="table">
                <div class="row">
                    <div class="cell" style="width: 324px">
                        <select class="add-sel"
                                onchange="validateAddCrew()">
                                <%--<jsp:useBean id="employees" type="java.util.List<by.epam.task6.entity.impl.Employee>" scope="request"/>--%>
                            <c:forEach var="employee" items="${employees}">
                                <option value="${employee.id}">${employee}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="cell" style="padding: 0 6px">
                        <input class="manBtn"
                               type="submit"
                               value="+"
                               onclick="addEmpl()">
                        <input class="manBtn"
                               type="submit"
                               value="-"
                               disabled
                               style="background: #e0e0e0;
                                      cursor: default"
                               onclick="deleteEmpl(this)">
                    </div>
                </div>
            </div>
        </div>
    </c:if>
    <c:forEach var="crew" items="${crews}" varStatus="loop">
        <div class="crewWrap"
                <c:if test="${user.status == 'dispatcher'}">
                    onmouseover="showManage('manage${loop.index + 1}')"
                    onmouseout="hideManage('manage${loop.index + 1}')"
                </c:if>>
            <div class="crewInfo">
                <input class="crew-id" type="hidden" value="${crew.id}">
                    ${crew.crewId}
                <c:if test="${user.status == 'dispatcher'}">
                    <div class="optional crewOpt"
                         id="manage${loop.index + 1}"
                         style="display: none;">
                        <button id="gear${loop.index + 1}"
                                onclick="manageCrew('gear${loop.index + 1}')"
                                class="manBtn crewMan">
                        </button>
                        <input class="manBtn crewEdit"
                               type="submit"
                               value="+"
                               onclick="showAddCrewBlock()"
                               style="display:none">
                        <input class="manBtn crewEdit"
                               type="submit"
                               value="-"
                               onclick="deleteCrew(this)"
                               style="display:none">
                        <input class="manBtn crewEdit"
                               type="submit"
                               value="✎"
                               onclick="editCrew(this)"
                               style="display:none">
                    </div>
                </c:if>
            </div>
            <div class="table">
                <c:forEach var="employee" items="${crew.employees}">
                    <div class="row">
                        <input type="hidden" value="${employee.id}">

                        <div class="cell text">${employee.firstName}</div>
                        <div class="cell text">${employee.lastName}</div>
                        <div class="cell text">${employee.profession}</div>
                        <div class="cell">${employee.dob}</div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </c:forEach>
</div>
<script src="jsp/js/jquery-min.js"></script>
<script src="jsp/js/script.js"></script>
<c:if test="${user.status == 'dispatcher'}">
    <script src="jsp/js/admin.js"></script>
</c:if>
<script>
    $('#header').text('<fmt:message key="crews.name"/>')
    $('#logo').siblings().eq(1).attr('id', 'selected');
</script>
</body>
</html>
