<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="ct" uri="/WEB-INF/tld/welctag.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="navBar" id="navBar">
    <button class="hamburger" onclick="showLeftBar(true)">Show</button>
    <div id="header" class="headerText">Flights</div>
    <c:set var="locale" value="${locale}"/>
    <fmt:setLocale value="${locale}"/>
    <fmt:setBundle basename="messages"/>
    <button id="locale"
            class="locale <c:if test="${locale == 'ru_RU'}">rus</c:if><c:if test="${locale == 'en_US'}">usa</c:if>"
            title="<fmt:message key="nav.title"/>"
            onclick="changeLocale()"
            onmouseover="viewPrevLocale()"
            onmouseout="setCurrLocale()">
        Change Language
    </button>
    <button class="personal" onclick="showSignIn(true)"><fmt:message key="nav.signin"/></button>
</div>
<jsp:useBean id="user" type="by.epam.task6.entity.impl.Account" scope="session"/>
<c:set var="authResult" value="${(!empty user)? (user.status != 'guest') : false}"/>
<c:if test="${authResult}">
    <div class="signIn authorized" id="signIn">
        <form action="controller" method="post" id="sign-out-form">
            <input type="hidden" name="command" value="sign_out">
        </form>
        <c:if test="${user.admin}">
            <form action="controller" method="get" id="accounts-form">
                <input type="hidden" name="command" value="open_accounts">
            </form>
        </c:if>
        <div class="elSign">
            <ct:welcome name="${user.name}" status="${user.status}"/>
                <%--Hi, ${user.name}!--%>
        </div>
        <input class="${user.admin ? 'subm' : 'submOut'}" form="sign-out-form" value="<fmt:message key="nav.signout"/>"
               type="submit"/>
        <c:if test="${user.admin}">
            <input id="reg-btn" class="register" form="accounts-form"
                   value="<fmt:message key="nav.register"/>" type="submit">
        </c:if>
    </div>
</c:if>
<c:if test="${!authResult}">
    <div class="signIn" id="signIn">
        <form action="controller" method="post" id="sign-in-form">
            <input type="hidden" name="command" value="sign_in">
        </form>
        <form action="controller" method="get" id="register-form">
            <input type="hidden" name="command" value="open_register">
        </form>
        <div class="elSign">
            <input class="input" form="sign-in-form" id="log" name="login" type="text"
                   placeholder="<fmt:message key="nav.placeh.log"/>" required/>
        </div>
        <div class="elSign">
            <input class="input" form="sign-in-form" id="pass" name="password" type="password"
                   placeholder="<fmt:message key="nav.placeh.pas"/>"
                   required>
        </div>
        <input class="subm" form="sign-in-form" value="<fmt:message key="nav.signin"/>" type="submit">
        <input id="reg-btn" class="register" form="register-form" value="<fmt:message key="nav.signup"/>" type="submit">
    </div>
</c:if>
<div class="shadowMask" id="mask" onclick="showMask(false)"></div>
<div class="leftBar" id="leftBar">
    <div class="barBlock" id="logo">MySQLAirlines</div>
    <div class="barBlock" onclick="submitForm('link-flights')">
        <form action="controller" id="link-flights" method="get">
            <input type="hidden" name="command" value="open_flights">
        </form>
        <fmt:message key="nav.flights"/>
    </div>
    <c:if test="${authResult}">
        <div class="barBlock" onclick="submitForm('link-crews')">
            <form action="controller" id="link-crews" method="get">
                <input type="hidden" name="command" value="open_crews">
            </form>
            <fmt:message key="nav.crews"/>
        </div>
        <%--<div class="barBlock" onclick="submitForm('link-planes')">
            <form action="controller" id="link-planes" method="get">
                <input type="hidden" name="command" value="open_planes">
            </form>
            <fmt:message key="nav.planes"/>
        </div>
        <div class="barBlock" onclick="submitForm('link-employees')">
            <form action="controller" id="link-employees" method="get">
                <input type="hidden" name="command" value="open_employees">
            </form>
            <fmt:message key="nav.employees"/>
        </div>--%>
    </c:if>
</div>
