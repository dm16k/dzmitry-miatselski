<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Welcome to MySQLAirlines</title>
    <link rel="stylesheet" href="jsp/css/style.css"/>
</head>
<body>
<%@include file="navbar.jsp" %>
<div class="selectors">
    <form id="load-airports" action="controller" method="post">
        <input type="hidden" name="command" value="load_airports">
    </form>
    <form id="filter-flights" action="controller" method="get">
        <input type="hidden" name="command" value="filter_flights">
    </form>
    <div class="selector">
        <div class="selWrapper">
            <div class="selText">
                <fmt:message key="flights.sel.airport"/>
            </div>
            <div class="contSel">
                <select class="select" form="filter-flights" id="airport-select" name="airport"
                        onchange="submitForm('filter-flights')"
                        >
                    <option value="0"><fmt:message key="flights.sel.any"/></option>
                    <c:forEach var="airport" items="${airports}">
                        <option value="${airport.id}" ${airport.id == airportSelected ? 'selected="selected"' : ''}>${airport}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
    </div>
    <div class="selector">
        <div class="selWrapper">
            <div class="selText">
                <fmt:message key="flights.sel.from"/>
            </div>
            <div class="contSel">
                <input id="dateFrom" class="datePicker" name="dateFrom" form="filter-flights" type="date"
                       onchange="setDateTo();submitForm('filter-flights')" value="${dateFrom}">
            </div>
        </div>
    </div>
    <div class="selector">
        <div class="selWrapper">
            <div class="selText">
                <fmt:message key="flights.sel.to"/>
            </div>
            <div class="contSel">
                <input id="dateTo" class="datePicker" name="dateTo" form="filter-flights" type="date"
                       onchange="submitForm('filter-flights')" value="${dateTo}">
            </div>
        </div>
    </div>
</div>
<div class="mainBlock">
    <div class="table">
        <div class="header">
            <div class="cell code"><fmt:message key="flights.col.flight"/></div>
            <div class="cell airport"><fmt:message key="flights.col.from"/></div>
            <div class="cell airport"><fmt:message key="flights.col.to"/></div>
            <div class="cell date"><fmt:message key="flights.col.departure"/></div>
            <div class="cell date"><fmt:message key="flights.col.arrival"/></div>
            <div class="cell plane"><fmt:message key="flights.col.plane"/></div>
            <div class="cell crew"><fmt:message key="flights.col.crew"/></div>
        </div>
        <c:if test="${user.admin}">
            <c:set var="planes" value="${requestScope.get('planes')}"/>
            <c:set var="crews" value="${requestScope.get('crews')}"/>
            <div id="add-flight" class="row" style="display: none">
                <div class="cell code text">
                    <input id="add-flight-code"
                           onkeyup="validateAddFlight()"
                           type="text"
                           style="width:36px;"
                           placeholder="Code" required
                           pattern="[A-Z]{2}[0-9]{2}">
                </div>
                <div class="cell airport text">
                    <select id="add-flight-airport-from"
                            onchange="validateAddFlight()"
                            style="width: 321px;">
                        <c:forEach var="airport" items="${airports}">
                            <option value="${airport.id}">${airport}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="cell airport text">
                    <select id="add-flight-airport-to"
                            onchange="validateAddFlight()"
                            style="width: 321px;">
                        <c:forEach var="airport" items="${airports}">
                            <option value="${airport.id}">${airport}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="cell date">
                    <input id="add-flight-departure"
                           onkeyup="validateAddFlight()"
                           style="width: 120px;"
                           type="datetime-local">
                </div>
                <div class="cell date">
                    <input id="add-flight-arrival"
                           onkeyup="validateAddFlight()"
                           style="width: 120px;"
                           type="datetime-local">
                </div>
                <div class="cell plane text">
                    <select id="add-flight-plane" style="width: 113px;">
                        <c:forEach var="plane" items="${planes}">
                            <option value="${plane.id}">${plane}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="cell crew text">
                    <select id="add-flight-crew">
                        <c:forEach var="crew" items="${crews}">
                            <option value="${crew.id}">${crew}</option>
                        </c:forEach>
                    </select>
                    <button id="confirmAdd" class="manBtn" onclick="addFlight()">✓</button>
                    <button class="manBtn" onclick="showAddFlightRow(false)">✕</button>
                </div>
            </div>
        </c:if>
        <c:forEach var="flight" items="${flights}" varStatus="loop">
            <div class="row"
                    <c:if test="${user.admin}">
                        onmouseover="showManage('manage<c:out value="${loop.index + 1}"/>')"
                        onmouseout="hideManage('manage<c:out value="${loop.index + 1}"/>')"
                    </c:if>
                    >
                <input type="hidden" name="flight-id" value="${flight.id}">

                <div class="cell code text">${flight.code}</div>
                <div class="cell airport text">${flight.from}</div>
                <div class="cell airport text">${flight.to}</div>
                <div class="cell date"><fmt:formatDate value="${flight.departureTime}" pattern="yyyy-MM-dd HH:mm"/></div>
                <div class="cell date"><fmt:formatDate value="${flight.arrivalTime}" pattern="yyyy-MM-dd HH:mm"/></div>
                <div class="cell plane text">${flight.plane}</div>
                <div class="cell crew crewNumber">${flight.crew}
                    <c:if test="${user.admin}">
                        <div class="optional" id="manage<c:out value="${loop.index + 1}"/>" style="display: none">
                            <input class="manBtn" type="submit" value="+" onclick="showAddFlightRow(true)">
                            <input class="manBtn" type="submit" value="-" onclick="deleteFlight(${flight.id}, this)">
                            <input class="manBtn" type="submit" value="✎" onclick="editFlight(this)">
                        </div>
                    </c:if>
                </div>
            </div>
        </c:forEach>
    </div>
    <c:if test="${empty flights}">
        <div class="noData">
            <fmt:message key="nodata"/>
            <c:if test="${user.admin}">
                <input class="manBtn" type="submit" value="+" onclick="showAddFlightRow(true)"
                       style="position: relative;float: right;top: 2px;right: 0;">
            </c:if>
        </div>
    </c:if>
</div>
<script src="jsp/js/jquery-min.js"></script>
<script src="jsp/js/script.js"></script>
<c:if test="${user.admin}">
    <script src="jsp/js/admin.js"></script>
</c:if>
<script>
    $('#header').text('<fmt:message key="flights.name"/>')
    $('#logo').siblings().eq(0).attr('id','selected');
</script>
</body>
</html>