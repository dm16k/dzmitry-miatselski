<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Oops!</title>
    <style>
        .info {
            position: fixed;
            top: 40%;
            text-align: center;
            color: #fff;
            font-size: 30px;
            width: 100%;
            text-shadow: #000 1px 0 0, #000 0 1px 0, #000 -1px 0 0, #000 0 -1px 0;
        }

        body {
            background: url("images/failed.jpg");
            background-size: 100%;
        }
    </style>
</head>
<body>
<div class="info">
    Sorry, but you have no access to view this page.<br>
    You'll be redirected to the main page in a few seconds.
    <form id="forward" action="../controller" method="get">
        <input type="hidden" name="command" value="open_flights">
    </form>
</div>
<script src="js/script.js"></script>
<script>
    setTimeout(function () {
        submitForm('forward');
    }, 5000);
</script>
</body>
</html>
