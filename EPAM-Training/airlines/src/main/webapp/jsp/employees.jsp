<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head lang="ru-RU">
    <meta charset="UTF-8">
    <title>Welcome to MySQLAirlines</title>
    <link rel="stylesheet" href="jsp/css/style.css"/>
    <script src="jsp/js/script.js"></script>
</head>

<body onload="setDate()">
<%@include file="navbar.jsp"%>
<div class="selectors">
    <div class="selector">
        <div class="selWrapper">
            <div class="contSel">
                <input class="nameFilter" type="text" placeholder="Name">
            </div>
        </div>
    </div>
    <div class="selector">
        <div class="selWrapper">
            <div class="selText">
                Profession
            </div>
            <div class="contSel">
                <select class="select">
                    <option>all</option>
                    <option>pilot</option>
                    <option>steward</option>
                </select>
            </div>
        </div>
    </div>
    <div class="selector">
        <div class="selWrapper">
            <div class="selText">
                Min. age
            </div>
            <div class="contSel">
                <input class="capPicker" type="number">
            </div>
        </div>
    </div>
</div>
<div class="mainBlock">
    <div class="table">
        <div class="header">
            <div class="cell" id="num">#</div>
            <div class="cell">Name</div>
            <div class="cell">Profession</div>
            <div class="cell">Birth</div>
        </div>
        <!--
        <c:forEach var="data" items="${dataList}">
            <div class="row">
                <c:forEach var="cell" items="${data}">
                    <div class="cell">${data}</div>
                </c:forEach>
            </div>
        </c:forEach>
        -->
    </div>
    <div class="noData">
        Nothing to show
    </div>
</div>
<script>
    $('#header').text('<fmt:message key="nav.employees"/>')
    $('#logo').siblings().eq(3).attr('id','selected');
</script>
</body>

</html>