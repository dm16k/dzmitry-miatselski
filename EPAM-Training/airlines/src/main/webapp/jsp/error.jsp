<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Oops!</title>
    <link href="jsp/css/style.css">
</head>
<body style="background-color: cornflowerblue;">
<c:set var="error" value="${requestScope.get('exception')}"/>
<div style="margin: 40%; font-size:14px;">${error}</div>
</body>
</html>
