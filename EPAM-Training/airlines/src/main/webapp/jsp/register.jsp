<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Registration</title>
    <link rel="stylesheet" href="jsp/css/style.css"/>
    <script src="jsp/js/jquery-min.js"></script>
    <script src="jsp/js/script.js"></script>
    <script src="jsp/js/admin.js"></script>
</head>
<body>
<%@include file="navbar.jsp" %>
<div class="registerBlock">
    <form id="register-form" action="controller" method="post">
        <div class="inpWrapper">
            <div class="login">
                <input id="reg-log" maxlength="50" onkeyup="checkLogin()" type="text"
                       placeholder="<fmt:message key="register.placeh.log"/>"
                       pattern="[A-Za-z][0-9A-Za-z_-]*$" autocomplete="off" required>
            </div>
            <div class="password">
                <input id="reg-pass" maxlength="50" onkeyup="checkPassword()" type="password"
                       placeholder="<fmt:message key="register.placeh.pas"/>"
                       pattern="^[A-Za-z0-9_-]+$" autocomplete="off" required>
            </div>
        </div>
        <div class="inpWrapper">
            <div class="name">
                <input id="reg-name" maxlength="100" onkeyup="checkName()" type="text"
                       placeholder="<fmt:message key="register.placeh.nam"/>"
                       pattern="(([A-ZА-ЯЁ][a-zа-яё]*)(\s[A-ZА-ЯЁ][a-zа-яё]*)*[a-zа-яё]$)" required>
            </div>
        </div>
        <input id="register"
               class="register"
               onclick="validateForm()"
               disabled="disabled"
               style="background: #e0e0e0; cursor: default;"
               type="button" value="<fmt:message key="register.reg"/>">
    </form>
    <form id="forward" action="controller" method="get">
        <input type="hidden" name="command" value="open_flights">
    </form>
</div>
<script>
    $('#reg-btn').css('background', '#e0e0e0').css('cursor', 'default').attr('disabled', 'disabled');
    $('#header').text('<fmt:message key="register.name"/>');
</script>
</body>
</html>
