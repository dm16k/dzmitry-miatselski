<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Accounts</title>
    <link rel="stylesheet" href="jsp/css/style.css"/>
</head>
<body>
<%@include file="navbar.jsp" %>
<div class="accsBlock">
    <div class="table">
        <div class="header acchead">
            <div class="cell text"><fmt:message key="accounts.login"/></div>
            <div class="cell text"><fmt:message key="accounts.pass"/></div>
            <div class="cell text"><fmt:message key="accounts.nam"/></div>
            <div class="cell text" style="width: 93px"><fmt:message key="accounts.rights"/></div>
        </div>
        <%--<jsp:useBean id="accounts" type="java.util.List<by.epam.task6.entity.impl.Account>"/>--%>
        <c:forEach var="account" items="${accounts}" varStatus="loop">
            <div class="row"
                 onmouseout="hideManage('manage${loop.index+1}')"
                 onmouseover="showManage('manage${loop.index+1}')">
                <input type="hidden" value="${account.id}">

                <div class="cell text">${account.login}</div>
                <div class="cell text">${account.password}</div>
                <div class="cell text">${account.name}</div>
                <div class="cell text">
                    <div class="text-wrapper">
                        <fmt:message key="accounts.adm.${account.admin}"/>
                    </div>
                    <div class="optional" id="manage${loop.index+1}">
                        <input class="manBtn" type="submit" value="-"
                               <c:if test="${!account.admin}">onclick="deleteAcc(this)"</c:if>
                               <c:if test="${account.admin}">style="background: #e0e0e0; cursor: default;"</c:if>>
                        <input class="manBtn" type="submit" value="A"
                        <c:if test="${!account.admin}">
                               onclick="makeAdmin(this)"
                        </c:if>
                        <c:if test="${account.admin}">
                               style="background: #e0e0e0; cursor: default;"
                        </c:if>>
                    </div>
                </div>
            </div>
        </c:forEach>
        <c:if test="${empty accounts}">
            <div class="noData">
                <fmt:message key="nodata"/>
            </div>
        </c:if>
    </div>
</div>
<script src="jsp/js/jquery-min.js"></script>
<script src="jsp/js/script.js"></script>
<script src="jsp/js/admin.js"></script>
<script>
    $('#header').text('<fmt:message key="accounts.name"/>')
</script>
</body>
</html>
