DROP SCHEMA IF EXISTS mysql_airlines;

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mysql_airlines
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mysql_airlines
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mysql_airlines`
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_general_ci;
USE `mysql_airlines`;

-- -----------------------------------------------------
-- Table `mysql_airlines`.`professions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mysql_airlines`.`professions` (
  `id`   INT         NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC)
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mysql_airlines`.`employees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mysql_airlines`.`employees` (
  `id`         INT         NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(10) NOT NULL,
  `last_name`  VARCHAR(10) NOT NULL,
  `profession` INT         NOT NULL,
  `dob`        DATE        NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_employee_professions1_idx` (`profession` ASC),
  CONSTRAINT `fk_employee_professions1`
  FOREIGN KEY (`profession`)
  REFERENCES `mysql_airlines`.`professions` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mysql_airlines`.`planes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mysql_airlines`.`planes` (
  `id`             INT          NOT NULL AUTO_INCREMENT,
  `model`          VARCHAR(20)  NOT NULL,
  `passengers_cap` INT UNSIGNED NOT NULL,
  UNIQUE INDEX `model_UNIQUE` (`model` ASC),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mysql_airlines`.`crews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mysql_airlines`.`crews` (
  `id`      INT NOT NULL AUTO_INCREMENT,
  `crew_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `crew_id` (`crew_id` ASC),
  UNIQUE INDEX `crew_id_UNIQUE` (`crew_id` ASC)
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mysql_airlines`.`airports`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mysql_airlines`.`airports` (
  `id`   INT         NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC)
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mysql_airlines`.`flights`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mysql_airlines`.`flights` (
  `id`             INT         NOT NULL AUTO_INCREMENT,
  `code`           VARCHAR(45) NOT NULL,
  `departure_time` DATETIME    NOT NULL DEFAULT NOW(),
  `arrival_time`   DATETIME    NOT NULL DEFAULT NOW(),
  `plane`          INT         NOT NULL,
  `from`           INT         NOT NULL,
  `to`             INT         NOT NULL,
  `crew`           INT         NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_flights_planes1_idx` (`plane` ASC),
  INDEX `fk_flights_airports1_from_idx` (`from` ASC),
  INDEX `fk_flights_airports1_to_idx` (`to` ASC),
  INDEX `fk_flights_crews1_idx` (`crew` ASC),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC),
  CONSTRAINT `fk_flights_planes1`
  FOREIGN KEY (`plane`)
  REFERENCES `mysql_airlines`.`planes` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_flights_airports1_from`
  FOREIGN KEY (`from`)
  REFERENCES `mysql_airlines`.`airports` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_flights_airports1_to`
  FOREIGN KEY (`to`)
  REFERENCES `mysql_airlines`.`airports` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_flights_crews1`
  FOREIGN KEY (`crew`)
  REFERENCES `mysql_airlines`.`crews` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mysql_airlines`.`staff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mysql_airlines`.`staff` (
  `id`       INT          NOT NULL AUTO_INCREMENT,
  `login`    VARCHAR(50)  NOT NULL,
  `password` VARCHAR(50)  NOT NULL,
  `name`     VARCHAR(100) NOT NULL,
  `admin`    TINYINT(1)   NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC)
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mysql_airlines`.`crews_has_employees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mysql_airlines`.`crews_has_employees` (
  `crews_id`     INT NOT NULL,
  `employees_id` INT NOT NULL,
  PRIMARY KEY (`crews_id`, `employees_id`),
  INDEX `fk_crews_has_employees_employees1_idx` (`employees_id` ASC),
  INDEX `fk_crews_has_employees_crews1_idx` (`crews_id` ASC),
  CONSTRAINT `fk_crews_has_employees_crews1`
  FOREIGN KEY (`crews_id`)
  REFERENCES `mysql_airlines`.`crews` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_crews_has_employees_employees1`
  FOREIGN KEY (`employees_id`)
  REFERENCES `mysql_airlines`.`employees` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;

GRANT ALL PRIVILEGES ON mysql_airlines.* TO dmitry@'%'
IDENTIFIED BY '105638qpalFJ';
#admin
INSERT INTO staff (login, password, name, admin) VALUES ('admin', '123', 'Metelsky Dmitry', TRUE);
# airports
INSERT INTO airports (name, city) VALUES ('Hartsfield Jackson International Airport', 'Atlanta, USA');
INSERT INTO airports (name, city) VALUES ('Beijing Capital International Airport', 'Beijing, China');
INSERT INTO airports (name, city) VALUES ('Dallas Fort Worth International Airport', 'Dallas, USA');
INSERT INTO airports (name, city) VALUES ('Tokyo International Airport', 'Tokyo, Japan');
INSERT INTO airports (name, city) VALUES ('Los Angeles International Airport', 'Los Angeles, USA');
INSERT INTO airports (name, city) VALUES ('Minsk-2 Airport', 'Minsk, Belarus');
INSERT INTO airports (name, city) VALUES ('George Bush Intercontinental Airport', 'Houston, USA');
INSERT INTO airports (name, city) VALUES ('London Heathrow Airport', 'London, UK');
INSERT INTO airports (name, city) VALUES ('Chicago O''Hare International Airport', 'Chicago, USA');
INSERT INTO airports (name, city) VALUES ('John F Kennedy International Airport', 'New-York, USA');
INSERT INTO airports (name, city) VALUES ('Charles de Gaulle International Airport', 'Paris, France');
# profession
INSERT INTO professions (name) VALUES ('Pilot');
INSERT INTO professions (name) VALUES ('Steward');
# employees
INSERT INTO employees (first_name, last_name, profession, dob) VALUES ('Дмитрий', 'Агафонов', '1', '1983-06-12');
INSERT INTO employees (first_name, last_name, profession, dob) VALUES ('Дарья', 'Машишкина', '2', '1988-04-05');
INSERT INTO employees (first_name, last_name, profession, dob) VALUES ('Joan', 'Roaling', '2', '1993-05-08');
INSERT INTO employees (first_name, last_name, profession, dob) VALUES ('Анна', 'Каренина', '2', '1995-08-15');
INSERT INTO employees (first_name, last_name, profession, dob) VALUES ('Lisa', 'McKinney', '2', '1989-10-6');
INSERT INTO employees (first_name, last_name, profession, dob) VALUES ('Виктор', 'Панов', '1', '1976-12-23');
INSERT INTO employees (first_name, last_name, profession, dob) VALUES ('John', 'Kennedy', '1', '1972-08-21');
INSERT INTO employees (first_name, last_name, profession, dob) VALUES ('Kyle', 'Reese', '2', '1992-03-26');
INSERT INTO employees (first_name, last_name, profession, dob) VALUES ('Jack', 'McMillan', '1', '1962-03-06');
INSERT INTO employees (first_name, last_name, profession, dob) VALUES ('John', 'Shepard', '1', '1985-12-03');
# planes
INSERT INTO planes (model, passengers_cap) VALUES ('Boeing 747-400', 624);
INSERT INTO planes (model, passengers_cap) VALUES ('Airbus A380-800', 757);
INSERT INTO planes (model, passengers_cap) VALUES ('Boeing 737-300', 149);
