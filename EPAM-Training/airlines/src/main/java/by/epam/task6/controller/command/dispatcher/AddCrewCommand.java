package by.epam.task6.controller.command.dispatcher;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.impl.Crew;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The AddCrewCommand class creates new crew entry in DB
 *
 * @author dmitr_000
 * @version 1.00 27-May-15
 */
public class AddCrewCommand implements ICommand {
    private final static String JSON = "json";

    /**
     * Creates new crew entry in DB
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            Crew crew = mapper.readValue(json, Crew.class);
            int res = 1;
            try {
                MySQLDaoFactory.getInstance().getDao(MySQLDaoFactory.DaoType.CREWS.name()).create(crew);
            } catch (DAOException e) {
                res = -1;
            }
            mapper.writeValue(response.getOutputStream(), res);
        } catch (IOException e) {
            throw new CommandException(e);
        }
        return null;
    }
}
