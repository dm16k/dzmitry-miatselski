package by.epam.task6.controller.command.admin;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.impl.Flight;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * The UpdateFlightCommand class updates flight entry in DB
 *
 * @author dmitr_000
 * @version 1.00 24-May-15
 */
public class UpdateFlightCommand implements ICommand {
    private final static String JSON = "json";

    /**
     * Updates flight entry in DB.
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm"));
        try {
            Flight flight = mapper.readValue(json, Flight.class);
            MySQLDaoFactory.getInstance().getDao(MySQLDaoFactory.DaoType.FLIGHTS.name()).update(flight);
            mapper.writeValue(response.getOutputStream(), -1);
        } catch (IOException | DAOException e) {
            throw new CommandException(e);
        }
        return null;
    }
}
