package by.epam.task6.entity.impl;

import by.epam.task6.entity.IEntity;

/**
 * The Profession class represents entries of 'professions' table in DB.
 * It provides getters and setters for every field of each entry.
 *
 * @author dmitr_000
 * @version 1.00 13-May-15
 */
public class Profession implements IEntity {
    private int id;
    private String name;

    /**
     * Default constructor
     */
    public Profession() {
    }

    /**
     * Constructor
     *
     * @param name specifies profession name
     */
    public Profession(String name) {
        this.name = name;
    }

    /**
     * Constructor
     *
     * @param id   specifies profession ID
     * @param name specifies profession name
     */
    public Profession(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Gets profession ID
     *
     * @return profession ID
     */
    public int getId() {
        return id;
    }

    /**
     * Sets profession ID
     *
     * @param id specifies profession ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets profession name
     *
     * @return profession name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets profession name
     *
     * @param name specifies profession name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns a hash code for all object fields
     *
     * @return hash code for this Profession object
     */
    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        return result;
    }

    /**
     * Returns a String object representing this Profession's name
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return name;
    }
}
