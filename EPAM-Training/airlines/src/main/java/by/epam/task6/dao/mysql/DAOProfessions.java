package by.epam.task6.dao.mysql;

import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.IDao;
import by.epam.task6.dao.connection.exception.ConnectionException;
import by.epam.task6.dao.connection.ConnectionManager;
import by.epam.task6.entity.IEntity;
import by.epam.task6.entity.impl.Profession;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The DAOProfessions class provides CRUD methods to work with 'professions' table of DB
 *
 * @author dmitr_000
 * @version 1.00 11-May-15
 */

public class DAOProfessions implements IDao {
    private final static Logger LOGGER = Logger.getLogger(DAOEmployees.class);
    private final static DAOProfessions instance = new DAOProfessions();
    private final String READ_ALL_QUERY = "SELECT * FROM professions;";
    private final String INSERT_QUERY = "INSERT INTO professions (name) VALUES(?);";
    private final String UPDATE_QUERY = "UPDATE professions SET name=? WHERE id=?;";
    private final String DELETE_QUERY = "DELETE FROM professions WHERE id=?;";
    private ConnectionManager manager = null;
    private PreparedStatement preparedStatement;

    /**
     * Default private constructor
     */
    private DAOProfessions() {
    }

    /**
     * Gets an instance of DAOProfessions class
     *
     * @return an instance of DAOProfessions class
     */
    public static DAOProfessions getInstance() {
        return instance;
    }

    /**
     * Creates new Profession entry in DB
     *
     * @param entry - an object to create entry
     * @throws DAOException
     */
    public void create(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Profession.class) {
            Profession profession = (Profession) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(INSERT_QUERY);
                preparedStatement.setString(1, profession.getName());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Gets all entries from 'professions' table of DB
     *
     * @return list of objects, representing entries in DB
     * @throws DAOException
     */
    public List<IEntity> readAll() throws DAOException {
        Connection cn = null;
        List<IEntity> lists = null;
        try {
            manager = ConnectionManager.getInstance();
            cn = manager.takeConnection();
            preparedStatement = cn.prepareStatement(READ_ALL_QUERY);
            ResultSet rs = preparedStatement.executeQuery();
            lists = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                lists.add(new Profession(id, name));
            }
        } catch (SQLException e) {
            throw new DAOException("Can't execute PS", e);
        } catch (ConnectionException e) {
            throw new DAOException("Can't get ConnectionManager's instance", e);
        } finally {
            closePSAndReleaseConnection(cn, preparedStatement);
        }
        return lists;
    }

    /**
     * Updates existing Profession entry in DB
     *
     * @param entry - an object to update entry
     * @throws DAOException
     */
    public void update(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Profession.class) {
            Profession profession = (Profession) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(UPDATE_QUERY);
                preparedStatement.setString(1, profession.getName());
                preparedStatement.setInt(2, profession.getId());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Deletes Profession entry from DB
     *
     * @param entry - an object to delete entry
     * @throws DAOException
     */
    public void delete(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Profession.class) {
            Profession profession = (Profession) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(DELETE_QUERY);
                preparedStatement.setInt(1, profession.getId());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Closes PreparedStatement object and puts connection back to ConnectionManager
     *
     * @param c  - a connection to release
     * @param ps - a statement to close
     */
    private void closePSAndReleaseConnection(Connection c, PreparedStatement ps) {
        if (manager != null) {
            try {
                if (ps != null && !ps.isClosed())
                    ps.close();
                if (c != null)
                    manager.releaseConnection(c);
            } catch (SQLException e) {
                LOGGER.error("Can't close PS", e);
            } catch (ConnectionException e) {
                LOGGER.error("Can't release connection", e);
            }
        } else
            LOGGER.error("Trying to release connection to null-referenced pool");
    }
}
