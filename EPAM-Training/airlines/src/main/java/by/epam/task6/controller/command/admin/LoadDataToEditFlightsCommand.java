package by.epam.task6.controller.command.admin;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.IDaoFactory;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.IEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The LoadDataToEditFlightsCommand class loads data for editing flights
 *
 * @author dmitr_000
 * @version 1.00 19-May-15
 */
public class LoadDataToEditFlightsCommand implements ICommand {
    private final static String PLANES = "planes";
    private final static String CREWS = "crews";

    /**
     * Loads data for editing flights
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        IDaoFactory factory = MySQLDaoFactory.getInstance();
        //planes
        try {
            List<IEntity> planes = factory.getDao(MySQLDaoFactory.DaoType.PLANES.name()).readAll();
            request.setAttribute(PLANES, planes);
        } catch (DAOException e) {
            throw new CommandException("Can't load planes list", e);
        }
        //crews
        try {
            List<IEntity> crews = factory.getDao(MySQLDaoFactory.DaoType.CREWS.name()).readAll();
            request.setAttribute(CREWS, crews);
        } catch (DAOException e) {
            throw new CommandException("Can't load crews list", e);
        }
        return null;
    }
}
