package by.epam.task6.controller.command.guest;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.impl.Account;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The RegisterCommand creates new user entry in DB
 *
 * @author dmitr_000
 * @version 1.00 25-May-15
 */
public class RegisterCommand implements ICommand {
    private final static String JSON = "json";

    /**
     * Creates new user entry in DB
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            Account user = mapper.readValue(json, Account.class);
            try {
                MySQLDaoFactory.getInstance().getDao(MySQLDaoFactory.DaoType.STAFF.name()).create(user);
            } catch (DAOException e) {
                mapper.writeValue(response.getOutputStream(), -1);
            }
            mapper.writeValue(response.getOutputStream(), 1);
        } catch (IOException e) {
            throw new CommandException(e);
        }
        return null;
    }
}
