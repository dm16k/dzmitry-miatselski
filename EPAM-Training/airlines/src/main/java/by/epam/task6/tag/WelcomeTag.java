package by.epam.task6.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * The WelcomeTag class describes how to handle custom tag <welcome/>
 *
 * @author dmitr_000
 * @version 1.00 08-Jul-15
 */
public class WelcomeTag extends TagSupport {
    private String status;
    private String name;

    /**
     * Sets name
     *
     * @param name - user name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets status
     *
     * @param status - user status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Overrides superclass method to handle custom tag <welcome/>
     *
     * @return constant SKIP_BODY, because that tag hasn't body
     * @throws JspException
     */
    @Override
    public int doStartTag() throws JspException {
        String message = null;
        if (!"guest".equalsIgnoreCase(status)) {
            message = "Hi, " + name + "!";
        } else {
            message = "";
        }
        try {
            pageContext.getOut().write(message);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
