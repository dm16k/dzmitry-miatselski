package by.epam.task6.dao.connection.exception;

/**
 * Superclass of all project exceptions.
 * Thrown if some error happened during setting/using/closing the connections to DB.
 *
 * @author dmitr_000
 * @version 1.00 06-May-15
 */
public class ConnectionException extends Exception {
    public ConnectionException(String msg) {
        super(msg);
    }

    public ConnectionException(Throwable cause) {
        super(cause);
    }

    public ConnectionException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
