package by.epam.task6.entity;

/**
 * An interface of all DB entities.
 *
 * @author dmitr_000
 * @version 1.00 13-May-15
 */
public interface IEntity {
    /**
     * Gets ID of DB entity
     *
     * @return ID
     */
    int getId();
}
