package by.epam.task6.controller.listener;

import by.epam.task6.entity.impl.Account;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * The SessionCreatingListener class implements methods of {@code HttpSessionListener} interface
 * When new {@code HttpSession} object created {@code sessionCreated} method invokes.
 * When {@code HttpSession} object destroyed {@code sessionDestroyed) method invokes.
 *
 * @author dmitr_000
 * @version 1.00 25-May-15
 */
@WebListener
public class SessionCreatingListener implements HttpSessionListener {
    private static final String USER = "user";
    private static final String LOCALE = "locale";

    /**
     * Sets new {@code User} object and 'en_US' locale to current session.
     *
     * @param httpSessionEvent - the HttpSessionEvent containing the session
     */
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        HttpSession session = httpSessionEvent.getSession();
        synchronized (this) {
            session.setAttribute(USER, new Account());
            session.setAttribute(LOCALE, "en_US");
        }
    }

    /**
     * Overrides superclass method
     *
     * @param httpSessionEvent - the HttpSessionEvent containing the session
     */
    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
    }
}