package by.epam.task6.controller.command.dispatcher;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.IDaoFactory;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.IEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The LoadDataToEditCrewsCommand class loads data for editing crews
 *
 * @author dmitr_000
 * @version 1.00 27-May-15
 */
public class LoadDataToEditCrewsCommand implements ICommand {
    private final static String EMPLOYEES = "employees";

    /**
     * Loads data for editing crews
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        IDaoFactory daoFactory = MySQLDaoFactory.getInstance();
        List<IEntity> employees;
        try {
            employees = daoFactory.getDao(MySQLDaoFactory.DaoType.EMPLOYEES.name()).readAll();
            request.setAttribute(EMPLOYEES, employees);
        } catch (DAOException e) {
            throw new CommandException("Can't load employees list", e);
        }
        return null;
    }
}
