package by.epam.task6.dao;

import java.util.ResourceBundle;

/**
 * The DataBaseConfigManager class has method that gets DB parameters from resource file
 *
 * @author dmitr_000
 * @version 1.00 29-Apr-15
 */
public class DataBaseConfigManager {
    private final static DataBaseConfigManager instance = new DataBaseConfigManager();
    private ResourceBundle bundle = ResourceBundle.getBundle("database");

    /**
     * Default private constructor
     */
    private DataBaseConfigManager() {
    }

    /**
     * Gets an instance of DataBaseConfigManager class
     *
     * @return an instance of DataBaseConfigManager class
     */
    public static DataBaseConfigManager getInstance() {
        return instance;
    }

    /**
     * Gets DB parameter by {@code key}
     *
     * @param key - a string key
     * @return DB parameter
     */
    public String getValue(String key) {
        return bundle.getString(key);
    }
}
