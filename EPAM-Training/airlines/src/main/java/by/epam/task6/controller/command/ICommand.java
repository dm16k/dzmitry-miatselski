package by.epam.task6.controller.command;

import by.epam.task6.controller.command.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The ICommand interface provides {@code execute()} method
 *
 * @author dmitr_000
 * @version 1.00 06-May-15
 */
public interface ICommand {
    String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
