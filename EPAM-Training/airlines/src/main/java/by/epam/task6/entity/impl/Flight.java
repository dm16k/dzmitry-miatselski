package by.epam.task6.entity.impl;

import by.epam.task6.entity.IEntity;

import java.sql.Timestamp;

/**
 * The Flight class represents entries of 'flights' table in DB.
 * It provides getters and setters for every field of each entry.
 *
 * @author dmitr_000
 * @version 1.00 13-May-15
 */
public class Flight implements IEntity {
    private int id;
    private String code;
    private Timestamp departureTime;
    private Timestamp arrivalTime;
    private Plane plane;
    private Crew crew;
    private Airport from;
    private Airport to;

    /**
     * Default constructor
     */
    public Flight() {
    }

    /**
     * Constructor
     *
     * @param id specifies flight id
     */
    public Flight(String id) {
        this.id = Integer.valueOf(id);
    }

    /**
     * Constructor
     *
     * @param id            specifies flight id
     * @param code          specifies flight code
     * @param departureTime specifies flight departure time
     * @param arrivalTime   specifies flight arrival time
     * @param plane         specifies flight plane
     * @param crew          specifies flight crew
     * @param from          specifies flight departure airport
     * @param to            specifies flight arrival airport
     */
    public Flight(int id, String code, Timestamp departureTime, Timestamp arrivalTime,
                  Plane plane, Crew crew, Airport from, Airport to) {
        this.id = id;
        this.code = code;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.plane = plane;
        this.crew = crew;
        this.from = from;
        this.to = to;
    }

    /**
     * Gets flight ID
     *
     * @return flight ID
     */
    public int getId() {
        return id;
    }

    /**
     * Sets flight ID
     *
     * @param id specifies flight ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets flight code
     *
     * @return flight code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets flight code
     *
     * @param code specifies flight code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets flight departure time
     *
     * @return flight departure time
     */
    public Timestamp getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets flight departure time
     *
     * @param departureTime specifies flight departure time
     */
    public void setDepartureTime(Timestamp departureTime) {
        this.departureTime = departureTime;
    }

    /**
     * Gets flight arrival time
     *
     * @return flight arrival time
     */
    public Timestamp getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets flight arrival time
     *
     * @param arrivalTime specifies flight arrival time
     */
    public void setArrivalTime(Timestamp arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * Gets flight plane
     *
     * @return flight plane
     */
    public Plane getPlane() {
        return plane;
    }

    /**
     * Sets flight plane
     *
     * @param plane specifies flight plane
     */
    public void setPlane(Plane plane) {
        this.plane = plane;
    }

    /**
     * Gets flight crew
     *
     * @return flight crew
     */
    public Crew getCrew() {
        return crew;
    }

    /**
     * Sets flight crew
     *
     * @param crew specifies flight crew
     */
    public void setCrew(Crew crew) {
        this.crew = crew;
    }

    /**
     * Gets flight departure airport
     *
     * @return flight departure airport
     */
    public Airport getFrom() {
        return from;
    }

    /**
     * Sets flight departure airport
     *
     * @param from specifies flight departure airport
     */
    public void setFrom(Airport from) {
        this.from = from;
    }

    /**
     * Gets flight arrival airport
     *
     * @return flight arrival airport
     */
    public Airport getTo() {
        return to;
    }

    /**
     * Sets flight arrival airport
     *
     * @param to specifies flight arrival airport
     */
    public void setTo(Airport to) {
        this.to = to;
    }

    /**
     * Returns a String object representing this Flight's data
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", departureTime=" + departureTime +
                ", arrivalTime=" + arrivalTime +
                ", plane=" + plane +
                ", crew=" + crew +
                ", from=" + from +
                ", to=" + to +
                '}';
    }
}
