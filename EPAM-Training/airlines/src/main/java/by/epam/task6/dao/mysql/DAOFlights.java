package by.epam.task6.dao.mysql;

import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.IDao;
import by.epam.task6.dao.connection.exception.ConnectionException;
import by.epam.task6.dao.connection.ConnectionManager;
import by.epam.task6.entity.IEntity;
import by.epam.task6.entity.impl.Airport;
import by.epam.task6.entity.impl.Crew;
import by.epam.task6.entity.impl.Flight;
import by.epam.task6.entity.impl.Plane;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The DAOFlights class provides CRUD methods to work with 'flights' table of DB
 *
 * @author dmitr_000
 * @version 1.00 06-May-15
 */
public class DAOFlights implements IDao {
    private final static Logger LOGGER = Logger.getLogger(DAOFlights.class);
    private final static DAOFlights instance = new DAOFlights();
    private final String READ_ALL_QUERY = "SELECT " +
            "f.id, f.code, f.departure_time, f.arrival_time, " +
            "f.plane, p.model, p.passengers_cap, f.`from`, " +
            "af.name, af.city, f.`to`, t.name, t.city, f.crew, c.crew_id " +
            "FROM flights f " +
            "JOIN planes p ON p.id = f.plane " +
            "JOIN airports af ON af.id = f.`from` " +
            "JOIN airports t ON t.id = f.`to` " +
            "JOIN crews c ON c.id = f.crew;";
    private final String INSERT_QUERY = "INSERT INTO flights (code, departure_time, arrival_time, plane, `from`, `to`, crew)  VALUES(?,?,?,?,?,?,?);";
    private final String UPDATE_QUERY = "UPDATE flights SET code=?, departure_time=?, arrival_time=?, plane=?, `from`=?, `to`=?, crew=? WHERE id=?;";
    private final String DELETE_QUERY = "DELETE FROM flights WHERE id=?;";
    private final String READ_WITH_RESTRICTIONS = READ_ALL_QUERY.substring(0, READ_ALL_QUERY.length() - 1) +
            " WHERE " +
            "((departure_time BETWEEN ? AND ?) " +
            "OR (arrival_time BETWEEN ? AND ?)) " +
            "AND (af.id=? OR t.id=?) " +
            "ORDER BY departure_time ASC;";
    private final String READ_ALL_BETWEEN_DATES = READ_ALL_QUERY.substring(0, READ_ALL_QUERY.length() - 1) +
            " WHERE ((departure_time BETWEEN ? AND ?) " +
            "OR (arrival_time BETWEEN ? AND ?)) ORDER BY departure_time ASC;";
    private ConnectionManager manager = null;
    private PreparedStatement preparedStatement;

    /**
     * Default private constructor
     */
    private DAOFlights() {
    }

    /**
     * Gets an instance of DAOFlights class
     *
     * @return an instance of DAOFlights class
     */
    public static DAOFlights getInstance() {
        return instance;
    }

    /**
     * Creates new Flight entry in DB
     *
     * @param entry - an object to create entry
     * @throws DAOException
     */
    public void create(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Flight.class) {
            Flight flight = (Flight) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(INSERT_QUERY);
                preparedStatement.setString(1, flight.getCode());
                preparedStatement.setTimestamp(2, flight.getDepartureTime());
                preparedStatement.setTimestamp(3, flight.getArrivalTime());
                preparedStatement.setInt(4, flight.getPlane().getId());
                preparedStatement.setInt(5, flight.getFrom().getId());
                preparedStatement.setInt(6, flight.getTo().getId());
                preparedStatement.setInt(7, flight.getCrew().getId());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Gets all entries from 'flights' table of DB
     *
     * @return list of objects, representing entries in DB
     * @throws DAOException
     */
    public List<IEntity> readAll() throws DAOException {
        Connection cn = null;
        List<IEntity> list = null;
        try {
            manager = ConnectionManager.getInstance();
            cn = manager.takeConnection();
            preparedStatement = cn.prepareStatement(READ_ALL_QUERY);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt(1);
                String code = rs.getString(2);
                Timestamp departureTime = rs.getTimestamp(3);
                Timestamp arrivalTime = rs.getTimestamp(4);
                int planeId = rs.getInt(5);
                String model = rs.getString(6);
                int capacity = rs.getInt(7);
                int fromId = rs.getInt(8);
                String fromName = rs.getString(9);
                String fromCity = rs.getString(10);
                int toId = rs.getInt(11);
                String toName = rs.getString(12);
                String toCity = rs.getString(13);
                int crewId = rs.getInt(14);
                int crewIDNumber = rs.getInt(15);
                Plane plane = new Plane(planeId, model, capacity);
                Airport fromAirport = new Airport(fromId, fromName, fromCity);
                Airport toAirport = new Airport(toId, toName, toCity);
                Crew crew = new Crew(crewId, crewIDNumber);
                Flight flight = new Flight(id, code, departureTime, arrivalTime, plane, crew, fromAirport, toAirport);
                list.add(flight);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            throw new DAOException("Can't execute PS", e);
        } catch (ConnectionException e) {
            throw new DAOException("Can't get ConnectionManager's instance", e);
        } finally {
            closePSAndReleaseConnection(cn, preparedStatement);
        }
        return list;
    }

    /**
     * Updates existing Flight entry in DB
     *
     * @param entry - an object to update entry
     * @throws DAOException
     */
    public void update(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Flight.class) {
            Flight flight = (Flight) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(UPDATE_QUERY);
                preparedStatement.setString(1, flight.getCode());
                preparedStatement.setTimestamp(2, flight.getDepartureTime());
                preparedStatement.setTimestamp(3, flight.getArrivalTime());
                preparedStatement.setInt(4, flight.getPlane().getId());
                preparedStatement.setInt(5, flight.getFrom().getId());
                preparedStatement.setInt(6, flight.getTo().getId());
                preparedStatement.setInt(7, flight.getCrew().getId());
                preparedStatement.setInt(8, flight.getId());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Deletes Flight entry from DB
     *
     * @param entry - an object to delete entry
     * @throws DAOException
     */
    public void delete(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Flight.class) {
            Flight flight = (Flight) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(DELETE_QUERY);
                preparedStatement.setInt(1, flight.getId());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Gets entries from 'flights' table of DB that match the given airport and time range
     *
     * @return list of objects, representing entries in DB
     * @throws DAOException
     */
    public List<IEntity> readByAirportIdBetweenDates(int airportId, String startDate, String endDate) throws DAOException {
        Connection cn = null;
        List<IEntity> list = null;
        try {
            manager = ConnectionManager.getInstance();
            cn = manager.takeConnection();
            if (airportId == 0) {
                preparedStatement = cn.prepareStatement(READ_ALL_BETWEEN_DATES);
            } else {
                preparedStatement = cn.prepareStatement(READ_WITH_RESTRICTIONS);
                preparedStatement.setInt(5, airportId);
                preparedStatement.setInt(6, airportId);
            }
            preparedStatement.setString(1, startDate);
            preparedStatement.setString(2, endDate);
            preparedStatement.setString(3, startDate);
            preparedStatement.setString(4, endDate);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt(1);
                String code = rs.getString(2);
                Timestamp departureTime = rs.getTimestamp(3);
                Timestamp arrivalTime = rs.getTimestamp(4);
                int planeId = rs.getInt(5);
                String model = rs.getString(6);
                int capacity = rs.getInt(7);
                int fromId = rs.getInt(8);
                String fromName = rs.getString(9);
                String fromCity = rs.getString(10);
                int toId = rs.getInt(11);
                String toName = rs.getString(12);
                String toCity = rs.getString(13);
                int crewId = rs.getInt(14);
                int crewIDNumber = rs.getInt(15);
                Plane plane = new Plane(planeId, model, capacity);
                Airport fromAirport = new Airport(fromId, fromName, fromCity);
                Airport toAirport = new Airport(toId, toName, toCity);
                Crew crew = new Crew(crewId, crewIDNumber);
                Flight flight = new Flight(id, code, departureTime, arrivalTime, plane, crew, fromAirport, toAirport);
                list.add(flight);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            throw new DAOException("Can't execute PS", e);
        } catch (ConnectionException e) {
            throw new DAOException("Can't get ConnectionManager's instance", e);
        } finally {
            closePSAndReleaseConnection(cn, preparedStatement);
        }
        return list;
    }

    /**
     * Closes PreparedStatement object and puts connection back to ConnectionManager
     *
     * @param c  - a connection to release
     * @param ps - a statement to close
     */
    private void closePSAndReleaseConnection(Connection c, PreparedStatement ps) {
        if (manager != null) {
            try {
                if (ps != null && !ps.isClosed())
                    ps.close();
                if (c != null)
                    manager.releaseConnection(c);
            } catch (SQLException e) {
                LOGGER.error("Can't close PS", e);
            } catch (ConnectionException e) {
                LOGGER.error("Can't release connection", e);
            }
        } else
            LOGGER.error("Trying to release connection to null-referenced pool");
    }
}
