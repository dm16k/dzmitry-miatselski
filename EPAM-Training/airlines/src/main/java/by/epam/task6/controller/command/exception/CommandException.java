package by.epam.task6.controller.command.exception;

import by.epam.task6.dao.exception.DAOException;

/**
 * Thrown if some error happened during executing some command
 * @author dmitr_000
 * @version 1.00 10-May-15
 */
public class CommandException extends DAOException {
    public CommandException(String msg) {
        super(msg);
    }

    public CommandException(Throwable cause) {
        super(cause);
    }

    public CommandException(String msg, Exception ex) {
        super(msg, ex);
    }
}
