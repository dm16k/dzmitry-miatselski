package by.epam.task6.controller.command.guest;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.controller.command.common.OpenFlightsCommand;
import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.IDao;
import by.epam.task6.dao.IDaoFactory;
import by.epam.task6.dao.mysql.DAOStaff;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.impl.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The SignInCommand class sets user, stored in DB, to current session
 *
 * @author dmitr_000
 * @version 1.00 17-May-15
 */
public class SignInCommand implements ICommand {
    private final static String LOGIN = "login";
    private final static String PASSWORD = "password";
    private final static String USER = "user";

    /**
     * Sets user to current session if entered credentials are correct and redirects user to start page
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return start page location
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        IDaoFactory daoFactory = MySQLDaoFactory.getInstance();
        IDao dao = daoFactory.getDao(MySQLDaoFactory.DaoType.STAFF.name());
        if (dao.getClass() == DAOStaff.class) {
            String login = request.getParameter(LOGIN);
            String password = request.getParameter(PASSWORD);
            try {
                Account account = ((DAOStaff) dao).getStaffByLoginAndPassword(login, password);
                if (account != null) {
                    session.setAttribute(USER, account);
                } else {
                    session.setAttribute(USER, new Account());
                }
            } catch (DAOException e) {
                throw new CommandException("Can't get account with login:" + login + ", password:" + password, e);
            }
        }
        return new OpenFlightsCommand().execute(request, response);
    }
}
