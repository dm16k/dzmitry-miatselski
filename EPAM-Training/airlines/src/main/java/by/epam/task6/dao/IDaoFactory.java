package by.epam.task6.dao;

/**
 * An interface for DAO factories classes
 *
 * @author dmitr_000
 * @version 1.00 05-May-15
 */
public interface IDaoFactory {
    IDao getDao(String name);
}