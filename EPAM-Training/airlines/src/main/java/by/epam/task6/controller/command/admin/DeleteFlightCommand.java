package by.epam.task6.controller.command.admin;

import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.impl.Flight;
import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The DeleteFlightCommand class removes flight entry from DB
 *
 * @author dmitr_000
 * @version 1.00 22-May-15
 */
public class DeleteFlightCommand implements ICommand {
    private final static String JSON = "json";

    /**
     * Removes flight entry from DB
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            Flight flight = mapper.readValue(json, Flight.class);
            MySQLDaoFactory.getInstance().getDao(MySQLDaoFactory.DaoType.FLIGHTS.name()).delete(flight);
            mapper.writeValue(response.getOutputStream(), -1);
        } catch (IOException | DAOException e) {
            throw new CommandException(e);
        }
        return null;
    }
}
