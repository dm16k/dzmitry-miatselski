package by.epam.task6.entity.impl;

import by.epam.task6.entity.IEntity;

/**
 * The Account class represents entries of 'staff' table in DB.
 * It provides getters and setters for every field of each entry.
 *
 * @author dmitr_000
 * @version 1.00 13-May-15
 */
public class Account implements IEntity {
    public final static String GUEST = "guest";
    public final static String ADMIN = "admin";
    public final static String DISPATCHER = "dispatcher";
    private int id;
    private String login;
    private String password;
    private String name;
    private boolean admin;
    private String status;

    /**
     * Default constructor
     */
    public Account() {
        status = GUEST;
        name = "Guest";
    }

    /**
     * Constructor
     *
     * @param login    specifies user login
     * @param password specifies user password
     * @param name     specifies user name
     */
    public Account(String login, String password, String name) {
        this.login = login;
        this.password = password;
        this.name = name;
        status = DISPATCHER;
    }

    /**
     * Constructor
     *
     * @param id       specifies user id
     * @param login    specifies user login
     * @param password specifies user password
     * @param name     specifies user name
     * @param admin    specifies type of account
     */
    public Account(int id, String login, String password, String name, boolean admin) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.name = name;
        this.admin = admin;
        status = admin ? ADMIN : DISPATCHER;
    }

    /**
     * Gets user status
     *
     * @return user status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets user status
     *
     * @param status specifies user status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets user ID
     *
     * @return user ID
     */
    public int getId() {
        return id;
    }

    /**
     * Sets user ID
     *
     * @param id specifies user ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets user login
     *
     * @return user login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets user status
     *
     * @param login specifies user login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets user password
     *
     * @return user password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets user password
     *
     * @param password specifies user password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets user name
     *
     * @return user name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets user name
     *
     * @param name specifies user name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets account type
     *
     * @return account type
     */
    public boolean isAdmin() {
        return admin;
    }

    /**
     * Sets account type
     *
     * @param admin specifies account type
     */
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    /**
     * Returns a String object representing this Account's data
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", admin=" + admin +
                ", status='" + status + '\'' +
                '}';
    }
}
