package by.epam.task6.controller.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * The EncodingFilter class sets request and response encoding
 *
 * @author dmitr_000
 * @version 1.00 26-May-15
 */
@WebFilter
public class EncodingFilter implements Filter {
    private String code;

    /**
     * Gets encoding string from filter configuration object
     *
     * @param filterConfig - a filter configuration object
     * @throws ServletException
     */
    public void init(FilterConfig filterConfig) throws ServletException {
        code = filterConfig.getInitParameter("encoding");
    }

    /**
     * This method is called by the container each time a request/response pair is passed through the chain
     * due to a client request for a resource at the end of the chain. It sets request and response encoding.
     *
     * @param servletRequest  -  an object to provide client request information to a servlet
     * @param servletResponse - object to assist a servlet in sending a response to the client
     * @param filterChain     - an object provided by the servlet container to the developer giving a view into the invocation chain of a filtered request for a resource
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        String codeRequest = servletRequest.getCharacterEncoding();
        if (code != null && !code.equalsIgnoreCase(codeRequest)) {
            servletRequest.setCharacterEncoding(code);
            servletResponse.setCharacterEncoding(code);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    /**
     * Called by the web container to indicate to a filter that it is being taken out of service
     */
    public void destroy() {
        code = null;
    }
}