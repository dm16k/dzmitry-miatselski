package by.epam.task6.dao.mysql;

import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.IDao;
import by.epam.task6.dao.connection.exception.ConnectionException;
import by.epam.task6.dao.connection.ConnectionManager;
import by.epam.task6.entity.IEntity;
import by.epam.task6.entity.impl.Account;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The DAOStaff class provides CRUD methods to work with 'staff' table of DB
 *
 * @author dmitr_000
 * @version 1.00 06-May-15
 */
public class DAOStaff implements IDao {
    private final static Logger LOGGER = Logger.getLogger(DAOStaff.class);
    private final static DAOStaff instance = new DAOStaff();
    private final String READ_ALL_QUERY = "SELECT * FROM staff;";
    private final String INSERT_QUERY = "INSERT INTO staff (login, password, name, admin) VALUES (?,?,?,?);";
    private final String UPDATE_QUERY = "UPDATE staff SET login=?, password=?, name=?, admin=? WHERE id=?;";
    private final String DELETE_QUERY = "DELETE FROM staff WHERE id=?;";
    private final String GET_STAFF_BY_LOGIN_AND_PASS = "SELECT * FROM staff WHERE login=? AND password=?;";
    private ConnectionManager manager = null;
    private PreparedStatement preparedStatement;

    /**
     * Default private constructor
     */
    private DAOStaff() {
    }

    /**
     * Gets an instance of DAOStaff class
     *
     * @return an instance of DAOStaff class
     */
    public static DAOStaff getInstance() {
        return instance;
    }

    /**
     * Creates new Account entry in DB
     *
     * @param entry - an object to create entry
     * @throws DAOException
     */
    public void create(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Account.class) {
            Account account = (Account) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(INSERT_QUERY);
                preparedStatement.setString(1, account.getLogin());
                preparedStatement.setString(2, account.getPassword());
                preparedStatement.setString(3, account.getName());
                preparedStatement.setBoolean(4, account.isAdmin());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Gets all entries from 'staff' table of DB
     *
     * @return list of objects, representing entries in DB
     * @throws DAOException
     */
    public List<IEntity> readAll() throws DAOException {
        Connection cn = null;
        List<IEntity> list = null;
        try {
            manager = ConnectionManager.getInstance();
            cn = manager.takeConnection();
            preparedStatement = cn.prepareStatement(READ_ALL_QUERY);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt(1);
                String login = rs.getString(2);
                String password = rs.getString(3);
                String name = rs.getString(4);
                Boolean admin = rs.getBoolean(5);
                list.add(new Account(id, login, password, name, admin));
            }
        } catch (SQLException e) {
            throw new DAOException("Can't execute PS", e);
        } catch (ConnectionException e) {
            throw new DAOException("Can't get ConnectionManager's instance", e);
        } finally {
            closePSAndReleaseConnection(cn, preparedStatement);
        }
        return list;
    }

    /**
     * Updates existing Account entry in DB
     *
     * @param entry - an object to update entry
     * @throws DAOException
     */
    public void update(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Account.class) {
            Account account = (Account) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(UPDATE_QUERY);
                preparedStatement.setString(1, account.getLogin());
                preparedStatement.setString(2, account.getPassword());
                preparedStatement.setString(3, account.getName());
                preparedStatement.setBoolean(4, account.isAdmin());
                preparedStatement.setInt(5, account.getId());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Deletes Account entry from DB
     *
     * @param entry - an object to delete entry
     * @throws DAOException
     */
    public void delete(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Account.class) {
            Account account = (Account) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(DELETE_QUERY);
                preparedStatement.setInt(1, account.getId());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Gets Account object by login and password or null if there is no account identified by entered parameters
     *
     * @param login    - login of account
     * @param password - password of account
     * @return an Account object or null
     * @throws DAOException
     */
    public Account getStaffByLoginAndPassword(String login, String password) throws DAOException {
        Connection cn = null;
        Account account = null;
        try {
            manager = ConnectionManager.getInstance();
            cn = manager.takeConnection();
            preparedStatement = cn.prepareStatement(GET_STAFF_BY_LOGIN_AND_PASS);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String login1 = rs.getString(2);
                String password1 = rs.getString(3);
                String name = rs.getString(4);
                Boolean admin = rs.getBoolean(5);
                account = new Account(id, login1, password1, name, admin);
            }
        } catch (SQLException e) {
            throw new DAOException("Can't execute PS", e);
        } catch (ConnectionException e) {
            throw new DAOException("Can't get ConnectionManager's instance", e);
        } finally {
            closePSAndReleaseConnection(cn, preparedStatement);
        }
        return account;
    }

    /**
     * Closes PreparedStatement object and puts connection back to ConnectionManager
     *
     * @param c  - a connection to release
     * @param ps - a statement to close
     */
    private void closePSAndReleaseConnection(Connection c, PreparedStatement ps) {
        if (manager != null) {
            try {
                if (ps != null && !ps.isClosed())
                    ps.close();
                if (c != null)
                    manager.releaseConnection(c);
            } catch (SQLException e) {
                LOGGER.error("Can't close PS", e);
            } catch (ConnectionException e) {
                LOGGER.error("Can't release connection", e);
            }
        } else
            LOGGER.error("Trying to release connection to null-referenced pool");
    }
}
