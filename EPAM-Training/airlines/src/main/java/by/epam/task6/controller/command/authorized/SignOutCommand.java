package by.epam.task6.controller.command.authorized;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.controller.command.common.OpenFlightsCommand;
import by.epam.task6.entity.impl.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The SignOutCommand class sets new guest user to current session
 *
 * @author dmitr_000
 * @version 1.00 17-May-15
 */
public class SignOutCommand implements ICommand {
    private final static String USER = "user";

    /**
     * Sets new guest user to current session and redirects user to start page
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return start page location
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        String page = new OpenFlightsCommand().execute(request, response);
        session.setAttribute(USER, new Account());
        return page;
    }
}
