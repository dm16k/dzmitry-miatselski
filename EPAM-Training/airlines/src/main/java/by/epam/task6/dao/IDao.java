package by.epam.task6.dao;

import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.entity.IEntity;

import java.util.List;

/**
 * An interface for DAO classes. Provides CRUD methods.
 *
 * @author dmitr_000
 * @version 1.00 05-May-15
 */
public interface IDao {
    void create(IEntity entry) throws DAOException;

    List<IEntity> readAll() throws DAOException;

    void update(IEntity entry) throws DAOException;

    void delete(IEntity entry) throws DAOException;
}
