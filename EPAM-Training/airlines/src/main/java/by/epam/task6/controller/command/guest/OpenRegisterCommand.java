package by.epam.task6.controller.command.guest;

import by.epam.task6.controller.PageHelper;
import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The OpenRegisterCommand gets location of registration page
 *
 * @author dmitr_000
 * @version 1.00 25-May-15
 */
public class OpenRegisterCommand implements ICommand {
    /**
     * Gets location of registration page
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return location of registration page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        return PageHelper.getInstance().getProperty(PageHelper.REGISTER_PAGE);
    }
}
