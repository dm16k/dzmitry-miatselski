package by.epam.task6.controller.filter;

import by.epam.task6.controller.PageHelper;
import by.epam.task6.controller.command.CommandName;
import by.epam.task6.entity.impl.Account;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The AccessFilter class checks access to execute command for current user
 *
 * @author dmitr_000
 * @version 1.00 25-May-15
 */
@WebFilter
public class AccessFilter implements Filter {
    /**
     * @param filterConfig - a filter configuration object
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /**
     * This method is called by the container each time a request/response pair is passed through the chain
     * due to a client request for a resource at the end of the chain. It checks access to execute command for current user.
     * If access isn't allowed it redirects user to error page.
     *
     * @param servletRequest  -  an object to provide client request information to a servlet
     * @param servletResponse - object to assist a servlet in sending a response to the client
     * @param filterChain     - an object provided by the servlet container to the developer giving a view into the invocation chain of a filtered request for a resource
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) servletRequest).getSession(false);
        String userStatus = ((Account) session.getAttribute("user")).getStatus();
        CommandName command = getCommandName((HttpServletRequest) servletRequest);
        boolean accessGranted = checkAccess(userStatus, command);
        if (accessGranted)
            filterChain.doFilter(servletRequest, servletResponse);
        else {
            ((HttpServletResponse) servletResponse).sendRedirect(PageHelper
                    .getInstance().getProperty(PageHelper.ACCESS_DENIED_PAGE));
        }
    }

    /**
     * Called by the web container to indicate to a filter that it is being taken out of service
     */
    @Override
    public void destroy() {
    }

    /**
     * Checks access to execute command for current user
     *
     * @param status  - user status
     * @param command - command to execute
     * @return true - if access is granted, false otherwise
     */
    private boolean checkAccess(String status, CommandName command) {
        boolean access = false;
        UserStatus userStatus = UserStatus.valueOf(status.toUpperCase());
        switch (userStatus) {
            case GUEST:
                switch (command) {
                    case CHANGE_LOCALE:
                    case FILTER_FLIGHTS:
                    case OPEN_FLIGHTS:
                    case OPEN_REGISTER:
                    case REGISTER:
                    case SIGN_IN:
                    case WRONG_COMMAND:
                    case SIGN_OUT:
                        access = true;
                        break;
                    default:
                        access = false;
                        break;
                }
                break;
            case DISPATCHER:
                switch (command) {
                    case ADD_CREW:
                    case CHANGE_LOCALE:
                    case DELETE_CREW:
                    case FILTER_FLIGHTS:
                    case LOAD_DATA_TO_EDIT_CREWS:
                    case OPEN_CREWS:
                    case OPEN_FLIGHTS:
                    case OPEN_PLANES:
                    case OPEN_REGISTER:
                    case SIGN_IN:
                    case SIGN_OUT:
                    case UPDATE_CREW:
                    case WRONG_COMMAND:
                        access = true;
                        break;
                    default:
                        access = false;
                        break;
                }
                break;
            case ADMIN:
                switch (command) {
                    case ADD_FLIGHT:
                    case CHANGE_LOCALE:
                    case DELETE_FLIGHT:
                    case DELETE_ACCOUNT:
                    case FILTER_FLIGHTS:
                    case LOAD_DATA_TO_EDIT_FLIGHTS:
                    case OPEN_ACCOUNTS:
                    case OPEN_CREWS:
                    case OPEN_FLIGHTS:
                    case OPEN_PLANES:
                    case OPEN_REGISTER:
                    case SIGN_IN:
                    case SIGN_OUT:
                    case UPDATE_ACCOUNT:
                    case UPDATE_FLIGHT:
                    case WRONG_COMMAND:
                        access = true;
                        break;
                    default:
                        access = false;
                        break;
                }
                break;
        }
        return access;
    }

    /**
     * Gets the CommandName enumeration constant from user request
     *
     * @param request - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @return CommandName enumeration constant
     */
    private CommandName getCommandName(HttpServletRequest request) {
        String commandName;
        if (isAjax(request)) {
            commandName = (String) request.getAttribute("command");
        } else {
            commandName = request.getParameter("command");
        }
        CommandName action;
        try {
            action = CommandName.valueOf(commandName.toUpperCase());
        } catch (IllegalArgumentException ex) {
            action = CommandName.WRONG_COMMAND;
        }
        return action;

    }

    /**
     * Checks the type of request header
     *
     * @param request - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @return true if there is an AJAX request
     */
    private boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

    private enum UserStatus {
        GUEST, DISPATCHER, ADMIN
    }
}
