package by.epam.task6.dao.mysql;

import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.IDao;
import by.epam.task6.dao.connection.exception.ConnectionException;
import by.epam.task6.dao.connection.ConnectionManager;
import by.epam.task6.entity.IEntity;
import by.epam.task6.entity.impl.Airport;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The DAOAirports class provides CRUD methods to work with 'airports' table of DB
 *
 * @author dmitr_000
 * @version 1.00 06-May-15
 */
public class DAOAirports implements IDao {
    private static final Logger LOGGER = Logger.getLogger(DAOAirports.class);
    private static final DAOAirports instance = new DAOAirports();
    private final String READ_ALL_QUERY = "SELECT * FROM airports;";
    private final String INSERT_QUERY = "INSERT INTO airports (name, city)  VALUES(?,?);";
    private final String UPDATE_QUERY = "UPDATE airports SET name=?, city=? WHERE id=?;";
    private final String DELETE_QUERY = "DELETE FROM airports WHERE id=?;";
    private ConnectionManager manager = null;
    private PreparedStatement preparedStatement;

    /**
     * Default private constructor
     */
    private DAOAirports() {
    }

    /**
     * Gets an instance of DAOAirports class
     *
     * @return an instance of DAOAirports class
     */
    public static DAOAirports getInstance() {
        return instance;
    }

    /**
     * Creates new Airport entry in DB
     *
     * @param entry - an object to create entry
     * @throws DAOException
     */
    public void create(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Airport.class) {
            Airport airport = (Airport) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(INSERT_QUERY);
                preparedStatement.setString(1, airport.getName());
                preparedStatement.setString(2, airport.getCity());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Gets all entries from 'airports' table of DB
     *
     * @return list of objects, representing entries in DB
     * @throws DAOException
     */
    public List<IEntity> readAll() throws DAOException {
        Connection cn = null;
        List<IEntity> list = null;
        try {
            manager = ConnectionManager.getInstance();
            cn = manager.takeConnection();
            preparedStatement = cn.prepareStatement(READ_ALL_QUERY);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String city = rs.getString(3);
                list.add(new Airport(id, name, city));
            }
        } catch (SQLException e) {
            throw new DAOException("Can't execute PS", e);
        } catch (ConnectionException e) {
            throw new DAOException("Can't get ConnectionManager's instance", e);
        } finally {
            closePSAndReleaseConnection(cn, preparedStatement);
        }
        return list;
    }

    /**
     * Updates existing Airport entry in DB
     *
     * @param entry - an object to update entry
     * @throws DAOException
     */
    public void update(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Airport.class) {
            Airport airport = (Airport) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(UPDATE_QUERY);
                preparedStatement.setString(1, airport.getName());
                preparedStatement.setString(2, airport.getCity());
                preparedStatement.setInt(3, airport.getId());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Deletes Airport entry from DB
     *
     * @param entry - an object to delete entry
     * @throws DAOException
     */
    public void delete(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Airport.class) {
            Airport airport = (Airport) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(DELETE_QUERY);
                preparedStatement.setInt(1, airport.getId());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

//    public IEntity getEntityById(int id) throws DAOException {
//        IEntity entity = null;
//        Connection cn = null;
//        try {
//            manager = ConnectionManager.getInstance();
//            cn = manager.takeConnection();
//            preparedStatement = cn.prepareStatement(GET_BY_ID_QUERY);
//            preparedStatement.setInt(1, id);
//            ResultSet rs = preparedStatement.executeQuery();
//            while (rs.next()) {
//                String name = rs.getString(2);
//                String city = rs.getString(3);
//                entity = new Airport(id, name, city);
//            }
//        } catch (SQLException e) {
//            throw new DAOException("Can't execute PS", e);
//        } catch (ConnectionException e) {
//            throw new DAOException("Can't get ConnectionManager's instance", e);
//        } finally {
//            closePSAndReleaseConnection(cn, preparedStatement);
//        }
//        return entity;
//    }

    /**
     * Closes PreparedStatement object and puts connection back to ConnectionManager
     *
     * @param c  - a connection to release
     * @param ps - a statement to close
     */
    private void closePSAndReleaseConnection(Connection c, PreparedStatement ps) {
        if (manager != null) {
            try {
                if (ps != null && !ps.isClosed())
                    ps.close();
                if (c != null)
                    manager.releaseConnection(c);
            } catch (SQLException e) {
                LOGGER.error("Can't close PS", e);
            } catch (ConnectionException e) {
                LOGGER.error("Can't release connection", e);
            }
        } else
            LOGGER.error("Trying to release connection to null-referenced pool");
    }
}
