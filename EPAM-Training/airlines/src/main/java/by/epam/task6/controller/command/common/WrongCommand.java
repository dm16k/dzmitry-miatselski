package by.epam.task6.controller.command.common;

import by.epam.task6.controller.PageHelper;
import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The WrongCommand class represents the command that sets error page for redirecting
 *
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public class WrongCommand implements ICommand {
    /**
     * Returns error page location.
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return String object containing error page location
     * @throws CommandException
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        return PageHelper.getInstance().getProperty(PageHelper.ERROR_PAGE);
    }
}
