package by.epam.task6.dao.exception;

import by.epam.task6.dao.connection.exception.ConnectionException;

/**
 * Thrown if some error happened while executing some operations with DB.
 *
 * @author dmitr_000
 * @version 1.00 06-May-15
 */
public class DAOException extends ConnectionException {
    public DAOException(String msg) {
        super(msg);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }

    public DAOException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
