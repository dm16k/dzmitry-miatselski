package by.epam.task6.entity.impl;

import by.epam.task6.entity.IEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * The Crew class represents entries of 'crews' table in DB.
 * It provides getters and setters for every field of each entry.
 *
 * @author dmitr_000
 * @version 1.00 13-May-15
 */
public class Crew implements IEntity {
    private int id;
    private int crewId;
    private List<Employee> employees;

    /**
     * Default constructor
     */
    public Crew() {
        employees = new ArrayList<>();
    }

    /**
     * Constructor
     *
     * @param id specifies crew ID
     */
    public Crew(String id) {
        this.id = Integer.valueOf(id);
    }

    /**
     * Constructor
     *
     * @param crewId    specifies crew unique number
     * @param employees specifies crew employees list
     */
    public Crew(String crewId, List<Employee> employees) {
        this.crewId = Integer.valueOf(crewId);
        this.employees = employees;
    }

    /**
     * Constructor
     *
     * @param id     specifies crew ID
     * @param crewId specifies crew unique number
     */
    public Crew(int id, int crewId) {
        this.id = id;
        this.crewId = crewId;
        this.employees = new ArrayList<>();
    }

    /**
     * Constructor
     *
     * @param id        specifies crew ID
     * @param crewId    specifies crew unique number
     * @param employees specifies crew employees list
     */
    public Crew(String id, String crewId, List<Employee> employees) {
        this.id = Integer.parseInt(id);
        this.crewId = Integer.parseInt(crewId);
        this.employees = employees;
    }

    /**
     * Constructor
     *
     * @param id        specifies crew ID
     * @param crewId    specifies crew unique number
     * @param employees specifies crew employees list
     */
    public Crew(int id, int crewId, List<Employee> employees) {
        this.id = id;
        this.crewId = crewId;
        this.employees = employees;
    }

    /**
     * Gets crew ID
     *
     * @return crew ID
     */
    public int getId() {
        return id;
    }

    /**
     * Sets crew ID
     *
     * @param id specifies crew ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets crew unique number
     *
     * @return crew unique number
     */
    public int getCrewId() {
        return crewId;
    }

    /**
     * Sets crew unique number
     *
     * @param crewId specifies crew unique number
     */
    public void setCrewId(int crewId) {
        this.crewId = crewId;
    }

    /**
     * Gets crew employees list
     *
     * @return crew employees list
     */
    public List<Employee> getEmployees() {
        return employees;
    }

    /**
     * Sets crew employees list
     *
     * @param employees specifies crew employees list
     */
    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    /**
     * Adds new employee into crew
     *
     * @param employee specifies new employee
     */
    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    /**
     * Deletes new employee from crew
     *
     * @param employee specifies employee to delete
     */
    public IEntity deleteEmployee(Employee employee) {
        return employees.remove(employees.indexOf(employee));
        //TODO check indexOf(Object o){...};
    }

    /**
     * Replaces employee in crew
     *
     * @param oldEmpl specifies employee to delete
     * @param newEmpl specifies employee to add
     */
    public void replaceEmployee(Employee oldEmpl, Employee newEmpl) {
        employees.set(employees.indexOf(oldEmpl), newEmpl);
    }

    /**
     * Returns a String object representing this Crew's unique number
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return String.valueOf(crewId);
    }
}
