package by.epam.task6.entity.impl;

import by.epam.task6.entity.IEntity;

/**
 * The Plane class represents entries of 'planes' table in DB.
 * It provides getters and setters for every field of each entry.
 *
 * @author dmitr_000
 * @version 1.00 13-May-15
 */
public class Plane implements IEntity {
    private int id;
    private String model;
    private int passengersCap;

    /**
     * Default constructor
     */
    public Plane() {
    }

    /**
     * Constructor
     *
     * @param id specifies plane id
     */
    public Plane(String id) {
        this.id = Integer.valueOf(id);
    }

    /**
     * Constructor
     *
     * @param model         specifies plane model
     * @param passengersCap specifies plane passengers cap
     */
    public Plane(String model, int passengersCap) {
        this.model = model;
        this.passengersCap = passengersCap;
    }

    /**
     * Constructor
     *
     * @param id            specifies plane id
     * @param model         specifies plane model
     * @param passengersCap specifies plane passengers cap
     */
    public Plane(int id, String model, int passengersCap) {
        this.id = id;
        this.model = model;
        this.passengersCap = passengersCap;
    }

    /**
     * Gets plane ID
     *
     * @return plane ID
     */
    public int getId() {
        return id;
    }

    /**
     * Sets plane ID
     *
     * @param id specifies plane ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets plane model
     *
     * @return plane model
     */
    public String getModel() {
        return model;
    }

    /**
     * Sets plane model
     *
     * @param model specifies plane model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Gets plane passengers cap
     *
     * @return plane passengers cap
     */
    public int getPassengersCap() {
        return passengersCap;
    }

    /**
     * Sets plane passengers cap
     *
     * @param passengersCap specifies plane passengers cap
     */
    public void setPassengersCap(int passengersCap) {
        this.passengersCap = passengersCap;
    }

    /**
     * Returns a String object representing this Plane's data
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return model;
    }
}
