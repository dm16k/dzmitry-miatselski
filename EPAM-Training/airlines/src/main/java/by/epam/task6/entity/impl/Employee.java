package by.epam.task6.entity.impl;

import by.epam.task6.entity.IEntity;

import java.sql.Date;

/**
 * The Employee class represents entries of 'employees' table in DB.
 * It provides getters and setters for every field of each entry.
 *
 * @author dmitr_000
 * @version 1.00 13-May-15
 */
public class Employee implements IEntity {
    private int id;
    private String firstName;
    private String lastName;
    private Profession profession;
    private Date dob;

    /**
     * Default constructor
     */
    public Employee() {
    }

    /**
     * Constructor
     *
     * @param id specifies employee ID
     */
    public Employee(String id) {
        this.id = Integer.parseInt(id);
    }

    /**
     * Constructor
     *
     * @param firstName  specifies employee first name
     * @param lastName   specifies employee last name
     * @param profession specifies employee profession
     * @param dob        specifies employee date of birth
     */
    public Employee(String firstName, String lastName, Profession profession, Date dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.profession = profession;
        this.dob = dob;
    }

    /**
     * @param id         specifies employee ID
     * @param firstName  specifies employee first name
     * @param lastName   specifies employee last name
     * @param profession specifies employee profession
     * @param dob        specifies employee date of birth
     */
    public Employee(int id, String firstName, String lastName, Profession profession, Date dob) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profession = profession;
        this.dob = dob;
    }

    /**
     * Returns true if and only if the argument is not null and is a Employee object that represents the same double values as this object
     *
     * @param o - the object to compare with
     * @return true if the Vegetable objects represent the same value; false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (id != employee.id) return false;
        if (!firstName.equals(employee.firstName)) return false;
        if (!lastName.equals(employee.lastName)) return false;
        if (!profession.equals(employee.profession)) return false;
        return dob.equals(employee.dob);

    }

    /**
     * Returns a hash code for all object fields
     *
     * @return hash code for this Employee object
     */
    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + profession.hashCode();
        result = 31 * result + dob.hashCode();
        return result;
    }

    /**
     * Gets employee ID
     *
     * @return employee ID
     */
    public int getId() {
        return id;
    }

    /**
     * Sets employee ID
     *
     * @param id specifies employee ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets employee first name
     *
     * @return employee first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets employee first name
     *
     * @param firstName specifies employee first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets employee last name
     *
     * @return employee last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets employee last name
     *
     * @param lastName specifies employee last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets employee profession
     *
     * @return employee profession
     */
    public Profession getProfession() {
        return profession;
    }

    /**
     * Sets employee profession
     *
     * @param profession specifies employee profession
     */
    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    /**
     * Gets employee date of birth
     *
     * @return employee date of birth
     */
    public Date getDob() {
        return dob;
    }

    /**
     * Sets employee date of birth
     *
     * @param dob specifies employee date of birth
     */
    public void setDob(Date dob) {
        this.dob = dob;
    }

    /**
     * Returns a String object representing this Employee's data
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return firstName + " " + lastName + ", " + profession;
    }
}
