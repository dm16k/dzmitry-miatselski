package by.epam.task6.controller.listener;

import by.epam.task6.dao.connection.exception.ConnectionException;
import by.epam.task6.dao.connection.ConnectionManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * The AppLifecycleListener class implements methods for receiving notification events about ServletContext lifecycle changes.
 *
 * @author dmitr_000
 * @version 1.00 25-May-15
 */
@WebListener
public class AppLifecycleListener implements ServletContextListener {
    private final static Logger LOGGER = Logger.getLogger(AppLifecycleListener.class);

    /**
     * Receives notification that the web application initialization process is starting.
     * Application initializes connections to the DB.
     *
     * @param servletContextEvent - the ServletContextEvent containing the ServletContext that is being initialized
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            ConnectionManager.getInstance().initConnectionsQueue();
        } catch (ConnectionException e) {
            LOGGER.error("Can't initialize connections pool", e);
        }
    }

    /**
     * Receives notification that the ServletContext is about to be shut down.
     * Application closes all connections to the DB.
     *
     * @param servletContextEvent - the ServletContextEvent containing the ServletContext that is being destroyed
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            ConnectionManager.getInstance().closeAllConnections();
        } catch (ConnectionException e) {
            LOGGER.error("Can't close connections in a pool", e);
        }
    }
}
