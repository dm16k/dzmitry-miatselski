package by.epam.task6.controller;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.CommandHelper;
import by.epam.task6.controller.command.ICommand;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The Controller class is the main class of web application.
 * It overrides {@code doPost()}, {@code doGet()} and {@code destroy()} methods of {@code HttpServlet} class.
 *
 * @author dmitr_000
 * @version 1.00 06-May-15
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {
    private final static Logger LOGGER = Logger.getLogger(Controller.class);
    private CommandHelper commandHelper = CommandHelper.getInstance();

    /**
     * Default constructor
     */
    public Controller() {
        super();
    }

    /**
     * Called by the server (via the service method) to allow a servlet to handle a GET request.
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @throws ServletException - if an input or output error is detected when the servlet handles the GET request
     * @throws IOException      - if the request for the GET could not be handled
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doAction(request, response);
    }

    /**
     * Called by the server (via the service method) to allow a servlet to handle a POST request.
     * The HTTP POST method allows the client to send data of unlimited length to the Web server a single time
     * and is useful when posting information such as credit card numbers.
     *
     * @param req  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param resp - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @throws ServletException - if an input or output error is detected when the servlet handles the request
     * @throws IOException      - if the request for the POST could not be handled
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doAction(req, resp);
    }

    /**
     * This method handles request from server.
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @throws ServletException - if an input or output error is detected when the servlet handles the request
     * @throws IOException      - if the request could not be handled
     */
    private void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isAjax = isAjax(request);
        String commandName;
        if (isAjax) {
            commandName = (String) request.getAttribute("command");
        } else {
            commandName = request.getParameter("command");
        }
        ICommand command = commandHelper.getCommand(commandName);
        String page;
        try {
            page = command.execute(request, response);
            if (isAjax) {
                return;
            }
        } catch (CommandException e) {
            page = PageHelper.getInstance().getProperty(PageHelper.ERROR_PAGE);
            request.setAttribute("exception", e);
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

    /**
     * Overrides superclass method
     */
    @Override
    public void destroy() {
    }

    /**
     * Checks the type of request header
     *
     * @param request - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @return true if there is an AJAX request
     */
    private boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }
}
