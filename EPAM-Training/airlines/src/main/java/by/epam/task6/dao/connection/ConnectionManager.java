package by.epam.task6.dao.connection;

import by.epam.task6.dao.DataBaseConfigManager;
import by.epam.task6.dao.connection.exception.ConnectionException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * The ConnectionManager class provides methods for opening/closing a certain amount of connections to DB,
 * providing opened connections and getting them back
 *
 * @author dmitr_000
 * @version 1.00 06-May-15
 */
public final class ConnectionManager {
    private static volatile ConnectionManager instance;
    private String url;
    private String user;
    private String password;
    private String driver;
    private int queueSize;
    private BlockingQueue<Connection> connectionsQueue;

    /**
     * Default private constructor. Gets parameters of DB connections from resource file.
     *
     * @throws ConnectionException
     */
    private ConnectionManager() throws ConnectionException {
        DataBaseConfigManager configManager = DataBaseConfigManager.getInstance();
        driver = configManager.getValue("driver");
        url = configManager.getValue("url");
        user = configManager.getValue("user");
        password = configManager.getValue("password");
        try {
            queueSize = Integer.parseInt(configManager.getValue("queue_size"));
        } catch (NumberFormatException e) {
            throw new ConnectionException("Can't read queueSize as integer value from database.properties", e);
        }
    }

    //lazy synchronization

    /**
     * Returns an instance of {@code ConnectionManager} class
     *
     * @return an instance of ConnectionManager class
     * @throws ConnectionException
     */
    public static ConnectionManager getInstance() throws ConnectionException {
        ConnectionManager localInstance = instance;
        if (localInstance == null) {
            synchronized (ConnectionManager.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ConnectionManager();
                }
            }
        }
        return localInstance;
    }

    /**
     * Initializes queue of connections to DB
     *
     * @throws ConnectionException
     */
    public void initConnectionsQueue() throws ConnectionException {
        try {
            Class.forName(driver);
            connectionsQueue = new ArrayBlockingQueue<>(queueSize);
            for (int i = 0; i < queueSize; i++) {
                connectionsQueue.add(DriverManager.getConnection(url, user, password));
            }
        } catch (SQLException e) {
            throw new ConnectionException("Can't get connection", e);
        } catch (ClassNotFoundException e) {
            throw new ConnectionException("Can't get Driver class", e);
        }
    }

    /**
     * Gets opened connection from queue
     *
     * @return opened connection
     * @throws ConnectionException
     */
    public Connection takeConnection() throws ConnectionException {
        Connection conn;
        try {
            conn = connectionsQueue.poll(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new ConnectionException("Can't take connection from pool", e);
        }
        return conn;
    }

    /**
     * Puts back opened connection to queue
     *
     * @param connection an opened connection to put back to queue
     * @throws ConnectionException
     */
    public void releaseConnection(Connection connection) throws ConnectionException {
        try {
            if (connection != null) {
                if (!connection.isClosed()) {
                    connectionsQueue.offer(connection);
                } else
                    throw new ConnectionException("Connection has been already closed");
            } else
                throw new ConnectionException("Null-referenced connection");
        } catch (SQLException e) {
            throw new ConnectionException("Can't process connection correctly", e);
        }
    }

    /**
     * Closes all connections to DB in queue if it's full, waits otherwise
     *
     * @throws ConnectionException
     */
    public void closeAllConnections() throws ConnectionException {
        try {
            if (connectionsQueue.size() == queueSize)
                for (Connection c : connectionsQueue)
                    c.close();
            else
                Thread.currentThread().wait(2000);
        } catch (SQLException e) {
            throw new ConnectionException("Can't close connection", e);
        } catch (InterruptedException e) {
            throw new ConnectionException("Thread waiting exception", e);
        }
    }
}
