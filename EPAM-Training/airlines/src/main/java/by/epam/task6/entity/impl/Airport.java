package by.epam.task6.entity.impl;

import by.epam.task6.entity.IEntity;

/**
 * The Airport class represents entries of 'airports' table in DB.
 * It provides getters and setters for every field of each entry.
 *
 * @author dmitr_000
 * @version 1.00 13-May-15
 */
public class Airport implements IEntity {
    private int id;
    private String name;
    private String city;

    /**
     * Default constructor
     */
    public Airport() {
    }

    /**
     * Constructor
     *
     * @param id specifies airport ID
     */
    public Airport(String id) {
        this.id = Integer.valueOf(id);
    }

    /**
     * Constructor
     *
     * @param name specifies airport name
     * @param city specifies airport city
     */
    public Airport(String name, String city) {
        this.name = name;
        this.city = city;
    }

    /**
     * Constructor
     *
     * @param id   specifies airport ID
     * @param name specifies airport name
     * @param city specifies airport city
     */
    public Airport(int id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    /**
     * Gets airport ID
     *
     * @return airport ID
     */
    public int getId() {
        return id;
    }

    /**
     * Sets airport ID
     *
     * @param id specifies airport ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets airport name
     *
     * @return airport name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets airport name
     *
     * @param name specifies airport name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets airport city
     *
     * @return airport city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets airport city
     *
     * @param city specifies airport city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Returns a String object representing this Airport's data
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return name + ", " + city;
    }
}
