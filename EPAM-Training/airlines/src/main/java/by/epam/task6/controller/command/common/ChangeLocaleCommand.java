package by.epam.task6.controller.command.common;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The ChangeLocaleCommand class changes the localization parameter stored in current session
 *
 * @author dmitr_000
 * @version 1.00 26-May-15
 */
public class ChangeLocaleCommand implements ICommand {
    private final static String JSON = "json";
    private static final String LOCALE = "locale";

    /**
     * Changes the localization parameter stored in current session
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            String locale = mapper.readValue(json, String.class);
            HttpSession session = request.getSession(true);
            session.setAttribute(LOCALE, locale);
            mapper.writeValue(response.getOutputStream(), 1);
        } catch (IOException e) {
            throw new CommandException(e);
        }
        return null;
    }
}
