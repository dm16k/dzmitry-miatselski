package by.epam.task6.dao.mysql;

import by.epam.task6.dao.IDao;
import by.epam.task6.dao.IDaoFactory;

/**
 * The MySQLDaoFactory class represents factory method to use different DAOs
 *
 * @author dmitr_000
 * @version 1.00 05-May-15
 */
public class MySQLDaoFactory implements IDaoFactory {
    private final static MySQLDaoFactory instance = new MySQLDaoFactory();

    /**
     * Default private constructor
     */
    private MySQLDaoFactory() {
    }

    /**
     * Gets an instance of MySQLDaoFactory class
     *
     * @return an instance of MySQLDaoFactory class
     */
    public static MySQLDaoFactory getInstance() {
        return instance;
    }

    /**
     * Gets DAO object by name
     *
     * @param name - name of DAO
     * @return DAO object
     */
    public IDao getDao(String name) {
        IDao dao;
        DaoType type = DaoType.valueOf(name.toUpperCase());
        switch (type) {
            case AIRPORTS:
                dao = DAOAirports.getInstance();
                break;
            case CREWS:
                dao = DAOCrews.getInstance();
                break;
            case EMPLOYEES:
                dao = DAOEmployees.getInstance();
                break;
            case FLIGHTS:
                dao = DAOFlights.getInstance();
                break;
            case PLANES:
                dao = DAOPlanes.getInstance();
                break;
            case PROFESSIONS:
                dao = DAOProfessions.getInstance();
                break;
            case STAFF:
                dao = DAOStaff.getInstance();
                break;
            default:
                throw new EnumConstantNotPresentException(DaoType.class, type.name());
        }
        return dao;
    }

    /**
     * Enumeration of all DAOs
     */
    public enum DaoType {
        AIRPORTS, CREWS, EMPLOYEES, FLIGHTS, PLANES, PROFESSIONS, STAFF
    }
}
