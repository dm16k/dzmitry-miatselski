package by.epam.task6.controller.command;

/**
 * Enumerations of all command constants
 *
 * @author dmitr_000
 * @version 1.00 06-May-15
 */
public enum CommandName {
    ADD_FLIGHT,
    ADD_CREW,
    CHANGE_LOCALE,
    DELETE_ACCOUNT,
    DELETE_FLIGHT,
    DELETE_CREW,
    FILTER_FLIGHTS,
    LOAD_DATA_TO_EDIT_FLIGHTS,
    LOAD_DATA_TO_EDIT_CREWS,
    OPEN_ACCOUNTS,
    OPEN_CREWS,
    OPEN_FLIGHTS,
    OPEN_PLANES,
    OPEN_REGISTER,
    REGISTER,
    SIGN_IN,
    SIGN_OUT,
    UPDATE_ACCOUNT,
    UPDATE_FLIGHT,
    UPDATE_CREW,
    WRONG_COMMAND
    }