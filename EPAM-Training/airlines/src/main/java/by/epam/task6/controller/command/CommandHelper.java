package by.epam.task6.controller.command;

import by.epam.task6.controller.command.admin.*;
import by.epam.task6.controller.command.authorized.OpenCrewsCommand;
import by.epam.task6.controller.command.authorized.SignOutCommand;
import by.epam.task6.controller.command.common.ChangeLocaleCommand;
import by.epam.task6.controller.command.common.FilterFlightsListCommand;
import by.epam.task6.controller.command.common.OpenFlightsCommand;
import by.epam.task6.controller.command.common.WrongCommand;
import by.epam.task6.controller.command.dispatcher.AddCrewCommand;
import by.epam.task6.controller.command.dispatcher.DeleteCrewCommand;
import by.epam.task6.controller.command.dispatcher.LoadDataToEditCrewsCommand;
import by.epam.task6.controller.command.dispatcher.UpdateCrewCommand;
import by.epam.task6.controller.command.guest.OpenRegisterCommand;
import by.epam.task6.controller.command.guest.RegisterCommand;
import by.epam.task6.controller.command.guest.SignInCommand;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * The CommandHelper class has method that gets commands by their name
 *
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public class CommandHelper {
    private final static Logger LOGGER = Logger.getLogger(CommandHelper.class);
    private final static CommandHelper instance = new CommandHelper();
    private Map<CommandName, ICommand> commands = new HashMap<CommandName, ICommand>();

    /**
     * Default private constructor.
     * Sets a {@code HashMap<CommandName, ICommand>} storing commands
     */
    private CommandHelper() {
        commands.put(CommandName.CHANGE_LOCALE, new ChangeLocaleCommand());
        commands.put(CommandName.WRONG_COMMAND, new WrongCommand());
        commands.put(CommandName.OPEN_FLIGHTS, new OpenFlightsCommand());
        commands.put(CommandName.OPEN_REGISTER, new OpenRegisterCommand());
        commands.put(CommandName.FILTER_FLIGHTS, new FilterFlightsListCommand());
        commands.put(CommandName.SIGN_IN, new SignInCommand());
        commands.put(CommandName.SIGN_OUT, new SignOutCommand());
        commands.put(CommandName.REGISTER, new RegisterCommand());
        commands.put(CommandName.LOAD_DATA_TO_EDIT_FLIGHTS, new LoadDataToEditFlightsCommand());
        commands.put(CommandName.ADD_FLIGHT, new AddFlightCommand());
        commands.put(CommandName.DELETE_FLIGHT, new DeleteFlightCommand());
        commands.put(CommandName.UPDATE_FLIGHT, new UpdateFlightCommand());
        commands.put(CommandName.OPEN_CREWS, new OpenCrewsCommand());
        commands.put(CommandName.LOAD_DATA_TO_EDIT_CREWS, new LoadDataToEditCrewsCommand());
        commands.put(CommandName.ADD_CREW, new AddCrewCommand());
        commands.put(CommandName.DELETE_CREW, new DeleteCrewCommand());
        commands.put(CommandName.UPDATE_CREW, new UpdateCrewCommand());
        commands.put(CommandName.OPEN_ACCOUNTS, new OpenAccountsCommand());
        commands.put(CommandName.DELETE_ACCOUNT, new DeleteAccountCommand());
        commands.put(CommandName.UPDATE_ACCOUNT, new UpdateAccountCommand());
    }

    /**
     * Gets an instance of CommandHelper class
     *
     * @return an instance of CommandHelper class
     */
    public static CommandHelper getInstance() {
        return instance;
    }

    /**
     * Gets command by its name
     *
     * @param commandName - a string name of command
     * @return command
     */
    public ICommand getCommand(String commandName) {
        CommandName name = null;
        try {
            name = CommandName.valueOf(commandName.toUpperCase());
        } catch (IllegalArgumentException e) {
            LOGGER.error("Enum constant with name" + commandName + " isn't presented");
        }
        ICommand command;
        if (name != null) {
            command = commands.get(name);
        } else {
            command = commands.get(CommandName.WRONG_COMMAND);
        }
        return command;
    }
}
