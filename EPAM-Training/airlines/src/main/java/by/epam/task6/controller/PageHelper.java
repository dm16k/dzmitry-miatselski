package by.epam.task6.controller;

import java.util.ResourceBundle;

/**
 * The PageHelper class has method that gets pages locations from resource file
 *
 * @author dmitr_000
 * @version 1.00 17-May-15
 */
public class PageHelper {
    public final static String START_PAGE = "START_PAGE";
    public final static String ACCOUNTS_PAGE = "ACCOUNTS_PAGE";
    public final static String CREWS_PAGE = "CREWS_PAGE";
    public final static String FLIGHTS_PAGE = "FLIGHTS_PAGE";
    public final static String ERROR_PAGE = "ERROR_PAGE";
    public final static String ACCESS_DENIED_PAGE = "ACCESS_DENIED_PAGE";
    public final static String REGISTER_PAGE = "REGISTER_PAGE";
    private final static String BUNDLE_NAME = "pages";
    private static PageHelper instance;
    private ResourceBundle bundle;

    /**
     * Default private constructor
     */
    private PageHelper() {
        bundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    /**
     * Gets an instance of PageHelper class
     *
     * @return an instance of PageHelper class
     */
    public static PageHelper getInstance() {
        PageHelper local = instance;
        if (local == null) {
            synchronized (PageHelper.class) {
                local = instance;
                if (local == null) {
                    instance = local = new PageHelper();
                }
            }
        }
        return local;
    }

    /**
     * Gets location of page by {@code key}
     *
     * @param key - a string key
     * @return location of needed page
     */
    public String getProperty(String key) {
        return (String) bundle.getObject(key);
    }
}
