package by.epam.task6.controller.command.authorized;

import by.epam.task6.controller.PageHelper;
import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.controller.command.dispatcher.LoadDataToEditCrewsCommand;
import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.IDaoFactory;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.IEntity;
import by.epam.task6.entity.impl.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * The OpenCrewsCommand loads data and gets location of page with crews.
 *
 * @author dmitr_000
 * @version 1.00 27-May-15
 */
public class OpenCrewsCommand implements ICommand {
    private final static String USER = "user";
    private final static String CREWS = "crews";
    private final static String DISPATCHER = "dispatcher";

    /**
     * Loads data of crews and gets location of page with crews.
     * If current user has 'DISPATCHER' status it also loads data for editing.
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return location of crews page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        IDaoFactory daoFactory = MySQLDaoFactory.getInstance();
        List<IEntity> crews;
        try {
            crews = daoFactory.getDao(MySQLDaoFactory.DaoType.CREWS.name()).readAll();
            request.setAttribute(CREWS, crews);
        } catch (DAOException e) {
            throw new CommandException("Can't load crews list", e);
        }
        HttpSession session = request.getSession(true);
        Account user = (Account) session.getAttribute(USER);
        if (user.getStatus().compareTo(DISPATCHER) == 0) {
            new LoadDataToEditCrewsCommand().execute(request, response);
        }
        return PageHelper.getInstance().getProperty(PageHelper.CREWS_PAGE);
    }
}
