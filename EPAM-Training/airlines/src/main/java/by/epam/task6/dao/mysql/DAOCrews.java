package by.epam.task6.dao.mysql;

import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.IDao;
import by.epam.task6.dao.connection.exception.ConnectionException;
import by.epam.task6.dao.connection.ConnectionManager;
import by.epam.task6.entity.IEntity;
import by.epam.task6.entity.impl.Crew;
import by.epam.task6.entity.impl.Employee;
import by.epam.task6.entity.impl.Profession;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The DAOCrews class provides CRUD methods to work with 'crews' and 'crews_has_employees' tables of DB
 *
 * @author dmitr_000
 * @version 1.00 06-May-15
 */
public class DAOCrews implements IDao {
    private static final Logger LOGGER = Logger.getLogger(DAOCrews.class);
    private static final DAOCrews instance = new DAOCrews();
    private final String READ_ALL_QUERY = "SELECT * FROM crews;";
    private final String INSERT_QUERY = "INSERT INTO crews (crew_id)  VALUES(?);";
    private final String UPDATE_QUERY = "UPDATE crews SET crew_id = ? WHERE id = ?;";
    private final String DELETE_QUERY = "DELETE FROM crews WHERE id = ?;";
    private final String GET_EMPLOYEES_BY_ID_QUERY = "SELECT " +
            "e.id, e.first_name, e.last_name, e.profession, " +
            "p.name, e.dob FROM crews_has_employees c " +
            "JOIN employees e ON e.id = c.employees_id " +
            "JOIN professions p ON p.id = e.profession " +
            "WHERE crews_id = ?;";
    private final String INSERT_EMPLOYEE_QUERY = "INSERT INTO crews_has_employees (crews_id, employees_id) VALUES (LAST_INSERT_ID(),?);";
    private final String INSERT_FOR_UPDATE_EMPLOYEE_QUERY = "INSERT INTO crews_has_employees (crews_id, employees_id) VALUES (?,?);";
    private final String DELETE_EMPLOYEE_BY_CREW_ID_QUERY = "DELETE FROM crews_has_employees WHERE crews_id=?;";
    private ConnectionManager manager = null;
    private PreparedStatement preparedStatement;

    /**
     * Default private constructor
     */
    private DAOCrews() {
    }

    /**
     * Gets an instance of DAOCrews class
     *
     * @return an instance of DAOCrews class
     */
    public static DAOCrews getInstance() {
        return instance;
    }

    /**
     * Creates new Crew entry in DB
     *
     * @param entry - an object to create entry
     * @throws DAOException
     */
    public void create(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Crew.class) {
            Crew crew = (Crew) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(INSERT_QUERY);
                int crewId = crew.getCrewId();
                preparedStatement.setInt(1, crewId);
                preparedStatement.executeUpdate();
                preparedStatement.close();
                List<Employee> employees = crew.getEmployees();
                if (employees.size() != 0) {
                    for (Employee employee : employees) {
                        if (employee != null) {
                            preparedStatement = cn.prepareStatement(INSERT_EMPLOYEE_QUERY);
                            preparedStatement.setInt(1, employee.getId());
                            preparedStatement.executeUpdate();
                            preparedStatement.close();
                        }
                    }
                }
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * Gets all entries from 'crews' table of DB
     *
     * @return list of objects, representing entries in DB
     * @throws DAOException
     */
    public List<IEntity> readAll() throws DAOException {
        Connection cn = null;
        List<IEntity> list = null;
        try {
            manager = ConnectionManager.getInstance();
            cn = manager.takeConnection();
            preparedStatement = cn.prepareStatement(READ_ALL_QUERY);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt(1);
                int crew_id = rs.getInt(2);
                PreparedStatement ps = cn.prepareStatement(GET_EMPLOYEES_BY_ID_QUERY);
                ps.setInt(1, id);
                ResultSet rsWithEmployees = ps.executeQuery();
                List<Employee> employees = new ArrayList<>();
                while (rsWithEmployees.next()) {
                    int employeeId = rsWithEmployees.getInt(1);
                    String firstName = rsWithEmployees.getString(2);
                    String lastName = rsWithEmployees.getString(3);
                    int profession = rsWithEmployees.getInt(4);
                    String name = rsWithEmployees.getString(5);
                    Date dob = rsWithEmployees.getDate(6);
                    Profession professionEntity = new Profession(profession, name);
                    Employee employee = new Employee(employeeId, firstName, lastName, professionEntity, dob);
                    employees.add(employee);
                }
                rsWithEmployees.close();
                IEntity crew = new Crew(id, crew_id, employees);
                list.add(crew);
            }
            rs.close();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new DAOException("Can't execute PS", e);
        } catch (ConnectionException e) {
            throw new DAOException("Can't get ConnectionManager's instance", e);
        } finally {
            closePSAndReleaseConnection(cn, preparedStatement);
        }
        return list;
    }

    /**
     * Updates existing Crew entry in DB
     *
     * @param entry - an object to update entry
     * @throws DAOException
     */
    public void update(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Crew.class) {
            Crew crew = (Crew) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(UPDATE_QUERY);
                preparedStatement.setInt(1, crew.getCrewId());
                preparedStatement.setInt(2, crew.getId());
                preparedStatement.executeUpdate();
                updateEmployees(crew);
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * In 'crews_has_employees' table of DB this method updates data for employees of particular crew when it changes
     *
     * @param crew - changed crew
     * @throws DAOException
     */
    public void updateEmployees(Crew crew) throws DAOException {
        Connection cn = null;
        try {
            manager = ConnectionManager.getInstance();
            cn = manager.takeConnection();
            int crew_id = crew.getId();
            deleteEmployee(crew_id, cn);
            List<Employee> employees = crew.getEmployees();
            if (employees != null && employees.size() != 0) {
                for (Employee e : employees)
                    if (e != null) {
                        insertEmployee(crew_id, e.getId(), cn);
                    }
            }
        } catch (ConnectionException e) {
            throw new DAOException("Can't get ConnectionManager's instance", e);
        } finally {
            closePSAndReleaseConnection(cn, preparedStatement);
        }
    }

    /**
     * Deletes Crew entry from DB
     *
     * @param entry - an object to delete entry
     * @throws DAOException
     */
    public void delete(IEntity entry) throws DAOException {
        Connection cn = null;
        if (entry.getClass() == Crew.class) {
            Crew crew = (Crew) entry;
            try {
                manager = ConnectionManager.getInstance();
                cn = manager.takeConnection();
                preparedStatement = cn.prepareStatement(DELETE_QUERY);
                preparedStatement.setInt(1, crew.getId());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                throw new DAOException("Can't execute PS", e);
            } catch (ConnectionException e) {
                throw new DAOException("Can't get ConnectionManager's instance", e);
            } finally {
                closePSAndReleaseConnection(cn, preparedStatement);
            }
        } else
            throw new DAOException("Trying to create wrong entity");
    }

    /**
     * In 'crews_has_employees' table of DB this method inserts new entries for employees of particular crew
     *
     * @param crew_id     - crew ID
     * @param employee_id - employee ID
     * @param cn          - connection to DB
     * @throws DAOException
     */
    private void insertEmployee(int crew_id, int employee_id, Connection cn) throws DAOException {
        try {
            PreparedStatement ps = cn.prepareStatement(INSERT_FOR_UPDATE_EMPLOYEE_QUERY);
            ps.setInt(1, crew_id);
            ps.setInt(2, employee_id);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new DAOException("Can't execute PS for " + INSERT_EMPLOYEE_QUERY, e);
        }
    }

    /**
     * In 'crews_has_employees' table of DB this method deletes entries of particular crew
     *
     * @param crew_id - crew ID
     * @param cn      - connection to DB
     * @throws DAOException
     */
    private void deleteEmployee(int crew_id, Connection cn) throws DAOException {
        try {
            PreparedStatement ps = cn.prepareStatement(DELETE_EMPLOYEE_BY_CREW_ID_QUERY);
            ps.setInt(1, crew_id);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new DAOException("Can't execute PS for " + DELETE_EMPLOYEE_BY_CREW_ID_QUERY, e);
        }
    }

    /**
     * Closes PreparedStatement object and puts connection back to ConnectionManager
     *
     * @param c  - a connection to release
     * @param ps - a statement to close
     */
    private void closePSAndReleaseConnection(Connection c, PreparedStatement ps) {
        if (manager != null) {
            try {
                if (ps != null && !ps.isClosed())
                    ps.close();
                if (c != null)
                    manager.releaseConnection(c);
            } catch (SQLException e) {
                LOGGER.error("Can't close PS", e);
            } catch (ConnectionException e) {
                LOGGER.error("Can't release connection", e);
            }
        } else
            LOGGER.error("Trying to release connection to null-referenced pool");
    }
}
