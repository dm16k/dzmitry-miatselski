package by.epam.task6.controller.command.common;

import by.epam.task6.controller.PageHelper;
import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.controller.command.admin.LoadDataToEditFlightsCommand;
import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.IDao;
import by.epam.task6.dao.IDaoFactory;
import by.epam.task6.dao.mysql.DAOFlights;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.IEntity;
import by.epam.task6.entity.impl.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * The FilterFlightsListCommand loads flights data depending on selected time range and airport and gets location of page with flights.
 *
 * @author dmitr_000
 * @version 1.00 13-May-15
 */
public class FilterFlightsListCommand implements ICommand {
    private final static String DATE_FROM = "dateFrom";
    private final static String DATE_TO = "dateTo";
    private final static String FLIGHTS = "flights";
    private final static String SELECTED_AIRPORT = "airportSelected";
    private final static String USER = "user";

    /**
     * Loads flights data depending on selected time range and airport and gets location of page with flights.
     * If current user has 'ADMIN' status it also loads data for editing.
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return location of flights page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        IDaoFactory factory = MySQLDaoFactory.getInstance();
        try {
            List<IEntity> airports = factory.getDao(MySQLDaoFactory.DaoType.AIRPORTS.name()).readAll();
            request.setAttribute("airports", airports);
        } catch (DAOException e) {
            throw new CommandException("Can't load airports list", e);
        }
        try {
            IDao dao = factory.getDao(MySQLDaoFactory.DaoType.FLIGHTS.name());
            if (dao != null && dao.getClass() == DAOFlights.class) {
                DAOFlights daoFlights = (DAOFlights) dao;
                String airport = request.getParameter("airport");
                int id = Integer.valueOf(airport);
                String dateFrom = request.getParameter(DATE_FROM);
                String dateTo = request.getParameter(DATE_TO);
                request.setAttribute(DATE_FROM, dateFrom);
                request.setAttribute(DATE_TO, dateTo);
                request.setAttribute(SELECTED_AIRPORT, id);
                List<IEntity> flights = daoFlights.readByAirportIdBetweenDates(id, dateFrom, dateTo);
                request.setAttribute(FLIGHTS, flights);
            }
        } catch (DAOException e) {
            throw new CommandException("Can't load airports list", e);
        }
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute(USER);
        if (user.isAdmin()) {
            new LoadDataToEditFlightsCommand().execute(request, response);
        }
        return PageHelper.getInstance().getProperty(PageHelper.FLIGHTS_PAGE);
    }
}
