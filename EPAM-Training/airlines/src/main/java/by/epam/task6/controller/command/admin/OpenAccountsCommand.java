package by.epam.task6.controller.command.admin;

import by.epam.task6.controller.PageHelper;
import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.IEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The OpenAccountsCommand loads data and gets location of page with accounts
 *
 * @author dmitr_000
 * @version 1.00 28-May-15
 */

public class OpenAccountsCommand implements ICommand {
    private final static String ACCOUNTS = "accounts";

    /**
     * Loads data and gets location of page with accounts
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return location of accounts page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            List<IEntity> accounts = MySQLDaoFactory.getInstance().getDao(MySQLDaoFactory.DaoType.STAFF.name()).readAll();
            request.setAttribute(ACCOUNTS, accounts);
        } catch (DAOException e) {
            throw new CommandException("Can't load accounts list", e);
        }
        return PageHelper.getInstance().getProperty(PageHelper.ACCOUNTS_PAGE);
    }
}
