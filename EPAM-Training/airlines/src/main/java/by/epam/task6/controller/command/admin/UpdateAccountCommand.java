package by.epam.task6.controller.command.admin;

import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.impl.Account;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * The UpdateAccountCommand class updates account entry in DB
 *
 * @author dmitr_000
 * @version 1.00 28-May-15
 */
public class UpdateAccountCommand implements ICommand {
    private final static String JSON = "json";

    /**
     * Updates account entry in DB
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            Account account = mapper.readValue(json, Account.class);
            int res = 1;
            try {
                MySQLDaoFactory.getInstance().getDao(MySQLDaoFactory.DaoType.STAFF.name()).update(account);
            } catch (DAOException e) {
                res = -1;
            }
            mapper.writeValue(response.getOutputStream(), res);
        } catch (IOException e) {
            throw new CommandException(e);
        }
        return null;
    }
}
