package by.epam.task6.controller.command.common;

import by.epam.task6.controller.PageHelper;
import by.epam.task6.controller.command.exception.CommandException;
import by.epam.task6.controller.command.ICommand;
import by.epam.task6.controller.command.admin.LoadDataToEditFlightsCommand;
import by.epam.task6.dao.exception.DAOException;
import by.epam.task6.dao.IDao;
import by.epam.task6.dao.IDaoFactory;
import by.epam.task6.dao.mysql.DAOFlights;
import by.epam.task6.dao.mysql.MySQLDaoFactory;
import by.epam.task6.entity.IEntity;
import by.epam.task6.entity.impl.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * The OpenFlightsCommand loads data and gets location of page with flights.
 *
 * @author dmitr_000
 * @version 1.00 13-May-15
 */
public class OpenFlightsCommand implements ICommand {
    private final static String AIRPORTS = "airports";
    private final static String DATE_FROM = "dateFrom";
    private final static String DATE_TO = "dateTo";
    private final static String FLIGHTS = "flights";
    private final static String USER = "user";
    private final static int ANY_AIRPORT_INDEX = 0;

    /**
     * Loads data of flights and gets location of flights page.
     * If current user has 'ADMIN' status it also loads data for editing.
     *
     * @param request  - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @param response - an {@code HttpServletResponse} object that contains the response the servlet sends to the client
     * @return location of flights page
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = PageHelper.getInstance().getProperty(PageHelper.FLIGHTS_PAGE);
        IDaoFactory factory = MySQLDaoFactory.getInstance();
        List<IEntity> airports;
        try {
            airports = factory.getDao(MySQLDaoFactory.DaoType.AIRPORTS.name()).readAll();
            request.setAttribute(AIRPORTS, airports);
        } catch (DAOException e) {
            throw new CommandException("Can't load airports list", e);
        }
        try {
            IDao dao = factory.getDao(MySQLDaoFactory.DaoType.FLIGHTS.name());
            if (dao.getClass() == DAOFlights.class &&
                    airports != null && airports.size() != 0) {
                int id = ANY_AIRPORT_INDEX;
                Calendar c = new GregorianCalendar();
                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
                String dateStart = f.format(c.getTime());
                request.setAttribute(DATE_FROM, dateStart);
                c.add(Calendar.DAY_OF_YEAR, 30);
                String dateEnd = f.format(c.getTime());
                request.setAttribute(DATE_TO, dateEnd);
                List<IEntity> flights = ((DAOFlights) dao).readByAirportIdBetweenDates(id, dateStart, dateEnd);
                request.setAttribute(FLIGHTS, flights);
            }
        } catch (DAOException e) {
            throw new CommandException("Can't load flights list", e);
        }
        HttpSession session = request.getSession(true);
        Account user = (Account) session.getAttribute(USER);
        if (user.isAdmin()) {
            new LoadDataToEditFlightsCommand().execute(request, response);
        }
        return page;
    }
}
