<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <origin>
            <xsl:apply-templates select="//model"/>
        </origin>
    </xsl:template>
    <xsl:template match="model">
        <xsl:element name="gun">
            <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
            </xsl:attribute>
            <xsl:element name="type">
                <xsl:value-of select="type"/>
            </xsl:element>
            <xsl:element name="handy">
                <xsl:value-of select="handy"/>
            </xsl:element>
            <xsl:element name="characteristics">
                <xsl:element name="range">
                    <xsl:attribute name="unit">
                        <xsl:value-of select="characteristics/range/@unit"/>
                    </xsl:attribute>
                    <xsl:element name="firing">
                        <xsl:value-of select="characteristics/range/firing"/>
                    </xsl:element>
                    <xsl:element name="projectile">
                        <xsl:value-of select="characteristics/range/projectile"/>
                    </xsl:element>
                </xsl:element>
                <xsl:element name="feedSystem">
                    <xsl:attribute name="type">
                        <xsl:value-of select="characteristics/feedSystem/@type"/>
                    </xsl:attribute>
                    <xsl:for-each select="characteristics/feedSystem/capacity">
                        <xsl:element name="capacity">
                            <xsl:value-of select="."/>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:element>
                <xsl:element name="optics">
                    <xsl:for-each select="characteristics/optics/variant">
                        <xsl:element name="variant">
                            <xsl:value-of select="."/>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>