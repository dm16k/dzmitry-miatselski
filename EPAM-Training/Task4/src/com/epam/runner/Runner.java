package com.epam.runner;


import com.sun.xml.internal.ws.developer.ValidationErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * @author dmitr_000
 * @version 1.00 20-Apr-15
 */
public class Runner {
    private static void validate() {
        try {
            SchemaFactory sf = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            File f = new File("src/resources/guns.xsd");
            Schema sc = sf.newSchema(f);
            Validator v = sc.newValidator();
            v.setErrorHandler(new ValidationErrorHandler() {
                @Override
                public void warning(SAXParseException exception) throws SAXException {
                    System.out.println(exception.toString());
                }

                @Override
                public void error(SAXParseException exception) throws SAXException {
                    System.out.println(exception.toString());
                }

                @Override
                public void fatalError(SAXParseException exception) throws SAXException {
                    System.out.println(exception.toString());
                }
            });
            v.validate(new StreamSource("src/resources/guns.xml"));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static void transform() {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource("src\\resources\\guns.xsl"));
            transformer.transform(new StreamSource("src\\resources\\guns2.xml"), new StreamResult("src\\resources\\newGuns.xml"));
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        System.out.println("Validation...");
        validate();
        System.out.println("Done!");
        System.out.println("Transformation...");
        transform();
        System.out.println("Done!");
    }
}
