package by.epam.task1.entities;

/**
 * An abstract class of vegetables
 * This class provides methods of counting calories and weight of vegetable used to make a salad
 *
 * @author dmitr_000
 * @version 1.00 31.03.2015
 */
public abstract class Vegetable implements Comparable<Vegetable> {
    private double weight;
    private double calories;

    /**
     * Constructor
     *
     * @param weight   specifies weight of vegetables
     * @param calories specifies calorie per 100 gram of vegetables
     */
    public Vegetable(double weight, double calories) {
        this.weight = weight;
        this.calories = calories;
    }

    /**
     * Gets weight of vegetable used to make a salad
     *
     * @return used weight
     */
    public double getUsedWeight() {
        return weight * usage();
    }

    /**
     * Gets total calories of vegetable in salad
     *
     * @return total calories
     */
    public double getTotalCalories() {
        return calories * getUsedWeight() / 100;
    }

    /**
     * Gets calories per 100 gram of vegetable
     *
     * @return calories
     */
    public double getCalories() {
        return calories;
    }

    /**
     * Gets weight of vegetable
     *
     * @return weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * An Abstract method representing usage of current vegetable in salad
     *
     * @return double value between 0 and 1
     */
    public abstract double usage();

    /**
     * Returns true if and only if the argument is not null and is a Vegetable object that represents the same double values as this object
     *
     * @param o - the object to compare with
     * @return true if the Vegetable objects represent the same value; false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vegetable vegetable = (Vegetable) o;

        return Double.compare(vegetable.calories, calories) == 0 && Double.compare(vegetable.weight, weight) == 0;

    }

    /**
     * Returns a hash code for a double values
     *
     * @return hash code for this Vegetable object
     */
    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(weight);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(calories);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    /**
     * Returns a String object representing this Vegetable's value
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return "calories=" + calories +
                ", weight=" + weight;
    }

    /**
     * Compares this Vegetable instance with another.
     *
     * @param o - the Vegetable instance to be compared
     * @return a positive value if this objects calorie is higher than the argument's calorie; a negative value if this objects calorie is lower than the argument's calorie;
     * if calories are equal method returns zero if this object represents the same weight values as the argument;
     * a positive value if this objects weight is higher than the argument's calorie; and a negative value otherwise;
     */
    @Override
    public int compareTo(Vegetable o) {
        if (o == null)
            return 1;
        if (Double.compare(this.calories, o.getCalories()) == 0)
            return (Double.compare(this.weight, o.getWeight()));
        else
            return Double.compare(this.calories, o.getCalories());
    }
}