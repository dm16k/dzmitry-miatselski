package by.epam.task1.entities;

/**
 * Enumeration of all sorts of cabbage
 * @author dmitr_000
 * @version 1.00 31.03.2015
 */
public enum CabaggeSort {
    BROCCOLI, BEIJING, WHITE
}