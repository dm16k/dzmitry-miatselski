package by.epam.task1.entities;

/**
 * The Cucumber class provides methods of counting calories and weight of cucumbers used to make a salad
 * @author dmitr_000
 * @version 1.00 31.03.2015
 */
public class Cucumber extends Vegetable {
    private double length;

    /**
     * Constructor
     * @param weight specifies weight of cucumbers
     * @param calories specifies calorie per 100 gram of cucumbers
     */
    public Cucumber(double weight, double calories) {
        super(weight, calories);
    }

    /**
     * Sets length of cucumbers
     * @param length specifies length of current cucumbers
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * Returns double value that represents usage of cucumber in salad
     * @return double value of 1 if length less or equal to 6; 0.99 otherwise
     */
    @Override
    public double usage() {
        if (length <= 6)
            return 1;
        else
            return 0.99;
    }

    /**
     * Returns true if and only if superclasses are equal, the argument is not null and is a Cucumber object that represents the same double value as this object
     * @param o - the object to compare with
     * @return true if the Cucumber objects represent the same value; false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Cucumber cucumber = (Cucumber) o;

        return Double.compare(cucumber.length, length) == 0;

    }

    /**
     * Returns a hash code for a double value and superclass Vegetables
     * @return hash code for this Cucumber object
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(length);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    /**
     * Returns a String object representing this Cucumber's value
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return "Cucumber\t{" +
                super.toString() +
                ", length=" + length +
                '}';
    }
}