package by.epam.task1.entities;

/**
 * The Tomato class provides methods of counting calories and weight of tomatoes used to make a salad
 *
 * @author dmitr_000
 * @version 1.00 31.03.2015
 */
public class Tomato extends Vegetable {
    private TomatoColor color;

    /**
     * Constructor
     *
     * @param weight   specifies weight of tomatoes
     * @param calories specifies calorie per 100 gram of tomatoes
     */
    public Tomato(double weight, double calories) {
        super(weight, calories);
    }

    /**
     * Sets color of tomato
     *
     * @param color specifies color of current tomatoes
     */
    public void setColor(TomatoColor color) {
        this.color = color;
    }

    /**
     * Returns double value that represents usage of tomato in salad
     *
     * @return double value of 0.95
     */
    @Override
    public double usage() {
        return 0.95;
    }

    /**
     * Returns total calories of tomatoes used to make salad
     *
     * @return super.getTotalCalories() * 0.9 if the color of tomato is BLACKPRINCE, super.getTotalCalories() if the color is RED
     */
    @Override
    public double getTotalCalories() {
        switch (color) {
            case RED:
                return super.getTotalCalories();
            case BLACKPRINCE:
                return super.getTotalCalories() * 0.9;
            default:
                throw new EnumConstantNotPresentException(TomatoColor.class, color.name());
        }
    }

    /**
     * Returns true if and only if superclasses are equal, the argument is not null and is a Tomato object that represents the same enum TomatoColor value as this object
     *
     * @param o - the object to compare with
     * @return true if the Tomato objects represent the same value; false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Tomato tomato = (Tomato) o;

        return color == tomato.color;

    }

    /**
     * Returns a hash code for an enum TomatoColor value and superclass Vegetables
     *
     * @return hash code for this Tomato object
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + color.hashCode();
        return result;
    }

    /**
     * Returns a String object representing this Tomato's value
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return "Tomato\t\t{" +
                super.toString() +
                ", color=" + color.name() +
                '}';
    }
}