package by.epam.task1.entities;

/**
 * Enumeration of all colors of tomato
 * @author dmitr_000
 * @version 1.00 31.03.2015
 */
public enum TomatoColor {
    RED, BLACKPRINCE
}