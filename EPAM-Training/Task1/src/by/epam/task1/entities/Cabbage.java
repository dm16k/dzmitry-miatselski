package by.epam.task1.entities;

/**
 * The Cabbage class provides methods of counting calories and weight of cabbages used to make a salad
 * @author dmitr_000
 * @version 1.00 31.03.2015
 */
public class Cabbage extends Vegetable {
    private CabaggeSort sort;

    /**
     * Constructor
     * @param weight specifies weight of cabbages
     * @param calories specifies calorie per 100 gram of cabbages
     */
    public Cabbage(double weight, double calories) {
        super(weight, calories);
    }

    /**
     * Sets sort of cabbages
     * @param sort specifies color of current cabbages
     */
    public void setSort(CabaggeSort sort) {
        this.sort = sort;
    }

    /**
     * Returns double value that represents usage of cabbage in salad
     * @return double value of 1 if sort is Broccoli, 0.95 if sort is Beijing, 0.9 if sort is White
     */
    @Override
    public double usage() {
        switch (sort) {
            case BROCCOLI:
                return 1;
            case BEIJING:
                return 0.95;
            case WHITE:
                return 0.9;
            default:
                throw new EnumConstantNotPresentException(CabaggeSort.class, sort.name());
        }
    }

    /**
     * Returns true if and only if superclasses are equal, the argument is not null and is a Cabbage object that represents the same enum CabaggeSort value as this object
     * @param o - the object to compare with
     * @return true if the Cabbage objects represent the same value; false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Cabbage cabbage = (Cabbage) o;

        return sort == cabbage.sort;

    }

    /**
     * Returns a hash code for a enum CabaggeSort value and superclass Vegetables
     * @return hash code for this Cabbage object
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + sort.hashCode();
        return result;
    }

    /**
     * Returns a String object representing this Cabbage's value
     *
     * @return a string representation of this object
     */
    @Override
    public String toString() {
        return "Cabbage\t\t{" +
                super.toString() +
                ", sort=" + sort.name() +
                '}';
    }
}