package by.epam.task1.runner;

import by.epam.task1.entities.Cabbage;
import by.epam.task1.entities.Cucumber;
import by.epam.task1.salad.Salad;
import by.epam.task1.entities.Tomato;
import by.epam.task1.entities.CabaggeSort;
import by.epam.task1.entities.TomatoColor;
import by.epam.task1.entities.Vegetable;
import by.epam.task1.factory.VegetableFactory;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * The SaladMaker class provides methods to initialize vegetables and to create a salad, print contents of salad, the main method of application
 *
 * @author dmitr_000
 * @version 1.00 01.04.2015
 */

public class SaladMaker {
    private static final Logger log = Logger.getLogger(SaladMaker.class);
    private static double tomatoWeight = 700;
    private static double cabbageWeight = 800;
    private static double cucumberWeight = 600;
    private static double tomatoCalories = 30;
    private static double cabbageCalories = 30;
    private static double cucumberCalories = 10;
    private static double cucumberLength = 5;
    private static double minCalories = 100;
    private static double maxCalories = 800;
    private static Salad salad;

    /**
     * Initializes vegetables and creates a salad
     */
    private static void initializeVegetables() {
        ArrayList<Vegetable> list = new ArrayList<Vegetable>();
        Cucumber cucumber = (Cucumber) VegetableFactory.getVegetable("cucumber", cucumberCalories, cucumberWeight);
        cucumber.setLength(cucumberLength);
        list.add(cucumber);

        Cabbage cabbage = (Cabbage) VegetableFactory.getVegetable("cabbage", cabbageCalories, cabbageWeight);
        cabbage.setSort(CabaggeSort.BROCCOLI);
        list.add(cabbage);

        Tomato tomato = (Tomato) VegetableFactory.getVegetable("tomato", tomatoCalories, tomatoWeight);
        tomato.setColor(TomatoColor.BLACKPRINCE);
        list.add(tomato);

        salad = new Salad(list);
    }

    /**
     * Prints salad contents
     */
    private static void printSaladContents() {
        for (Vegetable v : salad.getVegetables())
            System.out.println(v.toString());
    }

    /**
     * Prints each item of vegetables list
     *
     * @param v - vegetables list
     */
    private static void printSaladContents(ArrayList<Vegetable> v) {
        if (v.size() != 0)
            for (Vegetable ve : v)
                System.out.println(ve.toString());
        else
            System.out.println("nothing to show");
    }

    /**
     * Main method of application
     *
     * @param args command line parameters
     */
    public static void main(String[] args) {
        try {
            initializeVegetables();
            printSaladContents();
            System.out.println("--");
            salad.sortContents();
            printSaladContents();
            System.out.println("--");
            System.out.println("Total calories");
            System.out.println(salad.getTotalCalories());
            System.out.println("--");
            System.out.println("Total weight");
            System.out.println(salad.getTotalWeight());
            System.out.println("--");
            System.out.println("Vegetables with calorie value in [" + minCalories + ", " + maxCalories + "] ");
            printSaladContents(salad.searchVegetables(minCalories, maxCalories));
        } catch (EnumConstantNotPresentException ex) {
            log.error("Wrong enumeration constant choice!", ex);
        } catch (Exception ex) {
            log.error("An error occurred!", ex);
        }
    }
}