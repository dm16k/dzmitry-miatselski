package by.epam.task1.salad;

import by.epam.task1.entities.Vegetable;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Salad class contains an ArrayList of vegetables used to make a salad
 * This class contains methods for counting calorific value and total weight of salad,
 * sorting vegetables by calorific value and weight,
 * searching vegetables belonging to a specific calorie range
 * @author dmitr_000
 * @version 1.00 31.03.2015
 */
public class Salad {
    private ArrayList<Vegetable> vegetables;

    /**
     * Constructor
     * @param vegetables ArrayList of vegetables in salad
     */
    public Salad(ArrayList<Vegetable> vegetables) {
        this.vegetables = vegetables;
    }

    /**
     * Returns a copy of ArrayList of vegetables in salad
     * @return a copy of vegetables' ArrayList
     */
    public ArrayList<Vegetable> getVegetables() {
        return (ArrayList<Vegetable>) vegetables.clone();
    }

    /**
     * Counts total calorie of vegetables used to make a salad
     * @return total calorie of salad
     */
    public double getTotalCalories() {
        double totalCalories = 0;
        for (Vegetable v : vegetables)
            totalCalories += v.getTotalCalories();
        return totalCalories;
    }

    /**
     * Counts total weight of vegetables used to make a salad
     * @return total weight of salad
     */
    public double getTotalWeight() {
        double totalWeight = 0;
        for (Vegetable v : vegetables)
            totalWeight += v.getUsedWeight();
        return totalWeight;
    }

    /**
     * Searches vegetables belonging to calorie range between min and max values
     * @param minCalorie minimum calorie
     * @param maxCalorie maximum calorie
     * @return an ArrayList of vegetables with calorie range between min and max values
     */
    public ArrayList<Vegetable> searchVegetables(double minCalorie, double maxCalorie) {
        ArrayList<Vegetable> v = new ArrayList<Vegetable>();
        for (Vegetable ve : vegetables)
            if (ve.getTotalCalories() >= minCalorie && ve.getTotalCalories() <= maxCalorie)
                v.add(ve);
        return v;
    }

    /**
     * Sorts list of vegetables in ascending order by calorie and weight
     */
    public void sortContents() {
        Collections.sort(vegetables);
    }
}