package by.epam.task1.factory;

import by.epam.task1.entities.Cabbage;
import by.epam.task1.entities.Cucumber;
import by.epam.task1.entities.Tomato;
import by.epam.task1.entities.Vegetable;

/**
 * The VegetableFactory class provides an only one static method of creating specified vegetable with specified calorie and weight
 *
 * @author dmitr_000
 * @version 1.00 01.04.2015
 */
public class VegetableFactory {
    /**
     * Creates specified vegetable with specified calorie and weight
     *
     * @param type     - type of vegetable to create
     * @param calories - calorie per 100 gram
     * @param weight   - weight of vegetable
     * @return specified Vegetable object
     */
    public static Vegetable getVegetable(String type, double calories, double weight) {
        Vegetables v = Vegetables.valueOf(type.toUpperCase());
        switch (v) {
            case CABBAGE: {
                return new Cabbage(weight, calories);
            }
            case CUCUMBER: {
                return new Cucumber(weight, calories);
            }
            case TOMATO: {
                return new Tomato(weight, calories);
            }
            default:
                throw new EnumConstantNotPresentException(Vegetables.class, type);
        }
    }
}