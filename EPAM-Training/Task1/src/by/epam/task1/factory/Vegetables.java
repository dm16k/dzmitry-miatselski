package by.epam.task1.factory;

/**
 * Enumeration of all vegetables
 *
 * @author dmitr_000
 * @version 1.00 01.04.2015
 */
public enum Vegetables {
    TOMATO, CABBAGE, CUCUMBER
}
