package by.epam.task2.runner;

import by.epam.task2.entity.implementors.blocks.Text;
import by.epam.task2.logic.TextLogic;
import by.epam.task2.logic.parser.Parser;
import by.epam.task2.logic.parser.implementors.ParagraphParser;
import by.epam.task2.logic.parser.implementors.SentenceParser;
import by.epam.task2.logic.parser.implementors.TextParser;
import by.epam.task2.logic.reader.DataReader;
import by.epam.task2.logic.writer.DataWriter;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class Runner {
    private Runner() {
    }

    public static void main(String[] args) {
        String baseText = new DataReader().readFile();
        Parser parser = new TextParser(new ParagraphParser(new SentenceParser()));
        Text text = (Text) parser.parse(baseText);
        new DataWriter().writeFile(text);

        System.out.println("Longest palindrome string in text is:");
        System.out.println(TextLogic.findLongestPalindrome(text.toString()));
        new DataWriter("src\\by\\epam\\task2\\out\\words.txt").writeFile(TextLogic.printWordsAscending(text));
        char first = 'a';
        char last = 'b';
        new DataWriter("src\\by\\epam\\task2\\out\\newsentences(" + first + "," + last + ").txt").
                writeFile(TextLogic.removeSubstringInSentences(text, first, last));
    }
}