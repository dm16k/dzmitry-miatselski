package by.epam.task2.logic.writer;

import by.epam.task2.entity.implementors.blocks.Text;
import by.epam.task2.logic.reader.PropertyReader;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class DataWriter {
    private static Logger LOG = Logger.getLogger(DataWriter.class);
    private String fileName;

    public DataWriter() {
        fileName = PropertyReader.getProperty("outfile");
    }

    public DataWriter(String fileName) {
        if (fileName != null && fileName.length() != 0) this.fileName = fileName;
        else this.fileName = "src\\by\\epam\\task2\\resources\\parsedtext.txt";
    }

    public void writeFile(Text text) {
        try {
            FileWriter writer = new FileWriter(new File(fileName));
            writer.write(text.toString());
            writer.close();
        } catch (IOException e) {
            LOG.error("can't write data to file:" + fileName);
            e.printStackTrace();
        }
    }

    public void writeFile(String text) {
        try {
            FileWriter writer = new FileWriter(new File(fileName));
            writer.write(text);
            writer.close();
        } catch (IOException e) {
            LOG.error("can't write data to file: " + fileName);
            e.printStackTrace();
        }
    }
}