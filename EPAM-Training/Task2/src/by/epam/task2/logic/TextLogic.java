package by.epam.task2.logic;

import by.epam.task2.entity.TextElement;
import by.epam.task2.entity.implementors.blocks.Paragraph;
import by.epam.task2.entity.implementors.blocks.Sentence;
import by.epam.task2.entity.implementors.blocks.Text;
import by.epam.task2.entity.implementors.indivisible.Word;
import by.epam.task2.logic.comparator.WordsFirstCharacterComparator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
6. Напечатать слова текста в алфавитном порядке по первой букве. Слова, начинающиеся с новой буквы, печатать с новой строки.
11. В каждом предложении текста исключить подстроку максимальной длины, начинающуюся и заканчивающуюся заданными символами.
14. В заданном тексте найти подстроку максимальной длины, являющуюся палиндромом, т.е. читающуюся справа налево и слева направо одинаково.
*/

/**
 * @author dmitr_000
 * @version 1.00 09.04.2015
 */
public class TextLogic {
    private TextLogic() {
    }

    public static String printWordsAscending(Text text) {
        List<Word> wordsList = new ArrayList<Word>();
        for (TextElement elPar : text.getElements())
            if (elPar != null && elPar.getClass() == Paragraph.class)
                for (TextElement elSent : elPar.getElements())
                    if (elSent != null && elSent.getClass() == Sentence.class)
                        for (TextElement elWord : elSent.getElements())
                            if (elWord != null && elWord.getClass() == Word.class) {
                                Word word = (Word) elWord;
                                if (!wordsList.contains(word))
                                    wordsList.add(word);
                            }
        List<Word> sortedWordsList = new ArrayList<Word>(wordsList);
        sortedWordsList.sort(new WordsFirstCharacterComparator());
        char firstChar = sortedWordsList.get(0).toString().charAt(0);
        StringBuilder builder = new StringBuilder();
        for (Word word : sortedWordsList) {
            if (Character.compare(firstChar, word.toString().charAt(0)) != 0) {
                builder.replace(builder.length() - 2, builder.length(), "\n");
            }
            builder.append(word.toString().toLowerCase()).append(", ");
            firstChar = word.toString().charAt(0);
        }
        return builder.replace(builder.length() - 2, builder.length(), "").toString();
    }

    public static String removeSubstringInSentences(Text text, char first, char last) {
        StringBuilder builder = new StringBuilder();
        for (TextElement elText : text.getElements())
            if (elText.getClass() == Paragraph.class) {
                for (TextElement elPar : elText.getElements())
                    if (elPar.getClass() == Sentence.class)
                        builder.append(removeLongestSubstring(elPar.toString(), first, last)).append(" ");
                    else builder.append(elPar.toString());
                if (builder.length() > 0)
                    builder.append('\n');
            } else builder.append(elText.toString().trim()).append('\n');
        return builder.toString();
    }

    private static String removeLongestSubstring(String string, char first, char last) {
        int firstIndex = string.indexOf(first);
        int lastIndex = string.lastIndexOf(last);
        if (firstIndex < lastIndex)
            return string.replace(string.substring(firstIndex, lastIndex + 1), "");
        else return string;
    }

    public static String findLongestPalindrome(String s) {
        if (s == null || s.length() == 0)
            return "";
        char[] s2 = addBoundaries(s.toCharArray());
        int[] p = new int[s2.length];
        int c = 0, r = 0;
        int m, n = 0;
        for (int i = 1; i < s2.length; i++) {
            if (i > r) {
                p[i] = 0;
                m = i - 1;
                n = i + 1;
            } else {
                int i2 = c * 2 - i;
                if (p[i2] < (r - i)) {
                    p[i] = p[i2];
                    m = -1;
                } else {
                    p[i] = r - i;
                    n = r + 1;
                    m = i * 2 - n;
                }
            }
            while (m >= 0 && n < s2.length && s2[m] == s2[n]) {
                p[i]++;
                m--;
                n++;
            }
            if ((i + p[i]) > r) {
                c = i;
                r = i + p[i];
            }
        }
        int len = 0;
        c = 0;
        for (int i = 1; i < s2.length; i++) {
            if (len < p[i]) {
                len = p[i];
                c = i;
            }
        }
        char[] ss = Arrays.copyOfRange(s2, c - len, c + len + 1);
        return String.valueOf(removeBoundaries(ss));
    }

    private static char[] addBoundaries(char[] cs) {
        if (cs == null || cs.length == 0)
            return "||".toCharArray();
        char[] cs2 = new char[cs.length * 2 + 1];
        for (int i = 0; i < (cs2.length - 1); i = i + 2) {
            cs2[i] = '|';
            cs2[i + 1] = cs[i / 2];
        }
        cs2[cs2.length - 1] = '|';
        return cs2;
    }

    private static char[] removeBoundaries(char[] cs) {
        if (cs == null || cs.length < 3)
            return "".toCharArray();
        char[] cs2 = new char[(cs.length - 1) / 2];
        for (int i = 0; i < cs2.length; i++) {
            cs2[i] = cs[i * 2 + 1];
        }
        return cs2;
    }
}