package by.epam.task2.logic.parser.implementors;

import by.epam.task2.entity.TextElement;
import by.epam.task2.entity.implementors.blocks.Paragraph;
import by.epam.task2.entity.implementors.indivisible.UndefinedElement;
import by.epam.task2.logic.parser.Parser;
import by.epam.task2.logic.parser.RegularExpressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class ParagraphParser implements Parser {
    private Parser innerParser;

    public ParagraphParser(Parser sentenceParser) {
        this.innerParser = sentenceParser;
    }

    @Override
    public TextElement parse(String textToParse) {
        Paragraph paragraph = new Paragraph();
        Pattern sentencePattern = Pattern.compile(RegularExpressions.SENTENCE_REGEXP);
        Matcher sentenceMatcher = sentencePattern.matcher(textToParse);
        int startIndex = 0;
        int endIndex = 0;
        int nextStartIndex;
        while (endIndex != textToParse.length()) {
            if (sentenceMatcher.find(startIndex)) {
                endIndex = sentenceMatcher.start(0);
                nextStartIndex = sentenceMatcher.end(0);
            } else {
                endIndex = textToParse.length();
                nextStartIndex = -1;
            }
            String undefined = textToParse.substring(startIndex, endIndex);
            if (!undefined.matches(RegularExpressions.WHITESPACES_REGEXP)) {
                if (undefined.contains("\n"))
                    undefined = undefined.replace("\n", "");
                if (undefined.contains("\t"))
                    undefined = undefined.replace("\t", "");
                paragraph.addElement(new UndefinedElement(undefined));
            }
            startIndex = nextStartIndex;
            if (endIndex != textToParse.length())
                paragraph.addElement(innerParser.parse(sentenceMatcher.group(0)));
        }
        return paragraph;
    }
}
