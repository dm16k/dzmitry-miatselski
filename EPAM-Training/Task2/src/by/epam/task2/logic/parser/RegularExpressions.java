package by.epam.task2.logic.parser;

import by.epam.task2.logic.reader.PropertyReader;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class RegularExpressions {
    public static final String CODE_BLOCKS_REGEXP = PropertyReader.getProperty("code");
    public static final String PARAGRAPHS_REGEXP = PropertyReader.getProperty("parg");
    public static final String SENTENCE_REGEXP = PropertyReader.getProperty("sent");
    public static final String WORDS_REGEXP = PropertyReader.getProperty("word");
    public static final String NUMBERS_REGEXP = PropertyReader.getProperty("numb");
    public static final String SENTENCE_ELEMENTS_REGEXP = PropertyReader.getProperty("elem");
    public static final String PUNCTUATIONS_REGEXP = PropertyReader.getProperty("punc");
    public static final String WHITESPACES_REGEXP = PropertyReader.getProperty("spac");

    private RegularExpressions() {
    }
}