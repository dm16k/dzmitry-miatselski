package by.epam.task2.logic.parser;

import by.epam.task2.entity.TextElement;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public interface Parser {
    public TextElement parse(String textToParse);
}