package by.epam.task2.logic.parser.implementors;

import by.epam.task2.entity.implementors.blocks.CodeBlock;
import by.epam.task2.entity.implementors.blocks.Text;
import by.epam.task2.logic.parser.Parser;
import by.epam.task2.logic.parser.RegularExpressions;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author dmitr_000
 * @version 1.00 04.04.2015
 */
public class TextParser implements Parser {
    private Logger LOG = Logger.getLogger(TextParser.class);
    private Parser innerParser;

    public TextParser(Parser paragraphParser) {
        this.innerParser = paragraphParser;
    }

    public Text parse(String textToParse) {
        Text text = new Text();
        Pattern paragraphPattern = Pattern.compile(RegularExpressions.PARAGRAPHS_REGEXP);
        Pattern codePattern = Pattern.compile(RegularExpressions.CODE_BLOCKS_REGEXP);
        Matcher codeMatcher = codePattern.matcher(textToParse);
        int startIndex = 0;
        int endIndex = 0;
        int nextStartIndex;
        LOG.debug("started text parsing");
        while (endIndex != textToParse.length()) {
            if (codeMatcher.find(startIndex)) {
                endIndex = codeMatcher.start();
                nextStartIndex = codeMatcher.end(0);
            } else {
                endIndex = textToParse.length();
                nextStartIndex = -1;
            }
            String paragraphs = textToParse.substring(startIndex, endIndex);
            Matcher paragraphMatcher = paragraphPattern.matcher(paragraphs);
            while (paragraphMatcher.find()) {
                text.addElement(innerParser.parse(paragraphMatcher.group(0) + "\n"));
            }
            if (endIndex != textToParse.length()) {
                text.addElement(new CodeBlock(codeMatcher.group(0)));
            }
            startIndex = nextStartIndex;
        }
        LOG.debug("ended text parsing");
        return text;
    }
}