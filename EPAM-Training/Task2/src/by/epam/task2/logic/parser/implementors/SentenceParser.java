package by.epam.task2.logic.parser.implementors;

import by.epam.task2.entity.TextElement;
import by.epam.task2.entity.implementors.blocks.Sentence;
import by.epam.task2.entity.implementors.indivisible.PunctuationMark;
import by.epam.task2.entity.implementors.indivisible.Space;
import by.epam.task2.entity.implementors.indivisible.UndefinedElement;
import by.epam.task2.entity.implementors.indivisible.Word;
import by.epam.task2.logic.parser.Parser;
import by.epam.task2.logic.parser.RegularExpressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class SentenceParser implements Parser {
    @Override
    public TextElement parse(String textToParse) {
        Sentence sentence = new Sentence();
        Pattern numbersPattern = Pattern.compile(RegularExpressions.NUMBERS_REGEXP);
        Pattern wordsPattern = Pattern.compile(RegularExpressions.WORDS_REGEXP);
        Pattern whitespacesPattern = Pattern.compile(RegularExpressions.WHITESPACES_REGEXP);
        Pattern punctuationsPattern = Pattern.compile(RegularExpressions.PUNCTUATIONS_REGEXP);
        Pattern sentenceElementsPattern = Pattern.compile(RegularExpressions.SENTENCE_ELEMENTS_REGEXP);
        Matcher sentenceElementsMatcher = sentenceElementsPattern.matcher(textToParse);
        while (sentenceElementsMatcher.find()) {
            String sentencePart = sentenceElementsMatcher.group(0);
            Matcher wordMatcher = wordsPattern.matcher(sentencePart);
            Matcher numbersMatcher = numbersPattern.matcher(sentencePart);
            if (numbersMatcher.find())
                sentence.addElement(new UndefinedElement(sentenceElementsMatcher.group(0)));
            if (wordMatcher.find()) {
                String word = wordMatcher.group(0);
                sentence.addElement(new Word(word));
                sentencePart = sentencePart.replace(word, "");
                char[] remainedSymbols = sentencePart.toCharArray();
                for (Character ch : remainedSymbols) {
                    if (whitespacesPattern.matcher(ch.toString()).matches())
                        sentence.addElement(new Space());
                    else if (punctuationsPattern.matcher(ch.toString()).matches())
                        sentence.addElement(new PunctuationMark(ch.toString()));
                    else
                        sentence.addElement(new UndefinedElement(ch.toString()));
                }
            }
        }
        return sentence;
    }
}