package by.epam.task2.logic.reader;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author dmitr_000
 * @version 1.00 09.04.2015
 */
public class PropertyReader {
    private static Logger LOG = Logger.getLogger(PropertyReader.class);
    private static Properties properties;

    static {
        properties = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream is = loader.getResourceAsStream("by\\epam\\task2\\resources\\config.properties");
        try {
            properties.load(is);
        } catch (IOException e) {
            LOG.error("can't load config.properties");
            e.printStackTrace();
        }
    }

    private PropertyReader(){}

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }
}