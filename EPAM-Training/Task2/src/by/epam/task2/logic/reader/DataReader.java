package by.epam.task2.logic.reader;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class DataReader {
    private static Logger LOG = Logger.getLogger(DataReader.class);
    private String fileName;

    public DataReader() {
        fileName = PropertyReader.getProperty("infile");
    }

    public DataReader(String fileName) {
        if (fileName != null && fileName.length() != 0) this.fileName = fileName;
        else this.fileName = "src\\by\\epam\\task2\\resources\\originaltext.txt";
    }

    public String readFile() {
        String resultData = "";
        try {
            BufferedReader reader = new BufferedReader(new java.io.FileReader(new File(fileName)));
            StringBuilder builder = new StringBuilder();
            while (reader.ready())
                builder.append(reader.readLine()).append('\n');
            reader.close();
            resultData = builder.toString();
        } catch (FileNotFoundException e) {
            LOG.error("file: " + fileName + " not found");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultData;
    }
}