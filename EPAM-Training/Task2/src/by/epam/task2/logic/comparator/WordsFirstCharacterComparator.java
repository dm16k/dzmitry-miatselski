package by.epam.task2.logic.comparator;

import by.epam.task2.entity.implementors.indivisible.Word;

import java.util.Comparator;

/**
 * @author dmitr_000
 * @version 1.00 09.04.2015
 */

public class WordsFirstCharacterComparator implements Comparator<Word> {
    private int index;

    public WordsFirstCharacterComparator() {
        index = 0;
    }

    public WordsFirstCharacterComparator(int index) {
        this.index = index;
    }

    @Override
    public int compare(Word o1, Word o2) {
        return Character.compare(o1.toString().charAt(index), o2.toString().charAt(index));
    }
}