package by.epam.task2.entity;

import java.util.List;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public interface TextElement {
    public void addElement(TextElement element);

    public List<TextElement> getElements();

    public TextElement getElement(int index);
}
