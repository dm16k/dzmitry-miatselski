package by.epam.task2.entity.implementors.blocks;

import by.epam.task2.entity.TextElement;

import java.util.ArrayList;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class Sentence implements TextElement {
    private ArrayList<TextElement> sentenceParts;

    public Sentence() {
        sentenceParts = new ArrayList<TextElement>();
    }

    @Override
    public void addElement(TextElement element) {
        sentenceParts.add(element);
    }

    @Override
    public ArrayList<TextElement> getElements() {
        return (ArrayList<TextElement>) sentenceParts.clone();
    }

    @Override
    public TextElement getElement(int index) {
        return sentenceParts.get(index);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sentence sentence = (Sentence) o;

        return !(sentenceParts != null ? !sentenceParts.equals(sentence.sentenceParts) : sentence.sentenceParts != null);

    }

    @Override
    public int hashCode() {
        return sentenceParts != null ? sentenceParts.hashCode() : 0;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (TextElement element : sentenceParts)
            builder.append(element.toString());
        return builder.toString().replace("  ", " ");
    }
}
