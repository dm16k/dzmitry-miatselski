package by.epam.task2.entity.implementors.indivisible;

import by.epam.task2.entity.TextParts;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class PunctuationMark extends TextParts {
    private String punctuationMark;

    public PunctuationMark(String punctuationMark) {
        this.punctuationMark = punctuationMark;
    }

    @Override
    public String toString() {
        return punctuationMark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PunctuationMark that = (PunctuationMark) o;

        return !(punctuationMark != null ? !punctuationMark.equals(that.punctuationMark) : that.punctuationMark != null);

    }

    @Override
    public int hashCode() {
        return punctuationMark != null ? punctuationMark.hashCode() : 0;
    }
}