package by.epam.task2.entity.implementors.blocks;

import by.epam.task2.entity.TextElement;

import java.util.ArrayList;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class Paragraph implements TextElement {
    private ArrayList<TextElement> sentences;

    public Paragraph() {
        sentences = new ArrayList<TextElement>();
    }

    @Override
    public void addElement(TextElement element) {
        sentences.add(element);
    }

    @Override
    public ArrayList<TextElement> getElements() {
        return (ArrayList<TextElement>) sentences.clone();
    }

    @Override
    public TextElement getElement(int index) {
        return sentences.get(index);
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        for (TextElement element : sentences)
            buffer.append(element.toString()).append(" ");
        buffer.replace(buffer.length() - 1, buffer.length(), "").replace(0, 1, "");
        return buffer.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Paragraph paragraph = (Paragraph) o;

        return !(sentences != null ? !sentences.equals(paragraph.sentences) : paragraph.sentences != null);

    }

    @Override
    public int hashCode() {
        return sentences != null ? sentences.hashCode() : 0;
    }
}