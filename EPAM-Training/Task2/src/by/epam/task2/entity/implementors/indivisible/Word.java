package by.epam.task2.entity.implementors.indivisible;

import by.epam.task2.entity.TextParts;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class Word extends TextParts {
    private String word;

    public Word(String word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word1 = (Word) o;

        return !(word != null ? !word.equals(word1.word) : word1.word != null);

    }

    @Override
    public int hashCode() {
        return word != null ? word.hashCode() : 0;
    }

    @Override
    public String toString() {
        return word;
    }
}