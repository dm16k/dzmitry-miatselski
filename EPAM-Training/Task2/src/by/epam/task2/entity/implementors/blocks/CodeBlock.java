package by.epam.task2.entity.implementors.blocks;

import by.epam.task2.entity.TextParts;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class CodeBlock extends TextParts {
    private String codeBlock;

    public CodeBlock(String codeBlock) {
        this.codeBlock = codeBlock;
    }

    @Override
    public String toString() {
        return codeBlock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CodeBlock codeBlock1 = (CodeBlock) o;

        if (codeBlock != null ? !codeBlock.equals(codeBlock1.codeBlock) : codeBlock1.codeBlock != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return codeBlock != null ? codeBlock.hashCode() : 0;
    }
}