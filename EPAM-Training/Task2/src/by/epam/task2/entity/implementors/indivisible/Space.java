package by.epam.task2.entity.implementors.indivisible;

import by.epam.task2.entity.TextParts;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class Space extends TextParts {
    @Override
    public String toString() {
        return " ";
    }
}