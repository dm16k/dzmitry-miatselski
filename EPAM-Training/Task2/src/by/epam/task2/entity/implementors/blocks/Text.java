package by.epam.task2.entity.implementors.blocks;

import by.epam.task2.entity.TextElement;

import java.util.ArrayList;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class Text implements TextElement {
    private ArrayList<TextElement> paragraphs;

    public Text() {
        paragraphs = new ArrayList<TextElement>();
    }

    @Override
    public void addElement(TextElement element) {
        paragraphs.add(element);
    }

    @Override
    public ArrayList<TextElement> getElements() {
        return (ArrayList<TextElement>) paragraphs.clone();
    }

    @Override
    public TextElement getElement(int index) {
        return paragraphs.get(index);
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        for (TextElement element : paragraphs)
            buffer.append(element.toString()).append('\n');
        return buffer.toString();
    }
}