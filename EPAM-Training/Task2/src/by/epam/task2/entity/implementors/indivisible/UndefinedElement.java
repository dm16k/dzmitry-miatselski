package by.epam.task2.entity.implementors.indivisible;

import by.epam.task2.entity.TextParts;

/**
 * @author dmitr_000
 * @version 1.00 08.04.2015
 */
public class UndefinedElement extends TextParts {
    private String undefinedElement;

    public UndefinedElement(String undefinedElement) {
        this.undefinedElement = undefinedElement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UndefinedElement that = (UndefinedElement) o;

        return !(undefinedElement != null ? !undefinedElement.equals(that.undefinedElement) : that.undefinedElement != null);

    }

    @Override
    public int hashCode() {
        return undefinedElement != null ? undefinedElement.hashCode() : 0;
    }

    @Override
    public String toString() {
        return undefinedElement;
    }
}
