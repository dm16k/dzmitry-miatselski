package by.epam.task5.servlet.guns.logic;

import javax.servlet.http.HttpServletRequest;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public interface ICommand {
    String execute(HttpServletRequest request) throws CommandException;
}
