package by.epam.task5.servlet.guns.dao.impl.sax;

/**
 * @author dmitr_000
 * @version 1.00 26-Apr-15
 */
public enum TagNames {
    GUN, MODEL, TYPE, HANDY, ORIGIN, CHARACTERISTICS, RANGE, FIRING, PROJECTILE, FEEDSYSTEM, CAPACITY, OPTICS, VARIANT
}