package by.epam.task5.servlet.guns.dao.impl.sax;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static javax.xml.stream.XMLStreamConstants.*;

/**
 * @author dmitr_000
 * @version 1.00 26-Apr-15
 */
public class StAXParser {
    XMLStreamReader parser;
    private List<List<Object>> list;
    private int modelIndex;
    private int textIndex;
    private boolean parsingCompleted;
    private StringBuffer buffer;

    public StAXParser(File file) throws FileNotFoundException, XMLStreamException {
        list = new ArrayList<List<Object>>();
        modelIndex = 0;
        textIndex = 0;
        buffer = new StringBuffer();
        parsingCompleted = false;
        parser = XMLInputFactory.newInstance().createXMLStreamReader(new FileReader(file));
        parse();
    }

    private void parse() throws XMLStreamException {
        while (parser.hasNext()) {
            int event = parser.next();
            switch (event) {
                case START_ELEMENT: {
                    if (parser.getLocalName().equals("model") && parser.getAttributeCount() != 0) {
                        list.add(new ArrayList<Object>());
                        list.get(modelIndex).add(parser.getAttributeValue(0));
                    }
                    break;
                }
                case CHARACTERS: {
                    String text = parser.getText().trim();
                    if (text.length() != 0 && Character.compare(text.charAt(0), '<') != 0) {
                        if (textIndex < 3)
                            list.get(modelIndex).add(text);
                        else {
                            buffer.append(text).append('/');
                        }
                        textIndex++;
                    }
                    break;
                }
                case END_ELEMENT: {
                    TagNames tagName = TagNames.valueOf(parser.getLocalName().toUpperCase());
                    switch (tagName) {
                        case MODEL: {
                            modelIndex++;
                            textIndex = 0;
                            break;
                        }
                        case RANGE:
                        case FEEDSYSTEM:
                        case OPTICS: {
                            if (buffer.length() > 0)
                                buffer.deleteCharAt(buffer.length() - 1);
                            list.get(modelIndex).add(buffer.toString());
                            buffer.setLength(0);
                            break;
                        }
                    }
                }
                case END_DOCUMENT: {
                    parsingCompleted = true;
                    break;
                }
            }
        }
    }

    public List<List<Object>> getList() {
        if (parsingCompleted)
            return list;
        return null;
    }
}
