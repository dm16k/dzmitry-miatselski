package by.epam.task5.servlet.guns.logic;

import by.epam.task5.servlet.guns.exception.GunsException;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public class CommandException extends GunsException {
    public CommandException(String msg) {
        super(msg);
    }
    public CommandException(String msg, Exception ex) {
        super(msg, ex);
    }
}
