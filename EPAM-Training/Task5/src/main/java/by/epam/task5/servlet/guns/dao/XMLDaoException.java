package by.epam.task5.servlet.guns.dao;

import by.epam.task5.servlet.guns.exception.GunsException;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public class XMLDaoException extends GunsException {
    public XMLDaoException(String msg) {
        super(msg);
    }

    public XMLDaoException(String msg, Exception ex) {
        super(msg, ex);
    }
}
