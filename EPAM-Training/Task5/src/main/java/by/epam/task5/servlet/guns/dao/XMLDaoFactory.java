package by.epam.task5.servlet.guns.dao;

import by.epam.task5.servlet.guns.dao.impl.DOMXmlDao;
import by.epam.task5.servlet.guns.dao.impl.SAXXmlDao;
import by.epam.task5.servlet.guns.dao.impl.StAXXmlDao;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public class XMLDaoFactory {
    private static final XMLDaoFactory instance = new XMLDaoFactory();

    public static XMLDaoFactory getInstance() {
        return instance;
    }

    public synchronized XMLDao getDAO(DAOType type) throws XMLDaoException {
//        try {
//            Thread.sleep((long) (Math.random() * 100));
//        } catch (InterruptedException e) {
//            throw new XMLDaoException("Synchronization error", e);
//        }
        switch (type) {
            case DOM:
                return DOMXmlDao.getInstance();
            case SAX:
                return SAXXmlDao.getInstance();
            case STAX:
                return StAXXmlDao.getInstance();
            default: {
                throw new XMLDaoException("Wrong DAO type");
            }
        }
    }

    public enum DAOType {
        SAX, STAX, DOM
    }
}