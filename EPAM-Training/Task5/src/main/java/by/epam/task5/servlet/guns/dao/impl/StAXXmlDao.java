package by.epam.task5.servlet.guns.dao.impl;

import by.epam.task5.servlet.guns.dao.XMLDao;
import by.epam.task5.servlet.guns.dao.XMLDaoException;
import by.epam.task5.servlet.guns.dao.impl.sax.StAXParser;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author dmitr_000
 * @version 1.00 26-Apr-15
 */
public final class StAXXmlDao implements XMLDao {
    private static final StAXXmlDao instance = new StAXXmlDao();

    public static StAXXmlDao getInstance() {
        return instance;
    }

    public List<List<Object>> parse(String resourceName) throws XMLDaoException {
        File file = new File(resourceName);
        try {
            return new StAXParser(file).getList();
        } catch (XMLStreamException e) {
            e.printStackTrace();
            throw new XMLDaoException("Can't get stream from file " + file.getAbsolutePath(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new XMLDaoException("Can't read file " + file.getAbsolutePath(), e);
        }
    }
}
