package by.epam.task5.servlet.guns.dao.impl.dom.impl;

import by.epam.task5.servlet.guns.dao.impl.dom.IXMLNode;

import java.util.List;
import java.util.Map;

/**
 * @author dmitr_000
 * @version 1.00 25-Apr-15
 */
public class XMLLeafNode implements IXMLNode {
    private String name;
    private String value;
    private Map<String,String> attributes;
    private boolean isAllSiblingsAreLeafs;

    public XMLLeafNode(String name, String value, Map<String,String> attributes) {
        this.name = name;
        this.value = value;
        this.attributes = attributes;
        isAllSiblingsAreLeafs = false;
    }

    public void setAllSiblingsAreLeafs(boolean flag) {
        this.isAllSiblingsAreLeafs = flag;
    }

    public void toStringList(List<Object> list) {
        if (!isAllSiblingsAreLeafs)
            list.add(value);
    }

    public boolean isLeaf() {
        return true;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public List<IXMLNode> getChildren() {
        return null;
    }

    @Override
    public String toString() {
//        if (!isAllSiblingsAreLeafs)
        return value;
//        else return null;
    }
}
