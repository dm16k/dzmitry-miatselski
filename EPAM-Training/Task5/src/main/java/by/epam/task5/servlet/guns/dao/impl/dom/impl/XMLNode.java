package by.epam.task5.servlet.guns.dao.impl.dom.impl;

import by.epam.task5.servlet.guns.dao.impl.dom.IXMLNode;

import java.util.List;
import java.util.Map;

/**
 * @author dmitr_000
 * @version 1.00 25-Apr-15
 */
public class XMLNode implements IXMLNode {
    private String name;
    private Map<String, String> attributes;
    private List<IXMLNode> children;
    private boolean isLeafsOnlyParent;

    public XMLNode(String name, Map<String, String> attributes, List<IXMLNode> children) {
        this.name = name;
        this.attributes = attributes;
        this.children = children;
        isLeafsParent();
    }

    private void isLeafsParent() {
        int count = 0;
        for (IXMLNode child : children)
            if (child.isLeaf())
                count++;
        if (count == children.size()) {
            isLeafsOnlyParent = true;
            for (IXMLNode child : children)
                child.setAllSiblingsAreLeafs(true);
        }
    }

    public boolean isLeaf() {
        return false;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public List<IXMLNode> getChildren() {
        return children;
    }

    public void setAllSiblingsAreLeafs(boolean flag) {
    }

    public void toStringList(List<Object> list) {
        if (name.equals("gun:model"))
            list.add(attributes.get("name"));    //string
//            list.add(value);
        if (isLeafsOnlyParent) {
            StringBuilder builder = new StringBuilder();
            for (IXMLNode child : children)
                builder.append(child).append('/');
            if (builder.length() != 0)
                builder.deleteCharAt(builder.length() - 1);
            list.add(builder.toString());
        } else
            for (IXMLNode child : children)
                child.toStringList(list);
    }
}
