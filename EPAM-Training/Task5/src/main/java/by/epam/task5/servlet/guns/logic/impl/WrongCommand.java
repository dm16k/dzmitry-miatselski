package by.epam.task5.servlet.guns.logic.impl;

import by.epam.task5.servlet.guns.controller.JspPageName;
import by.epam.task5.servlet.guns.logic.CommandException;
import by.epam.task5.servlet.guns.logic.ICommand;

import javax.servlet.http.HttpServletRequest;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public class WrongCommand implements ICommand {
    public String execute(HttpServletRequest request) throws CommandException {
        return JspPageName.ERROR_PAGE;
    }
}
