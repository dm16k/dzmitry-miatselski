package by.epam.task5.servlet.guns.logic;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public enum CommandName {
    PARSE_COMMAND, WRONG_COMMAND
}
