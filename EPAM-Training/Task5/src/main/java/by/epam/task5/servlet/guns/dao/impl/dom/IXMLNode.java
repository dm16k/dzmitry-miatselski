package by.epam.task5.servlet.guns.dao.impl.dom;

import java.util.List;
import java.util.Map;

/**
 * @author dmitr_000
 * @version 1.00 25-Apr-15
 */
public interface IXMLNode {
    boolean isLeaf();
    Map<String, String> getAttributes();
    List<IXMLNode> getChildren();
    void toStringList(List<Object> list);
    void setAllSiblingsAreLeafs(boolean flag);
}
