package by.epam.task5.servlet.guns.dao.impl.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dmitr_000
 * @version 1.00 26-Apr-15
 */
public class SAXHandler extends DefaultHandler {
    private List<List<Object>> list;
    private int modelIndex;
    private int textIndex;
    private boolean parsingCompleted;
    private StringBuffer buffer;

    public SAXHandler() {
        list = new ArrayList<List<Object>>();
        modelIndex = 0;
        textIndex = 0;
        buffer = new StringBuffer();
        parsingCompleted = false;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (localName.equals("model") && attributes != null) {
            list.add(new ArrayList<Object>());
            list.get(modelIndex).add(attributes.getValue(0));
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String text = new String(ch, start, length).trim();
        if (text.length() != 0 && Character.compare(text.charAt(0), '<') != 0) {
            if (textIndex < 3)
                list.get(modelIndex).add(text);
            else {
                buffer.append(text).append('/');
            }
            textIndex++;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        TagNames tagName = TagNames.valueOf(localName.toUpperCase());
        switch (tagName) {
            case MODEL: {
                modelIndex++;
                textIndex = 0;
                break;
            }
            case RANGE:
            case FEEDSYSTEM:
            case OPTICS: {
                if (buffer.length() > 0)
                    buffer.deleteCharAt(buffer.length() - 1);
                list.get(modelIndex).add(buffer.toString());
                buffer.setLength(0);
                break;
            }
        }
    }

    @Override
    public void endDocument() throws SAXException {
        parsingCompleted = true;
    }

    public List<List<Object>> getList() {
        if (parsingCompleted)
            return list;
        return null;
    }
}
