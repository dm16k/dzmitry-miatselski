package by.epam.task5.servlet.guns.logic;

import by.epam.task5.servlet.guns.logic.impl.ParseCommand;
import by.epam.task5.servlet.guns.logic.impl.WrongCommand;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public class CommandHelper {
    private final static Logger logger = Logger.getLogger(CommandHelper.class);
    private static final CommandHelper instance = new CommandHelper();
    private Map<CommandName, ICommand> commands = new HashMap<CommandName, ICommand>();

    public CommandHelper() {
        commands.put(CommandName.PARSE_COMMAND, new ParseCommand());
        commands.put(CommandName.WRONG_COMMAND, new WrongCommand());
    }

    public static CommandHelper getInstance() {
        return instance;
    }

    public ICommand getCommand(String commandName) {
        CommandName name = null;
        try {
            name = CommandName.valueOf(commandName.toUpperCase());
        } catch (IllegalArgumentException e) {
            logger.error("Enum constant with name" + commandName + " isn't presented");
        }
        ICommand command;
        if (name != null) {
            command = commands.get(name);
        } else {
            command = commands.get(CommandName.WRONG_COMMAND);
        }
        return command;
    }
}
