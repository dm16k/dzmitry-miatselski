package by.epam.task5.servlet.guns.controller;

import by.epam.task5.servlet.guns.logic.CommandException;
import by.epam.task5.servlet.guns.logic.CommandHelper;
import by.epam.task5.servlet.guns.logic.ICommand;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public class Controller extends HttpServlet {
    public Controller() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String commandName = req.getParameter(RequestParameterName.COMMAND_NAME);
        ICommand command = CommandHelper.getInstance().getCommand(commandName);
        String page;
        try {
            page = command.execute(req);
        } catch (CommandException e) {
            page = JspPageName.ERROR_PAGE;
        } catch (Exception e) {
            page = JspPageName.ERROR_PAGE;
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher(page);
        if(dispatcher != null)
            dispatcher.forward(req, resp);
        else
            errorMessageDirectlyFromResponse(resp);
    }

    private void errorMessageDirectlyFromResponse (HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        resp.getWriter().print("ERROR");
    }
}
