package by.epam.task5.servlet.guns.logic.impl;

import by.epam.task5.servlet.guns.controller.JspPageName;
import by.epam.task5.servlet.guns.controller.RequestParameterName;
import by.epam.task5.servlet.guns.dao.XMLDao;
import by.epam.task5.servlet.guns.dao.XMLDaoException;
import by.epam.task5.servlet.guns.dao.XMLDaoFactory;
import by.epam.task5.servlet.guns.logic.CommandException;
import by.epam.task5.servlet.guns.logic.ICommand;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public class ParseCommand implements ICommand {
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        String link;
        XMLDao dao;
        try {
            String parserTypeName = request.getParameter(RequestParameterName.PARSER);
            XMLDaoFactory.DAOType parserType = XMLDaoFactory.DAOType.valueOf(parserTypeName.toUpperCase());
            List<List<Object>> gunList;
            dao = XMLDaoFactory.getInstance().getDAO(parserType);
            gunList = dao.parse(request.getParameter(RequestParameterName.FILE_NAME));
            link = "./";
            request.setAttribute(RequestParameterName.GUN, gunList);
            request.setAttribute(RequestParameterName.LINK, link);
            page = JspPageName.GUNS_PAGE;
        } catch (XMLDaoException e) {
            throw new CommandException("Can't get DAO", e);
        }
        return page;
    }
}
