package by.epam.task5.servlet.guns.controller;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public final class JspPageName {
    public static final String GUNS_PAGE = "jsp/gunPage.jsp";
    public static final String ERROR_PAGE = "error.jsp";

    private JspPageName() {
    }
}
