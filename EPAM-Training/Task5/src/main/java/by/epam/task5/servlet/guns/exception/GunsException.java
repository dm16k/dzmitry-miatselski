package by.epam.task5.servlet.guns.exception;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public class GunsException extends Exception {
    public GunsException (String msg) {
        super(msg);
    }
    public GunsException (String msg, Exception ex) {
        super(msg, ex);
    }
}
