package by.epam.task5.servlet.guns.dao.impl;

import by.epam.task5.servlet.guns.dao.XMLDao;
import by.epam.task5.servlet.guns.dao.XMLDaoException;
import by.epam.task5.servlet.guns.dao.impl.sax.SAXHandler;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public final class SAXXmlDao implements XMLDao {
    private static final SAXXmlDao instance = new SAXXmlDao();

    public static SAXXmlDao getInstance() {
        return instance;
    }

    public List<List<Object>> parse(String resourceName) throws XMLDaoException {
        File file = new File(resourceName);
        try {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setNamespaceAware(true);
            SAXParser parser = spf.newSAXParser();
            SAXHandler handler = new SAXHandler();
            parser.parse(file, handler);
            return handler.getList();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            throw new XMLDaoException("Can't get DocumentBuilder object", e);
        } catch (SAXException e) {
            e.printStackTrace();
            throw new XMLDaoException("Can't parse file " + file.getAbsolutePath(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new XMLDaoException("Can't read file " + file.getAbsolutePath(), e);
        }
    }
}
