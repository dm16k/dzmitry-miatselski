package by.epam.task5.servlet.guns.dao.impl.dom.impl;

import by.epam.task5.servlet.guns.dao.impl.dom.IXMLNode;

import java.util.List;
import java.util.Map;

/**
 * @author dmitr_000
 * @version 1.00 25-Apr-15
 */
public class XMLChildNodeInfo implements IXMLNode {
    private List<IXMLNode> childNodes;
    private Map<String, String> attributes;
    private boolean isLeaf;

    public XMLChildNodeInfo(List<IXMLNode> childNodes, Map<String, String> attributes, boolean isLeaf) {
        this.childNodes = childNodes;
        this.attributes = attributes;
        this.isLeaf = isLeaf;
    }

    public void toStringList(List<Object> list) {}

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setAllSiblingsAreLeafs(boolean flag) {}

    public List<IXMLNode> getChildren() {
        return childNodes;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
}