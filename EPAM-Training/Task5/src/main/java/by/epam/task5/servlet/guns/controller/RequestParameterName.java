package by.epam.task5.servlet.guns.controller;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public final class RequestParameterName {
    public final static String COMMAND_NAME = "command";
    public final static String GUN = "gun";
    public final static String FILE_NAME = "filename";
    public final static String PARSER = "parser";
    public final static String LINK = "link";
    private RequestParameterName() {
    }
}
