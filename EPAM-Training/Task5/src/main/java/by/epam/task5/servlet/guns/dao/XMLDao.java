package by.epam.task5.servlet.guns.dao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dmitr_000
 * @version 1.00 24-Apr-15
 */
public interface XMLDao {
    List<List<Object>> parse(String resourceName) throws XMLDaoException;
}
