package by.epam.task5.servlet.guns.dao.impl;

import by.epam.task5.servlet.guns.dao.XMLDao;
import by.epam.task5.servlet.guns.dao.XMLDaoException;
import by.epam.task5.servlet.guns.dao.impl.dom.IXMLNode;
import by.epam.task5.servlet.guns.dao.impl.dom.impl.XMLChildNodeInfo;
import by.epam.task5.servlet.guns.dao.impl.dom.impl.XMLLeafNode;
import by.epam.task5.servlet.guns.dao.impl.dom.impl.XMLNode;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dmitr_000
 * @version 1.00 25-Apr-15
 */
public final class DOMXmlDao implements XMLDao {
    private final static DOMXmlDao instance = new DOMXmlDao();

    public static DOMXmlDao getInstance() {
        return instance;
    }

    public List<List<Object>> parse(String resourceName) throws XMLDaoException {
        File file = new File(resourceName);
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(file);
            List<List<Object>> rootChildrenList = new ArrayList<List<Object>>();
            Element rootElement = document.getDocumentElement();
            NodeList rootElementChildNodes = rootElement.getChildNodes();
            for (int i = 0, count = 0; i < rootElementChildNodes.getLength(); i++) {
                Node node = rootElementChildNodes.item(i);
                if (node instanceof Element) {
                    Element nodeElement = (Element) node;
                    rootChildrenList.add(new ArrayList<Object>());
                    getChildrenElements(nodeElement).toStringList(rootChildrenList.get(count));
                    count++;
                }
            }
            return rootChildrenList;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            throw new XMLDaoException("Can't get DocumentBuilder object", e);
        } catch (SAXException e) {
            e.printStackTrace();
            throw new XMLDaoException("Can't parse file " + file.getAbsolutePath(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new XMLDaoException("Can't read file " + file.getAbsolutePath(), e);
        }
    }

    private IXMLNode getChildrenElements(Element element) {
        if (element.hasChildNodes()
                && ((element.getFirstChild() == null)
                || (element.getFirstChild().getTextContent().trim().equals("\n".trim())))) {
            String name = element.getNodeName();
            Map<String, String> attributes = null;
            if (element.hasAttributes()) {
                NamedNodeMap attrs = element.getAttributes();
                attributes = new HashMap<String, String>();
                for (int i = 0; i < attrs.getLength(); i++) {
                    attributes.put(attrs.item(i).getNodeName(), attrs.item(i).getFirstChild().getTextContent());
                }
            }
            NodeList nodeList = element.getChildNodes();
            List<IXMLNode> childNodes = new ArrayList<IXMLNode>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node instanceof Element) {
                    Element nodeElement = (Element) node;
                    IXMLNode childNode = getChildrenElements(nodeElement);
                    if (!childNode.isLeaf())
                        childNodes.add(new XMLNode(node.getNodeName(), childNode.getAttributes(), childNode.getChildren()));
                    else
                        childNodes.add(new XMLLeafNode(node.getNodeName(), node.getFirstChild().getTextContent(), childNode.getAttributes()));
                }
            }
            return new XMLNode(name, attributes, childNodes);
        } else {
            Map<String, String> attributes = new HashMap<String, String>();
            if (element.hasAttributes()) {
                NamedNodeMap attrs = element.getAttributes();
                for (int i = 0; i < attrs.getLength(); i++)
                    attributes.put(attrs.item(i).getNodeName(), attrs.item(i).getFirstChild().getTextContent());
            }
            return new XMLChildNodeInfo(null, attributes, true);
        }
    }
}
