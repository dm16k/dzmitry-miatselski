<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: dmitr_000
  Date: 24-Apr-15
  Time: 6:03 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>IndexPage</title>
    <style>
        <%@include file="jsp/css/indexStyle.css"%>
    </style>
</head>
<body>
<%--<c:url{requestScope['javax.servlet.forward.request_uri']}>--%>
<div class="formBlock">
    <form action="controller" method="post">
        <input type="hidden" name="parser" value="DOM"/>
        <input type="hidden" name="command" value="parse_command"/>
        <input type="hidden" name="filename" value="<%= application.getRealPath("/")%>\WEB-INF\classes\guns.xml"/>
        <input type="submit" class="button" value="DOM"/>
    </form>
</div>
<div class="formBlock">
    <form action="controller" method="post">
        <%--<input type="hidden" name="command" value="parse_command"/>--%>
        <input type="hidden" name="parser" value="SAX"/>
        <input type="hidden" name="command" value="parse_command"/>
        <input type="hidden" name="filename" value="<%= application.getRealPath("/")%>\WEB-INF\classes\guns.xml"/>
        <%--<input type="text" name="xxx" value=""/>--%>
        <input type="submit" class="button" value="SAX"/>
    </form>
</div>
<div class="formBlock">
    <form action="controller" method="post">
        <input type="hidden" name="parser" value="StAX"/>
        <input type="hidden" name="command" value="parse_command"/>
        <input type="hidden" name="filename" value="<%= application.getRealPath("/")%>\WEB-INF\classes\guns.xml"/>
        <input type="submit" class="button" value="StAX"/>
    </form>
</div>
<%--<c:set var="filepath" value="<%= application.getRealPath("/")%>\WEB-INF\classes\guns.xml" scope="application"/>--%>
</body>
</html>
