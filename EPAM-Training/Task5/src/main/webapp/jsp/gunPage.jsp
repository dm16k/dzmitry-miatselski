<%--
  Created by IntelliJ IDEA.
  User: dmitr_000
  Date: 24-Apr-15
  Time: 6:01 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Guns TTC</title>
    <style>
        <%@include file='css/gunsStyle.css' %>
    </style>
</head>
<body>
<button class="parent2">
    <table class="simple-table" cellspacing="0" onclick="document.getElementsByClassName('simple-table')">
        <c:forEach var="model" items="${gun}">
            <tr>
                <c:forEach var="child" items="${model}">
                    <td>${child}</td>
                </c:forEach>
            </tr>
        </c:forEach>
    </table>
</button>
<div class="clear"
<%--onclick="window.location.href='http://localhost:8083/'"--%>
        >
</div>
<button class="parent">
    <input type="button" value="BACK" class="backLink" id="back" onclick="window.location.href='${link}'"/>
</button>
</body>
</html>
