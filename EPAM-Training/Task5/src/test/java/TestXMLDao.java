import by.epam.task5.servlet.guns.dao.XMLDaoException;
import by.epam.task5.servlet.guns.dao.XMLDaoFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dmitr_000
 * @version 1.00 25-Apr-15
 */
public class TestXMLDao {
    public static void main(String[] args) {
        try {
            List<List<Object>> list = XMLDaoFactory.getInstance()
                    .getDAO(XMLDaoFactory.DAOType.STAX)
                    .parse("src\\main\\resources\\guns.xml");
            for (List<Object> item : list)
                for (Object inner : item)
                    System.out.println(inner);
        } catch (XMLDaoException e) {
            e.printStackTrace();
        }
    }
}
