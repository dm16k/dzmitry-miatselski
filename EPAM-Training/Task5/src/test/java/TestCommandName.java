import by.epam.task5.servlet.guns.logic.CommandHelper;

/**
 * @author dmitr_000
 * @version 1.00 27-Apr-15
 */
public class TestCommandName {
    public static void main(String[] args) {
        String name = "some_command";
        CommandHelper.getInstance().getCommand(name);
    }
}
